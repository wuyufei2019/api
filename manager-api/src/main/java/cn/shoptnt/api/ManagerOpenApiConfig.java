package cn.shoptnt.api;

import cn.shoptnt.framework.openapi.GlobalOperationCustomizer;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OpenAPI3.0  分组配置
 *
 * @author gy
 * @version 1.0
 * @sinc 7.3.0
 * @date 2022年12月08日 17:04
 * Knife4j 文档访问地址为：http://ip:port/doc.html  推荐使用
 * swagger3 访问地址为：  http://ip:port/swagger-ui/index.html
 */
@Configuration
public class ManagerOpenApiConfig {


    private static final String PACKAGES_PATH = "cn.shoptnt.api.manager";

    @Bean
    public GroupedOpenApi defaultApi() {
        return GroupedOpenApi.builder()
                .group("默认分组")
                .pathsToMatch("/**")
                .packagesToScan("cn.shoptnt.api")
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    /**
     * 管理端-消息模块
     * @return
     */
    @Bean
    public GroupedOpenApi messageManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-消息模块")
                .pathsToMatch("/admin/notice/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }


    @Bean
    public GroupedOpenApi distributionManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-分销模块")
                .pathsToMatch("/admin/distribution/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }


    @Bean
    public GroupedOpenApi baseManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-基础模块")
                .pathsToMatch("/admin/index/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi afterSaleManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-售后模块")
                .pathsToMatch("/admin/after-sales/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi goodsManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-商品模块")
                .pathsToMatch("/admin/goods/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();


    }

    @Bean
    public GroupedOpenApi goodsSearchManagerApi() {

        return GroupedOpenApi.builder()
                .group("管理端-商品搜索模块")
                .pathsToMatch("/admin/goodssearch/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi membersManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-会员模块")
                .pathsToMatch("/admin/members/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi orderBillManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-结算模块")
                .pathsToMatch("/admin/order/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi pageDataManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-楼层模块")
                .pathsToMatch("/admin/pages/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi promotionManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-促销模块")
                .pathsToMatch("/admin/promotion/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();

    }

    @Bean
    public GroupedOpenApi shopManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-店铺模块")
                .pathsToMatch("/admin/shops/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi statisticsManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-统计模块")
                .pathsToMatch("/admin/statistics/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi systemManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-系统模块")
                .pathsToMatch("/admin/system/**", "/admin/task/**", "/admin/setting/**", "/admin/sensitive-words/**", "/admin/system-logs/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi tradeManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-交易模块")
                .pathsToMatch("/admin/trade/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi passportManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-登录模块")
                .pathsToMatch("/admin/passport/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi i18nManagerApi() {
        return GroupedOpenApi.builder()
                .group("管理端-国际化模块")
                .pathsToMatch("/admin/i18n/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

}
