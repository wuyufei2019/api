/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.pagedata;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.support.validator.annotation.SortType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;

import cn.shoptnt.model.pagedata.SiteNavigation;
import cn.shoptnt.service.pagedata.SiteNavigationManager;

/**
 * 导航栏控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-12 17:07:22
 */
@RestController
@RequestMapping("/admin/pages/site-navigations")
@Tag(name = "导航栏相关API")
@Validated
public class SiteNavigationManagerController {

    @Autowired
    private SiteNavigationManager siteNavigationManager;


    @Operation(summary = "查询导航栏列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "client_type", description = "客户端类型", required = true,   in = ParameterIn.QUERY),
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String clientType) {

        return this.siteNavigationManager.list(pageNo, pageSize, clientType);
    }


    @Operation(summary = "添加导航栏")
    @PostMapping
    public SiteNavigation add(@Valid SiteNavigation siteNavigation) {

        this.siteNavigationManager.add(siteNavigation);

        return siteNavigation;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改导航栏")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public SiteNavigation edit(@Valid SiteNavigation siteNavigation, @PathVariable Long id) {

        this.siteNavigationManager.edit(siteNavigation, id);

        return siteNavigation;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除导航栏")
    @Parameters({
            @Parameter(name = "id", description = "要删除的导航栏主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.siteNavigationManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个导航栏")
    @Parameters({
            @Parameter(name = "id", description = "要查询的导航栏主键", required = true,  in = ParameterIn.PATH)
    })
    public SiteNavigation get(@PathVariable Long id) {

        SiteNavigation siteNavigation = this.siteNavigationManager.getModel(id);

        return siteNavigation;
    }

    @PutMapping("/{id}/{sort}")
    @Operation(summary = "上下移动导航栏菜单")
    @Parameters({
            @Parameter(name = "id", description = "导航主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "sort", description = "上移 up ，下移down", required = true,   in = ParameterIn.PATH)
    })
    public SiteNavigation updateSort(@PathVariable(name = "id") Long id, @PathVariable @SortType String sort) {

        return this.siteNavigationManager.updateSort(id, sort);
    }

}
