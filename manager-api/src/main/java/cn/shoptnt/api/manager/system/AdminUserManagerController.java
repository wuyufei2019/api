/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.validation.annotation.DemoSiteDisable;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import cn.shoptnt.service.base.service.ValidatorManager;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.errorcode.SystemErrorCode;
import cn.shoptnt.model.system.dos.AdminUser;
import cn.shoptnt.model.system.vo.AdminLoginVO;
import cn.shoptnt.model.system.vo.AdminUserVO;
import cn.shoptnt.service.system.AdminUserManager;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.service.system.SystemLogsManager;
import io.jsonwebtoken.ExpiredJwtException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 平台管理员控制器
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-20 20:38:26
 */
@RestController
@RequestMapping("/admin/systems/admin-users")
@Tag(name = "平台管理员相关API")
@Validated
public class AdminUserManagerController {

    @Autowired
    private AdminUserManager adminUserManager;
    @Autowired
    private ValidatorManager validatorManager;


    @GetMapping("/login")
    @Operation(summary = "用户名（手机号）/密码登录API")
    @Parameters({
            @Parameter(name = "username", description = "用户名", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "密码", required = true,   in = ParameterIn.QUERY)
    })
    public AdminLoginVO login(@NotEmpty(message = "用户名不能为空") String username, @NotEmpty(message = "密码不能为空") String password) {
        //验证登录参数是否正确
        this.validatorManager.validate();

        return adminUserManager.login(username, password);
    }

    @Operation(summary = "刷新token")
    @PostMapping("/token")
    @Parameter(name = "refresh_token", description = "刷新token", required = true,   in = ParameterIn.QUERY)
    public String refreshToken(@Parameter(hidden = true) @NotEmpty(message = "刷新token不能为空") String refreshToken) {
        try {
            return adminUserManager.exchangeToken(refreshToken);
        } catch (ExpiredJwtException e) {
            throw new ServiceException(MemberErrorCode.E109.code(), "当前token已经失效");
        }
    }

    @PutMapping
    @Operation(summary = "修改管理员密码及头像")
    @Parameters({
            @Parameter(name = "face", description = "头像", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "old_passwprd", description = "原密码",   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "新密码",   in = ParameterIn.QUERY)
    })
    @DemoSiteDisable
    public AdminUser edit(@NotEmpty(message = "管理员头像不能为空") String face, @Parameter(hidden = true) String oldPasswprd, String password) {
        Long uid = AdminUserContext.getAdmin().getUid();
        AdminUser adminUser = this.adminUserManager.getModel(uid);
        if (adminUser == null) {
            throw new ResourceNotFoundException("当前管理员不存在");
        }
        AdminUserVO adminUserVO = new AdminUserVO();
        BeanUtil.copyProperties(adminUser, adminUserVO);

        //校验密码
        if (!StringUtil.isEmpty(oldPasswprd) && StringUtil.isEmpty(password)) {
            throw new ServiceException(SystemErrorCode.E923.code(), "新密码不能为空");
        }
        if (StringUtil.isEmpty(oldPasswprd) && !StringUtil.isEmpty(password)) {
            throw new ServiceException(SystemErrorCode.E922.code(), "原始密码不能为空");
        }
        if (!StringUtil.isEmpty(oldPasswprd) && !StringUtil.isEmpty(password)) {
            String dbOldPassword = StringUtil.md5(oldPasswprd + adminUser.getUsername().toLowerCase());
            if (!dbOldPassword.equals(adminUser.getPassword())) {
                throw new ServiceException(SystemErrorCode.E921.code(), "原密码错误");
            }
            adminUserVO.setPassword(password);
        } else {
            adminUserVO.setPassword("");
        }
        adminUserVO.setFace(face);
        AdminUser upAdminUser = adminUserManager.edit(adminUserVO, adminUser.getId());
        this.adminUserManager.logout(uid);
        return upAdminUser;
    }

    @Operation(summary = "注销管理员登录")
    @PostMapping(value = "/logout")
    @Parameters({
            @Parameter(name = "uid", description = "管理员id",  in = ParameterIn.QUERY, required = true)
    })
    public String loginOut(@NotNull(message = "管理员id不能为空") Long uid) {
        this.adminUserManager.logout(uid);
        return null;
    }

}
