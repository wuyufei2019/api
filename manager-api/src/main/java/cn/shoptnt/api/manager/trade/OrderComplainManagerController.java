/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.trade;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.trade.complain.dos.OrderComplain;
import cn.shoptnt.model.trade.complain.dos.OrderComplainCommunication;
import cn.shoptnt.model.trade.complain.dto.ComplainQueryParam;
import cn.shoptnt.model.trade.complain.vo.OrderComplainVO;
import cn.shoptnt.service.trade.complain.OrderComplainCommunicationManager;
import cn.shoptnt.service.trade.complain.OrderComplainManager;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.security.model.Admin;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 交易投诉表控制器
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-27 16:48:27
 */
@RestController
@RequestMapping("/admin/trade/order-complains")
@Tag(name = "交易投诉表相关API")
public class OrderComplainManagerController {

    @Autowired
    private OrderComplainManager orderComplainManager;

    @Autowired
    private OrderComplainCommunicationManager orderComplainCommunicationManager;


    @Operation(summary = "查询交易投诉表列表")
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, ComplainQueryParam param) {

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        return this.orderComplainManager.list(param);
    }


    @PutMapping(value = "/{id}/to-appeal")
    @Operation(summary = "审核并交由商家申诉")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public OrderComplain auth(@PathVariable Long id) {

        return this.orderComplainManager.auth(id);
    }


    @PutMapping(value = "/{id}/complete")
    @Operation(summary = "直接仲裁结束流程")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "arbitration_result", description = "仲裁意见", required = true,   in = ParameterIn.QUERY),
    })
    public OrderComplain complete(@PathVariable Long id, String arbitrationResult) {

        return this.orderComplainManager.complete(id, arbitrationResult);
    }

    @PutMapping(value = "/{id}/communication")
    @Operation(summary = "提交对话")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "content", description = "对话内容", required = true,   in = ParameterIn.QUERY),
    })
    public OrderComplainCommunication communicate(@PathVariable Long id, String content) {

        Admin admin = AdminUserContext.getAdmin();
        OrderComplainCommunication communication = new OrderComplainCommunication(id,content,"平台",admin.getUsername(),admin.getUid());

        return this.orderComplainCommunicationManager.add(communication);
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个交易投诉")
    @Parameters({
            @Parameter(name = "id", description = "要查询的交易投诉表主键", required = true,  in = ParameterIn.PATH)
    })
    public OrderComplainVO get(@PathVariable Long id) {

        OrderComplainVO orderComplain = this.orderComplainManager.getModelAndCommunication(id);

        return orderComplain;
    }

}
