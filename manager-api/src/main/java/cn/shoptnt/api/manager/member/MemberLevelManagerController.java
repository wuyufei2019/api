package cn.shoptnt.api.manager.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberLevel;
import cn.shoptnt.model.member.dto.MemberEditDTO;
import cn.shoptnt.model.member.dto.MemberLevelCondition;
import cn.shoptnt.model.member.dto.MemberQueryParam;
import cn.shoptnt.service.member.MemberLevelManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;


/**
 * @author zh
 * @version 1.0
 * @title MemberLevelManagerController
 * @description 会员等级管理类
 * @program: api
 * 2024/5/20 16:36
 */
@RestController
@RequestMapping("/admin/members/level")
@Tag(name = "会员等级相关API")
@Validated
public class MemberLevelManagerController {

    @Autowired
    private MemberLevelManager memberLevelManager;

    @Operation(summary = "查询会员等级集合")
    @GetMapping
    public List<MemberLevel> list() {
        return this.memberLevelManager.list();
    }


    @Operation(summary = "查询会员等级详情")
    @GetMapping("/{id}")
    @Parameters({
            @Parameter(name = "id", description = "会员等级id", in = ParameterIn.PATH),
    })
    public MemberLevelCondition get(@PathVariable Long id) {
        return this.memberLevelManager.get(id);
    }

    @Operation(summary = "修改会员等级")
    @PutMapping("/{id}")
    @Parameters({
            @Parameter(name = "id", description = "会员等级id", in = ParameterIn.PATH),
    })
    public MemberLevelCondition edit(@PathVariable Long id, @RequestBody MemberLevelCondition memberLevelCondition) {
        return this.memberLevelManager.edit(id, memberLevelCondition);
    }

    @PostMapping
    @Operation(summary = "添加会员等级")
    public MemberLevel addMember(@Valid @RequestBody MemberLevelCondition memberLevelCondition) {
        return memberLevelManager.add(memberLevelCondition);
    }


    @Operation(summary = "删除会员等级")
    @DeleteMapping("/{id}")
    @Parameters({
            @Parameter(name = "id", description = "会员等级id", in = ParameterIn.PATH),
            @Parameter(name = "transfer_level_id", description = "新会员等级id", in = ParameterIn.QUERY),
    })
    public void delete(@PathVariable Long id, @RequestParam(value = "transfer_level_id", required = false) Long transferLevelId) {
        this.memberLevelManager.deleteLevel(id, transferLevelId);
    }


    @Operation(summary = "会员等级排序")
    @PutMapping("/sort/{id}")
    @Parameters({
            @Parameter(name = "id", description = "重新排序的会员等级的id", in = ParameterIn.PATH),
            @Parameter(name = "target_position", description = "拖动的会员等级放置的新位置", in = ParameterIn.QUERY),
    })
    public void sortOrder(@PathVariable Long id, @RequestParam(value = "target_position") Integer targetPosition) {
        this.memberLevelManager.updateSortOrder(id, targetPosition);
    }


    @Operation(summary = "会员等级默认")
    @PutMapping("/default/{id}")
    @Parameters({
            @Parameter(name = "id", description = "重新排序的会员等级的id", in = ParameterIn.PATH)
    })
    public void assignDefaultLevel(@PathVariable Long id) {
        this.memberLevelManager.assignDefaultLevel(id);
    }


    @Operation(summary = "修改会员等级")
    @PutMapping("/edit-level/{member_id}/{id}")
    @Parameters({
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.PATH),
            @Parameter(name = "id", description = "会员等级", in = ParameterIn.PATH)
    })
    public void editMemberLevel(@PathVariable(value = "member_id") Long memberId, @PathVariable Long id) {
        this.memberLevelManager.updateMemberLevel(memberId, id);
    }


}
