/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.orderbill;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.orderbill.validator.annotation.BillItemType;
import cn.shoptnt.model.orderbill.dos.Bill;
import cn.shoptnt.model.orderbill.vo.BillDetail;
import cn.shoptnt.model.orderbill.vo.BillExcel;
import cn.shoptnt.model.orderbill.vo.BillQueryParam;
import cn.shoptnt.service.orderbill.BillItemManager;
import cn.shoptnt.service.orderbill.BillManager;
import cn.shoptnt.framework.util.DateUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * @author fk
 * @version v1.0
 * @Description: 结算单账单控制器
 * @date 2018/4/27 11:49
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/order/bills")
@Tag(name = "结算相关API")
@Validated
public class OrderBillManagerController {

    @Autowired
    private BillManager billManager;

    @Autowired
    private BillItemManager billItemManager;

    @Operation(summary = "管理员查询所有周期结算单列表统计")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页数量",  in = ParameterIn.QUERY),
            @Parameter(name = "sn", description = "账单号",   in = ParameterIn.QUERY)
    })
    @GetMapping("/statistics")
    public WebPage getAllBill(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String sn){

        WebPage page = this.billManager.getAllBill(pageNo, pageSize, sn);

        return page;
    }


    @Operation(summary = "管理员查询某周期结算列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页数量",  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage getBillDetail(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, BillQueryParam param){

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        return  this.billManager.queryBills(param);
    }


    @Operation(summary = "查看某账单详细")
    @Parameters({
            @Parameter(name = "bill_id", description = "结算单id", required = true,  in = ParameterIn.PATH),
    })
    @GetMapping("/{bill_id}")
    public BillDetail queryBill(@PathVariable("bill_id") Long billId) {

        return  this.billManager.getBillDetail(billId, Permission.ADMIN);
    }

    @Operation(summary = "导出某账单详细")
    @Parameters({
            @Parameter(name = "bill_id", description = "结算单id", required = true,  in = ParameterIn.PATH),
    })
    @GetMapping("/{bill_id}/export")
    public BillExcel exportBill(@PathVariable("bill_id") Long billId) {

        return  this.billManager.exportBill(billId);
    }


    @Operation(summary = "对账单进行下一步操作")
    @Parameters({
            @Parameter(name = "bill_id", description = "账单id", required = true,  in = ParameterIn.PATH),
    })
    @Log(client = LogClient.admin,detail = "管理员操作结算单，结算单号：${billId}")
    @PutMapping(value = "/{bill_id}/next")
    public Bill nextBill(@PathVariable("bill_id")Long billId) {

        Bill bill = this.billManager.editStatus(billId, Permission.ADMIN);

        return bill;
    }

    @Operation(summary = "查看账单中的订单列表或者退款单列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页数量",  in = ParameterIn.QUERY),
            @Parameter(name = "bill_id", description = "账单id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "bill_type", description = "账单类型", required = true,   in = ParameterIn.PATH),
    })
    @GetMapping("/{bill_id}/{bill_type}")
    public WebPage queryBillItems(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("bill_id") Long billId, @BillItemType @PathVariable("bill_type") String billType) {

        return this.billItemManager.list(pageNo, pageSize, billId, billType);
    }

    @GetMapping("/init")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间 2020-06-01 00:00:01", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间 2020-06-30 23:59:59", required = true,  in = ParameterIn.QUERY),
    })
    public String createBill(@Parameter(hidden = true) String startTime,@Parameter(hidden = true) String endTime){

        this.billManager.createBills(DateUtil.getDateline(startTime,"yyyy-MM-dd HH:mm:ss"),DateUtil.getDateline(endTime,"yyyy-MM-dd HH:mm:ss"));

        return "";
    }
}
