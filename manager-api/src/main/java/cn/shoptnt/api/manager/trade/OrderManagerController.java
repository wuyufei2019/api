/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.trade;

import cn.shoptnt.client.member.MemberHistoryReceiptClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.handler.AdminTwoStepAuthentication;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.trade.cart.dos.OrderPermission;
import cn.shoptnt.model.trade.order.dos.OrderLogDO;
import cn.shoptnt.model.trade.order.dto.OrderQueryParam;
import cn.shoptnt.model.trade.order.vo.CancelVO;
import cn.shoptnt.model.trade.order.vo.OrderDetailVO;
import cn.shoptnt.model.trade.order.vo.OrderLineVO;
import cn.shoptnt.service.trade.order.OrderLogManager;
import cn.shoptnt.service.trade.order.OrderOperateManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * 平台订单API
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */
@Tag(name = "平台订单API")
@RestController
@RequestMapping("/admin/trade/orders")
@Validated
public class OrderManagerController {

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private OrderOperateManager orderOperateManager;

    @Autowired
    private OrderLogManager orderLogManager;

    @Autowired
    private MemberHistoryReceiptClient memberHistoryReceiptClient;

    @Autowired
    private AdminTwoStepAuthentication adminTwoStepAuthentication;


    @Operation(summary = "查询订单列表")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", in = ParameterIn.QUERY),
            @Parameter(name = "ship_name", description = "收货人", in = ParameterIn.QUERY),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "buyer_name", description = "买家名字", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺ID", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY,
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @Parameter(name = "member_id", description = "会员ID", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页数", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "条数", in = ParameterIn.QUERY),
            @Parameter(name = "payment_type", description = "支付方式", in = ParameterIn.QUERY,
                    example = "ONLINE:在线支付,COD:货到付款"),
            @Parameter(name = "client_type", description = "订单来源", in = ParameterIn.QUERY,
                    example = "PC:pc客户端,WAP:WAP客户端,NATIVE:原生APP,REACT:RNAPP,MINI:小程序")
    })
    @GetMapping()
    public WebPage list(@Parameter(hidden = true) String orderSn, @Parameter(hidden = true) String shipName, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) String buyerName,
                        @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime, @Parameter(hidden = true) String orderStatus,
                        @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long memberId, @Parameter(hidden = true) Long sellerId,
                        @Parameter(hidden = true) String paymentType, @Parameter(hidden = true) String clientType) {

        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setOrderStatus(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setMemberId(memberId);
        param.setSellerId(sellerId);
        param.setPaymentType(paymentType);
        param.setClientType(clientType);

        WebPage page = this.orderQueryManager.list(param);
        return page;
    }


    @Operation(summary = "导出订单列表")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", in = ParameterIn.QUERY),
            @Parameter(name = "ship_name", description = "收货人", in = ParameterIn.QUERY),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "buyer_name", description = "买家名字", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺ID", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY,
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @Parameter(name = "payment_type", description = "支付方式", in = ParameterIn.QUERY,
                    example = "ONLINE:在线支付,COD:货到付款"),
            @Parameter(name = "client_type", description = "订单来源", in = ParameterIn.QUERY,
                    example = "PC:pc客户端,WAP:WAP客户端,NATIVE:原生APP,REACT:RNAPP,MINI:小程序")
    })
    @GetMapping("/export")
    public List<OrderLineVO> export(@Parameter(hidden = true) String orderSn, @Parameter(hidden = true) String shipName, @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) String buyerName,
                                    @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime, @Parameter(hidden = true) String orderStatus, @Parameter(hidden = true) Long sellerId,
                                    @Parameter(hidden = true) String paymentType, @Parameter(hidden = true) String clientType) {
        //调用二次验证
        this.adminTwoStepAuthentication.sensitive();

        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setOrderStatus(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setSellerId(sellerId);
        param.setPaymentType(paymentType);
        param.setClientType(clientType);

        List<OrderLineVO> lineList = this.orderQueryManager.export(param);
        double pay = 0.0;
        for (OrderLineVO orderLineVO : lineList) {
            if (orderLineVO.getBalance() != null) {
                pay = 0.0;
                if (orderLineVO.getPayMoney() == null) {
                    pay = orderLineVO.getBalance();
                } else {
                    pay = CurrencyUtil.sub(orderLineVO.getPayMoney(), orderLineVO.getBalance());
                }
                orderLineVO.setPayMoney(pay);
            }
        }
        return lineList;
    }


    @Operation(summary = "查询单个订单明细")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{order_sn}")
    public OrderDetailVO get(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {
        OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, null);
        if (detailVO.getNeedReceipt().intValue() == 1) {
            detailVO.setReceiptHistory(memberHistoryReceiptClient.getReceiptHistory(orderSn));
        }

        return detailVO;
    }


    @Operation(summary = "确认收款")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH),
            @Parameter(name = "pay_price", description = "付款金额", in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/{order_sn}/pay")
    @Log(client = LogClient.admin, detail = "订单收款，订单号：${orderSn}")
    public String payOrder(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn, @Parameter(hidden = true) Double payPrice) {
        this.orderOperateManager.payOrder(orderSn, payPrice, "", OrderPermission.admin);
        return "";
    }


    @Operation(summary = "取消订单")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH),
    })
    @PostMapping(value = "/{order_sn}/cancelled")
    @Log(client = LogClient.admin, detail = "订单取消，订单号：${orderSn}")
    public String cancelledOrder(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {

        CancelVO cancelVO = new CancelVO();
        cancelVO.setReason("管理员取消");
        cancelVO.setOrderSn(orderSn);
        cancelVO.setOperator("平台管理员");

        this.orderOperateManager.cancel(cancelVO, OrderPermission.admin);
        return "";
    }


    @Operation(summary = "查询订单日志")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{order_sn}/log")
    public List<OrderLogDO> getList(@Parameter(hidden = true) @PathVariable("order_sn") String orderSn) {
        List<OrderLogDO> logDOList = this.orderLogManager.listAll(orderSn);
        return logDOList;
    }

}
