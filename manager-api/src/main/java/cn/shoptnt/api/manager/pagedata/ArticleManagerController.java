/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.pagedata;

import cn.shoptnt.model.pagedata.vo.ArticleDetail;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;




import javax.validation.Valid;

import cn.shoptnt.model.pagedata.Article;
import cn.shoptnt.service.pagedata.ArticleManager;

/**
 * 文章控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-12 10:43:18
 */
@RestController
@RequestMapping("/admin/pages/articles")
@Tag(name = "文章相关API")
public class ArticleManagerController {

    @Autowired
    private ArticleManager articleManager;


    @Operation(summary = "查询文章列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "文章名称",   in = ParameterIn.QUERY),
            @Parameter(name = "category_id", description = "文章分类",   in = ParameterIn.QUERY),
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, String name, @Parameter(hidden = true) Long categoryId) {

        return this.articleManager.list(pageNo, pageSize, name, categoryId);
    }


    @Operation(summary = "添加文章")
    @PostMapping
    public Article add(@Valid Article article) {

        this.articleManager.add(article);

        return article;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改文章")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public Article edit(@Valid Article article, @PathVariable Long id) {

        this.articleManager.edit(article, id);

        return article;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除文章")
    @Parameters({
            @Parameter(name = "id", description = "要删除的文章主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.articleManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个文章")
    @Parameters({
            @Parameter(name = "id", description = "要查询的文章主键", required = true,  in = ParameterIn.PATH)
    })
    public Article get(@PathVariable Long id) {

        Article article = this.articleManager.getModel(id);

        return article;
    }

}
