/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.system.dos.AdminUser;
import cn.shoptnt.model.system.vo.AdminUserVO;
import cn.shoptnt.service.system.AdminUserManager;
import cn.shoptnt.framework.database.WebPage;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 平台管理员控制器
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-20 20:38:26
 */
@RestController
@RequestMapping("/admin/systems/manager/admin-users")
@Tag(name = "平台管理员管理相关API")
@Validated
public class AdminUserOperationManagerController {

    @Autowired
    private AdminUserManager adminUserManager;


    @Operation(summary = "查询平台管理员列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keyword", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keyword) {
        return this.adminUserManager.list(pageNo, pageSize, keyword);
    }


    @Operation(summary = "添加平台管理员")
    @PostMapping
    @Log(client = LogClient.admin,detail = "添加管理员，管理员：${adminUserVO.username}",level = LogLevel.important)
    public AdminUser add(@Valid AdminUserVO adminUserVO) {
        return this.adminUserManager.add(adminUserVO);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改平台管理员")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    @Log(client = LogClient.admin,detail = "修改管理员，管理员id：${id}",level = LogLevel.important)
    public AdminUser edit(@Valid AdminUserVO adminUserVO, @PathVariable Long id) {
        return this.adminUserManager.edit(adminUserVO, id);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除平台管理员")
    @Parameters({
            @Parameter(name = "id", description = "要删除的平台管理员主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.adminUserManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个平台管理员")
    @Parameters({
            @Parameter(name = "id", description = "要查询的平台管理员主键", required = true,  in = ParameterIn.PATH)
    })
    public AdminUser get(@PathVariable Long id) {

        AdminUser adminUser = this.adminUserManager.getModel(id);

        return adminUser;
    }
}
