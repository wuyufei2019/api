/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.MessageTemplateDO;
import cn.shoptnt.model.system.dto.MessageTemplateDTO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import cn.shoptnt.service.system.MessageTemplateManager;

/**
 * 消息模版控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-05 16:38:43
 */
@RestController
@RequestMapping("/admin/systems/message-templates")
@Tag(name = "消息模版相关API")
@Validated
public class MessageTemplateManagerController {

    @Autowired
    private MessageTemplateManager messageTemplateManager;


    @Operation(summary = "查询消息模版列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "type", description = "消息类型", required = true,   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotNull(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotNull(message = "每页数量不能为空") Long pageSize, @NotEmpty(message = "模版类型必填") String type) {

        return this.messageTemplateManager.list(pageNo, pageSize, type);
    }


    @PutMapping(value = "/{id}")
    @Operation(summary = "修改消息模版")
    @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    public MessageTemplateDO edit(@Valid MessageTemplateDTO messageTemplate, @PathVariable("id") Long id) {
        return this.messageTemplateManager.edit(messageTemplate, id);
    }

}
