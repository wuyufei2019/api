/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.model.goods.dos.SpecValuesDO;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.service.goods.SpecValuesManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 规格值控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-20 10:23:53
 */
@RestController
@RequestMapping("/admin/goods/specs/{spec_id}/values")
@Tag(name = "规格值相关API")
@Validated
public class SpecValuesManagerController {

    @Autowired
    private SpecValuesManager specValuesManager;

    @Operation(summary = "查询规格值列表")
    @Parameters({
            @Parameter(name = "spec_id", description = "规格id", required = true,  in = ParameterIn.PATH),})
    @GetMapping
    public List<SpecValuesDO> list(@PathVariable("spec_id") Long specId) {
        return this.specValuesManager.listBySpecId(specId, Permission.ADMIN);
    }

    @Operation(summary = "添加某规格的规格值")
    @Parameters({
            @Parameter(name = "spec_id", description = "规格id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "value_list", description = "规格值集合",   in = ParameterIn.QUERY),})
    @PostMapping
    public List<SpecValuesDO> saveSpecValue(@PathVariable("spec_id") Long specId, @NotNull(message = "至少添加一个规格值") @Parameter(hidden = true) @RequestParam(value = "value_list") String[] valueList) {

        return this.specValuesManager.saveSpecValue(specId, valueList);
    }

}
