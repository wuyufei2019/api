package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.crowdfunding.query.CrowdfundingQueryParams;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingDTO;
import cn.shoptnt.service.promotion.crowdfunding.CrowdfundingManager;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.validation.Valid;

/**
 * 众筹活动 API
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@RestController
@RequestMapping("/admin/promotion/crowdfunding")
@Tag(name = "众筹活动API")
@Validated
public class CrowdfundingManagerController {

    @Autowired
    private CrowdfundingManager crowdfundingManager;

    @Operation(summary = "分页列表")
    @GetMapping
    public WebPage<CrowdfundingVO> list(@ParameterObject CrowdfundingQueryParams queryParams) {
        return crowdfundingManager.list(queryParams);
    }

    @Operation(summary = "添加")
    @PostMapping
    public void add(@RequestBody @Valid CrowdfundingDTO crowdfundingDTO) {
        crowdfundingManager.add(crowdfundingDTO);
    }

    @Operation(summary = "修改")
    @PutMapping("/{id}")
    public void edit(@PathVariable Long id, @RequestBody @Valid CrowdfundingDTO crowdfundingDTO) {
        crowdfundingDTO.setId(id);
        crowdfundingManager.edit(crowdfundingDTO);
    }

    @Operation(summary = "查询")
    @GetMapping("/{id}")
    public CrowdfundingVO getDetail(@PathVariable Long id) {
        return crowdfundingManager.getDetail(id);
    }

    @Operation(summary = "删除")
    @DeleteMapping("/{ids}")
    public void delete(@PathVariable List<Long> ids) {
        crowdfundingManager.delete(ids);
    }

    @Operation(summary = "终止活动")
    @PutMapping("/{ids}/stop")
    public void stop(@PathVariable List<Long> ids) {
        crowdfundingManager.stop(ids);
    }

    @Operation(summary = "修改活动结束是否展示")
    @PutMapping("/{ids}/updateEndShowFlag")
    public void updateEndShowFlag(@PathVariable List<Long> ids, @RequestParam Boolean flag) {
        crowdfundingManager.updateEndShowFlag(ids, flag);
    }

}

