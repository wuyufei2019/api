/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dto.CommentQueryParam;
import cn.shoptnt.model.member.vo.BatchAuditVO;
import cn.shoptnt.model.member.vo.CommentVO;
import cn.shoptnt.service.member.MemberCommentManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 评论控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 10:19:14
 */
@RestController
@RequestMapping("/admin/members/comments")
@Tag(name = "评论相关API")
public class MemberCommentManagerController {

    @Autowired
    private MemberCommentManager memberCommentManager;

    @Operation(summary = "查询评论列表")
    @GetMapping
    public WebPage list(@Valid CommentQueryParam param) {

        return this.memberCommentManager.list(param);
    }

    @Operation(summary = "批量审核商品评论")
    @PostMapping("/batch/audit")
    public String batchAuditComment(@Valid @RequestBody BatchAuditVO batchAuditVO) {

        this.memberCommentManager.batchAudit(batchAuditVO);

        return "";
    }

    @Operation(summary = "删除评论")
    @Parameters({
            @Parameter(name = "comment_id", description = "评论id", required = true,  in = ParameterIn.PATH),
    })
    @DeleteMapping(value = "/{comment_id}")
    public String deleteComment(@PathVariable(name = "comment_id") Long commentId) {

        this.memberCommentManager.delete(commentId);

        return "";
    }

    @Operation(summary = "查询会员商品评论详请")
    @Parameters({
            @Parameter(name = "comment_id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{comment_id}")
    public CommentVO get(@PathVariable("comment_id") Long commentId) {
        return this.memberCommentManager.get(commentId);
    }

}
