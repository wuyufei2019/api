/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.model.member.dos.ConnectSettingDO;
import cn.shoptnt.model.member.dto.ConnectSettingDTO;
import cn.shoptnt.model.member.vo.ConnectSettingVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.member.ConnectManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zjp
 * @version v7.0
 * @Description 信任登录设置
 * @ClassName ConnectController
 * @since v7.0 下午4:23 2018/6/15
 */
@Tag(name ="信任登录设置api")
@RestController
@RequestMapping("/admin/members/connect")
public class MemberConnectManagerController {

    @Autowired
    private ConnectManager connectManager;



    @GetMapping()
    @Operation(summary = "获取信任登录配置参数")
    public List<ConnectSettingVO> list(){
        return  connectManager.list();
    }

    @PutMapping(value = "/{type}")
    @Operation(summary = "修改信任登录参数")
    @Log(client = LogClient.admin,detail = "修改信任登录参数",level = LogLevel.important)
    @Parameter(name = "type", description = "用户名", required = true,   in = ParameterIn.PATH)
    public ConnectSettingDTO editConnectSetting(@RequestBody ConnectSettingDTO connectSettingDTO, @PathVariable("type")  String type) {
        connectManager.save(connectSettingDTO);
        return connectSettingDTO;
    }


}
