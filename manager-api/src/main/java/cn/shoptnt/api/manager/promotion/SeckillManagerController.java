/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;
import cn.shoptnt.model.promotion.seckill.dto.SeckillAuditParam;
import cn.shoptnt.model.promotion.seckill.dto.SeckillQueryParam;
import cn.shoptnt.model.promotion.seckill.enums.SeckillStatusEnum;
import cn.shoptnt.model.promotion.seckill.vo.SeckillVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.promotion.seckill.SeckillManager;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.util.DateUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 限时抢购活动控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 17:30:23
 */
@RestController
@RequestMapping("/admin/promotion/seckills")
@Tag(name = "限时抢购活动相关API")
@Validated
public class SeckillManagerController {

    @Autowired
    private SeckillManager seckillManager;


    @Operation(summary = "查询限时抢购列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "seckill_name", description = "活动名称",   in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "状态",   in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "限时抢购日期-开始日期",  in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "限时抢购日期-结束日期",  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<SeckillVO> list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String seckillName,
                                   @Parameter(hidden = true) String status, @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        SeckillQueryParam param = new SeckillQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setSeckillName(seckillName);
        param.setStatus(status);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        return this.seckillManager.list(param);
    }


    @Operation(summary = "添加限时抢购入库")
    @PostMapping
    public SeckillVO add(@Valid @RequestBody SeckillVO seckill) {
        this.verifyParam(seckill);
        seckill.setSeckillStatus(SeckillStatusEnum.EDITING.name());
        this.seckillManager.add(seckill);
        return seckill;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改限时抢购入库")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public SeckillVO edit(@Valid @RequestBody SeckillVO seckill, @PathVariable @NotNull(message = "限时抢购ID参数错误") Long id) {
        this.verifyParam(seckill);
        this.seckillManager.edit(seckill, id);
        return seckill;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除限时抢购入库")
    @Parameters({
            @Parameter(name = "id", description = "要删除的限时抢购入库主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.seckillManager.delete(id);

        return "";
    }

    @DeleteMapping(value = "/{id}/close")
    @Operation(summary = "关闭限时抢购")
    @Parameters({
            @Parameter(name = "id", description = "要关闭的限时抢购入库主键", required = true,  in = ParameterIn.PATH)
    })
    public String close(@PathVariable Long id) {

        this.seckillManager.close(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个限时抢购入库")
    @Parameters({
            @Parameter(name = "id", description = "要查询的限时抢购入库主键", required = true,  in = ParameterIn.PATH)
    })
    public SeckillVO get(@PathVariable Long id) {
        SeckillVO seckillVO = this.seckillManager.getModel(id);
        return seckillVO;
    }


    @Operation(summary = "发布限时抢购活动")
    @PostMapping("/{seckill_id}/release")
    @Parameters({
            @Parameter(name = "seckill_id", description = "要查询的限时抢购入库主键", required = true,  in = ParameterIn.PATH)
    })
    public SeckillVO publish(@Valid @RequestBody SeckillVO seckill, @Parameter(hidden = true) @PathVariable("seckill_id") Long seckillId) {

        this.verifyParam(seckill);
        //发布状态
        seckill.setSeckillStatus(SeckillStatusEnum.RELEASE.name());
        if (seckillId == null || seckillId == 0) {
            seckillManager.add(seckill);
        } else {
            seckillManager.edit(seckill, seckillId);
        }

        return seckill;
    }


    @Operation(summary = "批量审核商品")
    @PostMapping(value = "/batch/audit")
    @Log(client = LogClient.admin,detail = "审核限时抢购商品")
    public String batchAudit(@Valid @RequestBody SeckillAuditParam param) {

        this.seckillManager.batchAuditGoods(param);
        return "";
    }

    /**
     * 验证参数
     *
     * @param seckillVO
     */
    private void verifyParam(SeckillVO seckillVO) {
        //获取活动开始时间
        long startDay = seckillVO.getStartDay();

        //获取活动开始当天0点的时间
        String startDate = DateUtil.toString(startDay, "yyyy-MM-dd");
        long startTime = DateUtil.getDateline(startDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss");

        //获取报名截止时间
        long applyTime = seckillVO.getApplyEndTime();

        //获取当前时间
        long currentTime = DateUtil.getDateline();

        //获取当天开始时间
        String currentDay = DateUtil.toString(currentTime, "yyyy-MM-dd");
        long currentStartTime = DateUtil.getDateline(currentDay + " 00:00:00", "yyyy-MM-dd HH:mm:ss");

        //活动时间小于当天开始时间
        if (startDay < currentStartTime) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动时间不能小于当前时间");
        }
        //报名截止时间小于当前时间
        if (applyTime < currentTime) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "报名截止时间不能小于当前时间");
        }
        //报名截止时间大于活动开始当天的起始时间
        if (applyTime > startTime) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "报名截止时间不能大于活动开始时间");
        }

        List<Integer> termList = new ArrayList<>();
        for (Integer time : seckillVO.getRangeList()) {
            if (termList.contains(time)) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "抢购区间的值不能重复");
            }
            //抢购区间的值不在0到23范围内
            if (time < 0 || time > 23) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "抢购区间必须在0点到23点的整点时刻");
            }
            termList.add(time);
        }
    }

}
