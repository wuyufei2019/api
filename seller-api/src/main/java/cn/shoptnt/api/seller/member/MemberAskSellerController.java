/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.member;

import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.dto.AskQueryParam;
import cn.shoptnt.model.member.enums.AuditEnum;
import cn.shoptnt.service.member.MemberAskManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 咨询控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-04 17:41:18
 */
@RestController
@RequestMapping("/seller/members/asks")
@Tag(name = "咨询相关API")
@Validated
public class MemberAskSellerController {

    @Autowired
    private MemberAskManager memberAskManager;


    @Operation(summary = "查询咨询列表")
    @GetMapping
    public WebPage list(@Valid AskQueryParam param) {
        //卖家id和审核状态
        param.setSellerId(UserContext.getSeller().getSellerId());
        param.setAuthStatus(AuditEnum.PASS_AUDIT.value());

        return this.memberAskManager.list(param);
    }


    @Operation(summary = "商家回复会员商品咨询")
    @Parameters({
            @Parameter(name = "reply_content", description = "回复内容", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "ask_id", description = "咨询id", required = true,  in = ParameterIn.PATH)
    })
    @PutMapping("/{ask_id}/reply")
    public MemberAsk reply(@Parameter(hidden = true) @NotEmpty(message = "请输入回复内容") String replyContent, @Parameter(hidden = true) @PathVariable("ask_id")Long askId) {

        MemberAsk memberAsk = this.memberAskManager.reply(replyContent, askId);

        return memberAsk;
    }

    @Operation(summary = "查询会员商品咨询详请")
    @Parameters({
            @Parameter(name = "ask_id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{ask_id}")
    public MemberAsk get(@PathVariable("ask_id")Long askId) {
        return this.memberAskManager.getModel(askId);
    }
}
