/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.member;


import cn.shoptnt.service.member.MemberManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 会员控制器
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-16 11:33:56
 */
@RestController
@RequestMapping("/seller/members")
@Tag(name = "会员相关API")
public class MemberSellerController {

    @Autowired
    private MemberManager memberManager;

    @Operation(summary = "注销会员登录")
    @PostMapping(value = "/logout")
    @Parameters({
            @Parameter(name = "uid", description = "会员id",  in = ParameterIn.QUERY, required = true)
    })
    public String loginOut() {
        this.memberManager.logout();
        return "";
    }


}