/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.shop.vo.ShopVO;
import cn.shoptnt.model.statistics.enums.QueryDateType;
import cn.shoptnt.model.statistics.vo.MultipleChart;
import cn.shoptnt.model.statistics.vo.ShopDashboardVO;
import cn.shoptnt.service.shop.ShopManager;
import cn.shoptnt.service.statistics.DashboardStatisticManager;
import cn.shoptnt.service.statistics.GoodsStatisticManager;
import cn.shoptnt.service.statistics.OrderStatisticManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商家中心，首页数据
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/25 15:13
 */
@RestController
@RequestMapping("/seller/statistics/dashboard")
@Tag(name = "商家中心，首页数据")
public class DashboardStatisticsSellerController {

    @Autowired
    private DashboardStatisticManager dashboardStatisticManager;

    @Autowired
    private ShopManager shopManager;

    @Autowired
    private OrderStatisticManager orderStatisticManager;

    @Autowired
    private GoodsStatisticManager goodsStatisticManager;

    @Operation(summary = "首页数据")
    @GetMapping("/shop")
    public ShopDashboardVO shop() {
        return this.dashboardStatisticManager.getShopData();
    }

    @Operation(summary = "查询店铺评分")
    @GetMapping("/shopScore")
    public ShopVO shopScore() {
        return this.shopManager.getShop(UserContext.getSeller().getSellerId());
    }

    @Operation(summary = "交易量/交易额图表")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY, required = true),
    })
    @GetMapping(value = "/turnoverChart")
    public MultipleChart turnoverChart(@Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        return this.orderStatisticManager.getIndexTurnoverChart(startTime, endTime, UserContext.getSeller().getSellerId());
    }

    @Operation(summary = "商品交易额TOP5")
    @GetMapping(value = "/hotGoodsAmount")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY, required = true)
    })
    public WebPage hotGoodsAmount(@Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLimit(5);
        searchCriteria.setCycleType(QueryDateType.TIME.value());
        searchCriteria.setStartTime(startTime == null ? DateUtil.startOfYear() : startTime);
        searchCriteria.setEndTime(endTime == null ? DateUtil.getDateline() : endTime);
        searchCriteria.setSellerId(UserContext.getSeller().getSellerId());
        return this.goodsStatisticManager.getHotSalesMoneyPage(searchCriteria);
    }

    @Operation(summary = "商品交易量TOP5")
    @GetMapping(value = "/hotGoodsCount")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY, required = true)
    })
    public WebPage hotGoodsCount(@Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLimit(5);
        searchCriteria.setCycleType(QueryDateType.TIME.value());
        searchCriteria.setStartTime(startTime == null ? DateUtil.startOfYear() : startTime);
        searchCriteria.setEndTime(endTime == null ? DateUtil.getDateline() : endTime);
        searchCriteria.setSellerId(UserContext.getSeller().getSellerId());
        return this.goodsStatisticManager.getHotSalesNumPage(searchCriteria);
    }

}
