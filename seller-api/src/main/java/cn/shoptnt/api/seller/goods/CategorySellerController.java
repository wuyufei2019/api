/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.goods;

import cn.shoptnt.model.goods.dos.BrandDO;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.goods.vo.CategoryVO;
import cn.shoptnt.model.goods.vo.GoodsParamsGroupVO;
import cn.shoptnt.service.goods.BrandManager;
import cn.shoptnt.service.goods.CategoryManager;
import cn.shoptnt.service.goods.GoodsParamsManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * 商品分类控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-15 17:22:06
 */
@RestController
@RequestMapping("/seller/goods")
@Tag(name = "商品分类相关API")
public class CategorySellerController {

    @Autowired
    private CategoryManager categoryManager;
    @Autowired
    private GoodsParamsManager goodsParamsManager;
    @Autowired
    private BrandManager brandManager;

    @Operation(summary = "商品发布，获取当前登录用户选择经营类目的所有父")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id，顶级为0", required = true, in = ParameterIn.PATH),})
    @GetMapping(value = "/category/{category_id}/children")
    public List<CategoryDO> list(@Parameter(hidden = true) @PathVariable("category_id") Long categoryId) {
        List<CategoryDO> list = this.categoryManager.getSellerCategory(categoryId);
        return list;
    }


    @Operation(summary = "商家分类")
    @GetMapping(value = "/category/seller/children")
    public List<CategoryVO> listSeller() {
        List<CategoryVO> list = this.categoryManager.listAllSellerChildren();
        return list;
    }

    @Operation(summary = "商品发布，获取所选分类关联的参数信息")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "goods_id", description = "商品id", required = true, in = ParameterIn.PATH),
    })
    @GetMapping(value = "/category/{category_id}/{goods_id}/params")
    public List<GoodsParamsGroupVO> queryParams(@PathVariable("category_id") Long categoryId, @PathVariable("goods_id") Long goodsId) {

        List<GoodsParamsGroupVO> list = this.goodsParamsManager.queryGoodsParams(categoryId, goodsId);

        return list;
    }

    @Operation(summary = "发布商品，获取所选分类关联的参数信息")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/category/{category_id}/params")
    public List<GoodsParamsGroupVO> queryParams(@PathVariable("category_id") Long categoryId) {

        List<GoodsParamsGroupVO> list = this.goodsParamsManager.queryGoodsParams(categoryId);

        return list;
    }

    @Operation(summary = "修改商品，获取所选分类关联的品牌信息")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),
    })
    @GetMapping(value = "/category/{category_id}/brands")
    public List<BrandDO> queryBrands(@PathVariable("category_id") Long categoryId) {

        List<BrandDO> list = this.brandManager.getBrandsByCategory(categoryId);

        return list;
    }


}
