/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.model.shop.dos.ShopLogisticsSetting;
import cn.shoptnt.model.shop.vo.ShopLogisticsSettingVO;
import cn.shoptnt.model.system.dto.KDNParams;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

import cn.shoptnt.model.shop.vo.operator.SellerEditShop;
import cn.shoptnt.service.shop.ShopLogisticsCompanyManager;
import javax.validation.Valid;

/**
 * 店铺物流公司相关API
 *
 * @author zhangjiping
 * @version v7.0.0
 * @since v7.0.0
 * 2018年3月30日 上午10:56:11
 */
@RestController
@RequestMapping("/seller/shops/logi-companies")
@Tag(name = "店铺物流公司相关API")
public class ShopLogisticsCompanySellerController {

    @Autowired
    private ShopLogisticsCompanyManager shopLogisticsCompanyManager;


    @Operation(summary = "查询物流公司列表")
    @GetMapping
    public List list() {
        List list = this.shopLogisticsCompanyManager.list();

        return list;
    }


    @Operation(summary = "开启物流公司")
    @PostMapping("/{logi_id}")
    @Parameter(name = "logi_id", description = "物流公司id", required = true,  in = ParameterIn.PATH)
    public SellerEditShop add(@Parameter(hidden = true) @PathVariable("logi_id") Long logiId) {
        Seller seller = UserContext.getSeller();
        SellerEditShop sellerEditShop = new SellerEditShop();
        sellerEditShop.setSellerId(seller.getSellerId());
        sellerEditShop.setOperator("店铺开启物流公司");
        this.shopLogisticsCompanyManager.open(logiId);

        return sellerEditShop;
    }


    @Operation(summary = "设置电子面单")
    @PostMapping("/setting/{logi_id}")
    @Parameter(name = "logi_id", description = "物流公司id", required = true,  in = ParameterIn.PATH)
    public void setting(@Parameter(hidden = true) @PathVariable("logi_id") Long logiId, @RequestBody @Valid KDNParams kdnParams) {
        //店铺电子面单设置
        ShopLogisticsSetting shopLogisticsSetting = this.shopLogisticsCompanyManager.query(logiId, UserContext.getSeller().getSellerId());
        shopLogisticsSetting.setParams(kdnParams);
        this.shopLogisticsCompanyManager.edit(shopLogisticsSetting, shopLogisticsSetting.getId());
    }


    @DeleteMapping(value = "/{logi_id}")
    @Operation(summary = "关闭物流公司")
    @Parameter(name = "logi_id", description = "要删除的物流公司主键", required = true,  in = ParameterIn.PATH)
    public SellerEditShop delete(@Parameter(hidden = true) @PathVariable("logi_id") Long logiId) {
        Seller seller = UserContext.getSeller();
        SellerEditShop sellerEditShop = new SellerEditShop();
        sellerEditShop.setSellerId(seller.getSellerId());
        sellerEditShop.setOperator("店铺关闭物流公司");
        this.shopLogisticsCompanyManager.delete(logiId);

        return sellerEditShop;
    }

}
