/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.system;

import cn.shoptnt.model.system.vo.ExpressDetailVO;
import cn.shoptnt.service.system.ExpressPlatformManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 物流查询接口
 *
 * @author zh
 * @version v7.0
 * @date 18/7/12 上午10:30LogiCompanyManagerController
 * @since v7.0
 */
@Tag(name = "物流查询接口")
@RestController
@RequestMapping("/seller/express")
@Validated
public class ExpressDetailSellerController {

    @Autowired
    private ExpressPlatformManager expressPlatformManager;

    @Operation(summary = "查询物流详细")
    @Parameters({
            @Parameter(name = "id", description = "物流公司id",  in = ParameterIn.QUERY),
            @Parameter(name = "num", description = "快递单号",   in = ParameterIn.QUERY),
    })
    @GetMapping
    public ExpressDetailVO list(@Parameter(hidden = true) Long id, @Parameter(hidden = true) String num) {
        return this.expressPlatformManager.getExpressDetail(id, num);
    }
}
