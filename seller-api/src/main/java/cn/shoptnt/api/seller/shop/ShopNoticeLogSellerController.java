/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.MemberNoticeLog;
import cn.shoptnt.model.shop.dos.ShopNoticeLogDO;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import cn.shoptnt.service.shop.ShopNoticeLogManager;

/**
 * 店铺站内消息控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-10 10:21:45
 */
@RestController
@RequestMapping("/seller/shops/shop-notice-logs")
@Tag(name = "店铺站内消息相关API")
public class ShopNoticeLogSellerController {

    @Autowired
    private ShopNoticeLogManager shopNoticeLogManager;


    @Operation(summary = "查询店铺站内消息列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "is_read", description = "是否已读 1已读 0未读", in = ParameterIn.QUERY),
            @Parameter(name = "type", description = "消息类型", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Integer isRead, String type) {

        return this.shopNoticeLogManager.list(pageNo, pageSize, type, isRead);
    }

    @DeleteMapping(value = "/{ids}")
    @Operation(summary = "删除店铺站内消息")
    @Parameters({
            @Parameter(name = "ids", description = "要删除的消息主键", required = true, in = ParameterIn.PATH)
    })
    public void delete(@PathVariable("ids") Long[] ids) {

        this.shopNoticeLogManager.delete(ids);

    }

    @PutMapping(value = "/{ids}/read")
    @Operation(summary = "将消息设置为已读")
    @Parameters({
            @Parameter(name = "ids", description = "要设置为已读消息的id", required = true, in = ParameterIn.PATH)
    })
    public String read(@PathVariable Long[] ids) {
        this.shopNoticeLogManager.read(ids);
        return null;
    }

}
