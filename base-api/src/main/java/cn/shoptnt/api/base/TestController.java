/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.erp.ApiParams;
import cn.shoptnt.model.erp.MagicErpApiEnum;
import cn.shoptnt.model.erp.ThirdApiResponse;
import cn.shoptnt.model.system.dos.Regions;
import cn.shoptnt.service.erp.MagicErpHttpClint;
import cn.shoptnt.service.system.RegionsManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 地区api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/28 下午7:49
 * @since v7.0
 */
@RestController
@RequestMapping("/base/test")
@Tag(name= "地区API")
public class TestController {

    @Autowired
    private MagicErpHttpClint magicErpHttpClint;

    @GetMapping
    @Operation(description = "test")
    public String getChildrenById(String msg) {
        ApiParams apiParams = new ApiParams();
        apiParams.put("msg_id", "001");
        apiParams.put("type", "Test");
        apiParams.put("content", msg);
        apiParams.put("produce_time", DateUtil.getDateline());
        return magicErpHttpClint.execute(MagicErpApiEnum.MessageReceive, apiParams).getData();
    }

}
