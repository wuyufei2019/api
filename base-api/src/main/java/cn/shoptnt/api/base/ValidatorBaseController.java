/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.model.system.dto.ValidatorPlatformDTO;
import cn.shoptnt.service.system.ValidatorPlatformManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 验证方式API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-18
 */
@RestController
@RequestMapping("/base/validator")
@Tag(name = "验证方式API")
public class ValidatorBaseController {

    @Autowired
    private ValidatorPlatformManager validatorPlatformManager;

    @Operation(summary = "获取当前系统开启的验证平台信息")
    @GetMapping()
    public ValidatorPlatformDTO get() {

        return this.validatorPlatformManager.getCurrentOpen();
    }

}
