/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.model.member.vo.RegionVO;
import cn.shoptnt.model.system.dos.Regions;
import cn.shoptnt.service.system.RegionsManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * 地区api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/28 下午7:49
 * @since v7.0
 */
@RestController
@RequestMapping("/base/regions")
@Tag(name = "地区API")
public class RegionsBaseController {

    @Autowired
    private RegionsManager regionsManager;

    @GetMapping(value = "/{id}/children")
    @Operation(summary = "获取某地区的子地区")
    @Parameters({
            @Parameter(name = "id", description = "地区id", required = true,  in = ParameterIn.PATH)
    })
    public List<Regions> getChildrenById(@PathVariable Long id) {

        return regionsManager.getRegionsChildren(id);
    }


    @GetMapping(value = "/depth/{depth}")
    @Operation(summary = "根据地区深度查询组织好地区数据结构的地区")
    @Parameters({
            @Parameter(name = "depth", description = "深度", required = true,  in = ParameterIn.PATH)
    })
    public List<RegionVO> getRegionByDepth(@PathVariable Integer depth) {
        return regionsManager.getRegionByDepth(depth);
    }


}
