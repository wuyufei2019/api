package cn.shoptnt.api.open;

import cn.shoptnt.model.datasync.dto.MessageReceiveDTO;
import cn.shoptnt.service.datasync.receive.common.MessageReceiveManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 接收MagicErp系统的消息
 *
 * @author 张崧
 * @since 2024-04-01
 */
@RestController
@RequestMapping("/open/magic-erp/message-receive")
@Tag(name = "接收MagicErp系统的消息")
@Validated
public class MagicErpMessageReceiveOpenController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageReceiveManager messageReceiveManager;

    /**
     * 接收成功标识
     */
    private static final String SUCCESS = "SUCCESS";

    @PostMapping
    @Operation(description = "接收消息")
    public String receive(@RequestBody @Valid MessageReceiveDTO messageReceiveDTO) {

        logger.info("接收MagicErp系统的消息\n 类型：{}\n 消息id：{}\n 消息时间：{}\n 消息内容：{}",
                messageReceiveDTO.getType(), messageReceiveDTO.getMsgId(),
                messageReceiveDTO.getProduceTime(), messageReceiveDTO.getContent());

        messageReceiveManager.receive(messageReceiveDTO);
        return SUCCESS;
    }

}
