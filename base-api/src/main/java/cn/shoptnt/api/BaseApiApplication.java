/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by 妙贤 on 2018/3/18.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/18
 */
@SpringBootApplication
@ComponentScan(basePackages = "cn.shoptnt",
        excludeFilters  = @ComponentScan.Filter(type = FilterType.ASPECTJ, pattern="cn.shoptnt.mapper.*"))
@EnableScheduling
@EnableAsync
@MapperScan(basePackages = "cn.shoptnt.mapper")
@ServletComponentScan
public class BaseApiApplication {

    public static void main(String[] args) {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
        SpringApplication.run(BaseApiApplication.class, args);
    }
}
