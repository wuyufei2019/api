/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.sncreator.SnCreator;
import cn.shoptnt.framework.test.BaseTest;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.OrderItemsDO;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Random;

/**
 * 分库分表测试
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/6/10
 */
@Rollback(false)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class ShardTest extends BaseTest {
    @Autowired
    private DaoSupport daoSupport;

    @Autowired
    SnCreator snCreator;



    @Test
    public void snTest() {
        Long sn = snCreator.create(1);
        

    }


}
