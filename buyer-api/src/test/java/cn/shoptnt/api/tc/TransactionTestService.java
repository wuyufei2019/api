/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.tc;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/6/13
 */
public interface TransactionTestService {

     void orderAdd();
}
