/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.pagedata;

import cn.shoptnt.model.pagedata.HotKeyword;
import cn.shoptnt.service.pagedata.HotKeywordManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * 热门关键字控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-04 10:43:23
 */
@RestController
@RequestMapping("/buyer/pages/hot-keywords")
@Tag(name = "热门关键字相关API")
public class HotKeywordBuyerController {

    @Autowired
    private HotKeywordManager hotKeywordManager;


    @Operation(summary = "查询热门关键字列表")
    @Parameters({
            @Parameter(name = "num",description = "查询的数量", required = true,  in = ParameterIn.QUERY),
    })
    @GetMapping
    public List<HotKeyword> list(Integer num) {

        return this.hotKeywordManager.listByNum(num);
    }


}