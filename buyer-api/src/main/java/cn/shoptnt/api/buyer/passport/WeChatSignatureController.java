/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;

import cn.shoptnt.service.passport.signaturer.WechatSignaturer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.Map;

/**
 * 微信签名控制器
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2019-02-20 下午4:33
 */
@Tag(name = "微信签名工具")
@RestController
@RequestMapping("/buyer/wechat")
@Validated
public class WeChatSignatureController {

    @Autowired
    private WechatSignaturer wechatSignaturer;

    @Operation(summary = "小程序签名")
    @ResponseBody
    @Parameters({
            @Parameter(name = "type", description = "WAP/REACT/NATIVE/MINI 分别为 WAP/原生app/h5app/小程序"),
            @Parameter(name = "url", description = "url"),
    })
    @GetMapping
    public Map signature(String type, String url) {
        return wechatSignaturer.signature(type, url);
    }

}
