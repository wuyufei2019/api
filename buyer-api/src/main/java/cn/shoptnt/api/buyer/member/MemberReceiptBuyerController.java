/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.dos.MemberReceipt;
import cn.shoptnt.service.member.MemberReceiptManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 会员发票信息缓存相关API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@RestController
@RequestMapping("/buyer/members/receipt")
@Tag(name = "会员发票信息缓存相关API")
public class MemberReceiptBuyerController {

    @Autowired
    private MemberReceiptManager memberReceiptManager;


    @Operation(summary = "查询当前会员发票缓存信息集合")
    @Parameters({
            @Parameter(name = "type", description = "发票类型", required = true,
                    in = ParameterIn.PATH,  example = "ELECTRO：电子普通发票，VATORDINARY：增值税普通发票")
    })
    @GetMapping("/{type}")
    public List<MemberReceipt> list(@PathVariable String type) {
        return this.memberReceiptManager.list(type);
    }


    @Operation(summary = "添加会员发票信息")
    @PostMapping
    public MemberReceipt add(@Valid MemberReceipt memberReceipt) {
        return this.memberReceiptManager.add(memberReceipt);
    }

    @Operation(summary = "修改会员发票信息")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    @PutMapping(value = "/{id}")
    public MemberReceipt edit(@Valid MemberReceipt memberReceipt, @PathVariable Long id) {
        return this.memberReceiptManager.edit(memberReceipt, id);
    }

    @Operation(summary = "设置会员发票为默认")
    @Parameters({
            @Parameter(name = "id", description = "会员发票主键,发票抬头为个人时则设置此参数为0", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "type", description = "发票类型", required = true,
                    in = ParameterIn.PATH, example = "ELECTRO：电子普通发票，VATORDINARY：增值税普通发票")
    })
    @PutMapping(value = "/{id}/{type}")
    public void setDefault(@PathVariable Long id, @PathVariable String type) {
        this.memberReceiptManager.setDefaultReceipt(type, id);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除会员发票信息缓存")
    @Parameters({
            @Parameter(name = "id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.memberReceiptManager.delete(id);

        return "";
    }

}
