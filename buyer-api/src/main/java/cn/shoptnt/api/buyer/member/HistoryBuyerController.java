/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.HistoryDO;
import cn.shoptnt.model.member.dto.HistoryDelDTO;
import cn.shoptnt.service.member.HistoryManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Buyer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 会员足迹控制器
 *
 * @author zh
 * @version v7.1.4
 * @since vv7.1
 * 2019-06-18 15:18:56
 */
@RestController
@RequestMapping("/buyer/members/history")
@Tag(name = "会员足迹相关API")
public class HistoryBuyerController {

    @Autowired
    private HistoryManager historyManager;


    @Operation(summary = "查询会员足迹列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("list")
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.historyManager.list(pageNo, pageSize);
    }

    @Operation(summary = "查询会员足迹列表-im使用")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("/list-page")
    public WebPage listPage(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.historyManager.listPage(pageNo, pageSize);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "根据足迹id删除会员足迹")
    @Parameters({
            @Parameter(name = "id", description = "要删除的会员足迹主键", required = true,  in = ParameterIn.PATH)
    })
    public String delById(@PathVariable Long id) {
        HistoryDelDTO historyDelDTO = new HistoryDelDTO();
        historyDelDTO.setId(id);
        Buyer buyer = UserContext.getBuyer();
        historyDelDTO.setMemberId(buyer.getUid());
        historyManager.delete(historyDelDTO);

        return "";
    }


    @DeleteMapping(value = "/day/{day}")
    @Operation(summary = "根据日期清空会员足迹")
    @Parameters({
            @Parameter(name = "day", description = "当天的时间戳", required = true,   in = ParameterIn.PATH)
    })
    public String delByDate(@PathVariable String day) {
        HistoryDelDTO historyDelDTO = new HistoryDelDTO();
        historyDelDTO.setDate(day);
        Buyer buyer = UserContext.getBuyer();
        historyDelDTO.setMemberId(buyer.getUid());

        historyManager.delete(historyDelDTO);
        return "";
    }

    @DeleteMapping
    @Operation(summary = "清空所有会员足迹")
    public String delAll() {
        HistoryDelDTO historyDelDTO = new HistoryDelDTO();
        Buyer buyer = UserContext.getBuyer();
        historyDelDTO.setMemberId(buyer.getUid());
        this.historyManager.delete(historyDelDTO);
        return "";
    }


}
