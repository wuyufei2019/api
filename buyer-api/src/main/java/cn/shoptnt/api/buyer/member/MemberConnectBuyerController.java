/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.enums.ConnectTypeEnum;
import cn.shoptnt.model.member.vo.ConnectVO;
import cn.shoptnt.service.member.AbstractConnectLoginPlugin;
import cn.shoptnt.service.member.ConnectManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author zjp
 * @version v7.0
 * @Description 会员信任登录API
 * @ClassName MemberConnectController
 * @since v7.0 上午10:26 2018/6/14
 */
@Tag(name = "会员信任登录API")
@RestController
@RequestMapping("/buyer/account-binder")
public class MemberConnectBuyerController {

    @Autowired
    private ConnectManager connectManager;


    @GetMapping("/pc/{type}")
    @Operation(summary = "发起账号绑定")
    @Parameters({
            @Parameter(name = "type", description = "登录方式:QQ,微博,微信,支付宝", in = ParameterIn.PATH)
    })
    public String initiate(@PathVariable("type") @Parameter(hidden = true) String type) throws IOException {
        //信任登录类型枚举类
        ConnectTypeEnum connectTypeEnum = ConnectTypeEnum.valueOf(type);
        //信任登录插件基类
        AbstractConnectLoginPlugin connectionLogin = connectManager.getConnectionLogin(connectTypeEnum);
        return connectionLogin.getLoginUrl();
    }

    @Operation(summary = "会员解绑操作")
    @PostMapping("/unbind/{type}")
    @Parameter(name = "type", description = "登录方式:QQ,微博,微信,支付宝", in = ParameterIn.PATH)
    public void unbind(@PathVariable("type") String type) {
        if (ConnectTypeEnum.WECHAT.name().equals(type)) {
            //PC端解绑操作后，清除微信保存相关信息
            //解除绑定
            connectManager.unbind(ConnectTypeEnum.WECHAT.value());
            //解除绑定需要清空OPENID,否则微信服务消息在解除绑定之后会继续推送,并且分销提现会到账(在解绑之前的微信账号内)  add by liuyulei 2020-12-22
            connectManager.unbind(ConnectTypeEnum.WECHAT_OPENID.value());
            //解除小程序自动登录
            connectManager.unbind(ConnectTypeEnum.WECHAT_MINI.value());
        } else {
            connectManager.unbind(type);
        }
    }

    @Operation(summary = "微信退出解绑操作")
    @PostMapping("/unbind/out")
    public void wechatOut() {
        connectManager.wechatOut();
    }


    @Operation(summary = "登录绑定openid")
    @PostMapping("/login/{uuid}")
    @Parameter(name = "uuid", description = "客户端唯一标识", required = true,   in = ParameterIn.PATH)
    public Map openidBind(@PathVariable("uuid") @Parameter(hidden = true) String uuid) {
        return connectManager.openidBind(uuid);
    }

    @Operation(summary = "注册绑定openid")
    @PostMapping("/register/{uuid}")
    @Parameter(name = "uuid", description = "客户端唯一标识", required = true,   in = ParameterIn.PATH)
    public void registerBind(@PathVariable("uuid") @Parameter(hidden = true) String uuid) {
        connectManager.registerBind(uuid);
    }

    @Operation(summary = "获取绑定列表API")
    @GetMapping("/list")
    public List<ConnectVO> get() {
        return connectManager.get();
    }

}
