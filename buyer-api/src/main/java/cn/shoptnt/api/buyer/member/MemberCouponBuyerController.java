/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.dos.MemberCoupon;
import cn.shoptnt.model.member.dto.MemberCouponQueryParam;
import cn.shoptnt.model.member.vo.MemberCouponNumVO;
import cn.shoptnt.service.member.MemberCouponManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Buyer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 会员优惠券
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/members/coupon")
@Tag(name = "会员优惠券相关API")
@Validated
public class MemberCouponBuyerController {

    @Autowired
    private MemberCouponManager memberCouponManager;


    @Operation(summary = "查询我的优惠券列表")
    @Parameters({
            @Parameter(name = "status", description = "优惠券状态 0为全部，1为未使用且可用，2为已使用，3为已过期, 4为不可用优惠券（已使用和已过期）",  in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页数",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "条数",  in = ParameterIn.QUERY),
    })
    @GetMapping
    public WebPage<MemberCoupon> list(@Parameter(hidden = true) Integer status, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        //会员优惠券查询参数
        MemberCouponQueryParam param = new MemberCouponQueryParam();
        param.setStatus(status);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        return this.memberCouponManager.list(param);
    }


    @Operation(summary = "用户领取优惠券")
    @Parameters({
            @Parameter(name = "coupon_id", description = "优惠券id", required = true,   in = ParameterIn.PATH)
    })
    @PostMapping(value = "/{coupon_id}/receive")
    public String receiveBonus(@Parameter(hidden = true) @PathVariable("coupon_id") Long couponId) {

        Buyer buyer = UserContext.getBuyer();
        //领取优惠券
        this.memberCouponManager.receiveBonus(buyer.getUid(),buyer.getUsername(), couponId);
        return "";
    }


    @Operation(summary = "结算页—读取可用的优惠券列表")
    @Parameters({
            @Parameter(name = "seller_ids", description = "商家ID集合", required = true,  in = ParameterIn.PATH),
    })
    @GetMapping("/{seller_ids}")
    public List<MemberCoupon> listByCheckout(@Parameter(hidden = true) @PathVariable("seller_ids") @NotNull(message = "商家ID不能为空") Long[] sellerIds) {
        return this.memberCouponManager.listByCheckout(sellerIds, UserContext.getBuyer().getUid());
    }


    @Operation(summary = "优惠券—未使用,已使用,已过期状态总数量")
    @GetMapping("/num")
    public MemberCouponNumVO getStatusNum() {
        return this.memberCouponManager.statusNum();
    }

}
