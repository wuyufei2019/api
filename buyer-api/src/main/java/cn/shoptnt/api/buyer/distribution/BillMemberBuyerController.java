/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.distribution;

import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.vo.BillMemberVO;
import cn.shoptnt.model.distribution.vo.DistributionOrderVO;
import cn.shoptnt.model.distribution.vo.DistributionSellbackOrderVO;
import cn.shoptnt.service.distribution.BillMemberManager;
import cn.shoptnt.service.distribution.DistributionOrderManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Buyer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 分销总结算
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/23 上午8:32
 */
@RestController
@Tag(name = "分销总结算")
@RequestMapping("/buyer/distribution/bill")
public class BillMemberBuyerController {


    @Autowired
    private BillMemberManager billMemberManager;

    @Autowired
    private DistributionOrderManager distributionOrderManager;


    @Operation(summary = "获取某会员当前月份结算单")
    @GetMapping("/member")
    @Parameters({
            @Parameter(name = "bill_id", description = "总结算单id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id 为空时获取当前登录会员的结算单", in = ParameterIn.QUERY)
    })
    public BillMemberVO billMemberVO(@Parameter(hidden = true) Long billId, @Parameter(hidden = true) Long memberId) {
        Buyer buyer = UserContext.getBuyer();
        if (buyer == null) {
            throw new DistributionException(DistributionErrorCode.E1001.code(), DistributionErrorCode.E1001.des());
        }
        if (memberId == null || memberId == 0) {
            memberId = buyer.getUid();
        }
        return billMemberManager.getCurrentBillMember(memberId, billId);
    }

    @Operation(summary = "根据结算单获取订单信息")
    @GetMapping(value = "/order-list")
    @Parameters({
            @Parameter(name = "page_size", description = "分页大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "bill_id", description = "总结算单id", required = true, in = ParameterIn.QUERY ),
            @Parameter(name = "member_id", description = "会员id 为0代表查看当前会员业绩", in = ParameterIn.QUERY)
    })
    public WebPage<DistributionOrderVO> orderList(@Parameter(hidden = true) Long memberId, @Parameter(hidden = true) Long billId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        Buyer buyer = UserContext.getBuyer();
        if (buyer == null) {
            throw new DistributionException(DistributionErrorCode.E1001.code(), DistributionErrorCode.E1001.des());
        }
        if (memberId == null || memberId == 0) {
            memberId = buyer.getUid();
        }
        WebPage<DistributionOrderVO> voPage = distributionOrderManager.pageDistributionOrder(pageSize, pageNo, memberId, billId);
        return voPage;
    }

    @Operation(summary = "根据结算单获取退款订单信息")
    @GetMapping(value = "/sellback-order-list")
    @Parameters({
            @Parameter(name = "page_size", description = "分页大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "bill_id", description = "结算单id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id 为0代表查看当前会员业绩", in = ParameterIn.QUERY)
    })
    public WebPage<DistributionSellbackOrderVO> sellbackOrderList(@Parameter(hidden = true) Long memberId, @Parameter(hidden = true) Long billId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        Buyer buyer = UserContext.getBuyer();
        if (buyer == null) {
            throw new DistributionException(DistributionErrorCode.E1001.code(), DistributionErrorCode.E1001.des());
        }
        if (memberId == null || memberId == 0) {
            return distributionOrderManager.pageSellBackOrder(pageSize, pageNo, buyer.getUid(), billId);
        }
        return distributionOrderManager.pageSellBackOrder(pageSize, pageNo, memberId, billId);
    }


    @Operation(summary = "历史业绩")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "分页大小", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id 为0或空代表查看当前会员业绩", in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/history")
    public WebPage<BillMemberVO> historyBill(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long memberId) {
        Buyer buyer = UserContext.getBuyer();
        if (buyer == null) {
            throw new DistributionException(DistributionErrorCode.E1001.code(), DistributionErrorCode.E1001.des());
        }
        if (memberId == null || memberId == 0) {
            return billMemberManager.billMemberPage(pageNo, pageSize, buyer.getUid());
        }
        return billMemberManager.billMemberPage(pageNo, pageSize, memberId);
    }

}
