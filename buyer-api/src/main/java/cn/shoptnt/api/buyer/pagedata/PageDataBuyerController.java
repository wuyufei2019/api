/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.pagedata;

import cn.shoptnt.model.pagedata.PageData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import cn.shoptnt.service.pagedata.PageDataManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 楼层控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@RestController
@RequestMapping("/buyer/pages")
@Tag(name = "楼层相关API")
@Validated
public class PageDataBuyerController {

    @Autowired
    private PageDataManager pageManager;

    @GetMapping(value = "/{client_type}/index")
    @Operation(summary = "查询首页（pc/移动端）")
    @Parameters({
            @Parameter(name = "client_type",description = "要查询的客户端类型 MOBILE/PC", required = true,    in = ParameterIn.PATH),
    })
    public PageData getIndex(@PathVariable("client_type") String clientType) {
        PageData page = this.pageManager.getIndex(0L, clientType);

        return page;
    }

    @GetMapping(value = "/{client_type}/{seller_id}/index")
    @Operation(summary = "查询店铺首页（pc/移动端）")
    @Parameters({
            @Parameter(name = "client_type",description = "要查询的客户端类型 MOBILE/PC", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "seller_id",description = "商家id", required = true,   in = ParameterIn.PATH),
    })
    public PageData getIndex(@PathVariable("client_type")String clientType, @PathVariable ("seller_id")Long sellerId) {
        PageData page = this.pageManager.getIndex(sellerId, clientType);

        return page;
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询某个页面")
    @Parameters({
            @Parameter(name = "id",description = "微页面id", required = true,    in = ParameterIn.PATH),
    })
    public PageData get(@PathVariable("id") Long id) {
        PageData page = this.pageManager.getModel(id);

        return page;
    }


}