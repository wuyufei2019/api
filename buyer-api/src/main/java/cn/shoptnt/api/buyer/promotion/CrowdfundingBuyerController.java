package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import cn.shoptnt.model.promotion.crowdfunding.query.CrowdfundingQueryParams;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import cn.shoptnt.service.promotion.crowdfunding.CrowdfundingManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 众筹活动 API
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@RestController
@RequestMapping("/buyer/promotions/crowdfunding")
@Tag(name = "众筹活动API")
@Validated
public class CrowdfundingBuyerController {

    @Autowired
    private CrowdfundingManager crowdfundingManager;

    @Operation(summary = "分页列表")
    @GetMapping
    public WebPage<CrowdfundingVO> list(@ParameterObject CrowdfundingQueryParams queryParams) {
        queryParams.setBuyerFlag(true);
        return crowdfundingManager.list(queryParams);
    }

    @Operation(summary = "查询")
    @GetMapping("/{id}")
    public CrowdfundingVO getDetail(@PathVariable Long id) {
        return crowdfundingManager.getDetail(id);
    }

}

