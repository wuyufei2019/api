/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.shop;

import cn.shoptnt.model.shop.dos.ShopCatDO;
import cn.shoptnt.service.shop.ShopCatManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * 店铺分组控制器
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-24 11:18:37
 */
@RestController
@RequestMapping("/buyer/shops/cats")
@Tag(name = "店铺分组相关API")
public class ShopCatBuyerController {

	@Autowired
	private ShopCatManager shopCatManager;


	@Operation(summary	= "查询店铺分组列表")
	@GetMapping("/{shop_id}")
	@Parameters({
			@Parameter(name = "shop_id", description = "店铺id", required = true,  in = ParameterIn.PATH),
			@Parameter(name = "display", description = "是否展示,根据分类的显示状态查询：ALL(全部),SHOW(显示),HIDE(隐藏)",   in = ParameterIn.QUERY)
	})
	public List list(@PathVariable("shop_id") Long shopId,@Parameter(hidden = true)  String display)	{
		return	this.shopCatManager.list(shopId,display);
	}

}
