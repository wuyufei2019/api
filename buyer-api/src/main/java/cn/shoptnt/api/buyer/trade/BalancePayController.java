/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.trade.order.vo.BalancePayVO;
import cn.shoptnt.service.trade.order.BalanceManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Buyer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;

/**
 * @description: 预存款支付相关
 * @author: liuyulei
 * @create: 2020-01-01 11:37
 * @version:1.0
 * @since:7.1.4
 **/
@Tag(name = "预存款支付API")
@RestController
@RequestMapping("/buyer/balance/pay")
@Validated
public class BalancePayController {

    @Autowired
    private BalanceManager balanceManager;


    @Operation(summary = "使用预存款支付")
    @Parameters({
            @Parameter(name = "sn", description = "要支付的交易sn", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "trade_type", description = "交易类型", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "password", description = "支付密码", required = true,   in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/{trade_type}/{sn}")
    public BalancePayVO payTrade(@PathVariable(name = "sn") String sn, @PathVariable(name = "trade_type") String tradeType,
                                 @NotEmpty(message = "密码不能为空") String password) {
        Buyer buyer = UserContext.getBuyer();
        return balanceManager.balancePay(sn,buyer.getUid(),tradeType.toUpperCase(),password);

    }
}
