/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.CharacterConstant;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.dto.AskQueryParam;
import cn.shoptnt.model.member.vo.MemberAskVO;
import cn.shoptnt.model.util.sensitiveutil.SensitiveFilter;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.security.model.Buyer;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import cn.shoptnt.service.member.MemberAskManager;

import java.util.List;

/**
 * 会员商品咨询API
 *
 * @author duanmingyu
 * @version v2.0
 * @since v7.1.5
 * 2019-09-17
 */
@RestController
@RequestMapping("/buyer/members/asks")
@Tag(name = "会员商品咨询API")
@Validated
public class MemberAskBuyerController {

    @Autowired
    private MemberAskManager memberAskManager;


    @Operation(summary = "查询我的咨询列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        //会员咨询搜索参数
        AskQueryParam param = new AskQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        Buyer member = UserContext.getBuyer();
        param.setMemberId(member.getUid());

        return this.memberAskManager.list(param);
    }


    @Operation(summary = "添加咨询")
    @Parameters({
            @Parameter(name = "ask_content", description = "咨询内容", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "goods_id", description = "咨询商品id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "anonymous", description = "是否匿名 YES:是，NO:否", required = true, in = ParameterIn.QUERY)
    })
    @PostMapping
    public MemberAsk add(@NotEmpty(message = "请输入咨询内容") @Parameter(hidden = true) String askContent,
                         @NotNull(message = "咨询商品不能为空") @Parameter(hidden = true) Long goodsId,
                         @NotNull(message = "请选择是否匿名提问") @Parameter(hidden = true) String anonymous) {

        //咨询内容敏感词过滤
        askContent = SensitiveFilter.filter(askContent, CharacterConstant.WILDCARD_STAR);

        MemberAsk memberAsk = this.memberAskManager.add(askContent, goodsId, anonymous);

        return memberAsk;
    }

    @Operation(summary = "查看会员商品咨询详情")
    @Parameters({
            @Parameter(name = "ask_id", description = "会员商品咨询id", required = true, in = ParameterIn.PATH)
    })
    @GetMapping("/detail/{ask_id}")
    public MemberAskVO detail(@PathVariable("ask_id") Long askId) {

        MemberAskVO memberAskVO = this.memberAskManager.getModelVO(askId);

        return memberAskVO;
    }

    @Operation(summary = "查询某商品的咨询")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "goods_id", description = "商品ID", required = true)
    })
    @GetMapping("/goods/{goods_id}")
    public WebPage listGoodsAsks(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("goods_id") Long goodsId) {

        return this.memberAskManager.listGoodsAsks(pageNo, pageSize, goodsId);
    }

    @Operation(summary = "查询与会员商品咨询相关的其它咨询")
    @Parameters({
            @Parameter(name = "ask_id", description = "会员商品咨询ID", required = true, in = ParameterIn.PATH),
            @Parameter(name = "goods_id", description = "商品ID", required = true, in = ParameterIn.PATH)
    })
    @GetMapping("/relation/{ask_id}/{goods_id}")
    public List<MemberAsk> listRelationAsks(@PathVariable("ask_id") Long askId, @PathVariable("goods_id") Long goodsId) {

        return this.memberAskManager.listRelationAsks(askId, goodsId);
    }

    @Operation(summary = "删除咨询")
    @Parameters({
            @Parameter(name = "ask_id", description = "会员商品咨询id", in = ParameterIn.PATH),
    })
    @DeleteMapping("/{ask_id}")
    public String delete(@PathVariable("ask_id") Long askId) {

        Buyer buyer = UserContext.getBuyer();

        MemberAsk memberAsk = this.memberAskManager.getModel(askId);
        //判断是否有此咨询信息和该会员登录是否合法
        if (memberAsk == null || memberAsk.getMemberId().intValue() != buyer.getUid().intValue()) {
            throw new NoPermissionException("您没有权限删除此条咨询信息!");
        }

        this.memberAskManager.delete(askId);

        return "";
    }

}
