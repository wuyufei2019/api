/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyCatDO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyGoodsVO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyQueryParam;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyActiveManager;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyCatManager;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyGoodsManager;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.util.List;


/**
 * 团购相关API
 * @author Snow create in 2018/5/28
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/promotions/group-buy")
@Tag(name = "团购相关API")
public class GroupbuyBuyerController {

    @Autowired
    private GroupbuyGoodsManager groupbuyGoodsManager;

    @Autowired
    private GroupbuyCatManager groupbuyCatManager;

    @Autowired
    private GroupbuyActiveManager groupbuyActiveManager;


    @Operation(summary = "查询团购分类的所有标签")
    @GetMapping("/cats")
    public List<GroupbuyCatDO> getCat(){
        List<GroupbuyCatDO> groupbuyCatDOList = this.groupbuyCatManager.getList(0L);
        return groupbuyCatDOList;
    }


    @Operation(summary = "查询团购活动的信息")
    @Parameters({
            @Parameter(name = "active_id", description ="团购活动主键",  in = ParameterIn.QUERY)
    })
    @GetMapping("/active")
    public GroupbuyActiveDO getActive(@Parameter(hidden = true) Long activeId){
        GroupbuyActiveDO activeDO = this.groupbuyActiveManager.getModel(activeId);
        return activeDO;
    }


    @Operation(summary = "查询团购商品")
    @Parameters({
            @Parameter(name	= "cat_id", description ="团购分类id", in = ParameterIn.QUERY),
            @Parameter(name	= "page_no", description ="页码", 	in = ParameterIn.QUERY),
            @Parameter(name	= "page_size", description ="条数", 	in = ParameterIn.QUERY)
    })
    @GetMapping("/goods")
    public WebPage<GroupbuyGoodsVO> list(@Parameter(hidden = true) Long catId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        GroupbuyQueryParam param = new GroupbuyQueryParam();
        param.setCatId(catId);
        param.setPage(pageNo);
        param.setPageSize(pageSize);
        param.setMemberId(-1L);

        param.setStartTime(DateUtil.getDateline());
        param.setEndTime(DateUtil.getDateline());

        WebPage webPage = this.groupbuyGoodsManager.listPageByBuyer(param);
        return webPage;
    }


}
