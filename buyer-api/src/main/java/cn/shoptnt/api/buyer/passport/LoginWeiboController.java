/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;


import cn.shoptnt.model.member.dto.LoginAppDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import cn.shoptnt.service.passport.LoginWeiboManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

/**
 * 微博统一登陆
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2020-10-30
 */
@Tag(name = "微博统一登陆")
@RestController
@RequestMapping("/buyer/connect/weibo")
@Validated
public class LoginWeiboController {


    @Autowired
    private LoginWeiboManager loginWeiboManager;

    @Operation(summary = "获取授权页地址")
    @Parameter(name = "redirectUri", description = "授权成功跳转地址(需要urlEncode整体加密)", required = true,   in = ParameterIn.QUERY)
    @GetMapping("/wap/getLoginUrl")
    public String getLoginUrl(@RequestParam("redirectUri") String redirectUri) {
        return loginWeiboManager.getLoginUrl(redirectUri);
    }

    @Operation(summary = "网页登陆")
    @Parameters({
            @Parameter(name = "code", description = "授权登陆返回的code", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "此次登陆的随机数", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "redirect_uri", description = "授权成功跳转地址(需要urlEncode整体加密)", required = true,   in = ParameterIn.QUERY)
    })
    @PostMapping("/wap/login")
    public Map h5Login(@NotEmpty(message = "code不能为空") String code, @NotEmpty(message = "uuid不能为空") String uuid, @NotEmpty(message = "redirect_uri不能为空") String redirect_uri) {
        return loginWeiboManager.wapLogin(code, uuid, redirect_uri);
    }

    @Operation(summary = "app登陆")
    @PostMapping("/app/login")
    public Map appLogin(LoginAppDTO loginAppDTO) {
        return loginWeiboManager.appLogin(loginAppDTO);
    }

}
