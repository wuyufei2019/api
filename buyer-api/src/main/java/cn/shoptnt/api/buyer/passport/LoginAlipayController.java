/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;


import cn.shoptnt.service.passport.LoginAlipayManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 支付宝统一登陆
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2020-10-30
 */
@Tag(name = "支付宝统一登陆")
@RestController
@RequestMapping("/buyer/connect/alipay")
@Validated
public class LoginAlipayController {


    @Autowired
    private LoginAlipayManager loginAlipayManager;

    @Operation(summary = "获取授权页地址")
    @Parameter(name = "redirectUri", description = "授权成功跳转地址(需要urlEncode整体加密)", required = true,   in = ParameterIn.QUERY)
    @GetMapping("/wap/getLoginUrl")
    public String getLoginUrl(@RequestParam("redirectUri") String redirectUri) {
        return loginAlipayManager.getLoginUrl(redirectUri);
    }

    @Operation(summary = "网页登陆")
    @GetMapping("/wap/login")
    public Map h5Login(String code, String uuid) {
        return loginAlipayManager.wapLogin(code, uuid);
    }

}
