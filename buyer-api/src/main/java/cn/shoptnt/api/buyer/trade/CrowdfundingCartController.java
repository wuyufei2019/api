/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import cn.shoptnt.service.trade.crowdfunding.CrowdfundingCartManager;
import cn.shoptnt.service.trade.crowdfunding.impl.CrowdfundingTradeManagerImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;

/**
 * 众筹购物API
 *
 * @author 张崧
 * @since 2024-06-23
 */

@Tag(name = "众筹购物API")
@RestController
@RequestMapping("/buyer/crowdfunding")
public class CrowdfundingCartController {

    @Autowired
    private CrowdfundingCartManager crowdfundingCartManager;

    @Autowired
    private CrowdfundingTradeManagerImpl crowdfundingTradeManager;

    @Operation(summary = "获取购物车页面购物车详情")
    @GetMapping("/cart")
    public CartView cart() {
        return crowdfundingCartManager.getCart();
    }

    @Operation(summary = "立即众筹")
    @GetMapping("/buy")
    public void buy(@RequestParam("crowdfunding_id") Long crowdfundingId, @RequestParam("num") @Min(value = 1, message = "众筹数量最小为1") Integer num) {
        crowdfundingCartManager.buy(crowdfundingId, num);
    }

    @Operation(summary = "创建交易")
    @PostMapping(value = "/trade")
    @Parameters({
            @Parameter(name = "client", description = "客户端类型", in = ParameterIn.QUERY),
    })
    public TradeVO create(@Parameter(hidden = true) String client) {
        return this.crowdfundingTradeManager.createTrade(client);
    }

}
