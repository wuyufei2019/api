package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.trade.promotion.PromotionScriptInitDispatcher;
import cn.shoptnt.model.base.message.PromotionScriptInitMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 促销活动脚本初始化消息接收类
 *
 * @author dmy
 * 2022-09-23
 */
@Component
public class PromotionScriptInitReceiver {

    @Autowired
    private PromotionScriptInitDispatcher dispatcher;

    /**
     * 促销活动脚本初始化
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PROMOTION_SCRIPT_INIT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PROMOTION_SCRIPT_INIT, type = ExchangeTypes.FANOUT)
    ))
    public void scriptInit(PromotionScriptInitMessage message) {
        dispatcher.dispatch(message);
    }
}
