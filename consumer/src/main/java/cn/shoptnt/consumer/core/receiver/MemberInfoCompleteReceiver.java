/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.member.MemberInfoCompleteDispatcher;
import cn.shoptnt.model.base.message.MemberInfoCompleteMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员完善个人信息
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:30:58
 */
@Component
public class MemberInfoCompleteReceiver {

    @Autowired
    private MemberInfoCompleteDispatcher dispatcher;

    /**
     * 会员完善个人信息
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_INFO_COMPLETE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_INFO_COMPLETE, type = ExchangeTypes.FANOUT)
    ))
    public void memberInfoComplete(MemberInfoCompleteMessage message) {
        dispatcher.dispatch(message);
    }
}
