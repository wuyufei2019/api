package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.model.base.message.MessageReceiveMessage;
import cn.shoptnt.service.datasync.receive.common.MessageReceiveManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 第三方消息处理
 *
 * @author 张崧
 * @since 2024-04-01
 */
@Component
public class MessageReceiveReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageReceiveManager messageReceiveManager;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MESSAGE_RECEIVE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MESSAGE_RECEIVE, type = ExchangeTypes.FANOUT)
    ))
    public void receive(MessageReceiveMessage message) {
        try {
            messageReceiveManager.handle(message.getId());
        } catch (Exception e) {
            logger.error("第三方消息处理处理异常", e);
        }
    }
}
