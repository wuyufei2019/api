/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.goods.GoodsPriorityChangeDispatcher;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liuyulei
 * @version 1.0
 * @Description: 商品优先级变化消息接收者
 * @date 2019/6/13 17:15
 * @since v7.0
 */
@Component
public class GoodsPriorityChangeReceiver {

    @Autowired
    private GoodsPriorityChangeDispatcher dispatcher;

    /**
     * 商品变化
     *
     * @param goodsChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_PRIORITY_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_PRIORITY_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {
        dispatcher.dispatch(goodsChangeMsg);
    }
}
