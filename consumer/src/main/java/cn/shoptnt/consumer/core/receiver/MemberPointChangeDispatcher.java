package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.member.MemberPointChangeMessageDispatcher;
import cn.shoptnt.message.dispatcher.member.MemberSignMessageDispatcher;
import cn.shoptnt.model.base.message.MemberPointChangeMessage;
import cn.shoptnt.model.base.message.SignMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignDispatcher
 * @description <TODO description class>
 * @program: api
 * 2024/3/19 14:35
 */
@Component
public class MemberPointChangeDispatcher implements Serializable {
    private static final long serialVersionUID = -6085647180206068130L;


    @Autowired
    private MemberPointChangeMessageDispatcher memberPointChangeMessageDispatcher;


    /**
     * 会员积分消息变化

     * @param message 签到信息
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_POINT_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_POINT_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void sendMessage(MemberPointChangeMessage message) {
        memberPointChangeMessageDispatcher.dispatch(message);
    }
}
