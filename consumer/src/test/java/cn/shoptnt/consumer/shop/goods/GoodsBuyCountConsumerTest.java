/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.goods;

import cn.shoptnt.message.consumer.goods.GoodsBuyCountConsumer;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.vo.OrderSkuVO;
import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.test.BaseTest;
import cn.shoptnt.framework.util.JsonUtil;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fk
 * @version v1.0
 * @Description: 商品购买数量消费者
 * @date 2018/6/26 9:59
 * @since v7.0.0
 */
public class GoodsBuyCountConsumerTest extends BaseTest {

    @Autowired
    private GoodsBuyCountConsumer goodsBuyCountConsumer;
    @Autowired
    private DaoSupport daoSupport;

    @Test
    public void testEditBuyCount() {

        //添加一个商品
        GoodsDO goods = new GoodsDO();
        goods.setBuyCount(0);
        this.daoSupport.insert(goods);
        Long goodsId = this.daoSupport.getLastId("");

        OrderStatusChangeMsg orderMessage = new OrderStatusChangeMsg();
        orderMessage.setNewStatus(OrderStatusEnum.ROG);
        OrderDO order = new OrderDO();
        List<OrderSkuVO> skuList = new ArrayList<>();
        OrderSkuVO sku = new OrderSkuVO();
        sku.setGoodsId(goodsId);
        sku.setNum(1);
        skuList.add(sku);
        order.setItemsJson(JsonUtil.objectToJson(skuList));
        orderMessage.setOrderDO(order);

        goodsBuyCountConsumer.orderChange(orderMessage);

        //查询这个商品的购买数量，是1
        Map map = this.daoSupport.queryForMap("select buy_count from es_goods where goods_id = ?", goodsId);

        Assert.assertTrue(map.get("buy_count").toString().equals("1"));

    }


}
