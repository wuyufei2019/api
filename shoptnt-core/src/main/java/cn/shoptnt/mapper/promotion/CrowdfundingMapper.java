package cn.shoptnt.mapper.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.database.mybatisplus.base.BaseDO;
import cn.shoptnt.framework.database.mybatisplus.base.BaseMapperX;
import cn.shoptnt.framework.database.mybatisplus.wrapper.MPJLambdaWrapperX;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import cn.shoptnt.model.promotion.crowdfunding.query.CrowdfundingQueryParams;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 众筹活动 Mapper
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Mapper
public interface CrowdfundingMapper extends BaseMapperX<Crowdfunding> {

    default WebPage<CrowdfundingVO> selectPage(CrowdfundingQueryParams params) {
        CrowdfundingStatus status = params.getStatus();
        long dateline = DateUtil.getDateline();

        return selectJoinPage(params, CrowdfundingVO.class, new MPJLambdaWrapperX<Crowdfunding>()
                // 联表相关
                .leftJoin(GoodsSkuDO.class, GoodsSkuDO::getSkuId, Crowdfunding::getSkuId)
                .selectAll(Crowdfunding.class)
                .selectAssociation(GoodsSkuDO.class, CrowdfundingVO::getGoodsSku)
                // 查询条件
                .likeIfPresent(Crowdfunding::getName, params.getName())
                .betweenIfPresent(BaseDO::getCreateTime, params.getStartTime(), params.getEndTime())
                .orderByDesc(Crowdfunding::getId)
                // 根据状态查询
                .eq(status == CrowdfundingStatus.Stop, Crowdfunding::getStopFlag, true)
                .and(status == CrowdfundingStatus.Success, w -> w
                        .eq(Crowdfunding::getSettlementFlag, true)
                        .le(Crowdfunding::getRemainPrice, 0)
                )
                .and(status == CrowdfundingStatus.Fail, w -> w
                        .eq(Crowdfunding::getSettlementFlag, true)
                        .gt(Crowdfunding::getRemainPrice, 0)
                )
                .gt(status == CrowdfundingStatus.NotStart, Crowdfunding::getStartTime, dateline)
                .and(status == CrowdfundingStatus.WaitSettlement, w -> w
                        .eq(Crowdfunding::getStopFlag, false)
                        .eq(Crowdfunding::getSettlementFlag, false)
                        .lt(Crowdfunding::getEndTime, dateline)
                )
                .and(status == CrowdfundingStatus.Underway, w -> w
                        .eq(Crowdfunding::getStopFlag, false)
                        .le(Crowdfunding::getStartTime, dateline)
                        .ge(Crowdfunding::getEndTime, dateline)
                )
                // 会员端显示进行中的活动或者已结束且需要展示的活动
                .and(Boolean.TRUE.equals(params.getBuyerFlag()), w -> w
                        // 进行中
                        .and(w1 -> w1
                                .eq(Crowdfunding::getStopFlag, false)
                                .le(Crowdfunding::getStartTime, dateline)
                                .ge(Crowdfunding::getEndTime, dateline)
                        )
                        // 已结束并且需要显示
                        .or(w2 -> w2
                                .eq(Crowdfunding::getStopFlag, false)
                                .lt(Crowdfunding::getEndTime, dateline)
                                .eq(Crowdfunding::getEndShowFlag, true)
                        )
                )
        );
    }

}

