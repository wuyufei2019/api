package cn.shoptnt.mapper.datasync;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.database.mybatisplus.base.BaseMapperX;
import cn.shoptnt.model.datasync.dos.MessageReceiveDO;
import cn.shoptnt.model.datasync.dto.MessageReceiveQueryParams;
import cn.shoptnt.model.datasync.enums.MessageReceiveStatusEnum;

/**
 * 消息接收的Mapper
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
public interface MessageReceiveMapper extends BaseMapperX<MessageReceiveDO> {

    default WebPage<MessageReceiveDO> selectPage(MessageReceiveQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(MessageReceiveDO::getType, params.getType())
                .eqIfPresent(MessageReceiveDO::getMsgId, params.getMsgId())
                .likeIfPresent(MessageReceiveDO::getContent, params.getContent())
                .betweenIfPresent(MessageReceiveDO::getProduceTime, params.getProduceTime())
                .betweenIfPresent(MessageReceiveDO::getReceiveTime, params.getReceiveTime())
                .eqIfPresent(MessageReceiveDO::getStatus, params.getStatus())
                .likeIfPresent(MessageReceiveDO::getRemark, params.getRemark())
                .orderByDesc(MessageReceiveDO::getId)
                .page(params);
    }

    default void updateStatus(Long id, MessageReceiveStatusEnum status, String remark) {
        lambdaUpdate()
                .set(MessageReceiveDO::getStatus, status)
                .set(MessageReceiveDO::getRemark, remark)
                .eq(MessageReceiveDO::getId, id)
                .update();
    }
}

