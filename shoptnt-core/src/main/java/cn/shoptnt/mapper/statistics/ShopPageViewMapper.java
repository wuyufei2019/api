/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.statistics;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.statistics.dos.ShopPageView;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;

/**
 * 店铺访问量统计mapper
 * @author 张崧
 * @version v1.0
 * @since v7.2.2
 * 2020-08-03
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ShopPageViewMapper extends BaseMapper<ShopPageView> {

    default Integer selectVisitCount(long sellerId, int year, int month, int day){
        ShopPageView one = new QueryChainWrapper<>(this)
                .select("sum(vs_num) as num")
                .eq("seller_id", sellerId)
                .eq("vs_year", year)
                .eq("vs_month", month)
                .eq("vs_day", day)
                .one();

        return one == null || one.getNum() == null ? 0 : one.getNum();
    }
}
