package cn.shoptnt.mapper.member;

import cn.shoptnt.model.member.dos.MemberLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelMapper
 * @description 会员等级mapper
 * @program: api
 * 2024/5/20 16:29
 */
public interface MemberLevelMapper extends BaseMapper<MemberLevel> {
}
