package cn.shoptnt.mapper.member;

import cn.shoptnt.model.member.dos.MemberLevelBenefit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelMapper
 * @description 会员礼物mapper
 * @program: api
 * 2024/5/20 16:29
 */
public interface MemberLevelBenefitMapper extends BaseMapper<MemberLevelBenefit> {
}
