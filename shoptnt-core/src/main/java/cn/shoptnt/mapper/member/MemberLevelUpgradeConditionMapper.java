package cn.shoptnt.mapper.member;

import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditionsMapper
 * @description 会员等级升级条件Mapper
 * @program: api
 * 2024/5/20 21:18
 */
public interface MemberLevelUpgradeConditionMapper extends BaseMapper<MemberLevelUpgradeCondition> {
}
