/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.handler.annotation;

import cn.shoptnt.handler.enums.SecretType;

import java.lang.annotation.*;

/**
 * @author fk
 * @version v7.0
 * @since v5.2.3
 * 2021年11月19日10:29:05
 */
@Documented
@Target( {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SecretField {

    SecretType value() default SecretType.NO;

}
