/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.HotkeywordClient;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.pagedata.HotKeyword;
import cn.shoptnt.service.base.service.SettingManager;
import cn.shoptnt.service.pagedata.HotKeywordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 张崧
 * @version v1.0
 * @Description: 热点关键字
 * @date 2020-11-17
 * @since v7.2.2
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class HotKeywordClientDefaultImpl implements HotkeywordClient {

    @Autowired
    private HotKeywordManager hotKeywordManager;

    @Override
    public List<HotKeyword> listByNum(Integer num) {
        return hotKeywordManager.listByNum(num);
    }
}
