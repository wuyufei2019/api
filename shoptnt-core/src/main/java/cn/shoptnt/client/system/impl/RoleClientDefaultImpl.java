/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.RoleClient;
import cn.shoptnt.service.system.RoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description:
 * @date 2018/9/26 14:12
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class RoleClientDefaultImpl implements RoleClient {

    @Autowired
    private RoleManager roleManager;

    @Override
    public Map<String, List<String>> getRoleMap() {

        return roleManager.getRoleMap();
    }
}
