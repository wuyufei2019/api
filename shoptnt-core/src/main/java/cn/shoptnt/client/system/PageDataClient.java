/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;

import cn.shoptnt.model.pagedata.PageData;

import java.util.List;

/**
 * 微页面渲染客户端
 *
 * @author zh
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/8 下午4:10
 */
public interface PageDataClient {

    /**
     * 修改楼层商品
     *
     * @param page 数据
     * @param id   id
     * @return
     */
    PageData edit(PageData page, Long id);

    /**
     * 查询有某些商品的微页面
     * @param goodsIds
     * @return
     */
    List<PageData> selectPageByGoods(Long[] goodsIds);


}
