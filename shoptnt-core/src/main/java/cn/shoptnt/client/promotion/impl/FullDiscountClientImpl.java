package cn.shoptnt.client.promotion.impl;

import cn.shoptnt.client.promotion.FullDiscountClient;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountDO;
import cn.shoptnt.service.promotion.fulldiscount.FullDiscountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 满减满赠活动client实现
 *
 * @author 张崧
 * @version v7.0
 * @date 2021-12-16
 * @since v7.0
 */
@Service
public class FullDiscountClientImpl implements FullDiscountClient {


    @Autowired
    private FullDiscountManager fullDiscountManager;

    @Override
    public FullDiscountDO getModel(Long id) {
        return fullDiscountManager.getModel(id);
    }
}
