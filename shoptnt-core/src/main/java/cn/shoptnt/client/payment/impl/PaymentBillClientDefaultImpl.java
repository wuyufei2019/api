/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.payment.impl;

import cn.shoptnt.client.payment.PaymentBillClient;
import cn.shoptnt.model.payment.dos.PaymentBillDO;
import cn.shoptnt.model.payment.vo.PayBill;
import cn.shoptnt.service.payment.PaymentBillManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 支付账单client java的实现
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/16
 */
@Service
public class PaymentBillClientDefaultImpl implements PaymentBillClient {

    @Autowired
    private PaymentBillManager paymentBillManager;

    @Override
    public PayBill add(PaymentBillDO paymentBill) {
        return paymentBillManager.add(paymentBill);
    }
    @Override
    public void deletePaymentBill(Long billId) {
        paymentBillManager.delete(billId);
    }

    @Override
    public void closeTrade(PaymentBillDO paymentBillDO) {
        paymentBillManager.closeTrade(paymentBillDO);
    }

    @Override
    public void sendCloseTradeMessage(Map<String, Object> map) {
        paymentBillManager.sendCloseTradeMessage(map);
    }

}
