/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.model.aftersale.dos.RefundDO;
import cn.shoptnt.model.aftersale.vo.ApplyAfterSaleVO;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.service.aftersale.AfterSaleManager;
import cn.shoptnt.client.trade.AfterSaleClient;
import cn.shoptnt.service.aftersale.AfterSaleQueryManager;
import cn.shoptnt.service.aftersale.AfterSaleRefundManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 售后client 默认实现
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/2
 */
@Service
public class AfterSaleClientDefaultImpl implements AfterSaleClient {

    @Autowired
    protected AfterSaleManager afterSaleManager;

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Autowired
    private AfterSaleRefundManager afterSaleRefundManager;

    @Override
    public void cancelPintuanOrder(String orderSn, String cancelReason) {
        afterSaleManager.cancelPintuanOrder(orderSn, cancelReason);
    }

    @Override
    public ApplyAfterSaleVO detail(String serviceSn) {
        return afterSaleQueryManager.detail(serviceSn, Permission.CLIENT);
    }

    @Override
    public RefundDO getAfterSaleRefundModel(String serviceSn) {
        return afterSaleRefundManager.getModel(serviceSn);
    }

    @Override
    public void refundCompletion() {
        afterSaleManager.refundCompletion();
    }

    @Override
    public void editAfterSaleShopName(Long shopId, String shopName) {
        afterSaleManager.editAfterSaleShopName(shopId, shopName);
    }
}
