/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dto.DepositeParamDTO;
import cn.shoptnt.model.trade.deposite.DepositeLogDO;

/**
 *
 * @description: 预存款记录客户端
 * @author: liuyulei
 * @create: 2019/12/30 18:40
 * @version:1.0
 * @since:7.1.5
 **/
public interface DepositeLogClient {

    /**
     * 添加日志
     * @param logDO  日志实体
     */
    void add(DepositeLogDO logDO);


    /**
     * 获取日志列表  分页
     * @param paramDTO
     * @return
     */
    WebPage list(DepositeParamDTO paramDTO);
}
