package cn.shoptnt.client.member;

import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import cn.shoptnt.model.member.enums.MemberLevelConditionType;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditionClient
 * @description 会员等级升级条件
 * @program: api
 * 2024/5/24 16:26
 */
public interface MemberLevelUpgradeConditionClient {


    /**
     * 根据等级id获取升级条件
     *
     * @return
     */
    List<MemberLevelUpgradeCondition> getList();
}
