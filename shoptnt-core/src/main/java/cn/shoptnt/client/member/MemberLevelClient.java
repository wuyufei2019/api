package cn.shoptnt.client.member;

import cn.shoptnt.model.member.dos.MemberLevel;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelClient
 * @description 会员等级客户端
 * @program: api
 * 2024/5/24 16:21
 */
public interface MemberLevelClient {

    /**
     * 获取所有的会员等级
     *
     * @return
     */
    List<MemberLevel> getList();

    /**
     * 根据id查询会员等级
     *
     * @param id 会员等级id
     * @return
     */
    MemberLevel getById(Long id);

    /**
     * 修改会员等级信息
     *
     * @param memberId 会员等级
     * @param leveId   等级id
     */
    void updateMemberLevel(Long memberId, Long leveId);


}
