/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.ConnectClient;
import cn.shoptnt.model.member.dos.ConnectDO;
import cn.shoptnt.service.member.ConnectManager;
import cn.shoptnt.service.passport.signaturer.WechatSignaturer;
import cn.shoptnt.model.payment.enums.WechatTypeEnmu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 第三方连接client
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 下午3:51
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class ConnectClientDefaultImpl implements ConnectClient {

    @Autowired
    private ConnectManager connectManager;

    @Autowired
    private WechatSignaturer wechatSignaturer;

    @Override
    public ConnectDO getConnect(Long memberId, String unionType) {

        return connectManager.getConnect(memberId, unionType);
    }

    @Override
    public String getMemberOpenid(Long memberId) {

        return wechatSignaturer.getMemberOpenid(memberId);
    }

    @Override
    public String getCgiAccessToken(WechatTypeEnmu wechatTypeEnmu) {

        return wechatSignaturer.getCgiAccessToken(wechatTypeEnmu);
    }

    @Override
    public Map getConnectConfig(WechatTypeEnmu wechatTypeEnmu) {

        return wechatSignaturer.getConnectConfig(wechatTypeEnmu.name());
    }
}
