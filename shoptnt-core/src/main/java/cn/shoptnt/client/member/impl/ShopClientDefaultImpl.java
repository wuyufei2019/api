/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.ShopClient;
import cn.shoptnt.model.shop.dos.ShopDO;
import cn.shoptnt.model.shop.dto.ShopBankDTO;
import cn.shoptnt.model.shop.vo.ShopVO;
import cn.shoptnt.service.shop.ShopManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zjp
 * @version v7.0
 * @Description
 * @ClassName ShopClientDefaultImpl
 * @since v7.0 上午10:02 2018/7/13
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class ShopClientDefaultImpl implements ShopClient {

    @Autowired
    private ShopManager shopManager;

    @Override
    public ShopVO getShop(Long shopId) {
        return shopManager.getShop(shopId);
    }

    @Override
    public List<ShopBankDTO> listShopBankInfo() {
        return shopManager.listShopBankInfo();
    }

    @Override
    public void addCollectNum(Long shopId) {
        shopManager.addcollectNum(shopId);
    }

    @Override
    public void reduceCollectNum(Long shopId) {
        shopManager.reduceCollectNum(shopId);
    }

    @Override
    public void calculateShopScore() {
        shopManager.calculateShopScore();
    }

    @Override
    public void updateShopGoodsNum(Long sellerId, Long sellerGoodsCount) {
        shopManager.updateShopGoodsNum(sellerId, sellerGoodsCount);

    }

    @Override
    public Long queryShopCount() {

        return shopManager.queryShopCount();
    }

    @Override
    public List<ShopDO> queryShopByRange(Long i, Long pageSize) {
        return shopManager.queryShopByRange(i, pageSize);

    }


}
