package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberLevelClient;
import cn.shoptnt.model.member.dos.MemberLevel;
import cn.shoptnt.service.member.MemberLevelManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelClientImpl
 * @description 会员等级客户端实现类
 * @program: api
 * 2024/5/24 16:22
 */
@Service
public class MemberLevelClientImpl implements MemberLevelClient {

    @Autowired
    private MemberLevelManager memberLevelManager;

    @Override
    public List<MemberLevel> getList() {
        return memberLevelManager.list();
    }

    @Override
    public MemberLevel getById(Long id) {
        return memberLevelManager.getById(id);
    }

    @Override
    public void updateMemberLevel(Long memberId, Long leveId) {
        memberLevelManager.updateMemberLevel(memberId, leveId);
    }
}
