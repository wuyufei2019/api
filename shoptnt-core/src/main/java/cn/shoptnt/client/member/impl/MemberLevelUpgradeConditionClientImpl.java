package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberLevelUpgradeConditionClient;
import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import cn.shoptnt.model.member.enums.MemberLevelConditionType;
import cn.shoptnt.service.member.MemberLevelUpgradeConditionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditionClientImpl
 * @description 会员等级升级条件客户端
 * @program: api
 * 2024/5/24 16:30
 */

@Service
public class MemberLevelUpgradeConditionClientImpl implements MemberLevelUpgradeConditionClient {

    @Autowired
    private MemberLevelUpgradeConditionManager memberLevelUpgradeConditionManager;

    @Override
    public List<MemberLevelUpgradeCondition> getList() {
        return memberLevelUpgradeConditionManager.getList();
    }
}
