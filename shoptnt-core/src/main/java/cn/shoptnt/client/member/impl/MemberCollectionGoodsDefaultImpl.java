/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberCollectionGoodsClient;
import cn.shoptnt.service.member.MemberCollectionGoodsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 会员收藏商品默认实现
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 下午4:47
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class MemberCollectionGoodsDefaultImpl implements MemberCollectionGoodsClient {

    @Autowired
    private MemberCollectionGoodsManager memberCollectionGoodsManager;

    @Override
    public Long getGoodsCollectCount(Long goodsId) {
        return memberCollectionGoodsManager.getGoodsCollectCount(goodsId);
    }

    @Override
    public void updateGoodsName(Long goodsId, String goodsName) {
        memberCollectionGoodsManager.updateGoodsName(goodsId, goodsName);
    }
}
