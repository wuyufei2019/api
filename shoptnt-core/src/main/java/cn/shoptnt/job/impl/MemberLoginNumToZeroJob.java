/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.job.EveryMonthExecute;
import cn.shoptnt.client.member.MemberClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员登录数量归零
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-19 下午2:40
 */
@Component
public class MemberLoginNumToZeroJob implements EveryMonthExecute {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberClient memberClient;

    /**
     * 每月执行
     */
    @Override
    public void everyMonth() {
        try {
            memberClient.loginNumToZero();
        } catch (Exception e) {
            this.logger.error("会员登录次数归零异常：", e);
        }
    }
}
