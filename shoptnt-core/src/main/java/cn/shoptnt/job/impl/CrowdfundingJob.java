/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.job.EveryTenMinutesExecute;
import cn.shoptnt.service.trade.crowdfunding.CrowdfundingOrderManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 众筹活动定时任务
 *
 * @author 张崧
 * @since 2024-06-22
 */
@Component
@Slf4j
public class CrowdfundingJob implements EveryTenMinutesExecute {

    @Autowired
    private CrowdfundingOrderManager crowdfundingOrderManager;

    @Override
    public void everyTenMinutes() {
        crowdfundingOrderManager.scanWaitHandle();
    }

}
