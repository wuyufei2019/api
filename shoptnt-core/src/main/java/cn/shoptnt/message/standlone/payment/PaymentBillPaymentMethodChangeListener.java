package cn.shoptnt.message.standlone.payment;

import cn.shoptnt.message.dispatcher.payment.PaymentBillPaymentMethodChangeDispatcher;
import cn.shoptnt.model.base.message.PaymentBillPaymentMethodChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 支付账单支付方式改变 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class PaymentBillPaymentMethodChangeListener {

    @Autowired
    private PaymentBillPaymentMethodChangeDispatcher paymentBillPaymentMethodChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void billPaymentMethodChange(PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg) {
        paymentBillPaymentMethodChangeDispatcher.dispatch(paymentBillPaymentMethodChangeMsg);
    }
}
