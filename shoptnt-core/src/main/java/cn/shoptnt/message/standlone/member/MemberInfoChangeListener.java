package cn.shoptnt.message.standlone.member;

import cn.shoptnt.message.dispatcher.member.MemberInfoChangeDispatcher;
import cn.shoptnt.model.base.message.MemberInfoChangeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 修改会员资料 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class MemberInfoChangeListener {

    @Autowired
    private MemberInfoChangeDispatcher memberInfoChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void createIndexPage(MemberInfoChangeMessage message) {
        memberInfoChangeDispatcher.dispatch(message);
    }
}
