package cn.shoptnt.message.standlone.datasync;

import cn.shoptnt.message.dispatcher.goods.DisplayTimesGoodsDispatcher;
import cn.shoptnt.model.base.message.GoodsViewMessage;
import cn.shoptnt.model.base.message.MessageReceiveMessage;
import cn.shoptnt.service.datasync.receive.common.MessageReceiveManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 第三方消息处理
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @since 2024-04-01
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class MessageReceiveListener {

    @Autowired
    private MessageReceiveManager messageReceiveManager;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void viewGoods(MessageReceiveMessage message) {
        messageReceiveManager.handle(message.getId());
    }
}
