package cn.shoptnt.message.standlone.member;

import cn.shoptnt.message.dispatcher.member.MemberRegisterDispatcher;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 会员注册 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class MemberRegisterListener {

    @Autowired
    private MemberRegisterDispatcher memberRegisterDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        memberRegisterDispatcher.dispatch(memberRegisterMsg);
    }
}
