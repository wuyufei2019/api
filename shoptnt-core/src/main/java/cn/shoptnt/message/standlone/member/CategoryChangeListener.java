package cn.shoptnt.message.standlone.member;

import cn.shoptnt.message.dispatcher.goods.CategoryChangeDispatcher;
import cn.shoptnt.model.base.message.CategoryChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 分类变更 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class CategoryChangeListener {

    @Autowired
    private CategoryChangeDispatcher categoryChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void categoryChange(CategoryChangeMsg categoryChangeMsg) {
        categoryChangeDispatcher.dispatch(categoryChangeMsg);
    }
}
