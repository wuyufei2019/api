package cn.shoptnt.message.standlone.goods;

import cn.shoptnt.message.dispatcher.goods.DisplayTimesGoodsDispatcher;
import cn.shoptnt.model.base.message.GoodsViewMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 流量统计(店铺、商品) 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class DisplayTimesGoodsListener {

    @Autowired
    private DisplayTimesGoodsDispatcher displayTimesGoodsDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void viewGoods(GoodsViewMessage goodsViewMessage) {
        displayTimesGoodsDispatcher.dispatch(goodsViewMessage);
    }
}
