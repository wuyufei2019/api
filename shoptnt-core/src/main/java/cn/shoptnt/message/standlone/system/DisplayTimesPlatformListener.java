package cn.shoptnt.message.standlone.system;

import cn.shoptnt.message.dispatcher.goods.DisplayTimesPlatformDispatcher;
import cn.shoptnt.model.base.message.PlatformViewMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 流量统计(平台) 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author 张崧
 * @since 2024-04-24
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class DisplayTimesPlatformListener {

    @Autowired
    private DisplayTimesPlatformDispatcher dispatch;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void viewPlatform(PlatformViewMessage platformViewMessage) {
        dispatch.dispatch(platformViewMessage);
    }
}
