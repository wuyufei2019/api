/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.distribution;

import cn.shoptnt.message.event.MemberRegisterEvent;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import cn.shoptnt.client.distribution.CommissionTplClient;
import cn.shoptnt.client.distribution.DistributionClient;
import cn.shoptnt.model.distribution.dos.CommissionTpl;
import cn.shoptnt.model.distribution.dos.DistributionDO;
import cn.shoptnt.framework.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 注册后添加分销商
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/13 下午11:33
 */
@Component
public class DistributionRegisterConsumer implements MemberRegisterEvent{
    @Autowired
    private DistributionClient distributionClient;

    @Autowired
    private CommissionTplClient commissionTplClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private Cache cache;


    @Override
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        // 注册完毕后 在分销商表中添加会员信息
        DistributionDO distributor = new DistributionDO();
        try {
            distributor.setMemberId(memberRegisterMsg.getMember().getMemberId());
            //默认模版设置
            CommissionTpl commissionTpl= commissionTplClient.getDefaultCommission();
            distributor.setCurrentTplId(commissionTpl.getId());
            distributor.setCurrentTplName(commissionTpl.getTplName());
            distributor.setMemberName(memberRegisterMsg.getMember().getUname());
            distributor = this.distributionClient.add(distributor);
            distributor.setPath("0|" + distributor.getMemberId() + "|");
        } catch (RuntimeException e) {
            logger.error("会员注册异常",e);
        }

        //注册完毕后，给注册会员添加他的上级分销商id
        Object upMemberId = cache.get(CachePrefix.DISTRIBUTION_UP.getPrefix() + memberRegisterMsg.getUuid());

        try {
            //如果推广的会员id存在
            if (upMemberId != null) {
                long lv1MemberId = Long.parseLong(upMemberId.toString());
                logger.debug("上级会员："+lv1MemberId);
                DistributionDO parentDistributor = this.distributionClient.getDistributorByMemberId(lv1MemberId);
                distributor.setPath(parentDistributor.getPath() + distributor.getMemberId() + "|");

                //先更新 path
                this.distributionClient.edit(distributor);
                // 再更新parentId
                this.distributionClient.setParentDistributorId(memberRegisterMsg.getMember().getMemberId(), lv1MemberId);

            } else {
                logger.debug("没有上级会员");
                this.distributionClient.edit(distributor);
            }
            cache.remove(CachePrefix.DISTRIBUTION_UP.getPrefix() + memberRegisterMsg.getUuid());
        } catch (Exception e) {
            logger.error("会员注册异常",e);
        }
    }


}
