/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.trade;

import cn.shoptnt.client.promotion.CouponClient;
import cn.shoptnt.client.trade.OrderMetaClient;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderMetaKeyEnum;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 确认收款发放促销活动赠送优惠券
 *
 * @author Snow create in 2018/5/22
 * @version v2.0
 * @since v7.0.0
 */
@Component
public class CouponConsumer implements OrderStatusChangeEvent {


    @Autowired
    private OrderMetaClient orderMetaClient;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private CouponClient couponClient;


    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        if ((orderMessage.getNewStatus().name()).equals(OrderStatusEnum.PAID_OFF.name())) {

            OrderDO order = orderMessage.getOrderDO();

            //读取已发放的优惠券json
            String itemJson = this.orderMetaClient.getMetaValue(order.getSn(), OrderMetaKeyEnum.COUPON);
            List<CouponDO> couponList = JsonUtil.jsonToList(itemJson, CouponDO.class);

            if (couponList != null && couponList.size() > 0) {
                // 循环发放的优惠券
                for (CouponDO coupon : couponList) {
                    //获取当前数据库中优惠券的数据信息
                    CouponDO couponDO = this.couponClient.getModel(coupon.getCouponId());
                    //优惠券可领取数量不足时,不赠送优惠券
                    if (CurrencyUtil.sub(couponDO.getCreateNum(), couponDO.getReceivedNum()) <= 0) {
                        continue;
                    }
                    //获取当前订单的会员拥有该优惠券的数量
                    Long num = memberClient.getCoupon(order.getMemberId(), coupon.getCouponId());
                    if (num!=null){
                        //数量超过限领数量不赠送优惠券
                        if (CurrencyUtil.sub(num, couponDO.getLimitNum()) >= 0){
                            continue;
                        }
                    }
                    this.memberClient.receiveBonus(order.getMemberId(), order.getMemberName(), coupon.getCouponId());
                }
            }
        }
    }

}
