package cn.shoptnt.message.consumer.trade;

import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.service.promotion.crowdfunding.CrowdfundingManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 众筹订单相关
 *
 * @author 张崧
 * @since 2024-06-22
 */
@Component
public class CrowdfundingOrderConsumer implements OrderStatusChangeEvent {

    @Autowired
    private CrowdfundingManager crowdfundingManager;

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        OrderDO orderDO = orderMessage.getOrderDO();
        if (!orderDO.getCrowdfundingFlag()) {
            return;
        }

        // 订单确认后，累计众筹活动的数据
        if (orderMessage.getNewStatus() == OrderStatusEnum.CONFIRM) {
            double goodsPrice = CurrencyUtil.sub(orderDO.getOrderPrice(), orderDO.getShippingPrice());
            Long peopleCount = getPeopleCount(orderDO.getCrowdfundingId());
            crowdfundingManager.updateData(orderDO.getCrowdfundingId(), goodsPrice, orderDO.getGoodsNum(), peopleCount);
        }

        // 订单取消后，累计众筹活动的数据（退款类型的取消订单不在这里处理的原因是：售后必须完成后订单才会取消，拖的时间太久了，而是直接根据售后状态进行处理）
        if (orderMessage.getNewStatus() == OrderStatusEnum.CANCELLED) {
            double goodsPrice = CurrencyUtil.sub(orderDO.getOrderPrice(), orderDO.getShippingPrice());
            Long peopleCount = getPeopleCount(orderDO.getCrowdfundingId());
            crowdfundingManager.updateData(orderDO.getCrowdfundingId(), -goodsPrice, -orderDO.getGoodsNum(), peopleCount);
        }
    }

    private Long getPeopleCount(Long crowdfundingId) {
        return orderQueryManager.lambdaQuery()
                .select(OrderDO::getMemberId)
                .eq(OrderDO::getCrowdfundingFlag, true)
                .eq(OrderDO::getCrowdfundingId, crowdfundingId)
                .ne(OrderDO::getOrderStatus, OrderStatusEnum.CANCELLED.value())
                .groupBy(OrderDO::getMemberId)
                .list().stream().map(OrderDO::getMemberId).distinct().count();
    }
}
