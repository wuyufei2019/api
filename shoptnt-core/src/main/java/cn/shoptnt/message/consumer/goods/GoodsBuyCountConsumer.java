/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.goods;

import cn.shoptnt.client.promotion.PromotionGoodsClient;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.vo.OrderSkuVO;
import cn.shoptnt.framework.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author fk
 * @version v1.0
 * @Description: 商品购买数量变化
 * @date 2018/6/2510:13
 * @since v7.0.0
 */
@Service
public class GoodsBuyCountConsumer implements OrderStatusChangeEvent {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private OrderClient orderClient;
    @Autowired
    private PromotionGoodsClient groupbuyGoodsClient;


    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        //进行团购商品售卖数量增加
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.CONFIRM.name())) {
            //获取交易id
            String orderSn = orderMessage.getOrderDO().getSn();
            if (!orderSn.isEmpty()) {
                //截取id值
                //根据ID找到商品信息
                List<Map> list = orderClient.getItemsPromotionTypeandNum(orderSn);
                if (!list.isEmpty()) {
                    for (Map<String, Object> map : list) {
                        //根据商品类型进行判断
                        if (map.get("promotion_type") != null && PromotionTypeEnum.GROUPBUY.toString().equals(map.get("promotion_type").toString())) {
                            String goodId = (map.get("goods_id").toString());
                            String num = (map.get("num").toString());
                            String productId = (map.get("product_id").toString());
                            if (!goodId.isEmpty() && !num.isEmpty()) {
                                //更新库存
                                groupbuyGoodsClient.renewGroupbuyBuyNum(Long.parseLong(goodId), Integer.parseInt(num), Long.parseLong(productId));
                            }
                            //如果不是团购商品，打断循环退出
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        // 收货后更新商品的购买数量
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.ROG.name())) {
            OrderDO order = orderMessage.getOrderDO();
            String itemsJson = order.getItemsJson();
            List<OrderSkuVO> list = JsonUtil.jsonToList(itemsJson, OrderSkuVO.class);
            this.goodsClient.updateBuyCount(list);
        }
    }
}
