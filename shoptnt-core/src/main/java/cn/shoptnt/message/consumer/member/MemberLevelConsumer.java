package cn.shoptnt.message.consumer.member;

import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.client.member.MemberLevelClient;
import cn.shoptnt.client.member.MemberLevelUpgradeConditionClient;
import cn.shoptnt.message.event.MemberPointChangeEvent;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberLevel;
import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import cn.shoptnt.model.member.enums.MemberLevelConditionOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelConsumer
 * @description 会员等级消费者
 * @program: api
 * 2024/5/24 15:58
 */
@Service
public class MemberLevelConsumer implements MemberPointChangeEvent {

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private MemberLevelClient memberLevelClient;

    @Autowired
    private MemberLevelUpgradeConditionClient memberLevelUpgradeConditionClient;

    @Override
    public void memberPointChange(Long memberId) {
        Member member = memberClient.getModel(memberId);
        //如果此会员不参与等级体系 则跳过
        if (member.getIsUpgrade().equals(-1)) {
            return;
        }

        //查询会员等级
        MemberLevel memberLevel = memberLevelClient.getById(member.getLevelId());
        if (memberLevel != null && member != null && member.getLevelId() != null) {
            //查询所有的升级条件
            List<MemberLevelUpgradeCondition> memberLevelUpgradeConditions = memberLevelUpgradeConditionClient.getList();
            // 2. 初始化一个映射来跟踪每个等级的匹配状态
            Map<Long, Boolean> levelMatched = new HashMap<>();
            /* Double dd = 222D;*/
            // 3. 根据积分和消费总额匹配等级
            for (MemberLevelUpgradeCondition condition : memberLevelUpgradeConditions) {
                boolean isMatched = false;
                switch (condition.getConditionType()) {
                    case POINTS:
                        isMatched = member.getGradePoint() >= condition.getConditionValueMin() &&
                                member.getGradePoint() <= condition.getConditionValueMax();
                        break;
                  /*  case AMOUNT:
                        isMatched = dd >= condition.getConditionValueMin() &&
                                dd <= condition.getConditionValueMax();
                        break;*/
                }
                // 更新匹配状态
                levelMatched.putIfAbsent(condition.getLevelId(), isMatched);
                if (condition.getConditionOperator() == MemberLevelConditionOperator.AND) {
                    // 对于AND条件，如果不匹配，则将该等级的状态设置为false
                    levelMatched.put(condition.getLevelId(), levelMatched.get(condition.getLevelId()) && isMatched);
                } else if (condition.getConditionOperator() == MemberLevelConditionOperator.OR) {
                    // 对于OR条件，如果匹配，则保持true，否则不改变
                    levelMatched.put(condition.getLevelId(), levelMatched.get(condition.getLevelId()) || isMatched);
                }
            }
            // 找出匹配的等级ID
            Long matchedLevelId = levelMatched.entrySet().stream()
                    .filter(Map.Entry::getValue)
                    .map(Map.Entry::getKey)
                    .findFirst()
                    .orElse(null); // 如果没有匹配的等级，保持为null

            //查询要升级的等级id
            if (matchedLevelId != null) {
                MemberLevel upgradeMemberLevel = memberLevelClient.getById(matchedLevelId);
                // 4. 如果当前等级不是匹配的等级，并且我当前等级要小于升级的等级 则进行修改
                if (!member.getLevelId().equals(matchedLevelId) && memberLevel.getSortOrder() < upgradeMemberLevel.getSortOrder()) {
                    //修改会员等级
                    memberLevelClient.updateMemberLevel(memberId, matchedLevelId);
                }
            }

        }
    }
}