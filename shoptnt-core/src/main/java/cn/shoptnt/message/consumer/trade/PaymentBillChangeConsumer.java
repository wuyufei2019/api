/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.trade;

import cn.shoptnt.client.payment.PaymentBillClient;
import cn.shoptnt.message.event.PaymentBillPaymentMethodChangeEvent;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.base.message.PaymentBillPaymentMethodChangeMsg;
import cn.shoptnt.model.payment.dos.PaymentBillDO;
import cn.shoptnt.model.payment.enums.PaymentBillChangeTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 支付账单改变 需要取消第三方账单信息
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 */
@Component
public class PaymentBillChangeConsumer implements PaymentBillPaymentMethodChangeEvent {

    @Autowired
    private PaymentBillClient paymentBillClient;

    @Override
    public void billPaymentMethodChange(PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg) {
        //获取要更改的支付账单信息
        Map<String, Object> map = paymentBillPaymentMethodChangeMsg.getBills();
        if (!map.isEmpty()) {
            for (String key : map.keySet()) {
                PaymentBillDO paymentBillDO = (PaymentBillDO) map.get(key);
                //如果是交易类型改变了 需要删除掉之前的无用账单
                if (key.equals(PaymentBillChangeTypeEnum.SERVICE_TYPE.name())) {
                    //删除无用账单
                    paymentBillClient.deletePaymentBill(paymentBillDO.getBillId());
                }
                if (!StringUtil.isEmpty(paymentBillDO.getPaymentPluginId())) {
                    //关闭第三方交易
                    paymentBillClient.closeTrade(paymentBillDO);
                }
                

            }
        }
        

    }
}
