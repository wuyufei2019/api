package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.GoodsCommentEvent;
import cn.shoptnt.model.base.message.GoodsCommentMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品评论 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class GoodsCommentDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<GoodsCommentEvent> events;

    public void dispatch(GoodsCommentMsg goodsCommentMsg) {
        if (events != null) {
            for (GoodsCommentEvent event : events) {
                try {
                    event.goodsComment(goodsCommentMsg);
                } catch (Exception e) {
                    logger.error("处理商品评论出错", e);
                }
            }
        }
    }
}
