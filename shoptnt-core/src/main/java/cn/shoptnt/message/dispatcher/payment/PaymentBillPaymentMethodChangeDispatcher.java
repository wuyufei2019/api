package cn.shoptnt.message.dispatcher.payment;

import cn.shoptnt.message.event.PaymentBillPaymentMethodChangeEvent;
import cn.shoptnt.model.base.message.PaymentBillPaymentMethodChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 支付账单支付方式改变 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class PaymentBillPaymentMethodChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<PaymentBillPaymentMethodChangeEvent> events;

    public void dispatch(PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg) {
        if (events != null) {
            for (PaymentBillPaymentMethodChangeEvent event : events) {
                try {
                    event.billPaymentMethodChange(paymentBillPaymentMethodChangeMsg);
                } catch (Exception e) {
                    logger.error("支付账单支付方式改变消息出错", e);
                }
            }
        }
    }
}
