package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.GoodsIndexInitEvent;
import cn.shoptnt.model.base.message.IndexCreateMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 初始化商品索引 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class GoodsIndexInitDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<GoodsIndexInitEvent> events;

    public void dispatch(IndexCreateMessage indexCreateMessage) {
        if (events != null) {
            for (GoodsIndexInitEvent event : events) {
                try {
                    event.createGoodsIndex();
                } catch (Exception e) {
                    logger.error("初始化商品索引出错", e);
                }
            }
        }
    }
}
