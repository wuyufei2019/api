package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.SearchKeywordEvent;
import cn.shoptnt.model.base.message.GoodsSearchMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 搜索关键字变动 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class SearchKeywordDispatcher {

    @Autowired(required = false)
    private List<SearchKeywordEvent> events;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public void dispatch(GoodsSearchMessage goodsSearchMessage) {
        if (events != null) {
            for (SearchKeywordEvent event : events) {
                try {
                    event.updateOrAdd(goodsSearchMessage.getKeyword());
                } catch (Exception e) {
                    logger.error("处理搜索关键字消息出错", e);
                }
            }
        }
    }
}
