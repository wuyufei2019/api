package cn.shoptnt.message.dispatcher.shop;

import cn.shoptnt.message.event.ShopRegisterEvent;
import cn.shoptnt.model.shop.vo.ShopRegisterMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商家入驻 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class ShopRegisterDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShopRegisterEvent> events;

    public void dispatch(ShopRegisterMsg shopRegisterMsg) {
        if (events != null) {
            for (ShopRegisterEvent event : events) {
                try {
                    event.shopRegister(shopRegisterMsg);
                } catch (Exception e) {
                    logger.error("处理商家入驻消息出错", e);
                }
            }
        }
    }
}
