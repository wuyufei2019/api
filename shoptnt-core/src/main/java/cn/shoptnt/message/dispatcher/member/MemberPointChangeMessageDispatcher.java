package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberPointChangeEvent;
import cn.shoptnt.message.event.MemberSignEvent;
import cn.shoptnt.model.base.message.MemberPointChangeMessage;
import cn.shoptnt.model.base.message.SignMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员站内消息 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberPointChangeMessageDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberPointChangeEvent> events;

    public void dispatch(MemberPointChangeMessage message) {
        if (events != null) {
            for (MemberPointChangeEvent event : events) {
                try {
                    event.memberPointChange(message.getMemberId());
                } catch (Exception e) {
                    logger.error("积分消息改变发送出错", e);
                }
            }
        }
    }
}
