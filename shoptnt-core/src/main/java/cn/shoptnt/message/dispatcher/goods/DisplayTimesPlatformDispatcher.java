package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.client.statistics.DisplayTimesClient;
import cn.shoptnt.model.base.message.PlatformViewMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 平台流量统计 调度器
 *
 * @author 张崧
 * @since 2024-04-24
 */
@Service
public class DisplayTimesPlatformDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DisplayTimesClient displayTimesClient;

    public void dispatch(PlatformViewMessage platformViewMessage) {
        try {
            displayTimesClient.countPlatform(platformViewMessage.getPlatformPageViews());
        } catch (Exception e) {
            logger.error("流量整理：平台 异常", e);
        }
    }
}
