package cn.shoptnt.message.dispatcher.trade.aftersale;

import cn.shoptnt.message.event.ASNewOrderEvent;
import cn.shoptnt.model.base.message.AsOrderStatusChangeMsg;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 创建 换货或补发产生的新订单 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class ASNewOrderDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ASNewOrderEvent> events;

    public void dispatch(AsOrderStatusChangeMsg orderStatusChangeMsg) {
        if (events != null) {
            for (ASNewOrderEvent event : events) {
                try {
                    event.orderChange(orderStatusChangeMsg);
                } catch (Exception e) {
                    logger.error("处理商家创建新的换货、补发商品订单出错", e);
                }
            }
        }
    }
}
