package cn.shoptnt.message.dispatcher.payment;

import cn.shoptnt.message.event.PaymentBillPayStatusChangeEvent;
import cn.shoptnt.model.base.message.PaymentBillStatusChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 支付账单状态改变 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class PaymentBillChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<PaymentBillPayStatusChangeEvent> events;

    public void dispatch(PaymentBillStatusChangeMsg paymentBillMessage) {
        if (events != null) {
            for (PaymentBillPayStatusChangeEvent event : events) {
                try {
                    event.billChange(paymentBillMessage);
                } catch (Exception e) {
                    logger.error("支付账单状态改变消息出错", e);
                }
            }
        }
    }
}
