package cn.shoptnt.message.dispatcher.shop;

import cn.shoptnt.message.event.ShipTemplateChangeEvent;
import cn.shoptnt.model.goods.vo.ShipTemplateMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 运费模板变化 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class ShipTemplateChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShipTemplateChangeEvent> events;

    public void dispatch(ShipTemplateMsg shipTemplateMsg) {
        if (events != null) {
            for (ShipTemplateChangeEvent event : events) {
                try {
                    event.shipTemplateChange(shipTemplateMsg);
                } catch (Exception e) {
                    logger.error("处理商品分类变化消息出错", e);
                }
            }
        }
    }
}
