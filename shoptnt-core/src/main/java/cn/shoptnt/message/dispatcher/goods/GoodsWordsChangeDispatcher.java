package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.GoodsWordsChangeEvent;
import cn.shoptnt.model.base.message.GoodsWordChangeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 提示词变化 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class GoodsWordsChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<GoodsWordsChangeEvent> events;

    public void dispatch(GoodsWordChangeMessage wordChangeMessage) {
        if (events != null) {
            for (GoodsWordsChangeEvent event : events) {
                try {
                    event.wordsChange(wordChangeMessage.getWord());
                } catch (Exception e) {
                    logger.error("处理提示词变化消息出错", e);
                }
            }
        }
    }
}
