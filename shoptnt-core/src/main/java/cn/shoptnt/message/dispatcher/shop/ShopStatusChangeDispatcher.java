package cn.shoptnt.message.dispatcher.shop;

import cn.shoptnt.message.event.ShopStatusChangeEvent;
import cn.shoptnt.model.base.message.ShopStatusChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 店铺状态变更 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class ShopStatusChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShopStatusChangeEvent> events;

    public void dispatch(ShopStatusChangeMsg shopStatusChangeMsg) {
        if (events != null) {
            for (ShopStatusChangeEvent event : events) {
                try {
                    event.changeStatus(shopStatusChangeMsg);
                } catch (Exception e) {
                    logger.error("店铺状态变更消息出错", e);
                }
            }
        }

    }
}
