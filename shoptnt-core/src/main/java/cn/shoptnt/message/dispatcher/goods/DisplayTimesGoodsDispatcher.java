package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.client.statistics.DisplayTimesClient;
import cn.shoptnt.model.base.message.GoodsViewMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品流量统计 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class DisplayTimesGoodsDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DisplayTimesClient displayTimesClient;

    public void dispatch(GoodsViewMessage goodsViewMessage) {
        try {
            displayTimesClient.countGoods(goodsViewMessage.getGoodsPageViews());
        } catch (Exception e) {
            logger.error("流量整理：商品 异常", e);
        }
    }
}
