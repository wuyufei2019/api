package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberRegisterEvent;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员注册 调度器
 *
 * @author 张崧
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberRegisterDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberRegisterEvent> events;

    public void dispatch(MemberRegisterMsg vo) {
        if (events != null) {
            for (MemberRegisterEvent event : events) {
                try {
                    event.memberRegister(vo);
                } catch (Exception e) {
                    logger.error("会员注册消息出错", e);
                }
            }
        }
    }
}
