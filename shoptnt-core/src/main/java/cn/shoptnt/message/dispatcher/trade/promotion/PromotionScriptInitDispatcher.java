package cn.shoptnt.message.dispatcher.trade.promotion;


import cn.shoptnt.message.event.PromotionScriptInitEvent;
import cn.shoptnt.model.base.message.PromotionScriptInitMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 促销脚本初始化调度器
 *
 * @author 张崧
 * @since 2024-03-13
 */
@Service
public class PromotionScriptInitDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private List<PromotionScriptInitEvent> events;

    public void dispatch(PromotionScriptInitMessage promotionScriptInitMessage) {
        if (events != null) {
            for (PromotionScriptInitEvent event : events) {
                try {
                    event.scriptInit();
                } catch (Exception e) {
                    logger.error("促销脚本初始化消息异常", e);
                }
            }
        }
    }
}
