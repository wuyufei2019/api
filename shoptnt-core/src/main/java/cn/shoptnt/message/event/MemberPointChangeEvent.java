package cn.shoptnt.message.event;

/**
 * @author zh
 * @version 1.0
 * @title MemberPointChangeEvent
 * @description 会员积分变化消息
 * @program: api
 * 2024/5/24 15:48
 */
public interface MemberPointChangeEvent {

    /**
     * 会员积分变化事件
     *
     * @param memberId 会员id
     */
    void memberPointChange(Long memberId);

}
