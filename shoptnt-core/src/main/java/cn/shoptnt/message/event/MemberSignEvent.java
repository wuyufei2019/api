/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.promotion.sign.dos.SignInRecord;

import java.util.List;

/**
 * 会员签到
 *
 * @author fk
 * @version v2.0
 * @since v7.0 2018年3月23日 上午10:16:59
 */
public interface MemberSignEvent {

    /**
     * 会员签到
     *
     * @param signInRecords 签到记录
     */
    void sign(List<SignInRecord> signInRecords);
}
