/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.passport.impl;

import cn.shoptnt.model.member.dto.AppleIDUserDTO;
import cn.shoptnt.model.member.dto.LoginUserDTO;
import cn.shoptnt.model.member.enums.ConnectTypeEnum;
import cn.shoptnt.service.member.ConnectManager;
import cn.shoptnt.service.passport.LoginAppleIDManager;
import cn.shoptnt.service.passport.LoginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * AppleID登陆相关接口
 * @author snow
 * @version v1.0
 * @since v7.2.2
 * 2020-12-17
 */
@Service
public class LoginAppleIDManagerImpl implements LoginAppleIDManager {

    @Autowired
    private ConnectManager connectManager;

    @Autowired
    private LoginManager loginManager;

    @Override
    public Map appleIDLogin(String uuid, AppleIDUserDTO appleIDUserDTO) {
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUuid(uuid);
        loginUserDTO.setTokenOutTime(null);
        loginUserDTO.setRefreshTokenOutTime(null);
        loginUserDTO.setUnionType(ConnectTypeEnum.ALIPAY);
        loginUserDTO.setUnionid(appleIDUserDTO.getOpenid());
        return loginManager.loginByUnionId(loginUserDTO);
    }
}
