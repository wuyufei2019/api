/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security;

/**
 * 签名数据扫描任务接口
 * @author 妙贤
 * @data 2021/11/22 22:33
 * @version 1.0
 **/
public interface SignScanTask {

    /**
     * 扫描能力
     * @param rounds 扫描的轮次
     * @param moduleName 扫描的模块
     * @see cn.shoptnt.service.security.model.SecurityModuleEnum
     * @throws IllegalAccessException
     */
    void scan(String rounds,String moduleName) throws IllegalAccessException;

    /**
     * 相应的模块进行重新签名的能力
     */
    void reSign();
}
