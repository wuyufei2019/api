/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.goods;

import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.goods.dto.Stock;
import cn.shoptnt.model.goods.dto.StockParam;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;

import java.util.List;

/**
 * 商品sku业务层
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-21 11:48:40
 */
public interface GoodsSkuManager {
    /**
     * 查询SKU列表
     * @param param 查询条件
     * @return
     */
    WebPage list(GoodsQueryParam param);

    /**
     * 添加商品sku
     *
     * @param skuList sku集合
     * @param goods 商品do对象
     */
    void add(List<GoodsSkuVO> skuList, GoodsDO goods);

    /**
     * 修改商品sku
     *
     * @param skuList sku集合
     * @param goods 商品do对象
     */
    void edit(List<GoodsSkuVO> skuList, GoodsDO goods);

    /**
     * 删除商品关联的sku
     *
     * @param goodsIds 商品id数组
     */
    void delete(Long[] goodsIds);

    /**
     * 查询某商品的sku
     *
     * @param goodsId 商品id
     * @return
     */
    List<GoodsSkuVO> listByGoodsId(Long goodsId);

    /**
     * 缓存中查询sku信息
     *
     * @param skuId skuid
     * @return
     */
    GoodsSkuVO getSkuFromCache(Long skuId);

    /**
     * 查询sku信息
     *
     * @param skuId skuid
     * @return
     */
    GoodsSkuVO getSku(Long skuId);

    /**
     * 查询某商家的可售卖的商品的sku集合
     *
     * @return
     */
    List<GoodsSkuDO> querySellerAllSku();

    /**
     * 判断商品是否都是某商家的商品
     *
     * @param skuIds
     * @return
     */
    void checkSellerGoodsCount(Long[] skuIds);

    /**
     * 查询单个sku
     *
     * @param id skuid
     * @return
     */
    GoodsSkuDO getModel(Long id);


    /**
     * 根据商品sku主键id集合获取商品信息
     * @param skuIds sku数组
     * @return
     */
    List<GoodsSkuVO> query(Long[] skuIds);

    /**
     * 根据商品信息更新sku的图片等信息
     * @param goods 商品do对象
     */
    void updateSkuByGoods(GoodsDO goods);

    /**
     * 更新商品的sku，可能是添加，可能是修改
     * @param skuList sku集合
     * @param goodsId 商品id
     */
    void editSkus(List<GoodsSkuVO> skuList, Long goodsId);

    /**
     * 生成积分兑换商品的脚本信息
     * @param skuId 商品skuID
     * @param exchangeId 积分兑换商品ID
     * @param price 兑换积分商品所需的价钱
     * @param point 兑换积分商品所需的积分
     */
    void createSkuExchangeScript(Long skuId, Long exchangeId, Double price, Integer point);

    /**
     * 删除积分兑换商品的脚本信息
     * @param skuId 商品skuID
     */
    void deleteSkuExchangeScript(Long skuId);

    /**
     * 删除积分兑换商品的脚本信息
     * 此方法用于商家修改商品时，修改之前商品类型为积分商品，修改后为普通商品的情况
     * @param goodsId 商品id
     * @param oldType 原商品类型
     * @param newType 现商品类型
     */
    void deleteSkuExchangeScript(Long goodsId, String oldType, String newType);


    /**
     * 检测sku是否存在
     * @param skuId
     * @return
     */
    Boolean checkExist(Long skuId);

    /**
     * sku篡改数据扫描
     * @return
     */
    ScanResult scanModule(ScanModuleDTO scanModuleDTO);

    /**
     * sku数据重新签名
     */
    void reSign();

    /**
     * 修复sku数据
     * @param skuId
     */
    void repair(Long skuId);

    /**
     * 从数据库查询sku库存
     * @param skuIds
     * @return
     */
    List<Stock> getSkuQuantity(List<Long> skuIds);

    /**
     * 更新数据库库存
     * @param param
     * @return
     */
    Boolean updateQuantity(StockParam param);
}
