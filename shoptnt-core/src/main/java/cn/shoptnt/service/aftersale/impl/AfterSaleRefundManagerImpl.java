/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.aftersale.impl;

import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.client.payment.PaymentClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.mapper.trade.aftersale.AfterSaleRefundMapper;
import cn.shoptnt.mapper.trade.aftersale.RefundMapper;
import cn.shoptnt.model.errorcode.AftersaleErrorCode;
import cn.shoptnt.model.aftersale.dos.AfterSaleRefundDO;
import cn.shoptnt.model.aftersale.dos.RefundDO;
import cn.shoptnt.model.aftersale.dto.RefundQueryParam;
import cn.shoptnt.model.aftersale.enums.*;
import cn.shoptnt.model.aftersale.vo.ApplyAfterSaleVO;
import cn.shoptnt.model.aftersale.vo.RefundApplyVO;
import cn.shoptnt.model.aftersale.vo.RefundRecordVO;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.service.aftersale.*;
import cn.shoptnt.model.base.message.AfterSaleChangeMessage;
import cn.shoptnt.client.member.DepositeClient;
import cn.shoptnt.client.payment.RefundClient;
import cn.shoptnt.model.payment.dos.PaymentMethodDO;
import cn.shoptnt.model.support.FlowCheckOperate;
import cn.shoptnt.model.system.enums.DeleteStatusEnum;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.OrderItemsDO;
import cn.shoptnt.model.trade.order.enums.OrderTypeEnum;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 退款单业务接口实现
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-22
 */
@SuppressWarnings("Duplicates")
@Service
public class AfterSaleRefundManagerImpl implements AfterSaleRefundManager {
    @Autowired
    private RefundMapper refundMapper;

    @Autowired
    private AfterSaleRefundMapper afterSaleRefundMapper;

    @Autowired
    private RefundClient refundClient;

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Autowired
    private AfterSaleManager afterSaleManager;

    @Autowired
    private AfterSaleLogManager afterSaleLogManager;

    @Autowired
    private PaymentClient paymentClient;

    @Autowired
    private AfterSaleDataCheckManager afterSaleDataCheckManager;

    @Autowired
    private DepositeClient depositeClient;

    @Autowired
    private DirectMessageSender messageSender;

    @Autowired
    private LockFactory lockFactory;

    @Override
    public WebPage list(RefundQueryParam param) {
        //创建查询条件包装器
        QueryWrapper<RefundDO> wrapper = this.createQueryWrapper(param);
        //获取退款单分页列表数据
        IPage<RefundDO> iPage = refundMapper.selectPage(new Page<>(param.getPageNo(), param.getPageSize()), wrapper);

        //转换商品数据
        List<RefundDO> refundDOList = iPage.getRecords();
        List<RefundRecordVO> recordVOList = new ArrayList<>();
        //循环初始化退款单对象
        for (RefundDO refund : refundDOList) {
            RefundRecordVO recordVO = new RefundRecordVO(refund);
            recordVOList.add(recordVO);
        }

        //转换分页对象
        WebPage<RefundRecordVO> webPage = new WebPage(param.getPageNo(), iPage.getTotal(), param.getPageSize(), recordVOList);
        return webPage;
    }


    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void adminRefund(String serviceSn, Double refundPrice, String remark, ServiceOperateTypeEnum typeEnum) {

        Lock lock = lockFactory.getLock(serviceSn);
        try {
            //在多线程的情况下 售后信息可能会出现脏读的情况 需要加锁
            lock.lock();

            //参数校验
            this.afterSaleDataCheckManager.checkAdminRefund(serviceSn, refundPrice, remark);

            //获取售后服务单详细信息
            Permission permission = Permission.CLIENT;
            if (ServiceOperateTypeEnum.SELLER_REFUND.equals(typeEnum)) {
                permission = Permission.SELLER;
            } else if (ServiceOperateTypeEnum.ADMIN_REFUND.equals(typeEnum)) {
                permission = Permission.ADMIN;
            }
            ApplyAfterSaleVO applyAfterSaleVO = this.afterSaleQueryManager.detail(serviceSn, permission);

            //操作权限验证
            if (!FlowCheckOperate.checkOperate(applyAfterSaleVO.getServiceType(), applyAfterSaleVO.getServiceStatus(), typeEnum.value())) {
                throw new ServiceException(AftersaleErrorCode.E601.name(), "当前售后服务单状态不允许进行退款操作");
            }

            //根据售后服务单号获取退款单信息并校验
            RefundDO refund = this.getModel(serviceSn);
            if (refund == null) {
                throw new ServiceException(AftersaleErrorCode.E603.name(), "售后退款单信息不存在");
            }

            //获取退款时间
            long refundTime = DateUtil.getDateline();

            //新建修改条件包装器
            UpdateWrapper<RefundDO> wrapper = new UpdateWrapper<>();
            //修改退款单状态为已完成
            wrapper.set("refund_status", RefundStatusEnum.COMPLETED.value());
            //修改退款时间
            wrapper.set("refund_time", refundTime);
            //修改实际退款金额
            wrapper.set("actual_price", refundPrice);
            //以售后服务单号为修改条件
            wrapper.eq("sn", serviceSn);
            //修改退款单信息
            refundMapper.update(new RefundDO(), wrapper);

            //新建修改条件包装器
            UpdateWrapper<AfterSaleRefundDO> asWrapper = new UpdateWrapper<>();
            //修改退款时间
            asWrapper.set("refund_time", refundTime);
            //修改实际退款金额
            asWrapper.set("actual_price", refundPrice);
            //以售后服务单号为修改条件
            asWrapper.eq("service_sn", serviceSn);
            //修改退款单信息
            afterSaleRefundMapper.update(new AfterSaleRefundDO(), asWrapper);

            //将售后服务单状态和退款备注
            this.afterSaleManager.updateServiceStatus(serviceSn, ServiceStatusEnum.COMPLETED.value(), null, null, remark, null);

            //发送售后服务完成消息
            AfterSaleChangeMessage changeMessage = new AfterSaleChangeMessage(serviceSn, ServiceTypeEnum.valueOf(applyAfterSaleVO.getServiceType()), ServiceStatusEnum.COMPLETED);
            messageSender.send(changeMessage);

            //新增退款操作日志
            String log = "已成功将退款退还给买家，当前售后服务已完成。";
            this.afterSaleLogManager.add(serviceSn, log, "系统");
        }finally {
            lock.unlock();
        }
    }

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void refund(String serviceSn, Permission permission) {

        Lock lock = lockFactory.getLock(serviceSn);
        try {
            //在多线程的情况下 售后信息可能会出现脏读的情况 需要加锁
            lock.lock();

            //获取售后服务单详细信息
            ApplyAfterSaleVO applyAfterSaleVO = this.afterSaleQueryManager.detail(serviceSn, permission);

            //获取售后服务单状态
            String serviceStatus = applyAfterSaleVO.getServiceStatus();

            //操作权限验证
            if (!FlowCheckOperate.checkOperate(applyAfterSaleVO.getServiceType(), serviceStatus, ServiceOperateTypeEnum.SELLER_REFUND.value())) {
                throw new ServiceException(AftersaleErrorCode.E601.name(), "当前售后服务单状态不允许进行退款操作");
            }

            //获取退款单相关信息
            RefundDO refund = this.getModel(serviceSn);
            if (refund == null) {
                throw new ServiceException(AftersaleErrorCode.E606.name(), "退款单信息不存在");
            }

            //日志详细
            String log;

            //获取退款时间
            long refundTime = DateUtil.getDateline();
            //如果是退款至预存款
            if (RefundWayEnum.BALANCE.name().equals(refund.getRefundWay())) {
                boolean bool = depositeClient.increase(refund.getAgreePrice(), refund.getMemberId(), "退款成功，退款单号:" + serviceSn);
                if (bool) {
                    serviceStatus = ServiceStatusEnum.COMPLETED.value();

                    //新建修改条件包装器
                    UpdateWrapper<RefundDO> wrapper = new UpdateWrapper<>();
                    //修改退款单状态为已完成
                    wrapper.set("refund_status", serviceStatus);
                    //修改退款时间
                    wrapper.set("refund_time", refundTime);
                    //修改实际退款金额
                    wrapper.set("actual_price", refund.getAgreePrice());
                    //以售后服务单号为修改条件
                    wrapper.eq("sn", serviceSn);
                    //如果退款至与承诺款操作成功，就将退款单状态修改为已完成并修改退款日期以及实际退款字段值
                    refundMapper.update(new RefundDO(), wrapper);

                    //新建修改条件包装器
                    UpdateWrapper<AfterSaleRefundDO> asWrapper = new UpdateWrapper<>();
                    //修改退款时间
                    asWrapper.set("refund_time", refundTime);
                    //修改实际退款金额
                    asWrapper.set("actual_price", refund.getAgreePrice());
                    //以售后服务单号为修改条件
                    asWrapper.eq("service_sn", serviceSn);
                    //修改售后服务退款相关信息
                    afterSaleRefundMapper.update(new AfterSaleRefundDO(), asWrapper);

                    log = "售后服务退款至预存款中已退款成功，当前售后服务已完成，如有疑问请及时联系商家或平台。";
                } else {
                    serviceStatus = ServiceStatusEnum.WAIT_FOR_MANUAL.value();
                    //新建修改条件包装器
                    UpdateWrapper<RefundDO> wrapper = new UpdateWrapper<>();
                    //修改退款单状态为已完成
                    wrapper.set("refund_status", RefundStatusEnum.REFUNDING.value());
                    //以售后服务单号为修改条件
                    wrapper.eq("sn", serviceSn);
                    //如果退款至预存款操作失败，就将退款单状态修改为退款中，等待平台线下退款
                    refundMapper.update(new RefundDO(), wrapper);

                    log = "售后服务退款至预存款操作失败，需要商家或平台进行人工退款处理。";
                }
            } else if (refund.getRefundWay().equals(RefundWayEnum.ORIGINAL.value())) {
                //如果支持原路退回，则商品入库后直接原路退款
                //原路退款操作
                Map map = refundClient.originRefund(refund.getPayOrderNo(), serviceSn, refund.getAgreePrice(), "b2b2c");

                //如果原路退款成功
                if ("true".equals(map.get("result").toString())) {
                    serviceStatus = ServiceStatusEnum.REFUNDING.value();

                    //新建修改条件包装器
                    UpdateWrapper<RefundDO> wrapper = new UpdateWrapper<>();
                    //修改退款单状态为已完成
                    wrapper.set("refund_status", serviceStatus);
                    //修改退款时间
                    wrapper.set("refund_time", refundTime);
                    //修改实际退款金额
                    wrapper.set("actual_price", refund.getAgreePrice());
                    //以售后服务单号为修改条件
                    wrapper.eq("sn", serviceSn);
                    //如果原路退款操作成功，就将退款单状态修改为已完成并修改退款日期以及实际退款字段值
                    refundMapper.update(new RefundDO(), wrapper);

                    //新建修改条件包装器
                    UpdateWrapper<AfterSaleRefundDO> asWrapper = new UpdateWrapper<>();
                    //修改退款时间
                    asWrapper.set("refund_time", refundTime);
                    //修改实际退款金额
                    asWrapper.set("actual_price", refund.getAgreePrice());
                    //以售后服务单号为修改条件
                    asWrapper.eq("service_sn", serviceSn);
                    //修改售后服务退款相关信息
                    afterSaleRefundMapper.update(new AfterSaleRefundDO(), asWrapper);

                    //新增售后操作日志
                    log = "售后服务已退款成功，请关注您账户退款到账信息。";
                } else {
                    serviceStatus = ServiceStatusEnum.WAIT_FOR_MANUAL.value();

                    //新建修改条件包装器
                    UpdateWrapper<RefundDO> wrapper = new UpdateWrapper<>();
                    //修改退款单状态为已完成
                    wrapper.set("refund_status", RefundStatusEnum.REFUNDING.value());
                    //以售后服务单号为修改条件
                    wrapper.eq("sn", serviceSn);
                    //如果原路退款操作失败，就将退款单状态修改为退款中，等待平台线下退款
                    refundMapper.update(new RefundDO(), wrapper);

                    log = "售后服务原路退款操作失败，原因：" + map.get("fail_reason") + "，需要商家或平台进行人工退款处理。";
                }
            } else {
                serviceStatus = ServiceStatusEnum.WAIT_FOR_MANUAL.value();

                //新建修改条件包装器
                UpdateWrapper<RefundDO> wrapper = new UpdateWrapper<>();
                //修改退款单状态为已完成
                wrapper.set("refund_status", RefundStatusEnum.REFUNDING.value());
                //以售后服务单号为修改条件
                wrapper.eq("sn", serviceSn);
                //如果退款方式是线下支付，就将退款单状态修改为退款中，等待平台线下退款
                refundMapper.update(new RefundDO(), wrapper);

                log = "等待平台进行人工退款处理。";
            }

            //修改售后服务单状态
            this.afterSaleManager.updateServiceStatus(serviceSn, serviceStatus);

            //新增售后日志
            String operator = permission == Permission.SELLER ? UserContext.getSeller().getUsername() : "系统";
            this.afterSaleLogManager.add(serviceSn, log, operator);

            //发送售后服务商家退款消息
            AfterSaleChangeMessage afterSaleChangeMessage = new AfterSaleChangeMessage(serviceSn, ServiceTypeEnum.valueOf(applyAfterSaleVO.getServiceType()), ServiceStatusEnum.valueOf(serviceStatus));
            afterSaleChangeMessage.setRefundFlag(true);
            messageSender.send(afterSaleChangeMessage);

        } finally {
            lock.unlock();
        }
    }

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public RefundDO fillRefund(Double refundPrice, ApplyAfterSaleVO applyAfterSaleVO) {
        RefundDO refundDO = new RefundDO();
        BeanUtil.copyProperties(applyAfterSaleVO, refundDO);
        refundDO.setRefundWay(applyAfterSaleVO.getRefundInfo().getRefundWay());
        refundDO.setAccountType(applyAfterSaleVO.getRefundInfo().getAccountType());
        refundDO.setReturnAccount(applyAfterSaleVO.getRefundInfo().getReturnAccount());
        refundDO.setBankName(applyAfterSaleVO.getRefundInfo().getBankName());
        refundDO.setBankAccountNumber(applyAfterSaleVO.getRefundInfo().getBankAccountNumber());
        refundDO.setBankAccountName(applyAfterSaleVO.getRefundInfo().getBankAccountName());
        refundDO.setBankDepositName(applyAfterSaleVO.getRefundInfo().getBankDepositName());
        refundDO.setRefundPrice(applyAfterSaleVO.getRefundInfo().getRefundPrice());
        refundDO.setAgreePrice(refundPrice);
        refundDO.setRefundStatus(RefundStatusEnum.APPLY.value());
        refundDO.setDisabled(DeleteStatusEnum.NORMAL.value());
        refundDO.setPayOrderNo(applyAfterSaleVO.getRefundInfo().getPayOrderNo());
        Long id = this.addRefund(refundDO);
        refundDO.setId(id);

        //新建修改条件包装器
        UpdateWrapper<AfterSaleRefundDO> asWrapper = new UpdateWrapper<>();
        //修改商家同意的退款金额
        asWrapper.set("agree_price", refundPrice);
        //以售后服务单号为修改条件
        asWrapper.eq("service_sn", applyAfterSaleVO.getSn());
        //修改售后服务退款相关信息
        afterSaleRefundMapper.update(new AfterSaleRefundDO(), asWrapper);

        return refundDO;
    }

    @Override
    public Long addRefund(RefundDO refundDO) {
        //退款单信息入库
        refundMapper.insert(refundDO);
        //返回退款单主键
        return refundDO.getId();
    }

    @Override
    public RefundDO getModel(String serviceSn) {
        //创建查询条件包装器
        QueryWrapper<RefundDO> wrapper = new QueryWrapper<>();
        //以售后服务单号为查询条件
        wrapper.eq("sn", serviceSn);
        //返回按售后服务单号查询出的退款单信息
        return refundMapper.selectOne(wrapper);
    }

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void fillAfterSaleRefund(String serviceSn, Integer returnNum, OrderDO orderDO, OrderItemsDO itemsDO, AfterSaleRefundDO refundDO) {
        //填充售后退款账户相关信息
        refundDO.setServiceSn(serviceSn);
        refundDO.setPayOrderNo(orderDO.getPayOrderNo());

        //计算用户申请退货的可退款金额
        double refundPrice = 0.00;
        if (returnNum.intValue() == itemsDO.getNum().intValue()) {
            logger.debug("item", itemsDO);
            refundPrice = itemsDO.getRefundPrice();
        } else {
            refundPrice = CurrencyUtil.mul(returnNum, CurrencyUtil.div(itemsDO.getRefundPrice(), itemsDO.getNum(), 4));
        }
        refundDO.setRefundPrice(refundPrice);

        //获取订单的支付方式
        PaymentMethodDO paymentMethodDO = null;
        if (orderDO.getPaymentPluginId() != null) {
            paymentMethodDO = this.paymentClient.getByPluginId(orderDO.getPaymentPluginId());
        }

        if (orderDO.getBalance() != null && orderDO.getBalance() > 0) {
            refundDO.setRefundWay(RefundWayEnum.BALANCE.value());
        } else if (paymentMethodDO == null || paymentMethodDO.getIsRetrace() == 0) {
            //如果订单没有支付方式信息或者支付方式不支持原路退款
            //校验退款信息
            this.afterSaleDataCheckManager.checkRefundInfo(refundDO);

            //退款方式为账户退款
            refundDO.setRefundWay(RefundWayEnum.ACCOUNT.value());

        } else {
            //如果支付方式支持原路退回，填充退款账号信息
            fillAccountInfo(refundDO, paymentMethodDO.getPluginId());
        }

        //退款相关信息入库
        this.addAfterSaleRefund(refundDO);
    }

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void fillCancelOrderRefund(String serviceSn, OrderDO orderDO, RefundApplyVO refundApplyVO) {
        //填充并校验售后退款相关信息
        AfterSaleRefundDO afterSaleRefundDO = new AfterSaleRefundDO();
        BeanUtil.copyProperties(refundApplyVO, afterSaleRefundDO);
        afterSaleRefundDO.setServiceSn(serviceSn);
        afterSaleRefundDO.setPayOrderNo(orderDO.getPayOrderNo());

        //获取订单的支付方式
        PaymentMethodDO paymentMethodDO = null;
        if (orderDO.getPaymentPluginId() != null) {
            paymentMethodDO = this.paymentClient.getByPluginId(orderDO.getPaymentPluginId());
        }

        if (orderDO.getBalance() != null && orderDO.getBalance() > 0) {
            afterSaleRefundDO.setRefundWay(RefundWayEnum.BALANCE.value());
        } else if (paymentMethodDO == null || paymentMethodDO.getIsRetrace() == 0) {
            //如果订单没有支付方式信息或者支付方式不支持原路退款
            if (OrderTypeEnum.PINTUAN.name().equals(orderDO.getOrderType())) {
                //退款方式为线下退款
                afterSaleRefundDO.setRefundWay(RefundWayEnum.OFFLINE.value());
            } else {
                //如果是普通订单申请取消需要校验申请参数
                this.afterSaleDataCheckManager.checkRefundInfo(afterSaleRefundDO);

                //退款方式为账户退款
                afterSaleRefundDO.setRefundWay(RefundWayEnum.ACCOUNT.value());
            }

        } else {

            //如果支付方式支持原路退回，填充退款账号信息
            fillAccountInfo(afterSaleRefundDO, paymentMethodDO.getPluginId());
        }

        //退款信息入库
        this.addAfterSaleRefund(afterSaleRefundDO);
    }

    @Override
    public void addAfterSaleRefund(AfterSaleRefundDO refundDO) {
        //售后退款/退货信息入库
        afterSaleRefundMapper.insert(refundDO);
    }

    @Override
    public AfterSaleRefundDO getAfterSaleRefund(String serviceSn) {
        //创建查询条件包装器
        QueryWrapper<AfterSaleRefundDO> wrapper = new QueryWrapper<>();
        //以售后服务单号为查询条件
        wrapper.eq("service_sn", serviceSn);
        //返回根据售后服务单号查询出来的信息
        return afterSaleRefundMapper.selectOne(wrapper);
    }

    @Override
    public List<RefundRecordVO> exportExcel(RefundQueryParam param) {
        //新建查询条件包装器
        QueryWrapper<RefundDO> wrapper = this.createQueryWrapper(param);
        //获取要导出的退款单信息集合
        List<RefundDO> refundDOList = this.refundMapper.selectList(wrapper);

        //转换商品数据
        List<RefundRecordVO> recordVOList = new ArrayList<>();

        for (RefundDO refund : refundDOList) {
            RefundRecordVO recordVO = new RefundRecordVO(refund);
            recordVOList.add(recordVO);
        }

        return recordVOList;
    }

    /**
     * 初始化查询条件包装器
     *
     * @param param 查询条件参数
     * @return
     */
    private QueryWrapper<RefundDO> createQueryWrapper(RefundQueryParam param) {
        //新建查询条件包装器
        QueryWrapper<RefundDO> wrapper = new QueryWrapper<>();
        //以售后服务状态为正常状态作为查询条件
        wrapper.eq("disabled", DeleteStatusEnum.NORMAL.value());
        //如果会员ID不为空并且不等于0，则以会员ID为查询条件
        wrapper.eq(param.getMemberId() != null && param.getMemberId() != 0, "member_id", param.getMemberId());
        //如果商家ID不为空并且不等于0，则以商家ID为查询条件
        wrapper.eq(param.getSellerId() != null && param.getSellerId() != 0, "seller_id", param.getSellerId());
        //如果查询关键字不为空，以售后服务单号或订单编号或售后商品信息作为条件进行模糊查询
        wrapper.and(StringUtil.notEmpty(param.getKeyword()), ew -> {
            ew.like("sn", param.getKeyword()).or().like("order_sn", param.getKeyword()).or().like("goods_json", param.getKeyword());
        });
        //如果售后服务单号不为空，则以售后服务单号作为条件进行模糊查询
        wrapper.like(StringUtil.notEmpty(param.getServiceSn()), "sn", param.getServiceSn());
        //如果订单编号不为空，则以订单编号作为条件进行模糊查询
        wrapper.like(StringUtil.notEmpty(param.getOrderSn()), "order_sn", param.getOrderSn());
        //如果商品名称不为空，则以商品名称作为条件进行模糊查询
        wrapper.like(StringUtil.notEmpty(param.getGoodsName()), "goods_json", param.getGoodsName());
        //如果退款单状态不为空，则以退款单状态为查询条件
        wrapper.eq(StringUtil.notEmpty(param.getRefundStatus()), "refund_status", param.getRefundStatus());
        //如果退款方式不为空，则以退款方式为查询条件
        wrapper.eq(StringUtil.notEmpty(param.getRefundWay()), "refund_way", param.getRefundWay());
        //如果退款单创建时间-起始时间不为空也不等于0，则按退款单创建时间大于等于当前这个起始时间进行查询
        wrapper.ge(param.getStartTime() != null && param.getStartTime() != 0, "create_time", param.getStartTime());
        //如果退款单创建时间-结束时间不为空也不等于0，则按退款单创建时间小于等于当前这个结束时间进行查询
        wrapper.le(param.getEndTime() != null && param.getEndTime() != 0, "create_time", param.getEndTime());
        //如果退款单创建渠道不为空，则以退款单创建渠道为查询条件
        wrapper.eq(StringUtil.notEmpty(param.getCreateChannel()), "create_channel", param.getCreateChannel());
        //按照退款单创建时间倒序排序
        wrapper.orderByDesc("create_time");
        return wrapper;
    }

    /**
     * 填充退款账号信息
     *
     * @param afterSaleRefundDO
     * @param pluginId
     */
    private void fillAccountInfo(AfterSaleRefundDO afterSaleRefundDO, String pluginId) {
        //如果支付方式支持原路退回
        String accountType = null;

        if ("weixinPayPlugin".equals(pluginId)) {
            accountType = AccountTypeEnum.WEIXINPAY.value();
        } else if ("alipayDirectPlugin".equals(pluginId)) {
            accountType = AccountTypeEnum.ALIPAY.value();
        } else if ("unionpayPlugin".equals(pluginId)) {
            accountType = AccountTypeEnum.BANKTRANSFER.value();
        } else if ("chinapayPlugin".equals(pluginId)) {
            accountType = AccountTypeEnum.BANKTRANSFER.value();
        }

        afterSaleRefundDO.setAccountType(accountType);
        //退款方式为原路退回
        afterSaleRefundDO.setRefundWay(RefundWayEnum.ORIGINAL.value());
    }
}
