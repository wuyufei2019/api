package cn.shoptnt.service.datasync.receive.common;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.database.mybatisplus.base.BaseService;
import cn.shoptnt.model.datasync.dos.MessageReceiveDO;
import cn.shoptnt.model.datasync.dto.MessageReceiveDTO;
import cn.shoptnt.model.datasync.dto.MessageReceiveQueryParams;
import cn.shoptnt.model.datasync.vo.MessageReceiveVO;

/**
 * 消息接收业务层接口
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
public interface MessageReceiveManager extends BaseService<MessageReceiveDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<MessageReceiveVO> list(MessageReceiveQueryParams queryParams);

    /**
     * 接收一条商城消息
     *
     * @param type    消息类型
     * @param msgId   三方系统消息唯一id
     * @param msgInfo 消息内容
     * @param msgTime 消息时间
     */
    void receive(MessageReceiveDTO messageReceiveDTO);

    /**
     * 处理一条消息
     *
     * @param id
     */
    void handle(Long id);
}

