package cn.shoptnt.service.datasync.receive.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.database.mybatisplus.base.BaseServiceImpl;
import cn.shoptnt.framework.database.mybatisplus.util.BeanUtils;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.datasync.MessageReceiveMapper;
import cn.shoptnt.model.base.message.MessageReceiveMessage;
import cn.shoptnt.model.datasync.dos.MessageReceiveDO;
import cn.shoptnt.model.datasync.dto.MessageReceiveDTO;
import cn.shoptnt.model.datasync.dto.MessageReceiveQueryParams;
import cn.shoptnt.model.datasync.dto.Result;
import cn.shoptnt.model.datasync.enums.MessageReceiveStatusEnum;
import cn.shoptnt.model.datasync.enums.MessageReceiveTypeEnum;
import cn.shoptnt.model.datasync.vo.MessageReceiveVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 消息接收业务层实现
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@Service
public class MessageReceiveManagerImpl extends BaseServiceImpl<MessageReceiveMapper, MessageReceiveDO> implements MessageReceiveManager {

    @Autowired
    private DirectMessageSender directMessageSender;

    @Autowired(required = false)
    private List<MessageReceiveExecutor> executorList;

    private Map<MessageReceiveTypeEnum, MessageReceiveExecutor> executorMap;

    @PostConstruct
    private void init() {
        executorMap = executorList == null ? Collections.emptyMap() :
                executorList.stream().collect(Collectors.toMap(MessageReceiveExecutor::getType, Function.identity()));
    }

    @Override
    public WebPage<MessageReceiveVO> list(MessageReceiveQueryParams queryParams) {
        WebPage<MessageReceiveDO> webPage = baseMapper.selectPage(queryParams);
        return BeanUtils.toBean(webPage, MessageReceiveVO.class);
    }

    @Override
    public void receive(MessageReceiveDTO messageReceiveDTO) {
        MessageReceiveDO messageReceiveDO = BeanUtil.copyProperties(messageReceiveDTO, MessageReceiveDO.class);
        messageReceiveDO.setReceiveTime(DateUtil.getDateline());
        messageReceiveDO.setStatus(MessageReceiveStatusEnum.Wait);

        try {
            // 先消息入库，msgId字段使用唯一索引实现消息幂等
            super.save(messageReceiveDO);
        } catch (Exception e) {
            // 如果已存在该消息，则跳过
            if (StringUtil.isUniqueException(e)) {
                log.warn("接收到重复消息：" + messageReceiveDO.getMsgId());
                return;
            }
            throw e;
        }

        // 异步处理
        MessageReceiveMessage messageReceiveMessage = new MessageReceiveMessage();
        messageReceiveMessage.setId(messageReceiveDO.getId());
        directMessageSender.send(messageReceiveMessage);
    }

    @Override
    public void handle(Long id) {
        MessageReceiveDO messageReceiveDO = super.getById(id);
        if (messageReceiveDO.getStatus() == MessageReceiveStatusEnum.Success) {
            throw new ServiceException("处理成功的消息不能重复执行");
        }

        MessageReceiveExecutor messageExecutor = executorMap.get(messageReceiveDO.getType());
        if (messageExecutor == null) {
            throw new ServiceException(StrUtil.format("未找到消息执行器：{}", messageReceiveDO.getType()));
        }

        try {
            Result executeResult = messageExecutor.execute(messageReceiveDO.getContent());
            MessageReceiveStatusEnum status = executeResult.getSuccess() ? MessageReceiveStatusEnum.Success : MessageReceiveStatusEnum.Fail;
            baseMapper.updateStatus(messageReceiveDO.getId(), status, executeResult.getRemark());
        } catch (Exception e) {
            log.error("消息执行失败，id：" + messageReceiveDO.getId(), e);
            baseMapper.updateStatus(messageReceiveDO.getId(), MessageReceiveStatusEnum.Fail, StringUtil.formatException(e));
        }
    }

}

