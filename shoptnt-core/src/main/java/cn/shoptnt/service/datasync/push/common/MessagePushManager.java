package cn.shoptnt.service.datasync.push.common;


import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.database.mybatisplus.base.BaseService;
import cn.shoptnt.model.datasync.dos.MessagePushDO;
import cn.shoptnt.model.datasync.dto.MessagePushQueryParams;
import cn.shoptnt.model.datasync.enums.TargetSystemEnum;
import cn.shoptnt.model.datasync.vo.MessagePushVO;

/**
 * 消息推送业务层接口
 *
 * @author 张崧
 * @since 2023-12-18 16:24:10
 */
public interface MessagePushManager extends BaseService<MessagePushDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<MessagePushVO> list(MessagePushQueryParams queryParams);

    /**
     * 推送消息
     *
     * @param targetSystem 推送的系统
     * @param businessType 消息业务类型
     * @param content      消息内容
     * @param summary      摘要
     */
    void convertAndPush(TargetSystemEnum targetSystem, String businessType, Object content, String summary);

    /**
     * 扫描推送失败的消息并进行重试
     */
    void scanFailedAndRetry();

    /**
     * 推送消息（同步阻塞的）
     *
     * @param messagePushDO 消息
     */
    void pushSync(MessagePushDO messagePushDO);
}

