package cn.shoptnt.service.datasync.receive.common;


import cn.shoptnt.model.datasync.dto.Result;
import cn.shoptnt.model.datasync.enums.MessageReceiveTypeEnum;

/**
 * 接收消息的执行器
 *
 * @author 张崧
 * 2023-12-20
 */
public interface MessageReceiveExecutor {

    /**
     * 处理一条消息
     *
     * @param content 消息内容
     * @return 处理结果
     */
    Result execute(String content);

    /**
     * 类型
     * @return 类型
     */
    MessageReceiveTypeEnum getType();

}

