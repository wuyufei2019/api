package cn.shoptnt.service.datasync.push.common;


import cn.shoptnt.model.datasync.dos.MessagePushDO;
import cn.shoptnt.model.datasync.dto.Result;
import cn.shoptnt.model.datasync.enums.TargetSystemEnum;

/**
 * 消息推送策略
 *
 * @author 张崧
 * 2024-01-05
 */
public interface MessagePushStrategy {

    /**
     * 推送一条消息
     *
     * @param messagePushDO 消息内容
     * @return 推送结果
     */
    Result push(MessagePushDO messagePushDO);

    /**
     * 要推送的目标系统
     *
     * @return 目标系统
     */
    TargetSystemEnum targetSystem();

}

