/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.distribution.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import cn.shoptnt.mapper.distribution.DistributionMapper;
import cn.shoptnt.mapper.distribution.DistributionOrderMapper;
import cn.shoptnt.model.distribution.dos.DistributionOrderDO;
import cn.shoptnt.service.distribution.WithdrawCountManager;
import cn.shoptnt.framework.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * WithdrawCountManagerImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-15 上午8:43
 */
@Service
public class WithdrawCountManagerImpl implements WithdrawCountManager {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DistributionOrderMapper distributionOrderMapper;
    @Autowired
    private DistributionMapper distributionMapper;

    /**
     * 整理解冻金额
     */
    @Override
    public void withdrawCount() {
        try {
            Long currentData = DateUtil.getDateline();
            distributionMapper.updateCanRebate(currentData);
//            String sql = "UPDATE es_distribution distribution SET can_rebate = can_rebate + \n" +
//                    "IFNULL(( SELECT ( IFNULL( ( SELECT sum(disorder.grade1_rebate) FROM es_distribution_order disorder WHERE member_id_lv1 = distribution.member_id AND disorder.is_withdraw = 0 and disorder.settle_cycle <? ), 0 )+ IFNULL( ( SELECT sum(disorder.grade2_rebate) FROM es_distribution_order disorder  WHERE member_id_lv2 = distribution.member_id AND disorder.is_withdraw = 0 and disorder.settle_cycle <? ), 0 ) ) ), 0 )\n" +
//                    ",commission_frozen = commission_frozen-IFNULL(( SELECT ( IFNULL( ( SELECT sum(disorder.grade1_rebate) FROM es_distribution_order disorder WHERE member_id_lv1 = distribution.member_id AND disorder.is_withdraw = 0 and disorder.settle_cycle <? ), 0 )+ IFNULL( ( SELECT sum(disorder.grade2_rebate) FROM es_distribution_order disorder  WHERE member_id_lv2 = distribution.member_id AND disorder.is_withdraw = 0 and disorder.settle_cycle <? ), 0 ) ) ), 0 )\n";
//
//            this.daoSupport.execute(sql, currentData, currentData, currentData, currentData);


            //修改当前时间大于订单解冻日期的所有订单状态为已结算状态
            DistributionOrderDO distributionOrderDo = new DistributionOrderDO();
            UpdateWrapper<DistributionOrderDO> wrapper = new UpdateWrapper<>();
            wrapper.lt("settle_cycle",currentData);
            distributionOrderDo.setIsWithdraw(1);
            distributionOrderMapper.update(distributionOrderDo,wrapper);
        } catch (Exception e) {
            logger.error("每日将解锁金额自动添加到可提现金额异常：", e);
        }
    }
}
