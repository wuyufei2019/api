/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order.impl;

import cn.shoptnt.client.payment.PaymentClient;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import cn.shoptnt.model.errorcode.PaymentErrorCode;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.TradeDO;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;
import cn.shoptnt.service.trade.order.TradeQueryManager;
import cn.shoptnt.service.trade.order.plugin.PaymentServicePlugin;
import cn.shoptnt.model.payment.dto.PayParam;
import cn.shoptnt.service.trade.order.OrderPayManager;
import cn.shoptnt.framework.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 订单支付
 * @date 2018/4/1617:10
 * @description: 子业务发起支付  重构
 * @author: liuyulei
 * @create: 2019/12/27 18:15
 * @version:3.0
 * @since:7.1.5
 * @since v7.0.0
 */
@Service
public class OrderPayManagerImpl implements OrderPayManager {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private List<PaymentServicePlugin> paymentServicePlugin;

    @Autowired
    private PaymentClient paymentClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private TradeQueryManager tradeQueryManager;

    @Autowired
    private LockFactory lockFactory;

    /**
     * 支付
     *
     * @param param 支付使用参数
     * @return 支付结果
     */
    @Override
    public Map pay(PayParam param) {

        Lock lock = lockFactory.getLock(param.getSn());
        try {

            //在多线程的情况下 交易信息可能会出现 脏读的请况
            lock.lock();

            PaymentServicePlugin servicePlugin = this.findServicePlugin(param.getTradeType());
            //对订单状态进行检测，只有已确认的订单才可以进行支付
            boolean isLegitimate = servicePlugin.checkStatus(param.getSn(), 0);

            //判断订单状态是否已确认可以订单支付，否抛出异常
            if (!isLegitimate) {
                logger.error("[" + param.getSn() + "]该订单状态不正确，无法支付");
                throw new ServiceException(PaymentErrorCode.E506.code(), "该交易状态不正确，无法支付");
            }
            //获取订单|交易单价格
            Double orderPrice = servicePlugin.getPrice(param.getSn());
            //设置价格
            param.setPrice(orderPrice);
            // 对一个子订单发起支付
            Map map = paymentClient.pay(param);
            //交易方式名字
            String methodName = map.get("payment_method").toString();
            //判断为订单类型
            if (TradeTypeEnum.ORDER.name().equals(param.getTradeType())) {
                //查询订单。
                OrderDO order = orderClient.getOrder(param.getSn());
                //订单编号不为空，且预存款金额大于0
                if (order != null && order.getBalance() > 0) {
                    methodName = "预存款 + " + map.get("payment_method").toString();
                }
            }
            //判断为交易类型
            if (TradeTypeEnum.TRADE.name().equals(param.getTradeType())) {
                //查询交易
                TradeDO tradeDO = tradeQueryManager.getModel(param.getSn());
                //订单编号不为空，且预存款金额大于0
                if (tradeDO != null && tradeDO.getBalance() > 0) {
                    methodName = "预存款 + " + map.get("payment_method").toString();
                }
            }
            //更新订单或交易单支付方式
            servicePlugin.updatePaymentMethod(param.getSn(), param.getPaymentPluginId(), methodName);
            return map;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 支付成功调用
     *
     * @param tradeType     交易类型
     * @param subSn         业务单号
     * @param returnTradeNo 第三方平台回传单号（第三方平台的支付单号）
     * @param payPrice      支付金额
     */
    @Override
    public void paySuccess(String tradeType, String subSn, String returnTradeNo, Double payPrice) {
        //查询对应业务插件
        PaymentServicePlugin device = findServicePlugin(tradeType);

        if (device == null) {
            logger.error("支付回调失败，原因为：【" + tradeType + "没有适配回调器】");
            throw new ServiceException(PaymentErrorCode.E507.code(), "支付回调失败，原因为：【" + tradeType + "没有适配回调器】");
        }

        //调用回调器完成交易状态的变更
        device.paySuccess(subSn, returnTradeNo, payPrice);
    }

    /**
     * 在支付子业务插件中  找到对应业务插件
     *
     * @param tradeType
     * @return
     */
    private PaymentServicePlugin findServicePlugin(String tradeType) {
        for (PaymentServicePlugin plugin : paymentServicePlugin) {
            if (tradeType.equals(plugin.getServiceType())) {
                return plugin;
            }
        }
        return null;
    }
}
