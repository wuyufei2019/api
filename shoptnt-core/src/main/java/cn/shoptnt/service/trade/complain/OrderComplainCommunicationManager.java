/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.complain;

import cn.shoptnt.model.trade.complain.dos.OrderComplainCommunication;

import java.util.List;

/**
 * 交易投诉对话表业务层
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-29 10:46:34
 */
public interface OrderComplainCommunicationManager {

    /**
     * 查询交易投诉对话表列表
     *
     * @param complainId   交易投诉id
     * @return 交易投诉对话表列表
     */
    List<OrderComplainCommunication> list(long complainId);

    /**
     * 添加交易投诉对话表
     *
     * @param orderComplainCommunication 交易投诉对话表
     * @return OrderComplainCommunication 交易投诉对话表
     */
    OrderComplainCommunication add(OrderComplainCommunication orderComplainCommunication);

}
