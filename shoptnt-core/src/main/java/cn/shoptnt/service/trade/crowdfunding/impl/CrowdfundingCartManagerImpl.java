/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.crowdfunding.impl;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.trade.cart.enums.CartType;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.model.trade.cart.vo.CrowdfundingCartVO;
import cn.shoptnt.service.trade.cart.cartbuilder.CartBuilder;
import cn.shoptnt.service.trade.cart.cartbuilder.CartPriceCalculator;
import cn.shoptnt.service.trade.cart.cartbuilder.CartShipPriceCalculator;
import cn.shoptnt.service.trade.cart.cartbuilder.CartSkuRenderer;
import cn.shoptnt.service.trade.cart.cartbuilder.impl.DefaultCartBuilder;
import cn.shoptnt.service.trade.crowdfunding.CrowdfundingCartManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 众筹购物车业务类实现
 *
 * @author 张崧
 * @since 2024-06-21
 */
@Service
public class CrowdfundingCartManagerImpl implements CrowdfundingCartManager {

    @Autowired
    @Qualifier(value = "crowdfundingCartPriceCalculatorImpl")
    private CartPriceCalculator cartPriceCalculator;

    @Autowired
    @Qualifier(value = "crowdfundingCartSkuRenderer")
    private CartSkuRenderer crowdfundingCartSkuRenderer;

    @Autowired
    private CartShipPriceCalculator cartShipPriceCalculator;

    @Autowired
    private Cache cache;


    /**
     * 获取拼团购物车
     *
     * @return 购物车视图
     */
    @Override
    public CartView getCart() {
        //调用CartView生产流程线进行生产
        CartBuilder cartBuilder = new DefaultCartBuilder(CartType.Crowdfunding, crowdfundingCartSkuRenderer, null, cartPriceCalculator, null, cartShipPriceCalculator, null);

        //生产流程为：渲染sku->计算运费->计算价格->生成成品
        return cartBuilder.renderSku(CheckedWay.CART).countShipPrice().countPrice(false).build();
    }

    @Override
    public void buy(Long crowdfundingId, Integer num) {
        cache.put(getOriginKey(), new CrowdfundingCartVO(crowdfundingId, num));
    }

    @Override
    public void clean() {
        cache.remove(getOriginKey());
    }

    protected String getOriginKey() {
        return CachePrefix.CROWDFUNDING_CART.getPrefix() + UserContext.getBuyer().getUid();
    }
}
