/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.crowdfunding;

/**
 * 众筹订单业务类接口
 *
 * @author 张崧
 * @since 2024-06-22
 */
public interface CrowdfundingOrderManager {

    /**
     * 扫描待处理的众筹订单
     */
    void scanWaitHandle();
}
