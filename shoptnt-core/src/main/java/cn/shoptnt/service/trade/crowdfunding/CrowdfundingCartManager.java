/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.crowdfunding;

import cn.shoptnt.model.trade.cart.vo.CartView;

/**
 * 众筹购物车业务类接口
 *
 * @author 张崧
 * @since 2024-06-21
 */
public interface CrowdfundingCartManager {

    /**
     * 获取购物车
     *
     * @return 购物车视图
     */
    CartView getCart();

    /**
     * 加入到购物车
     *
     * @param crowdfundingId 众筹活动id
     * @param num            加入的数量
     */
    void buy(Long crowdfundingId, Integer num);

    /**
     * 清空购物车数据
     */
    void clean();
}
