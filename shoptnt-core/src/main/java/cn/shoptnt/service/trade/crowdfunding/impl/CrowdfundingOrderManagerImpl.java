/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.crowdfunding.impl;

import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.model.aftersale.enums.CreateChannelEnum;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingOrderStatus;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import cn.shoptnt.model.trade.cart.dos.OrderPermission;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.vo.CancelVO;
import cn.shoptnt.service.aftersale.AfterSaleManager;
import cn.shoptnt.service.promotion.crowdfunding.CrowdfundingManager;
import cn.shoptnt.service.trade.crowdfunding.CrowdfundingOrderManager;
import cn.shoptnt.service.trade.order.OrderOperateManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 众筹订单业务类实现
 *
 * @author 张崧
 * @since 2024-06-22
 */
@Service
@Slf4j
public class CrowdfundingOrderManagerImpl implements CrowdfundingOrderManager {

    @Autowired
    private CrowdfundingManager crowdfundingManager;

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private AfterSaleManager afterSaleManager;

    @Autowired
    private OrderOperateManager orderOperateManager;

    @Override
    public void scanWaitHandle() {
        // 查询未结算的活动
        List<Crowdfunding> crowdfundingList = crowdfundingManager.getWaitSettlementList();

        for (Crowdfunding crowdfunding : crowdfundingList) {
            CrowdfundingStatus crowdfundingStatus = CrowdfundingStatus.getStatus(crowdfunding);
            // 只需要处理这两个状态的众筹活动
            if (crowdfundingStatus != CrowdfundingStatus.Stop && crowdfundingStatus != CrowdfundingStatus.WaitSettlement) {
                continue;
            }

            // 查询活动下面待处理的众筹订单
            List<OrderDO> orderList = this.getWaitHandleList(crowdfunding.getId());

            // 活动终止后的订单处理
            if (crowdfundingStatus == CrowdfundingStatus.Stop) {
                handleStop(crowdfunding, orderList);
            }

            // 活动结束后的订单处理
            if (crowdfundingStatus == CrowdfundingStatus.WaitSettlement) {
                handleWaitSettlement(crowdfunding, orderList);
            }

            // 将活动改为结算完成
            crowdfundingManager.settlementComplete(crowdfunding.getId());
        }
    }

    private void handleStop(Crowdfunding crowdfunding, List<OrderDO> orderList) {
        for (OrderDO orderDO : orderList) {
            OrderStatusEnum orderStatus = OrderStatusEnum.valueOf(orderDO.getOrderStatus());

            // 未付款订单直接取消
            if (orderStatus == OrderStatusEnum.CONFIRM) {
                CancelVO cancelVO = new CancelVO(orderDO.getSn(), "众筹终止，系统自动取消", "系统");
                orderOperateManager.cancel(cancelVO, OrderPermission.client);
            }

            // 已付款的订单自动退款
            if (orderStatus == OrderStatusEnum.PAID_OFF) {
                afterSaleManager.autoCancel(CreateChannelEnum.CROWDFUNDING, orderDO.getSn(), "众筹终止，系统自动退款");
            }
        }

        // 更新订单众筹状态
        this.updateOrderCrowdfundingStatus(crowdfunding.getId(), CrowdfundingOrderStatus.Stop);
    }

    private void handleWaitSettlement(Crowdfunding crowdfunding, List<OrderDO> orderList) {
        // 拆分出未付款和已付款的订单
        List<OrderDO> notPayOrderList = orderList.stream().filter(orderDO -> orderDO.getOrderStatus().equals(OrderStatusEnum.CONFIRM.value())).collect(Collectors.toList());
        List<OrderDO> payOrderList = orderList.stream().filter(orderDO -> orderDO.getOrderStatus().equals(OrderStatusEnum.PAID_OFF.value())).collect(Collectors.toList());

        // 先将未付款的订单取消
        double cancelOrderPrice = 0.0;
        for (OrderDO orderDO : notPayOrderList) {
            CancelVO cancelVO = new CancelVO(orderDO.getSn(), "众筹结束，系统自动取消", "系统");
            orderOperateManager.cancel(cancelVO, OrderPermission.client);
            cancelOrderPrice = CurrencyUtil.add(cancelOrderPrice, CurrencyUtil.sub(orderDO.getOrderPrice(), orderDO.getShippingPrice()));
        }

        // 看一下是众筹成功还是众筹失败了
        double remainPrice = CurrencyUtil.add(crowdfunding.getRemainPrice(), cancelOrderPrice);
        boolean success = CrowdfundingStatus.isSuccess(remainPrice);

        // 如果众筹失败了，自动退款
        if (!success) {
            for (OrderDO orderDO : payOrderList) {
                afterSaleManager.autoCancel(CreateChannelEnum.CROWDFUNDING, orderDO.getSn(), "众筹失败，系统自动退款");
            }
        }

        // 更新订单众筹状态
        CrowdfundingOrderStatus status = success ? CrowdfundingOrderStatus.Success : CrowdfundingOrderStatus.Fail;
        this.updateOrderCrowdfundingStatus(crowdfunding.getId(), status);
    }

    private List<OrderDO> getWaitHandleList(Long crowdfundingId) {
        return orderQueryManager.lambdaQuery()
                .select(OrderDO::getSn, OrderDO::getOrderStatus, OrderDO::getOrderPrice, OrderDO::getShippingPrice, OrderDO::getGoodsNum)
                .eq(OrderDO::getCrowdfundingFlag, true)
                .eq(OrderDO::getCrowdfundingId, crowdfundingId)
                .in(OrderDO::getOrderStatus, OrderStatusEnum.CONFIRM.value(), OrderStatusEnum.PAID_OFF.value())
                .list();
    }

    private void updateOrderCrowdfundingStatus(Long crowdfundingId, CrowdfundingOrderStatus status) {
        orderQueryManager.lambdaUpdate().set(OrderDO::getCrowdfundingStatus, status).eq(OrderDO::getCrowdfundingId, crowdfundingId).update();
    }
}
