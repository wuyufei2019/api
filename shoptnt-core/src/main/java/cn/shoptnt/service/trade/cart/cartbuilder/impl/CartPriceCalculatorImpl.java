/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder.impl;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberLevelBenefit;
import cn.shoptnt.model.member.enums.MemberLevelBenefits;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.promotion.tool.vo.GiveGiftVO;
import cn.shoptnt.model.trade.cart.enums.CartType;
import cn.shoptnt.model.trade.cart.vo.*;
import cn.shoptnt.service.member.MemberLevelBenefitManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.trade.cart.CartPromotionManager;
import cn.shoptnt.service.trade.cart.cartbuilder.CartPriceCalculator;
import cn.shoptnt.service.trade.cart.cartbuilder.ScriptProcess;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by 妙贤 on 2018/12/10.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/10
 */
@Service(value = "cartPriceCalculator")
public class CartPriceCalculatorImpl implements CartPriceCalculator {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CartPromotionManager cartPromotionManager;

    @Autowired
    private ScriptProcess scriptProcess;

    @Autowired
    private MemberManager memberManager;

    @Autowired
    private MemberLevelBenefitManager memberLevelBenefitManager;


    @Override
    public PriceDetailVO countPrice(List<CartVO> cartList, Boolean includeCoupon) {

        //如果不包含优惠券计算，则将选中的优惠券信息删除
        if (!includeCoupon) {
            cartPromotionManager.cleanCoupon();
        }

        //根据规则计算价格
        PriceDetailVO priceDetailVO = this.countPriceWithScript(cartList, includeCoupon);

        return priceDetailVO;
    }


    /**
     * 根据促销脚本计算价格
     *
     * @param cartList      购物车列表
     * @param includeCoupon 是否包含优惠券
     * @return
     */
    private PriceDetailVO countPriceWithScript(List<CartVO> cartList, boolean includeCoupon) {

        PriceDetailVO price = new PriceDetailVO();
        //获取选中的促销活动
        SelectedPromotionVo selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        //用户选择的组合活动
        Map<Long, CartPromotionVo> groupPromotionMap = selectedPromotionVo.getGroupPromotionMap();
        //用户选择的单品活动
        Map<Long, List<CartPromotionVo>> singlePromotionMap = selectedPromotionVo.getSinglePromotionMap();
        //用户选择使用的优惠券
        Map<Long, CouponVO> couponMap = selectedPromotionVo.getCouponMap();
        // 查询会员等级的折扣优惠
        MemberLevelBenefit memberLevelDiscount = getMemberLevelDiscount();

        for (CartVO cart : cartList) {
            //未选中的购物车不参与计算
            if (cart.getChecked() == 0) {
                continue;
            }
            //获取当前购物车用户选择的组合活动
            CartPromotionVo groupPromotion = groupPromotionMap.get(cart.getSellerId());
            //获取当前购物车用户选择的单品活动
            List<CartPromotionVo> singlePromotions = singlePromotionMap.get(cart.getSellerId());

            PriceDetailVO cartPrice = new PriceDetailVO();
            cartPrice.setFreightPrice(cart.getPrice().getFreightPrice());
            List<CartSkuVO> groupSkuList = new ArrayList<>();
            for (CartSkuVO cartSku : cart.getSkuList()) {
                //未选中的商品不参与计算
                if (cartSku.getChecked() == 0) {
                    continue;
                }
                //计算会员等级折扣优惠
                this.calculatorMemberLevelDiscount(cartSku, memberLevelDiscount);
                //计算单品活动促销优惠
                this.calculatorSingleScript(cart, singlePromotions, cartPrice, cartSku);
                if (cartSku.getGroupList().contains(groupPromotion)) {
                    groupSkuList.add(cartSku);
                }
            }
            //计算满减优惠，返回是否免运费
            Boolean freeShipping = calculatorGroupScript(cart, groupPromotion, cartPrice, groupSkuList);

            //单品规则中有免运费或满减里有免运费
            if (freeShipping) {
                cartPrice.setIsFreeFreight(1);
                cartPrice.setFreightPrice(0D);
            }

            //是否计算优惠券优惠价格
            Long sellerId = cart.getSellerId();
            if (includeCoupon) {
                CouponVO couponVO = couponMap.get(sellerId);
                calculatorCoupon(cartPrice, couponVO);

            }

            //计算店铺商品总优惠金额
            double totalDiscount = CurrencyUtil.add(cartPrice.getCashBack(), cartPrice.getCouponPrice());
            cartPrice.setDiscountPrice(totalDiscount);

            //总价为商品价加运费
            double totalPrice = CurrencyUtil.add(cartPrice.getTotalPrice(), cartPrice.getFreightPrice());
            cartPrice.setTotalPrice(totalPrice);

            cart.setPrice(cartPrice);
            price = price.plus(cartPrice);

        }
        //是否计算优惠券价格 此处针对平台优惠券
        if (includeCoupon) {
            CouponVO couponVO = couponMap.get(0L);
            calculatorCoupon(price, couponVO);
        }

        logger.debug("计算完优惠后购物车数据为：", cartList);

        logger.debug("价格为：", price);

        return price;
    }

    private void calculatorMemberLevelDiscount(CartSkuVO cartSku, MemberLevelBenefit memberLevelDiscount) {
        cartSku.setMemberLevelDiscountPrice(0.0);
        if (memberLevelDiscount == null) {
            return;
        }

        // 折扣比例
        double discountRatio = CurrencyUtil.div(memberLevelDiscount.getBenefitValue(), 100);
        // 成交价格
        double purchaseTotalPrice = CurrencyUtil.mul(cartSku.getSubtotal(), discountRatio);
        // 会员等级优惠的价格
        double discountPrice = CurrencyUtil.sub(cartSku.getSubtotal(), purchaseTotalPrice);

        cartSku.setSubtotal(purchaseTotalPrice);
        cartSku.setMemberLevelDiscountPrice(discountPrice);
        cartSku.setPurchasePrice(CurrencyUtil.div(purchaseTotalPrice, cartSku.getNum()));
        cartSku.setActualPrice(cartSku.getPurchasePrice());
    }

    private MemberLevelBenefit getMemberLevelDiscount() {
        Member member = memberManager.getModel(UserContext.getBuyer().getUid());
        List<MemberLevelBenefit> benefitList = memberLevelBenefitManager.getByLevel(member.getLevelId());
        return benefitList.stream().filter(memberLevelBenefit -> memberLevelBenefit.getBenefitType() == MemberLevelBenefits.DISCOUNT).findFirst().orElse(null);
    }

    /**
     * 计算组合活动优惠
     *
     * @param cart           购物车
     * @param groupPromotion 组合活动
     * @param cartPrice      购物车价格VO
     * @param groupSkuList   参与组合活动的skuVO
     * @return 是否免运费  true 免运费   false不免运费
     */
    private Boolean calculatorGroupScript(CartVO cart, CartPromotionVo groupPromotion, PriceDetailVO cartPrice, List<CartSkuVO> groupSkuList) {
        Boolean freeShipping = false;
        if (groupPromotion != null && groupSkuList.size() > 0) {
            Double cost;
            String giftJson;
            Double totalGroupPrice = 0D;
            //sku列表 按小计正序排序 防止丢失精度
            List<CartSkuVO> collect = groupSkuList.stream().sorted(Comparator.comparing(CartSkuVO::getSubtotal)).collect(Collectors.toList());
            //所有sku小计总和
            double sum = groupSkuList.stream().mapToDouble(CartSkuVO::getSubtotal).sum();
            Map param = new HashedMap();
            param.put("$price", sum);
            //优惠后总金额
            cost = scriptProcess.countPrice(groupPromotion.getPromotionScript(), param);

            Double surplusFmPrice = 0D;
            //优惠金额
            Double diff = CurrencyUtil.sub(sum, cost);
            for (int i = 0; i < collect.size(); i++) {
                CartSkuVO cartSku = collect.get(i);
                if (i != collect.size() - 1) {
                    //优惠的金额  公式为S1 = X A m / (A m + B n)。此时注意是先优惠金额乘SKU的金额，再除以SKU总金额。
                    Double discount = CurrencyUtil.div(CurrencyUtil.mul(cartSku.getSubtotal(), diff), sum);
                    //实际总额
                    double total = CurrencyUtil.sub(cartSku.getSubtotal(), discount);
                    //商品实际支付单价
                    cartSku.setActualPrice(CurrencyUtil.div(total, cartSku.getNum()));
                    cartSku.setActualPayTotal(total);
                    //已使用优惠金额
                    surplusFmPrice = CurrencyUtil.add(surplusFmPrice, discount);
                } else {
                    //剩余优惠总额
                    Double surplus = CurrencyUtil.sub(diff, surplusFmPrice);
                    //实际总额
                    double actualPay = CurrencyUtil.sub(cartSku.getSubtotal(), surplus);
                    cartSku.setActualPayTotal(actualPay);
                    cartSku.setActualPrice(CurrencyUtil.div(actualPay, cartSku.getNum()));
                }
            }


            //获取赠品信息
            giftJson = scriptProcess.giveGift(groupPromotion.getPromotionScript(), param);
            //设置购物车赠品信息
            cart.setGiftJson(giftJson);
            //计算优惠前后差价
            Double diffPrice = CurrencyUtil.sub(sum, cost);
            //设置返现金额
            cartPrice.setCashBack(CurrencyUtil.add(cartPrice.getCashBack(), diffPrice));
            //设置满减优惠金额
            cartPrice.setFullMinus(diffPrice);
            //设置优惠后的金额
            cartPrice.setTotalPrice(CurrencyUtil.sub(cartPrice.getTotalPrice(), diffPrice));
            if (!StringUtil.isEmpty(giftJson)) {
                List<GiveGiftVO> giftList = JsonUtil.jsonToList(giftJson, GiveGiftVO.class);
                for (GiveGiftVO giveGiftVO : giftList) {
                    if ("freeShip".equals(giveGiftVO.getType())) {
                        freeShipping = (Boolean) giveGiftVO.getValue();
                    }
                }
            }
        }
        return freeShipping;
    }

    /**
     * 计算单品活动优惠
     *
     * @param cart             购物车
     * @param singlePromotions 参与的单品活动
     * @param cartPrice        当前购物车价格
     * @param cartSku          当前skuVO
     */
    private void calculatorSingleScript(CartVO cart, List<CartPromotionVo> singlePromotions, PriceDetailVO cartPrice, CartSkuVO cartSku) {
        Double cost;
        Integer point = 0;
        if (CartType.CHECKOUT.equals(cart.getCartType()) && cartSku.getChecked() == 0) {
            return;
        }

        CartPromotionVo promotionVo = null;
        if (singlePromotions != null) {
            for (CartPromotionVo cartPromotionVo : singlePromotions) {
                if (cartSku.getSkuId().equals(cartPromotionVo.getSkuId())) {
                    promotionVo = cartPromotionVo;
                    break;
                }
            }
        }
        //转换脚本参数
        ScriptSkuVO skuVO = new ScriptSkuVO(cartSku);
        Map param = new HashedMap();
        param.put("$sku", skuVO);
        //商品参与了用户选择的活动
        if (promotionVo != null) {
            //获取优惠后的价格
            cost = scriptProcess.countPrice(promotionVo.getPromotionScript(), param);
            //获取积分兑换所需积分
            logger.debug("用户选择参与的促销活动类型:" + promotionVo.getPromotionType());
            if (PromotionTypeEnum.EXCHANGE.name().equals(promotionVo.getPromotionType())) {
                point = scriptProcess.countPoint(promotionVo.getPromotionScript(), param);
            }
            //设置商品单品成交单价
            calculatorPurchasePrice(cartSku, cost, promotionVo.getPromotionType());

            cartSku.setPoint(point);
            cartSku.setSubtotal(cost);
            cartSku.setActualPrice(CurrencyUtil.div(cost, cartSku.getNum()));
            // 如果是单品立减，需要计算该活动减去了多少，并记录在 PriceDetailVO cartPrice中
            if (promotionVo.getPromotionType().equals(PromotionTypeEnum.MINUS.name())) {
                cartPrice.setMinusPrice(CurrencyUtil.sub(skuVO.get$totalPrice(), cost));
            }
        }

        //未选中的不计入合计中
        if (cartSku.getChecked() == 0) {
            return;
        }

        //购物车全部商品的原价合
        cartPrice.setOriginalPrice(CurrencyUtil.add(cartPrice.getOriginalPrice(), CurrencyUtil.mul(cartSku.getOriginalPrice(), cartSku.getNum())));

        //购物车所有小计合
        cartPrice.setGoodsPrice(CurrencyUtil.add(cartPrice.getGoodsPrice(), cartSku.getSubtotal()));

        //会员等级优惠累计
        cartPrice.setMemberLevelDiscountPrice(CurrencyUtil.add(cartPrice.getMemberLevelDiscountPrice(), cartSku.getMemberLevelDiscountPrice()));

        //购物车返现合
        cartPrice.setCashBack(CurrencyUtil.add(cartPrice.getCashBack(), CurrencyUtil.sub(CurrencyUtil.mul(cartSku.getOriginalPrice(), cartSku.getNum()), cartSku.getSubtotal())));
        cartPrice.setCashBack(CurrencyUtil.sub(cartPrice.getCashBack(), cartPrice.getMemberLevelDiscountPrice()));

        //购物车使用积分
        cartPrice.setExchangePoint(cartPrice.getExchangePoint() + cartSku.getPoint());

        //购物车小计
        cartPrice.setTotalPrice(CurrencyUtil.add(cartPrice.getTotalPrice(), cartSku.getSubtotal()));

        //累计商品重量
        double weight = CurrencyUtil.mul(cartSku.getGoodsWeight(), cartSku.getNum());
        double cartWeight = CurrencyUtil.add(cart.getWeight(), weight);
        cart.setWeight(cartWeight);
    }

    /**
     * 计算优惠后商品单价
     *
     * @param cartSku       购物车商品sku
     * @param cost          优惠后价格
     * @param promotionType 优惠活动类型
     */
    private void calculatorPurchasePrice(CartSkuVO cartSku, Double cost, String promotionType) {
        if (PromotionTypeEnum.EXCHANGE.name().equals(promotionType) ||
                PromotionTypeEnum.GROUPBUY.name().equals(promotionType) ||
                PromotionTypeEnum.SECKILL.name().equals(promotionType) ||
                PromotionTypeEnum.MINUS.name().equals(promotionType)) {
            cartSku.setPurchasePrice(CurrencyUtil.div(cost, cartSku.getNum()));
        }
    }

    /**
     * 计算优惠券优惠价格
     *
     * @param price    价格对象
     * @param couponVO 优惠券对象
     */
    private void calculatorCoupon(PriceDetailVO price, CouponVO couponVO) {
        Double totalGroupPrice;
        if (couponVO != null) {
            String couponScript = this.scriptProcess.readCouponScript(couponVO.getCouponId());
            Map param = new HashedMap();
            param.put("$price", price.getTotalPrice());
            if (couponScript != null) {
                totalGroupPrice = scriptProcess.countPrice(couponScript, param);
                if (totalGroupPrice >= 0) {
                    price.setCouponPrice(CurrencyUtil.sub(price.getTotalPrice(), totalGroupPrice));
                    price.setTotalPrice(totalGroupPrice);
                }
            }
        }
    }


}
