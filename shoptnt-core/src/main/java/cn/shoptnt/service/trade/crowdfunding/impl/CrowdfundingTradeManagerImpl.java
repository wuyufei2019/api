/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.crowdfunding.impl;

import cn.shoptnt.model.member.dos.MemberAddress;
import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.enums.OrderTypeEnum;
import cn.shoptnt.model.trade.order.vo.CheckoutParamVO;
import cn.shoptnt.model.trade.order.vo.OrderParam;
import cn.shoptnt.model.trade.order.vo.TradeParam;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import cn.shoptnt.service.trade.crowdfunding.CrowdfundingCartManager;
import cn.shoptnt.service.trade.order.OrderCenterManager;
import cn.shoptnt.service.trade.order.TradeCenterManager;
import cn.shoptnt.service.trade.order.TradeValidator;
import cn.shoptnt.service.trade.order.impl.DefaultTradeValidator;
import cn.shoptnt.service.trade.order.impl.TradeManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * 众筹交易业务类
 *
 * @author 张崧
 * @since 2024-06-22
 */
@Service
public class CrowdfundingTradeManagerImpl extends TradeManagerImpl {

    @Autowired
    private TradeCenterManager tradeCenterManager;

    @Autowired
    private OrderCenterManager orderCenterManager;

    @Autowired
    private CrowdfundingCartManager crowdfundingCartManager;

    /**
     * 创建订单并创建拼团订单
     *
     * @param client
     * @param pinTuanOrderId
     * @return
     */
    public TradeVO createTrade(String client) {

        //获取结算参数
        CheckoutParamVO param = checkoutParamManager.getParam();

        //获取购物车视图
        CartView cartView = crowdfundingCartManager.getCart();
        //获取收货地址
        MemberAddress memberAddress = this.memberAddressClient.getModel(param.getAddressId());

        //创建交易检测者
        TradeValidator tradeValidator = new DefaultTradeValidator(param, cartView, memberAddress, param.getPaymentType()).setTradeSnCreator(snCreator).setGoodsClient(goodsClient).setMemberClient(memberClient).setShippingManager(shippingManager).setCheckoutParamManager(checkoutParamManager);
        //检测配送范围==>检测商品的有效性
        tradeValidator.checkShipRange().checkGoods();

        TradeParam tradeParam = super.getTradeParam(client, param, cartView);

        //创建交易
        TradeVO tradeVO = tradeCenterManager.createTrade(tradeParam);

        // 循环购物车列表
        CartVO cartVO = cartView.getCartList().get(0);
        OrderParam orderParam = super.convertSku(tradeVO, null, cartVO);
        OrderDTO orderDTO = this.orderCenterManager.createOrder(orderParam);
        orderDTO.setCrowdfundingId(cartVO.getSkuList().get(0).getCrowdfundingId());

        tradeVO.setOrderList(Collections.singletonList(orderDTO));

        //交易入库
        tradeIntodbManager.intoDB(tradeVO);

        //清除已购买的商品购物车数据
        crowdfundingCartManager.clean();
        //清空备注信息
        this.checkoutParamManager.setRemark("");
        //清空发票信息
        this.checkoutParamManager.deleteReceipt();
        //清空客户端类型
        this.checkoutParamManager.setClientType("");

        //清空备注信息
        this.checkoutParamManager.setRemark("");
        //清空发票信息
        this.checkoutParamManager.deleteReceipt();
        //清空客户端类型
        this.checkoutParamManager.setClientType("");

        return tradeVO;
    }
}
