/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder.impl;

import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.model.trade.cart.vo.CartSkuVO;
import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.model.trade.cart.vo.PriceDetailVO;
import cn.shoptnt.service.trade.cart.cartbuilder.CartPriceCalculator;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 众筹价格计算
 *
 * @author 张崧
 * @since 2024-06-23
 */
@Service
public class CrowdfundingCartPriceCalculatorImpl implements CartPriceCalculator {

    @Override
    public PriceDetailVO countPrice(List<CartVO> cartList, Boolean includeCoupon) {

        PriceDetailVO priceDetailVO = new PriceDetailVO();

        for (CartVO cart : cartList) {
            PriceDetailVO cartPrice = new PriceDetailVO();
            cartPrice.setFreightPrice(cart.getPrice().getFreightPrice());
            for (CartSkuVO cartSku : cart.getSkuList()) {
                // 合计购物车金额
                cartPrice.setOriginalPrice(CurrencyUtil.add(cartPrice.getOriginalPrice(), cartSku.getSubtotal()));
                cartPrice.setGoodsPrice(CurrencyUtil.add(cartPrice.getGoodsPrice(), cartSku.getSubtotal()));

                //累计商品重量
                double weight = CurrencyUtil.mul(cartSku.getGoodsWeight(), cartSku.getNum());
                double cartWeight = CurrencyUtil.add(cart.getWeight(), weight);
                cart.setWeight(cartWeight);
            }

            // 总价为商品价加运费
            double totalPrice = CurrencyUtil.add(cartPrice.getGoodsPrice(), cartPrice.getFreightPrice());
            cartPrice.setTotalPrice(totalPrice);
            cart.setPrice(cartPrice);

            priceDetailVO = priceDetailVO.plus(cartPrice);
        }

        return priceDetailVO;
    }
}
