/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.crowdfunding.impl;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import cn.shoptnt.model.trade.cart.enums.CartType;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.cart.vo.CartSkuVO;
import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.model.trade.cart.vo.CrowdfundingCartVO;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.goods.GoodsSkuManager;
import cn.shoptnt.service.promotion.crowdfunding.CrowdfundingManager;
import cn.shoptnt.service.trade.cart.cartbuilder.CartSkuRenderer;
import cn.shoptnt.service.trade.cart.cartbuilder.impl.CartSkuFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 众筹购物车渲染器
 *
 * @author 张崧
 * @since 2024-06-21
 */
@Service
public class CrowdfundingCartSkuRenderer implements CartSkuRenderer {

    @Autowired
    private Cache cache;

    @Autowired
    private CrowdfundingManager crowdfundingManager;

    @Autowired
    private GoodsSkuManager goodsSkuManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Override
    public void renderSku(List<CartVO> cartList, CartType cartType, CheckedWay way) {
        CrowdfundingCartVO crowdfundingCartVO = cache.get(getOriginKey());
        if (crowdfundingCartVO == null) {
            throw new ServiceException("购物车为空");
        }
        Crowdfunding crowdfunding = crowdfundingManager.getById(crowdfundingCartVO.getCrowdfundingId());
        if (crowdfunding == null) {
            throw new ServiceException("众筹活动不存在");
        }
        CrowdfundingVO crowdfundingVO = new CrowdfundingVO(crowdfunding);
        if (crowdfundingVO.getStatus() != CrowdfundingStatus.Underway) {
            throw new ServiceException("众筹活动非进行中状态");
        }

        GoodsSkuVO goodsSku = this.goodsSkuManager.getSkuFromCache(crowdfunding.getSkuId());
        if (goodsSku == null) {
            throw new ServiceException(TradeErrorCode.E451.code(), "商品已失效");
        }

        GoodsDO goodsDO = goodsQueryManager.getModel(goodsSku.getGoodsId());
        if (goodsDO.getIsAuth() != 1 || goodsDO.getMarketEnable() != 1) {
            throw new ServiceException("商品已下架，不能购买");
        }

        // 读取sku的可用库存
        Integer enableQuantity = goodsSku.getEnableQuantity();
        if (enableQuantity < crowdfundingCartVO.getNum()) {
            throw new ServiceException(TradeErrorCode.E451.code(), "商品库存已不足，不能购买。");
        }

        CartSkuVO skuVO = new CartSkuVO();

        skuVO.setSellerId(goodsSku.getSellerId());
        skuVO.setSellerName(goodsSku.getSellerName());
        skuVO.setGoodsId(goodsSku.getGoodsId());
        skuVO.setSkuId(goodsSku.getSkuId());
        skuVO.setCatId(goodsSku.getCategoryId());
        skuVO.setGoodsImage(goodsSku.getThumbnail());
        skuVO.setName(goodsSku.getGoodsName());
        skuVO.setSkuSn(goodsSku.getSn());

        // 众筹成交价
        skuVO.setPurchasePrice(crowdfundingVO.getCurrPrice());

        // 众筹商品原始价格
        skuVO.setOriginalPrice(crowdfundingVO.getCurrPrice());

        skuVO.setSpecList(goodsSku.getSpecList());
        skuVO.setIsFreeFreight(goodsSku.getGoodsTransfeeCharge());
        skuVO.setGoodsWeight(goodsSku.getWeight());
        skuVO.setTemplateId(goodsSku.getTemplateId());
        skuVO.setEnableQuantity(goodsSku.getEnableQuantity());
        skuVO.setLastModify(goodsSku.getLastModify());
        skuVO.setNum(crowdfundingCartVO.getNum());
        skuVO.setChecked(1);
        skuVO.setGoodsType(goodsSku.getGoodsType());

        // 计算小计
        double subTotal = CurrencyUtil.mul(skuVO.getNum(), skuVO.getPurchasePrice());
        skuVO.setSubtotal(subTotal);

        skuVO.setCrowdfundingId(crowdfundingCartVO.getCrowdfundingId());

        CartVO cartVO = new CartVO(goodsSku.getSellerId(), goodsSku.getSellerName(), cartType);
        cartVO.setSkuList(Collections.singletonList(skuVO));

        cartList.add(cartVO);
    }

    @Override
    public void renderSku(List<CartVO> cartList, CartSkuFilter cartFilter, CartType cartType, CheckedWay way) {

        //创建一个临时的list
        List<CartVO> tempList = new ArrayList<>();

        //将临时的list渲染好
        renderSku(tempList, cartType, way);

        //进行过滤
        tempList.forEach(cartVO -> {

            cartVO.getSkuList().forEach(cartSkuVO -> {
                //如果过滤成功才继续
                if (!cartFilter.accept(cartSkuVO)) {
                    cartList.add(cartVO);
                }
            });

        });
    }

    protected String getOriginKey() {
        return CachePrefix.CROWDFUNDING_CART.getPrefix() + UserContext.getBuyer().getUid();
    }
}
