/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.model.trade.order.vo.TradeVO;

/**
 * 交易入库业务接口
 *
 * @author Snow create in 2018/5/9
 * @version v2.0
 * @since v7.0.0
 */
public interface TradeIntodbManager {

    /**
     * 入库处理
     * @param tradeVO 交易VO
     */
    void intoDB(TradeVO tradeVO);


}
