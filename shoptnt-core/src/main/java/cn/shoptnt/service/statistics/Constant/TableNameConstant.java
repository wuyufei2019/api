/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.Constant;


/**
 * 定义数据库表名基本常量
 * @author 张崧
 * @version v1.0
 * @date 2020-07-31
 * @since v7.2.2
 */
public interface TableNameConstant {
    //数据库表es_sss_order_goods_data的表名
    String ES_SSS_ORDER_GOODS_DATA = "es_sss_order_goods_data";

    //数据库表es_sss_order_data的表名
    String ES_SSS_ORDER_DATA = "es_sss_order_data";

    //数据库表es_sss_order_data的表名
    String ES_SSS_GOODS_DATA = "es_sss_goods_data";

    //数据库表es_sss_order_data的表名
    String ES_SSS_SHOP_DATA = "es_sss_shop_data";

    //数据库表es_sss_refund_data的表名
    String ES_SSS_REFUND_DATA = "es_sss_refund_data";

    //数据库表es_sss_shop_pv的表名
    String ES_SSS_SHOP_PV = "es_sss_shop_pv";

    //数据库表es_sss_goods_pv的表名
    String ES_SSS_GOODS_PV = "es_sss_goods_pv";
}
