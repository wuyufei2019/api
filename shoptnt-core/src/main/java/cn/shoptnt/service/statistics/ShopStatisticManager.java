/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;

/**
 * 店铺相关统计
 *
 * @author 张崧
 * @since 2024-04-24
 */
public interface ShopStatisticManager {

    /**
     * 热卖店铺按金额统计
     *
     * @param searchCriteria 搜索参数
     * @return 热卖店铺列表
     */
    WebPage getHotSalesMoneyPage(SearchCriteria searchCriteria);

}
