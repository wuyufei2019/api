/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.client.member.MemberAskClient;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.mapper.statistics.ShopPageViewMapper;
import cn.shoptnt.model.statistics.vo.ShopDashboardVO;
import cn.shoptnt.model.trade.order.vo.OrderStatusNumVO;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.statistics.DashboardStatisticManager;
import cn.shoptnt.service.trade.complain.OrderComplainManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * 仪表盘业务实现类
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/25 10:41
 */
@Service
public class DashboardStatisticManagerImpl implements DashboardStatisticManager {

    @Autowired
    private MemberAskClient memberAskClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private OrderComplainManager orderComplainManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private ShopPageViewMapper shopPageViewMapper;

    /**
     * 获取仪表盘数据
     *
     * @return 商家中心数据
     */
    @Override
    public ShopDashboardVO getShopData() {

        // 获取商家id
        long sellerId = UserContext.getSeller().getSellerId();

        // 返回值
        ShopDashboardVO shopDashboardVO = new ShopDashboardVO();

        //商品总数
        Long num = goodsClient.queryGoodsCountByParam(1, sellerId);

        // 出售中的商品数量
        String marketGoods = null == num ? "0" : num.toString();
        shopDashboardVO.setMarketGoods(null == marketGoods ? "0" : marketGoods);

        // 仓库待上架的商品数量
        num = goodsClient.queryGoodsCountByParam(0, sellerId);


        String pendingGoods = null == num ? "0" : num.toString();
        shopDashboardVO.setPendingGoods(null == pendingGoods ? "0" : pendingGoods);

        // 待处理买家咨询数量
        String pendingMemberAsk = this.memberAskClient.getNoReplyCount(sellerId).toString();
        shopDashboardVO.setPendingMemberAsk(null == pendingMemberAsk ? "0" : pendingMemberAsk);

        OrderStatusNumVO orderStatusNumVO = this.orderClient.getOrderStatusNum(null, sellerId);
        // 所有订单数量
        shopDashboardVO.setAllOrdersNum(null == orderStatusNumVO.getAllNum() ? "0" : orderStatusNumVO.getAllNum().toString());
        // 待付款订单数量
        shopDashboardVO.setWaitPayOrderNum(null == orderStatusNumVO.getWaitPayNum() ? "0" : orderStatusNumVO.getWaitPayNum().toString());
        // 待发货订单数量
        shopDashboardVO.setWaitShipOrderNum(null == orderStatusNumVO.getWaitShipNum() ? "0" : orderStatusNumVO.getWaitShipNum().toString());
        // 待收货订单数量
        shopDashboardVO.setWaitDeliveryOrderNum(null == orderStatusNumVO.getWaitRogNum() ? "0" : orderStatusNumVO.getWaitRogNum().toString());
        // 待处理申请售后订单数量
        shopDashboardVO.setAftersaleOrderNum(null == orderStatusNumVO.getRefundNum() ? "0" : orderStatusNumVO.getRefundNum().toString());
        // 待评价订单数量
        shopDashboardVO.setWaitCommentOrderCount(orderQueryManager.getWaitCommentOrderCount(sellerId));
        // 待处理交易投诉数量
        shopDashboardVO.setWaitHandleComplainCount(orderComplainManager.getWaitHandleCount(sellerId));
        // 库存预警数量
        shopDashboardVO.setWarningGoodsCount(goodsQueryManager.getWarningGoodsCount(sellerId));
        // 待审核商品数量
        shopDashboardVO.setWaitAuditGoodsCount(goodsQueryManager.getGoodsCountByParam(null, sellerId, 0, null, null));
        // 今日统计
        long startOfTodDay = DateUtil.startOfTodDay();
        long endOfTodDay = DateUtil.endOfTodDay();
        LocalDate localDate = LocalDate.now();
        shopDashboardVO.setTodayVisitCount(shopPageViewMapper.selectVisitCount(sellerId, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));
        shopDashboardVO.setTodayGoodsCount(goodsQueryManager.getGoodsCountByParam(null, sellerId, null, startOfTodDay, endOfTodDay));
        shopDashboardVO.setTodayOrderAmount(orderQueryManager.getTurnoverAmount(sellerId, startOfTodDay, endOfTodDay));
        shopDashboardVO.setTodayOrderCount(orderQueryManager.getCount(sellerId, startOfTodDay, endOfTodDay));

        return shopDashboardVO;
    }

}
