package cn.shoptnt.service.member;

import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import cn.shoptnt.model.member.enums.MemberLevelConditionType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditionsManager
 * @description 会员等级升级条件管理类
 * @program: api
 * 2024/5/20 21:20
 */
public interface MemberLevelUpgradeConditionManager extends IService<MemberLevelUpgradeCondition> {
    /**
     * 根据等级id查询升级条件
     *
     * @param levelId 等级id
     * @return 升级条件
     */
    List<MemberLevelUpgradeCondition> getByLevelId(Long levelId);

    /**
     * 根据等级id查询升级条件
     *
     * @return 升级条件
     */
    List<MemberLevelUpgradeCondition> getList();

    /**
     * 根据等级id删除升级条件
     *
     * @param levelId 等级id
     */
    void deleteUpgradeCondition(Long levelId);

}
