package cn.shoptnt.service.member.impl;

import cn.shoptnt.mapper.member.MemberLevelBenefitMapper;
import cn.shoptnt.model.member.dos.MemberLevelBenefit;
import cn.shoptnt.service.member.MemberLevelBenefitManager;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelBenefitManagerImpl
 * @description 会员等级礼物相关
 * @program: api
 * 2024/5/22 11:15
 */
@Service
public class MemberLevelBenefitManagerImpl extends ServiceImpl<MemberLevelBenefitMapper, MemberLevelBenefit> implements MemberLevelBenefitManager {


    @Override
    public List<MemberLevelBenefit> getByLevel(Long levelId) {
        return this.list(new LambdaQueryWrapper<MemberLevelBenefit>().eq(MemberLevelBenefit::getLevelId, levelId));
    }

    @Override
    public void deleteBenefit(Long levelId) {
        this.remove(new LambdaQueryWrapper<MemberLevelBenefit>().eq(MemberLevelBenefit::getLevelId, levelId));
    }
}
