package cn.shoptnt.service.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberLevel;
import cn.shoptnt.model.member.dto.MemberLevelCondition;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelManager
 * @description 会员等级业务类
 * @program: api
 * 2024/5/20 16:31
 */
public interface MemberLevelManager extends IService<MemberLevel> {


    /**
     * 获取会员等级数据
     *
     * @return
     */
    List<MemberLevel> list();

    /**
     * 添加会员等级
     *
     * @param memberLevelCondition 会员等级信息
     * @return
     */
    MemberLevel add(MemberLevelCondition memberLevelCondition);

    /**
     * 查询会员等级详情
     *
     * @param id 会员等级id
     * @return
     */
    MemberLevelCondition get(Long id);


    /**
     * 修改会员等级
     *
     * @param id                   会员等级id
     * @param memberLevelCondition 修改参数
     * @return
     */
    MemberLevelCondition edit(Long id, MemberLevelCondition memberLevelCondition);

    /**
     * 查询会员默认等级详情
     *
     * @return
     */
    MemberLevel getDefault();

    /**
     * 删除会员等级
     *
     * @param levelId         要删除的等级id
     * @param transferLevelId 删除的会员等级迁移到的等级
     */
    void deleteLevel(Long levelId, Long transferLevelId);

    /**
     * 迁移会员至新等级
     *
     * @param members    会员
     * @param newLevel 新等级
     */
    void transferMembersToNewLevel(List<Member> members, MemberLevel newLevel);

    /**
     * 会员等级排序
     *
     * @param id             重新排序的会员等级的id
     * @param targetPosition 拖动的会员等级放置的新位置
     */
    void updateSortOrder(Long id, Integer targetPosition);


    /**
     * 设置默认会员等级
     *
     * @param id 重新排序的会员等级的id
     */
    void assignDefaultLevel(Long id);

    /**
     * 修改会员等级
     *
     * @param memberId 会员id
     * @param levelId  等级id
     */
    void updateMemberLevel(Long memberId, Long levelId);

}
