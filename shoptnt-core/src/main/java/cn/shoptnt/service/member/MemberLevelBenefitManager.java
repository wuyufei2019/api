package cn.shoptnt.service.member;

import cn.shoptnt.model.member.dos.MemberLevelBenefit;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelBenefitManager
 * @description 等级赠送项
 * @program: api
 * 2024/5/22 11:12
 */
public interface MemberLevelBenefitManager extends IService<MemberLevelBenefit> {
    /**
     * 根据等级id查询福利详情
     *
     * @param levelId 等级id
     * @return 福利详情
     */
    List<MemberLevelBenefit> getByLevel(Long levelId);

    /**
     * 根据会员等级删除福利
     *
     * @param levelId 等级id
     */
    void deleteBenefit(Long levelId);
}
