package cn.shoptnt.service.member.impl;

import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.mapper.member.MemberLevelMapper;
import cn.shoptnt.mapper.member.MemberLevelUpgradeConditionMapper;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberLevel;
import cn.shoptnt.model.member.dos.MemberLevelBenefit;
import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import cn.shoptnt.model.member.dto.MemberLevelCondition;
import cn.shoptnt.model.member.enums.MemberLevelConditionType;
import cn.shoptnt.service.member.MemberLevelBenefitManager;
import cn.shoptnt.service.member.MemberLevelManager;
import cn.shoptnt.service.member.MemberLevelUpgradeConditionManager;
import cn.shoptnt.service.member.MemberManager;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelManagerImpl
 * @description 会员等级实现类
 * @program: api
 * 2024/5/20 16:32
 */
@Service
public class MemberLevelManagerImpl extends ServiceImpl<MemberLevelMapper, MemberLevel> implements MemberLevelManager {


    @Autowired
    private MemberLevelMapper memberLevelMapper;

    @Autowired
    private MemberLevelBenefitManager memberLevelBenefitManager;

    @Autowired
    private MemberLevelUpgradeConditionManager memberLevelUpgradeConditionManager;

    @Autowired
    private MemberManager memberManager;


    @Override
    public List<MemberLevel> list() {
        return memberLevelMapper.selectList(new LambdaQueryWrapper<MemberLevel>().orderByAsc(MemberLevel::getSortOrder));

    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public MemberLevel add(MemberLevelCondition memberLevelCondition) {

        MemberLevel memberLevel = new MemberLevel();
        memberLevel.setLevelIcon(memberLevelCondition.getLevelIcon());
        memberLevel.setDescription(memberLevelCondition.getDescription());
        memberLevel.setAllocationRatio(memberLevelCondition.getAllocationRatio());
        memberLevel.setCreateTime(DateUtil.getDateline());
        memberLevel.setLevelName(memberLevelCondition.getLevelName());
        //校验等级名称重复
        List<MemberLevel> levels = memberLevelMapper.selectList(new LambdaQueryWrapper<MemberLevel>().eq(MemberLevel::getLevelName, memberLevelCondition.getLevelName()));
        if (!levels.isEmpty()) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员等级名称重复");
        }
        //校验店铺分摊比例
        if (memberLevelCondition.getAllocationRatio() > 100) {
            throw new ServiceException(MemberErrorCode.E147.code(), "店铺分摊比例错误");
        }
        //第一个添加的等级为默认等级
        List<MemberLevel> memberLevels = memberLevelMapper.selectList(new LambdaQueryWrapper<MemberLevel>());
        if (memberLevels.isEmpty()) {
            memberLevel.setIsDefault(1);
            memberLevel.setSortOrder(0);
        }
        //处理排序字段
        int maxOrderSort = memberLevels.stream()
                .max(Comparator.comparingInt(MemberLevel::getSortOrder))
                .map(MemberLevel::getSortOrder)
                .orElse(0);
        memberLevel.setSortOrder(maxOrderSort + 1);
        //添加会员等级
        memberLevelMapper.insert(memberLevel);
        //添加会员升级条件
        if (memberLevelCondition.getUpgradeConditions().isEmpty()) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员升级条件缺失");
        }
        //查询所有升级条件 为了校验条件是否正确
        List<MemberLevelUpgradeCondition> levelUpgradeConditionCheck = memberLevelUpgradeConditionManager.list();
        levelUpgradeConditionCheck.addAll(memberLevelCondition.getUpgradeConditions());
        boolean isValid = this.validateConditions(levelUpgradeConditionCheck);
        if (!isValid) {
            throw new ServiceException(MemberErrorCode.E147.code(), "区间出现重叠");
        }
        //创建升级条件
        List<MemberLevelUpgradeCondition> levelUpgradeConditions = new ArrayList<>();
        for (MemberLevelUpgradeCondition memberLevelUpgradeCondition : memberLevelCondition.getUpgradeConditions()) {
            if (memberLevelUpgradeCondition.getConditionValueMin() >= memberLevelUpgradeCondition.getConditionValueMax()) {
                throw new ServiceException(MemberErrorCode.E147.code(), "区间填写错误，请检查后重新输入");
            }
            MemberLevelUpgradeCondition levelUpgradeCondition = new MemberLevelUpgradeCondition();
            levelUpgradeCondition.setConditionType(memberLevelUpgradeCondition.getConditionType());
            levelUpgradeCondition.setLevelId(memberLevel.getId());
            levelUpgradeCondition.setConditionValueMin(memberLevelUpgradeCondition.getConditionValueMin());
            levelUpgradeCondition.setConditionValueMax(memberLevelUpgradeCondition.getConditionValueMax());
            levelUpgradeCondition.setConditionOperator(memberLevelUpgradeCondition.getConditionOperator());
            levelUpgradeConditions.add(levelUpgradeCondition);
        }
        memberLevelUpgradeConditionManager.saveBatch(levelUpgradeConditions);
        //添加礼物项
        List<MemberLevelBenefit> memberLevelBenefits = new ArrayList<>();
        for (MemberLevelBenefit memberLevelBenefit : memberLevelCondition.getBenefits()) {
            memberLevelBenefit.setLevelId(memberLevel.getId());
            memberLevelBenefits.add(memberLevelBenefit);
        }
        memberLevelBenefitManager.saveBatch(memberLevelBenefits);
        return memberLevel;
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public MemberLevelCondition edit(Long id, MemberLevelCondition memberLevelCondition) {
        //查询会员等级
        MemberLevel memberLevel = this.getById(id);
        if (memberLevel == null) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员等级不存在!");
        }
        //校验名称是否重复
        List<MemberLevel> levels = memberLevelMapper.selectList(new LambdaQueryWrapper<MemberLevel>()
                .eq(MemberLevel::getLevelName, memberLevelCondition.getLevelName())
                .ne(MemberLevel::getId, id));
        if (!levels.isEmpty()) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员等级名称重复");
        }
        //校验店铺分摊比例
        if (memberLevelCondition.getAllocationRatio() > 100) {
            throw new ServiceException(MemberErrorCode.E147.code(), "店铺分摊比例错误");
        }

        BeanUtil.copyProperties(memberLevelCondition, memberLevel);
        this.updateById(memberLevel);
        //删除关联表 福利
        memberLevelBenefitManager.deleteBenefit(id);

        //添加 福利
        List<MemberLevelBenefit> memberLevelBenefits = new ArrayList<>();
        for (MemberLevelBenefit memberLevelBenefit : memberLevelCondition.getBenefits()) {
            memberLevelBenefit.setLevelId(id);
            memberLevelBenefits.add(memberLevelBenefit);
        }
        memberLevelBenefitManager.saveBatch(memberLevelBenefits);
        //删除关联表 升级条件
        memberLevelUpgradeConditionManager.deleteUpgradeCondition(id);
        //查询所有升级条件 为了校验条件是否正确
        List<MemberLevelUpgradeCondition> levelUpgradeConditionCheck = memberLevelUpgradeConditionManager.list();
        levelUpgradeConditionCheck.addAll(memberLevelCondition.getUpgradeConditions());
        boolean isValid = validateConditions(levelUpgradeConditionCheck);
        if (!isValid) {
            throw new ServiceException(MemberErrorCode.E147.code(), "区间填写错误，请检查后重新输入");
        }
        //创建升级条件
        List<MemberLevelUpgradeCondition> levelUpgradeConditions = new ArrayList<>();
        for (MemberLevelUpgradeCondition memberLevelUpgradeCondition : memberLevelCondition.getUpgradeConditions()) {
            if (memberLevelUpgradeCondition.getConditionValueMin() > memberLevelUpgradeCondition.getConditionValueMax()) {
                throw new ServiceException(MemberErrorCode.E147.code(), "区间填写错误，请检查后重新输入");
            }
            MemberLevelUpgradeCondition levelUpgradeCondition = new MemberLevelUpgradeCondition();
            levelUpgradeCondition.setConditionType(memberLevelUpgradeCondition.getConditionType());
            levelUpgradeCondition.setLevelId(id);
            levelUpgradeCondition.setConditionValueMin(memberLevelUpgradeCondition.getConditionValueMin());
            levelUpgradeCondition.setConditionValueMax(memberLevelUpgradeCondition.getConditionValueMax());
            levelUpgradeCondition.setConditionOperator(memberLevelUpgradeCondition.getConditionOperator());
            levelUpgradeConditions.add(levelUpgradeCondition);
        }
        memberLevelUpgradeConditionManager.saveBatch(levelUpgradeConditions);
        return memberLevelCondition;
    }

    @Override
    public MemberLevelCondition get(Long id) {
        //查询会员等级
        MemberLevel memberLevel = this.getById(id);
        if (memberLevel == null) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员等级不存在!");
        }
        MemberLevelCondition memberLevelCondition = new MemberLevelCondition();
        BeanUtil.copyProperties(memberLevel, memberLevelCondition);
        //查询会员升级条件
        List<MemberLevelUpgradeCondition> conditions = memberLevelUpgradeConditionManager.getByLevelId(id);
        if (!conditions.isEmpty()) {
            memberLevelCondition.setUpgradeConditions(conditions);
        }
        //查询会员礼物赠送
        List<MemberLevelBenefit> benefits = memberLevelBenefitManager.getByLevel(id);
        if (!benefits.isEmpty()) {
            memberLevelCondition.setBenefits(benefits);
        }
        return memberLevelCondition;
    }

    @Override
    public MemberLevel getDefault() {
        //这里使用list是为了避免出现数据错误（多个默认等级）
        List<MemberLevel> list = lambdaQuery().eq(MemberLevel::getIsDefault, 1).list();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteLevel(Long levelId, Long transferLevelId) {
        MemberLevel memberLevel = this.getById(levelId);
        if (memberLevel == null) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员等级不存在!");
        }
        if (memberLevel.getIsDefault().equals(1)) {
            throw new ServiceException(MemberErrorCode.E147.code(), "默认等级无法删除!");
        }
        //根据会员等级查询会员
        List<Member> members = memberManager.getByLevelId(levelId);

        // 检查是否有会员与该等级关联
        if (!members.isEmpty()) {
            // 如果有会员关联，那么transferLevelId必须有值
            if (transferLevelId == null) {
                throw new ServiceException(MemberErrorCode.E147.code(), "此等级已关联会员，需要选择迁移至的新等级");
            } else {
                MemberLevel level = this.getById(transferLevelId);
                if (level == null) {
                    throw new ServiceException(MemberErrorCode.E147.code(), "迁移处的新等级不存在!");
                }
                // 如果transferLevelId有值，执行会员转移逻辑
                transferMembersToNewLevel(members, level);
            }
        }
        //执行删除会员等级操作
        this.removeById(levelId);
        //删除后排序规则改变
        List<MemberLevel> memberLevels = this.list(new LambdaQueryWrapper<MemberLevel>());
        //重新排序
        deleteMemberLevel(memberLevels);
        //执行更改 将删除后的会员等级重新进行排序
        this.updateBatchById(memberLevels);

        //删除关联表 升级条件
        memberLevelBenefitManager.deleteBenefit(levelId);
        //删除关联表 福利
        memberLevelUpgradeConditionManager.deleteUpgradeCondition(levelId);

    }


    @Override
    public void transferMembersToNewLevel(List<Member> members, MemberLevel newLevel) {
        List<Member> newMembers = members.stream().map(member -> {
            member.setLevelId(newLevel.getId());
            member.setLevelIcon(newLevel.getLevelIcon());
            member.setLevelName(newLevel.getLevelName());
            return member;
        }).collect(Collectors.toList());
        //执行修改
        memberManager.updateBatchById(newMembers);
    }


    @Override
    public void updateSortOrder(Long id, Integer targetPosition) {
        List<MemberLevel> memberLevels = this.list(new LambdaQueryWrapper<MemberLevel>());
        //执行移动
        moveMemberLevel(memberLevels, id, targetPosition);
        //执行修改
        this.updateBatchById(memberLevels);
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void assignDefaultLevel(Long id) {
        MemberLevel memberLevel = this.getById(id);
        if (memberLevel == null) {
            throw new ServiceException(MemberErrorCode.E147.code(), "会员等级不存在!");
        }
        //修改所有等级为非默认
        List<MemberLevel> memberLevels = this.list(new LambdaQueryWrapper<MemberLevel>());
        memberLevels.stream().map(level -> {
            level.setIsDefault(0);
            return level;
        }).collect(Collectors.toList());
        this.updateBatchById(memberLevels);
        //修改当前会员等级为默认
        memberLevel.setIsDefault(1);
        this.updateById(memberLevel);
    }

    /**
     * 移动会员等级重新排序
     *
     * @param memberLevels 会员等级
     * @param levelId      等级id
     * @param newSortOrder 排序
     */
    private void moveMemberLevel(List<MemberLevel> memberLevels, Long levelId, Integer newSortOrder) {
        MemberLevel memberToMove = null;
        for (MemberLevel member : memberLevels) {
            if (member.getId().equals(levelId)) {
                memberToMove = member;
                break;
            }
        }
        if (memberToMove == null) {
            throw new ServiceException(MemberErrorCode.E147.code(), "参数错误!");
        }
        memberLevels.remove(memberToMove);
        memberLevels.add(newSortOrder, memberToMove);
        // 重新分配sortOrder
        for (int i = 0; i < memberLevels.size(); i++) {
            memberLevels.get(i).setSortOrder(i);
        }

    }

    /**
     * 删除某个会员重新排序
     *
     * @param memberLevels 会员等级
     */
    private void deleteMemberLevel(List<MemberLevel> memberLevels) {

        for (int i = 0; i < memberLevels.size(); i++) {
            memberLevels.get(i).setSortOrder(i);
        }
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void updateMemberLevel(Long memberId, Long levelId) {
        //查询会员信息
        Member member = memberManager.getModel(memberId);
        //查询当前会员的等级
        MemberLevel currentMemberLevel = this.getById(levelId);

        //查询会员等级
        MemberLevel memberLevel = this.getById(levelId);

        if (memberLevel != null && member != null) {
            //如果修改等级  降级情况下 此会员不允许在参与等级体系升级
            if (currentMemberLevel.getSortOrder() > memberLevel.getSortOrder()) {
                member.setIsUpgrade(-1);
            }
            member.setLevelId(memberLevel.getId());
            member.setLevelIcon(memberLevel.getLevelIcon());
            member.setLevelName(memberLevel.getLevelName());
            memberManager.updateById(member);
        }
    }

    /**
     * 校验区间正确性
     *
     * @param levelUpgradeConditions 升级条件
     * @return
     */
    public boolean validateConditions(List<MemberLevelUpgradeCondition> levelUpgradeConditions) {
        // 按照条件类型分组
        Map<MemberLevelConditionType, List<MemberLevelUpgradeCondition>> groupedConditions = levelUpgradeConditions.stream()
                .collect(Collectors.groupingBy(MemberLevelUpgradeCondition::getConditionType));

        // 对每个分组的条件进行校验
        for (Map.Entry<MemberLevelConditionType, List<MemberLevelUpgradeCondition>> entry : groupedConditions.entrySet()) {
            List<MemberLevelUpgradeCondition> conditionsForType = entry.getValue();
            // 按照最小值排序
            conditionsForType.sort(Comparator.comparingLong(MemberLevelUpgradeCondition::getConditionValueMin));

            // 遍历条件列表进行校验
            long previousMax = 0; // 初始化为0，假设没有条件的最大值是小于1的
            for (MemberLevelUpgradeCondition condition : conditionsForType) {
                // 如果当前条件的最小值小于或等于前一个条件的最大值，则校验失败
                if (condition.getConditionValueMin() <= previousMax) {
                    return false;
                }
                // 更新前一个条件的最大值
                previousMax = condition.getConditionValueMax();
            }
        }
        return true;
    }


}
