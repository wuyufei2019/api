package cn.shoptnt.service.member.impl;

import cn.shoptnt.mapper.member.MemberLevelUpgradeConditionMapper;
import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import cn.shoptnt.model.member.enums.MemberLevelConditionType;
import cn.shoptnt.service.member.MemberLevelUpgradeConditionManager;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditionsManagerImpl
 * @description 会员等级升级条件实现管理类
 * @program: api
 * 2024/5/20 21:23
 */
@Service
public class MemberLevelUpgradeConditionsManagerImpl extends ServiceImpl<MemberLevelUpgradeConditionMapper, MemberLevelUpgradeCondition> implements MemberLevelUpgradeConditionManager {


    @Override
    public List<MemberLevelUpgradeCondition> getByLevelId(Long levelId) {
        return this.list(new LambdaQueryWrapper<MemberLevelUpgradeCondition>().eq(MemberLevelUpgradeCondition::getLevelId, levelId));
    }

    @Override
    public List<MemberLevelUpgradeCondition> getList() {
        return this.list(new LambdaQueryWrapper<MemberLevelUpgradeCondition>());
    }

    @Override
    public void deleteUpgradeCondition(Long levelId) {
        this.remove(new LambdaQueryWrapper<MemberLevelUpgradeCondition>().eq(MemberLevelUpgradeCondition::getLevelId, levelId));
    }
}
