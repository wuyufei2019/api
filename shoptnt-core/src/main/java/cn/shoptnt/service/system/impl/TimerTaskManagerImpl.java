package cn.shoptnt.service.system.impl;

import cn.shoptnt.framework.trigger.Interface.TimerTaskData;
import cn.shoptnt.framework.trigger.standlone.TimerTaskDataSource;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.mapper.system.TimerTaskMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 延迟任务持久化实现
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/11/4 16:20
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class TimerTaskManagerImpl implements TimerTaskDataSource {

    private  final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private TimerTaskMapper timerTaskMapper;

    @Override
    public Boolean add(TimerTaskData timerTask) {
        return timerTaskMapper.insert(timerTask) == 1;
    }

    @Override
    public Boolean delete(String uniqueKey) {
        return timerTaskMapper.delete(new QueryWrapper<TimerTaskData>().eq("unique_key", uniqueKey)) == 1;
    }

    @Override
    public Boolean deleteInvalid() {
        //当前的秒数
        long now = DateUtil.getDateline();

        try {
            //删除触发时间小于当前时间的，即是失效的
            timerTaskMapper.delete(new QueryWrapper<TimerTaskData>().lt("trigger_time", now));
            return true;
        }catch (Exception e) {
            logger.error("删除失效任务失败", e);
            return false;
        }

    }


    @Override
    public Boolean update(String uniqueKey, TimerTaskData timerTask) {
        return timerTaskMapper.update(timerTask, new QueryWrapper<TimerTaskData>().eq("unique_key", uniqueKey)) == 1;

    }

    @Override
    public TimerTaskData getById(String id) {
        return timerTaskMapper.selectById(id);
    }

    @Override
    public List<TimerTaskData> getEnableTasks() {
        //当前的秒数
        long now = DateUtil.getDateline();

        //读取触发时间大于等于当前时间的
        return timerTaskMapper.selectList(new QueryWrapper<TimerTaskData>().ge("trigger_time", now));

    }


}