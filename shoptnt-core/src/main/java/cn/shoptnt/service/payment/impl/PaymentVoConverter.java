/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment.impl;

import cn.shoptnt.model.payment.vo.PaymentPluginVO;
import cn.shoptnt.service.payment.PaymentPluginManager;

/**
 * Payment vo转换器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/4/2
 */

public class PaymentVoConverter {

    /**
     * 通过插件转换vo
     * @param plugin
     * @return
     */
    public static PaymentPluginVO toValidatorPlatformVO(PaymentPluginManager plugin) {
        PaymentPluginVO vo = new PaymentPluginVO();
        vo.setMethodName(plugin.getPluginName());
        vo.setPluginId(plugin.getPluginId());
        vo.setIsRetrace(plugin.getIsRetrace());
        return vo;
    }


}
