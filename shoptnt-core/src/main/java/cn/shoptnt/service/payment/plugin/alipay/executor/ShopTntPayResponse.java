/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment.plugin.alipay.executor;
/**
 * shoptnt支付相应对象
 *
 * @author zh
 * @version v7.0
 * @date 18/7/19 下午4:47
 * @since v7.0
 */

import com.alipay.api.AlipayResponse;
import cn.shoptnt.model.payment.vo.Form;

public class ShopTntPayResponse extends AlipayResponse {
    /**
     * 组织好数据结构的表单信息
     */
    private Form form;


    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    @Override
    public String toString() {
        return "ShopTntPayResponse{" +
                "form=" + form +
                '}';
    }
}
