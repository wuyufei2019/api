package cn.shoptnt.service.erp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * MagicErp相关配置
 *
 * @author 张崧
 * @since 2024-04-01
 */
@Configuration
@ConfigurationProperties(prefix = "magic-erp")
public class MagicErpConfig {

    /**
     * 接口地址
     */
    private String httpAddress;

    /**
     * MagicErp提供的客户端id
     */
    private String clientId;

    /**
     * MagicErp提供的客户端密钥
     */
    private String clientSecret;

    /**
     * 签名密钥
     */
    private String signSecret;

    public String getHttpAddress() {
        return httpAddress;
    }

    public void setHttpAddress(String httpAddress) {
        this.httpAddress = httpAddress;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getSignSecret() {
        return signSecret;
    }

    public void setSignSecret(String signSecret) {
        this.signSecret = signSecret;
    }
}
