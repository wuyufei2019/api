package cn.shoptnt.service.erp;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.digest.HMac;
import cn.hutool.crypto.digest.HmacAlgorithm;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.erp.ApiParams;
import cn.shoptnt.model.erp.MagicErpApiEnum;
import cn.shoptnt.model.erp.ThirdApiResponse;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * MagicErp HTTP客户端
 *
 * @author 张崧
 * @since 2024-04-01
 */
@Component
public class MagicErpHttpClint {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MagicErpConfig magicErpConfig;

    @Autowired
    private Cache cache;

    public ThirdApiResponse execute(MagicErpApiEnum apiEnum, ApiParams apiParams) {
        String paramsJson = apiParams.toJson();
        logger.debug("【请求】MagicErp接口：{}，参数：{}", apiEnum.getUrl(), paramsJson);

        HttpResponse httpResponse = HttpUtil.createPost(magicErpConfig.getHttpAddress() + apiEnum.getUrl())
                .header("Authorization", "Bearer " + getTokenFromCache())
                .body(paramsJson)
                .execute();
        String body = httpResponse.body();

        logger.debug("【响应】MagicErp接口返回：{}", body);

        ThirdApiResponse thirdApiResponse = new ThirdApiResponse();
        if (httpResponse.isOk()) {
            thirdApiResponse.setSuccess(true);
            thirdApiResponse.setData(body);
        } else {
            JSONObject jsonObject = JSON.parseObject(body);
            thirdApiResponse.setSuccess(false);
            thirdApiResponse.setCode(jsonObject.getString("code"));
            thirdApiResponse.setMsg(jsonObject.getString("message"));
        }

        return thirdApiResponse;
    }

    private String getTokenFromCache() {
        String token = (String) cache.get(CachePrefix.MAGIC_ERP_TOKEN.getPrefix());
        if (StringUtil.isEmpty(token)) {
            token = getTokenAndPutCache();
        }
        return token;
    }

    private String getTokenAndPutCache() {
        long timestamp = System.currentTimeMillis();
        ApiParams params = new ApiParams();
        params.put("grant_type", "client_credentials");
        params.put("timestamp", timestamp);
        params.put("client_id", magicErpConfig.getClientId());
        params.put("sign", createSign(timestamp));

        HttpResponse httpResponse = HttpUtil
                .createGet(magicErpConfig.getHttpAddress() + "/oauth2/token")
                .form(params.getParams())
                .execute();

        String body = httpResponse.body();
        logger.debug("获取MagicErp token，code：{}，body：{}", httpResponse.getStatus(), body);

        JSONObject jsonObject = JSON.parseObject(body);
        if (!httpResponse.isOk()) {
            throw new ServiceException("获取magic-erp token失败：" + body);
        }

        String accessToken = jsonObject.getString("access_token");
        int expiresIn = jsonObject.getIntValue("expires_in");
        cache.put(CachePrefix.MAGIC_ERP_TOKEN.getPrefix(), accessToken, expiresIn - 600);

        return accessToken;
    }

    private String createSign(long timestamp) {
        String signContent = timestamp + magicErpConfig.getClientId() + timestamp;
        HMac mac = new HMac(HmacAlgorithm.HmacSHA256, magicErpConfig.getClientSecret().getBytes());
        return Base64.encode(mac.digest(signContent));
    }

}
