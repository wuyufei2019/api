package cn.shoptnt.service.promotion;

import cn.shoptnt.framework.database.mybatisplus.base.BaseService;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingDTO;
import cn.shoptnt.model.promotion.crowdfunding.query.CrowdfundingQueryParams;
import cn.shoptnt.framework.database.WebPage;
import java.util.List;

/**
 * 众筹活动业务层接口
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
public interface CrowdfundingManager extends BaseService<Crowdfunding> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<CrowdfundingVO> list(CrowdfundingQueryParams queryParams);

    /**
     * 添加
     * @param crowdfundingDTO 新增数据
     */
    void add(CrowdfundingDTO crowdfundingDTO);

    /**
     * 编辑
     * @param crowdfundingDTO 更新数据
     */
    void edit(CrowdfundingDTO crowdfundingDTO);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    CrowdfundingVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 终止
     * @param ids
     */
    void stop(List<Long> ids);
}

