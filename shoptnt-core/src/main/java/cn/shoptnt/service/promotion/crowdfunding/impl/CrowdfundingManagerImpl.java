package cn.shoptnt.service.promotion.crowdfunding.impl;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.database.mybatisplus.base.BaseServiceImpl;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.mapper.promotion.CrowdfundingMapper;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.promotion.crowdfunding.allowable.CrowdfundingAllowable;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingDTO;
import cn.shoptnt.model.promotion.crowdfunding.query.CrowdfundingQueryParams;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import cn.shoptnt.service.goods.GoodsSkuManager;
import cn.shoptnt.service.promotion.crowdfunding.CrowdfundingManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 众筹活动业务层实现
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Service
public class CrowdfundingManagerImpl extends BaseServiceImpl<CrowdfundingMapper, Crowdfunding> implements CrowdfundingManager {

    @Autowired
    private GoodsSkuManager goodsSkuManager;

    @Override
    public WebPage<CrowdfundingVO> list(CrowdfundingQueryParams queryParams) {
        return baseMapper.selectPage(queryParams);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(CrowdfundingDTO crowdfundingDTO) {
        // 校验参数
        checkParams(crowdfundingDTO);

        // 保存
        Crowdfunding crowdfunding = new Crowdfunding(crowdfundingDTO);
        this.save(crowdfunding);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(CrowdfundingDTO crowdfundingDTO) {
        Crowdfunding old = this.getById(crowdfundingDTO.getId());
        if (!new CrowdfundingAllowable(old).getEdit()) {
            throw new ServiceException("当前状态不允许进行编辑操作");
        }
        // 校验参数
        checkParams(crowdfundingDTO);

        // 更新
        Crowdfunding crowdfunding = new Crowdfunding(crowdfundingDTO);
        this.updateById(crowdfunding);
    }

    @Override
    public CrowdfundingVO getDetail(Long id) {
        Crowdfunding crowdfunding = getById(id);
        if (crowdfunding == null) {
            return null;
        }
        GoodsSkuDO goodsSkuDO = goodsSkuManager.getModel(crowdfunding.getSkuId());
        return new CrowdfundingVO(crowdfunding, goodsSkuDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        List<Crowdfunding> crowdfundingList = listByIds(ids);
        for (Crowdfunding crowdfunding : crowdfundingList) {
            if (!new CrowdfundingAllowable(crowdfunding).getDelete()) {
                throw ServiceException.format("【{}】不允许进行删除操作", crowdfunding.getName());
            }
        }
        this.removeBatchByIds(ids);
    }

    @Override
    public void stop(List<Long> ids) {
        List<Crowdfunding> crowdfundingList = listByIds(ids);
        for (Crowdfunding crowdfunding : crowdfundingList) {
            if (!new CrowdfundingAllowable(crowdfunding).getStop()) {
                throw ServiceException.format("【{}】不允许进行终止操作", crowdfunding.getName());
            }
        }
        lambdaUpdate().set(Crowdfunding::getStopFlag, true).in(Crowdfunding::getId, ids).update();
    }

    @Override
    public void updateData(Long id, Double goodsPrice, Integer goodsNum, Long peopleCount) {
        Crowdfunding crowdfunding = getById(id);
        lambdaUpdate()
                .set(Crowdfunding::getAlreadyPrice, CurrencyUtil.add(crowdfunding.getAlreadyPrice(), goodsPrice))
                .set(Crowdfunding::getAlreadyGoodsCount, crowdfunding.getAlreadyGoodsCount() + goodsNum)
                .set(Crowdfunding::getRemainPrice, CurrencyUtil.sub(crowdfunding.getRemainPrice(), goodsPrice))
                .set(Crowdfunding::getAlreadyPeopleCount, peopleCount)
                .eq(Crowdfunding::getId, id)
                .update();
    }

    @Override
    public List<Crowdfunding> getWaitSettlementList() {
        return lambdaQuery().eq(Crowdfunding::getSettlementFlag, false).list();
    }

    @Override
    public void settlementComplete(Long id) {
        lambdaUpdate().set(Crowdfunding::getSettlementFlag, true).eq(Crowdfunding::getId, id).update();
    }

    @Override
    public void updateEndShowFlag(List<Long> ids, Boolean flag) {
        lambdaUpdate().set(Crowdfunding::getEndShowFlag, flag).in(Crowdfunding::getId, ids).update();
    }

    private void checkParams(CrowdfundingDTO crowdfundingDTO) {
        Double targetPrice = CurrencyUtil.mul(crowdfundingDTO.getTotalPrice(), crowdfundingDTO.getPriceRatio() / 100);
        if (targetPrice == 0) {
            throw new ServiceException("众筹成功的金额不能为0");
        }
        crowdfundingDTO.setTargetPrice(targetPrice);

        GoodsSkuDO goodsSkuDO = goodsSkuManager.getModel(crowdfundingDTO.getSkuId());
        if (goodsSkuDO == null) {
            throw new ServiceException("skuId不存在");
        }
        crowdfundingDTO.setGoodsSkuDO(goodsSkuDO);
    }
}

