package cn.shoptnt.service.promotion.crowdfunding;

import cn.shoptnt.framework.database.mybatisplus.base.BaseService;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.vo.CrowdfundingVO;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingDTO;
import cn.shoptnt.model.promotion.crowdfunding.query.CrowdfundingQueryParams;
import cn.shoptnt.framework.database.WebPage;
import java.util.List;

/**
 * 众筹活动业务层接口
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
public interface CrowdfundingManager extends BaseService<Crowdfunding> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<CrowdfundingVO> list(CrowdfundingQueryParams queryParams);

    /**
     * 添加
     * @param crowdfundingDTO 新增数据
     */
    void add(CrowdfundingDTO crowdfundingDTO);

    /**
     * 编辑
     * @param crowdfundingDTO 更新数据
     */
    void edit(CrowdfundingDTO crowdfundingDTO);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    CrowdfundingVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 终止
     * @param ids
     */
    void stop(List<Long> ids);

    /**
     * 更新众筹活动数据
     *
     * @param id
     * @param goodsPrice
     * @param goodsNum
     * @param peopleCount
     */
    void updateData(Long id, Double goodsPrice, Integer goodsNum, Long peopleCount);

    /**
     * 查询未结算的活动
     * @return
     */
    List<Crowdfunding> getWaitSettlementList();

    /**
     * 结算完成
     * @param id
     */
    void settlementComplete(Long id);

    /**
     * 修改活动结束是否展示
     * @param ids
     * @param flag
     */
    void updateEndShowFlag(List<Long> ids, Boolean flag);
}

