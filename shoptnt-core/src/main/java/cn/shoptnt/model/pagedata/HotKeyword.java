/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.pagedata;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;


/**
 * 热门关键字实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-04 10:43:23
 */
@TableName("es_hot_keyword")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class HotKeyword implements Serializable {

    private static final long serialVersionUID = 9828142765402249L;

    /**
     * 主键ID
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 文字内容
     */
    @Schema(name = "hot_name", description = "文字内容", required = true)
    @NotEmpty(message = "关键字不能为空")
    private String hotName;
    /**
     * 排序
     */
    @Schema(name = "sort", description = "排序")
    private Integer sort;

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHotName() {
        return hotName;
    }

    public void setHotName(String hotName) {
        this.hotName = hotName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HotKeyword that = (HotKeyword) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (hotName != null ? !hotName.equals(that.hotName) : that.hotName != null) {
            return false;
        }
        return sort != null ? sort.equals(that.sort) : that.sort == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (hotName != null ? hotName.hashCode() : 0);
        result = 31 * result + (sort != null ? sort.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HotKeyword{" +
                "id=" + id +
                ", hotName='" + hotName + '\'' +
                ", sort=" + sort +
                '}';
    }


}
