/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.statistics.vo;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

/**
 * 商家中心，首页统计数据
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/25 10:19
 */
@Schema(description = "商家中心，首页统计数据")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShopDashboardVO {

    // ================= 总量统计 =================

    @Schema(description = "出售中的商品")
    private String marketGoods;

    @Schema(description = "待上架的商品")
    private String pendingGoods;

    @Schema(description = "订单总数")
    private String allOrdersNum;

    // ================= 待办统计 =================

    @Schema(description =  "待处理买家咨询")
    private String pendingMemberAsk;

    @Schema(description =  "待付款订单数")
    private String waitPayOrderNum;

    @Schema(description =  "待发货订单数")
    private String waitShipOrderNum;

    @Schema(description =  "待收货订单数")
    private String waitDeliveryOrderNum;

    @Schema(description =  "待处理售后订单数")
    private String afterSaleOrderNum;

    @Schema(name = "wait_comment_order_count", description = "待评价订单数量")
    private Long waitCommentOrderCount;

    @Schema(name = "wait_handle_complain_count", description = "待处理交易投诉数量")
    private Long waitHandleComplainCount;

    @Schema(name = "warning_goods_count", description = "库存预警商品数量")
    private Long warningGoodsCount;

    @Schema(name = "wait_audit_goods_count", description = "待审核商品数量")
    private Long waitAuditGoodsCount;

    // ================= 今日统计 =================

    @Schema(name = "today_visit_count", description = "今日访客数量")
    private Integer todayVisitCount;

    @Schema(name = "today_order_count", description = "今日订单数")
    private Long todayOrderCount;

    @Schema(name = "today_order_amount", description = "今日交易额")
    private Double todayOrderAmount;

    @Schema(name = "today_goods_count", description = "今日新增商品数量")
    private Long todayGoodsCount;

    public String getMarketGoods() {
        return marketGoods;
    }

    public void setMarketGoods(String marketGoods) {
        this.marketGoods = marketGoods;
    }

    public String getPendingGoods() {
        return pendingGoods;
    }

    public void setPendingGoods(String pendingGoods) {
        this.pendingGoods = pendingGoods;
    }

    public String getPendingMemberAsk() {
        return pendingMemberAsk;
    }

    public void setPendingMemberAsk(String buyerMessage) {
        this.pendingMemberAsk = buyerMessage;
    }

    public String getAllOrdersNum() {
        return allOrdersNum;
    }

    public void setAllOrdersNum(String allOrdersNum) {
        this.allOrdersNum = allOrdersNum;
    }

    public String getWaitPayOrderNum() {
        return waitPayOrderNum;
    }

    public void setWaitPayOrderNum(String waitPayOrderNum) {
        this.waitPayOrderNum = waitPayOrderNum;
    }

    public String getWaitShipOrderNum() {
        return waitShipOrderNum;
    }

    public void setWaitShipOrderNum(String waitShipOrderNum) {
        this.waitShipOrderNum = waitShipOrderNum;
    }

    public String getWaitDeliveryOrderNum() {
        return waitDeliveryOrderNum;
    }

    public void setWaitDeliveryOrderNum(String waitDeliveryOrderNum) {
        this.waitDeliveryOrderNum = waitDeliveryOrderNum;
    }

    public String getAfterSaleOrderNum() {
        return afterSaleOrderNum;
    }

    public void setAftersaleOrderNum(String afterSaleOrderNum) {
        this.afterSaleOrderNum = afterSaleOrderNum;
    }

    public void setAfterSaleOrderNum(String afterSaleOrderNum) {
        this.afterSaleOrderNum = afterSaleOrderNum;
    }

    public Long getWaitCommentOrderCount() {
        return waitCommentOrderCount;
    }

    public void setWaitCommentOrderCount(Long waitCommentOrderCount) {
        this.waitCommentOrderCount = waitCommentOrderCount;
    }

    public Long getWaitHandleComplainCount() {
        return waitHandleComplainCount;
    }

    public void setWaitHandleComplainCount(Long waitHandleComplainCount) {
        this.waitHandleComplainCount = waitHandleComplainCount;
    }

    public Long getWarningGoodsCount() {
        return warningGoodsCount;
    }

    public void setWarningGoodsCount(Long warningGoodsCount) {
        this.warningGoodsCount = warningGoodsCount;
    }

    public Long getWaitAuditGoodsCount() {
        return waitAuditGoodsCount;
    }

    public void setWaitAuditGoodsCount(Long waitAuditGoodsCount) {
        this.waitAuditGoodsCount = waitAuditGoodsCount;
    }

    public Integer getTodayVisitCount() {
        return todayVisitCount;
    }

    public void setTodayVisitCount(Integer todayVisitCount) {
        this.todayVisitCount = todayVisitCount;
    }

    public Long getTodayOrderCount() {
        return todayOrderCount;
    }

    public void setTodayOrderCount(Long todayOrderCount) {
        this.todayOrderCount = todayOrderCount;
    }

    public Double getTodayOrderAmount() {
        return todayOrderAmount;
    }

    public void setTodayOrderAmount(Double todayOrderAmount) {
        this.todayOrderAmount = todayOrderAmount;
    }

    public Long getTodayGoodsCount() {
        return todayGoodsCount;
    }

    public void setTodayGoodsCount(Long todayGoodsCount) {
        this.todayGoodsCount = todayGoodsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShopDashboardVO that = (ShopDashboardVO) o;
        return Objects.equals(marketGoods, that.marketGoods) &&
                Objects.equals(pendingGoods, that.pendingGoods) &&
                Objects.equals(pendingMemberAsk, that.pendingMemberAsk) &&
                Objects.equals(allOrdersNum, that.allOrdersNum) &&
                Objects.equals(waitPayOrderNum, that.waitPayOrderNum) &&
                Objects.equals(waitShipOrderNum, that.waitShipOrderNum) &&
                Objects.equals(waitDeliveryOrderNum, that.waitDeliveryOrderNum) &&
                Objects.equals(afterSaleOrderNum, that.afterSaleOrderNum);
    }

    @Override
    public int hashCode() {

        return Objects.hash(
                marketGoods,
                pendingGoods,
                pendingMemberAsk,
                allOrdersNum,
                waitPayOrderNum,
                waitShipOrderNum,
                waitDeliveryOrderNum,
                afterSaleOrderNum);

    }

    @Override
    public String toString() {
        return "ShopDashboardVO{" +
                "marketGoods='" + marketGoods + '\'' +
                ", pendingGoods='" + pendingGoods + '\'' +
                ", buyerMessage='" + pendingMemberAsk + '\'' +
                ", allOrdersNum='" + allOrdersNum + '\'' +
                ", waitPayOrderNum='" + waitPayOrderNum + '\'' +
                ", waitShipOrderNum='" + waitShipOrderNum + '\'' +
                ", waitDeliveryOrderNum='" + waitDeliveryOrderNum + '\'' +
                ", afterSaleOrderNum='" + afterSaleOrderNum + '\'' +
                '}';
    }
}
