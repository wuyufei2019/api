/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 申请售后服务--申请退货VO
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-18
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ReturnGoodsVO extends AfterSaleApplyVO implements Serializable {

    private static final long serialVersionUID = -2105785005872712137L;

    /**
     * 账号类型 ALIPAY：支付宝，WEIXINPAY：微信，BANKTRANSFER：银行转账
     */
    @Schema(name = "account_type", description = "账号类型 ALIPAY：支付宝，WEIXINPAY：微信，BANKTRANSFER：银行转账，BALANCE：退款至预存款", allowableValues = "ALIPAY,WEIXINPAY,BANKTRANSFER，BALANCE")
    private String accountType;
    /**
     * 退款账号
     */
    @Schema(name = "return_account", description = "退款账号" )
    private String returnAccount;
    /**
     * 银行名称
     */
    @Schema(name = "bank_name", description = "银行名称" )
    private String bankName;
    /**
     * 银行账户
     */
    @Schema(name = "bank_account_number", description = "银行账户")
    private String bankAccountNumber;
    /**
     * 银行开户名
     */
    @Schema(name = "bank_account_name", description = "银行开户名" )
    private String bankAccountName;
    /**
     * 银行开户行
     */
    @Schema(name = "bank_deposit_name", description = "银行开户行" )
    private String bankDepositName;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getReturnAccount() {
        return returnAccount;
    }

    public void setReturnAccount(String returnAccount) {
        this.returnAccount = returnAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankDepositName() {
        return bankDepositName;
    }

    public void setBankDepositName(String bankDepositName) {
        this.bankDepositName = bankDepositName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ReturnGoodsVO that = (ReturnGoodsVO) o;
        return Objects.equals(accountType, that.accountType) &&
                Objects.equals(returnAccount, that.returnAccount) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(bankAccountNumber, that.bankAccountNumber) &&
                Objects.equals(bankAccountName, that.bankAccountName) &&
                Objects.equals(bankDepositName, that.bankDepositName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), accountType, returnAccount, bankName, bankAccountNumber, bankAccountName, bankDepositName);
    }

    @Override
    public String toString() {
        return "ReturnGoodsVO{" +
                "accountType='" + accountType + '\'' +
                ", returnAccount='" + returnAccount + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankAccountNumber='" + bankAccountNumber + '\'' +
                ", bankAccountName='" + bankAccountName + '\'' +
                ", bankDepositName='" + bankDepositName + '\'' +
                '}';
    }
}
