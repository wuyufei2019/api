/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goodssearch;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 商品分词表实体
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2020-08-06 10:18:29
 */
@TableName("es_goods_words")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsWordsDO implements Serializable {

    private static final long serialVersionUID = 2525253328866549L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 分词
     */
    @Schema(name = "words", description = "分词")
    private String words;
    /**
     * 数量
     */
    @Schema(name = "goods_num", description = "数量")
    private Long goodsNum;
    /**
     * 全拼字母
     */
    @Schema(name = "quanpin", description = "全拼字母")
    private String quanpin;
    /**
     * 首字母
     */
    @Schema(name = "szm", description = "首字母")
    private String szm;
    /**
     * 类型
     */
    @Schema(name = "type", description = "类型")
    private String type;
    /**
     * 排序字段
     */
    @Schema(name = "sort", description = "排序字段")
    private Integer sort;


    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public Long getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Long goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getQuanpin() {
        return quanpin;
    }

    public void setQuanpin(String quanpin) {
        this.quanpin = quanpin;
    }

    public String getSzm() {
        return szm;
    }

    public void setSzm(String szm) {
        this.szm = szm;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GoodsWordsDO that = (GoodsWordsDO) o;
        if (words != null ? !words.equals(that.words) : that.words != null) {
            return false;
        }
        if (goodsNum != null ? !goodsNum.equals(that.goodsNum) : that.goodsNum != null) {
            return false;
        }
        if (quanpin != null ? !quanpin.equals(that.quanpin) : that.quanpin != null) {
            return false;
        }
        if (szm != null ? !szm.equals(that.szm) : that.szm != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        if (sort != null ? !sort.equals(that.sort) : that.sort != null) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (words != null ? words.hashCode() : 0);
        result = 31 * result + (goodsNum != null ? goodsNum.hashCode() : 0);
        result = 31 * result + (quanpin != null ? quanpin.hashCode() : 0);
        result = 31 * result + (szm != null ? szm.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (sort != null ? sort.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GoodsWordsDO{" +
                "id=" + id +
                ", words='" + words + '\'' +
                ", goodsNum=" + goodsNum +
                ", quanpin='" + quanpin + '\'' +
                ", szm='" + szm + '\'' +
                ", type='" + type + '\'' +
                ", sort=" + sort +
                '}';
    }
}