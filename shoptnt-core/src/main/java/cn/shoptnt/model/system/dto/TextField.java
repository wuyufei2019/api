/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * textfield类型表单对象
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2019-05-21 上午11:17
 */
public class TextField {

    @Schema(name = "label", description = "表单 名称")
    private String label;


    @Schema(name = "name", description = "表单 name")
    private String name;

    @Schema(name = "value", description = "值")
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
