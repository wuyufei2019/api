/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.message;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 第三方消息接收
 *
 * @author 张崧
 * @since 2024-04-01
 */
public class MessageReceiveMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -8761441924918746501L;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MESSAGE_RECEIVE;
    }
}
