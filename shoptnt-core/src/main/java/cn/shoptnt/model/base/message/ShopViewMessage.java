package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.statistics.dos.ShopPageView;

import java.io.Serializable;
import java.util.List;

/**
 * @author 张崧
 * @since 2024-03-08
 **/
public class ShopViewMessage implements Serializable, DirectMessage {
    private static final long serialVersionUID = -69785423123463579L;
    private List<ShopPageView> shopPageViews;

    public List<ShopPageView> getShopPageViews() {
        return shopPageViews;
    }

    public ShopViewMessage(List<ShopPageView> shopPageViews) {
        this.shopPageViews = shopPageViews;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.SHOP_VIEW_COUNT;
    }
}
