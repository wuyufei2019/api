package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.statistics.dos.PlatformPageView;

import java.io.Serializable;
import java.util.List;

/**
 * 平台访问流量统计
 *
 * @author 张崧
 * @since 2024-04-24
 **/
public class PlatformViewMessage implements Serializable, DirectMessage {
    private static final long serialVersionUID = -69785423123463579L;
    private final List<PlatformPageView> platformPageViews;

    public List<PlatformPageView> getPlatformPageViews() {
        return platformPageViews;
    }

    public PlatformViewMessage(List<PlatformPageView> platformPageViews) {
        this.platformPageViews = platformPageViews;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.PLATFORM_VIEW_COUNT;
    }
}
