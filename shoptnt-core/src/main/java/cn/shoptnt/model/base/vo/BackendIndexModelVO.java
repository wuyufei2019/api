/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.vo;

import cn.shoptnt.model.goods.vo.BackendGoodsVO;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import cn.shoptnt.model.statistics.vo.SalesTotal;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.List;

/**
 * 后台首页模型
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-29 上午9:03
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BackendIndexModelVO {

    // ================= 总量统计 =================

    @Schema(name = "goods_count", description = "商品数量")
    private Long goodsCount;

    @Schema(name = "order_count", description = "订单数量")
    private Long orderCount;

    @Schema(name = "member_count", description = "会员数量")
    private Long memberCount;

    @Schema(name = "shop_count", description = "店铺数量")
    private Long shopCount;

    // ================= 待办统计 =================

    @Schema(name = "wait_audit_goods_count", description = "待审核商品数量")
    private Long waitAuditGoodsCount;

    @Schema(name = "wait_audit_shop_count", description = "待审核店铺数量")
    private Long waitAuditShopCount;

    @Schema(name = "wait_handle_complain_count", description = "待审核交易投诉数量")
    private Long waitHandleComplainCount;

    @Schema(name = "wait_handle_service_count", description = "待处理售后数量")
    private Long waitHandleServiceCount;

    @Schema(name = "wait_audit_withdraw_count", description = "待审核分销提现数量")
    private Long waitAuditWithdrawCount;

    // ================= 流量统计 =================

    @Schema(name = "today_visit_count", description = "今日访客数量")
    private Integer todayVisitCount;

    @Schema(name = "yesterday_visit_count", description = "昨日访客数量")
    private Integer yesterdayVisitCount;

    @Schema(name = "seven_day_visit_count", description = "前7日访客数量")
    private Integer sevenDayVisitCount;

    @Schema(name = "thirty_day_visit_count", description = "前30日访客数量")
    private Integer thirtyDayVisitCount;

    // ================= 今日统计 =================

    @Schema(name = "today_order_count", description = "今日订单数")
    private Long todayOrderCount;

    @Schema(name = "today_order_amount", description = "今日交易额")
    private Double todayOrderAmount;

    @Schema(name = "today_shop_count", description = "今日新增店铺")
    private Long todayShopCount;

    @Schema(name = "today_member_count", description = "今日新增会员")
    private Long todayMemberCount;

    @Schema(name = "today_goods_count", description = "今日新增商品数量")
    private Long todayGoodsCount;

    @Schema(name = "today_comment_count", description = "今日新增评论")
    private Long todayCommentCount;

    public Long getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Long goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public Long getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Long memberCount) {
        this.memberCount = memberCount;
    }

    public Long getShopCount() {
        return shopCount;
    }

    public void setShopCount(Long shopCount) {
        this.shopCount = shopCount;
    }

    public Long getWaitAuditGoodsCount() {
        return waitAuditGoodsCount;
    }

    public void setWaitAuditGoodsCount(Long waitAuditGoodsCount) {
        this.waitAuditGoodsCount = waitAuditGoodsCount;
    }

    public Long getWaitAuditShopCount() {
        return waitAuditShopCount;
    }

    public void setWaitAuditShopCount(Long waitAuditShopCount) {
        this.waitAuditShopCount = waitAuditShopCount;
    }

    public Long getWaitHandleComplainCount() {
        return waitHandleComplainCount;
    }

    public void setWaitHandleComplainCount(Long waitHandleComplainCount) {
        this.waitHandleComplainCount = waitHandleComplainCount;
    }

    public Long getWaitHandleServiceCount() {
        return waitHandleServiceCount;
    }

    public void setWaitHandleServiceCount(Long waitHandleServiceCount) {
        this.waitHandleServiceCount = waitHandleServiceCount;
    }

    public Long getWaitAuditWithdrawCount() {
        return waitAuditWithdrawCount;
    }

    public void setWaitAuditWithdrawCount(Long waitAuditWithdrawCount) {
        this.waitAuditWithdrawCount = waitAuditWithdrawCount;
    }

    public Integer getTodayVisitCount() {
        return todayVisitCount;
    }

    public void setTodayVisitCount(Integer todayVisitCount) {
        this.todayVisitCount = todayVisitCount;
    }

    public Integer getYesterdayVisitCount() {
        return yesterdayVisitCount;
    }

    public void setYesterdayVisitCount(Integer yesterdayVisitCount) {
        this.yesterdayVisitCount = yesterdayVisitCount;
    }

    public Integer getSevenDayVisitCount() {
        return sevenDayVisitCount;
    }

    public void setSevenDayVisitCount(Integer sevenDayVisitCount) {
        this.sevenDayVisitCount = sevenDayVisitCount;
    }

    public Integer getThirtyDayVisitCount() {
        return thirtyDayVisitCount;
    }

    public void setThirtyDayVisitCount(Integer thirtyDayVisitCount) {
        this.thirtyDayVisitCount = thirtyDayVisitCount;
    }

    public Long getTodayOrderCount() {
        return todayOrderCount;
    }

    public void setTodayOrderCount(Long todayOrderCount) {
        this.todayOrderCount = todayOrderCount;
    }

    public Double getTodayOrderAmount() {
        return todayOrderAmount;
    }

    public void setTodayOrderAmount(Double todayOrderAmount) {
        this.todayOrderAmount = todayOrderAmount;
    }

    public Long getTodayShopCount() {
        return todayShopCount;
    }

    public void setTodayShopCount(Long todayShopCount) {
        this.todayShopCount = todayShopCount;
    }

    public Long getTodayMemberCount() {
        return todayMemberCount;
    }

    public void setTodayMemberCount(Long todayMemberCount) {
        this.todayMemberCount = todayMemberCount;
    }

    public Long getTodayGoodsCount() {
        return todayGoodsCount;
    }

    public void setTodayGoodsCount(Long todayGoodsCount) {
        this.todayGoodsCount = todayGoodsCount;
    }

    public Long getTodayCommentCount() {
        return todayCommentCount;
    }

    public void setTodayCommentCount(Long todayCommentCount) {
        this.todayCommentCount = todayCommentCount;
    }

    @Override
    public String toString() {
        return "BackendIndexModelVO{" +
                "goodsCount=" + goodsCount +
                ", orderCount=" + orderCount +
                ", memberCount=" + memberCount +
                ", shopCount=" + shopCount +
                ", waitAuditGoodsCount=" + waitAuditGoodsCount +
                ", waitAuditShopCount=" + waitAuditShopCount +
                ", waitHandleComplainCount=" + waitHandleComplainCount +
                ", waitHandleServiceCount=" + waitHandleServiceCount +
                ", waitAuditWithdrawCount=" + waitAuditWithdrawCount +
                ", todayVisitCount=" + todayVisitCount +
                ", yesterdayVisitCount=" + yesterdayVisitCount +
                ", sevenDayVisitCount=" + sevenDayVisitCount +
                ", thirtyDayVisitCount=" + thirtyDayVisitCount +
                ", todayOrderCount=" + todayOrderCount +
                ", todayOrderAmount=" + todayOrderAmount +
                ", todayShopCount=" + todayShopCount +
                ", todayMemberCount=" + todayMemberCount +
                ", todayGoodsCount=" + todayGoodsCount +
                ", todayCommentCount=" + todayCommentCount +
                '}';
    }
}
