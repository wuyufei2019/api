package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 店铺变更消息
 *
 * @author 张崧
 * @since 2024-03-08
 **/
public class ShopChangeRegisterMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -8761441921918746501L;

    private final Long shopId;

    public ShopChangeRegisterMessage(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.SHOP_CHANGE_REGISTER;
    }

}
