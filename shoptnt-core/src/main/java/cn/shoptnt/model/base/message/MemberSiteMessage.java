package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 会员站内信
 * @author 张崧
 * @since 2024-03-08
 **/
public class MemberSiteMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -36220292893377321L;
    private Long messageId;

    public Long getMessageId() {

        return messageId;
    }

    public MemberSiteMessage(Long messageId) {
        this.messageId = messageId;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MEMBER_MESSAGE;
    }
}
