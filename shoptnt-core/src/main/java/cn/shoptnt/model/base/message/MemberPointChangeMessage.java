package cn.shoptnt.model.base.message;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title MemberPointChangeMessage
 * @description 会员积分变化消息
 * @program: api
 * 2024/5/24 15:36
 */
public class MemberPointChangeMessage implements Serializable, DirectMessage {
    private static final long serialVersionUID = -702596053091387934L;


     private Long memberId;

    public MemberPointChangeMessage(Long memberId) {
        this.memberId = memberId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MEMBER_POINT_CHANGE;
    }
}
