package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 会员信息编辑完成消息
 * @author 张崧
 * @since 2024-03-08
 **/
public class MemberInfoCompleteMessage implements Serializable, DirectMessage {

    private Long memberId;

    private static final long serialVersionUID = -8761221921918746501L;

    public MemberInfoCompleteMessage(Long memberId) {
        this.memberId = memberId;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MEMBER_INFO_COMPLETE;
    }

    public Long getMemberId() {
        return memberId;
    }


}
