package cn.shoptnt.model.base;

/**
 * 常量
 *
 * @author 张崧
 * @since 2024-01-11
 */
public class ThreadPoolConstant {

    /**
     * 消息推送线程池名称
     */
    public static final String MESSAGE_PUSH_POOL = "messagePushThreadPool";

    /**
     * 公共线程池名称
     */
    public static final String COMMON_POOL = "commonThreadPool";

}
