package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 商品搜索消息
 * @author kingapex
 * @version 1.0
 * @data 2022/10/20 17:10
 **/
public class GoodsSearchMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -3372769927238407770L;

    private String keyword;

    public GoodsSearchMessage(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.SEARCH_KEYWORDS;
    }
}
