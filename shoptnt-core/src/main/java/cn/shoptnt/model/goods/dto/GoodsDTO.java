/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dto;

import cn.shoptnt.model.goods.dos.GoodsGalleryDO;
import cn.shoptnt.model.goods.dos.GoodsParamsDO;
import cn.shoptnt.model.goods.vo.GoodsMobileIntroVO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * 商品vo
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月21日 上午11:25:10
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsDTO implements Serializable {

    private static final long serialVersionUID = 3922135264669953741L;
    @Schema(hidden = true)
    private Long goodsId;

    @Schema(name = "category_id", description = "分类id", required = true)
    private Long categoryId;

    /**
     * 用作显示
     */
    @Schema(name = "category_name", description = "分类名称", required = true)
    private String categoryName;

    @Schema(name = "shop_cat_id", description = "店铺分类id", required = true)
    @Min(value = 0, message = "店铺分类值不正确")
    private Long shopCatId;

    @Schema(name = "brand_id", description = "品牌id")
    private Long brandId;

    @Schema(name = "goods_name", description = "商品名称", required = true)
    @NotEmpty(message = "商品名称不能为空")
    private String goodsName;

    @Schema(name = "sn", description = "商品编号", required = true)
    @Length(max = 30,message = "商品编号太长，不能超过30个字符")
    private String sn;

    @Schema(name = "price", description = "商品价格", required = true)
    @NotNull(message = "商品价格不能为空")
    @Min(value = 0, message = "商品价格不能为负数")
    @Max(value = 99999999, message = "商品价格不能超过99999999")
    private Double price;

    @Schema(name = "cost", description = "成本价格", required = true)
    @NotNull(message = "成本价格不能为空")
    @Min(value = 0, message = "成本价格不能为负数")
    @Max(value = 99999999, message = "成本价格不能超过99999999")
    private Double cost;

    @Schema(name = "mktprice", description = "市场价格", required = true)
    @NotNull(message = "市场价格不能为空")
    @Min(value = 0, message = "市场价格不能为负数")
    @Max(value = 99999999, message = "市场价格不能超过99999999")
    private Double mktprice;

    @Schema(name = "weight", description = "重量", required = true)
    @NotNull(message = "商品重量不能为空")
    @Min(value = 0, message = "重量不能为负数")
    @Max(value = 99999999,message = "重量不能超过99999999")
    private Double weight;

    @Schema(name = "goods_transfee_charge", description = "谁承担运费0：买家承担，1：卖家承担", required = true)
    @NotNull(message = "承担运费不能为空")
    @Min(value = 0, message = "承担运费值不正确")
    @Max(value = 1, message = "承担运费值不正确")
    private Integer goodsTransfeeCharge;

    @Schema(name = "intro", description = "详情")
    private String intro;

    @Schema(name = "have_spec", description = "是否有规格0没有1有", hidden = true)
    private Integer haveSpec;

    @Schema(name = "quantity", description = "库存")
    @Max(value = 99999999, message = "库存不能超过99999999")
    private Integer quantity;

    @Schema(name = "market_enable", description = "是否上架，1上架 0下架", required = true)
    @Min(value = 0, message = "是否上架值不正确")
    @Max(value = 1, message = "是否上架值不正确")
    private Integer marketEnable;

    @Schema(name = "goods_params_list", description = "商品参数")
    @Valid
    private List<GoodsParamsDO> goodsParamsList;

    @Schema(name = "goods_gallery_list", description = "商品相册", required = true)
    @NotNull(message = "商品相册图片不能为空")
    @Size(min = 1,message = "商品相册图片不能为空")
    private List<GoodsGalleryDO> goodsGalleryList;

    @Schema(name = "exchange", description = "积分兑换对象，不是积分兑换商品可以不传")
    private ExchangeVO exchange;

    @Schema(name = "has_changed", description = "sku数据变化或者组合变化判断 0:没变化，1：变化", required = true)
    private Integer hasChanged;

    @Schema(name = "page_title", description = "seo标题")
    private String pageTitle;

    @Schema(name = "meta_keywords", description = "seo关键字")
    private String metaKeywords;

    @Schema(name = "meta_description", description = "seo描述")
    private String metaDescription;

    @Schema(description = "商品的评论数量", hidden = true)
    private Integer commentNum;

    @Schema(name = "template_id", description = "运费模板id,不需要运费模板时值是0", required = true)
    private Long templateId;

    @Schema(description = "商品是否审核", hidden = true)
    private Integer isAuth;

    @Schema(description = "可用库存", hidden = true)
    private Integer enableQuantity;

    @Schema(name = "intro_list", description = "商品移动端详情数据集合")
    private List<GoodsMobileIntroVO> introList;

    @Schema(name = "goods_video", description = "商品视频")
    private String goodsVideo;

    public GoodsDTO() {
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getShopCatId() {
        return shopCatId;
    }

    public void setShopCatId(Long shopCatId) {
        this.shopCatId = shopCatId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getMktprice() {
        return mktprice;
    }

    public void setMktprice(Double mktprice) {
        this.mktprice = mktprice;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getGoodsTransfeeCharge() {
        return goodsTransfeeCharge;
    }

    public void setGoodsTransfeeCharge(Integer goodsTransfeeCharge) {
        this.goodsTransfeeCharge = goodsTransfeeCharge;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getHaveSpec() {
        return haveSpec;
    }

    public void setHaveSpec(Integer haveSpec) {
        this.haveSpec = haveSpec;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public List<GoodsParamsDO> getGoodsParamsList() {
        return goodsParamsList;
    }

    public void setGoodsParamsList(List<GoodsParamsDO> goodsParamsList) {
        this.goodsParamsList = goodsParamsList;
    }

    public List<GoodsGalleryDO> getGoodsGalleryList() {
        return goodsGalleryList;
    }

    public void setGoodsGalleryList(List<GoodsGalleryDO> goodsGalleryList) {
        this.goodsGalleryList = goodsGalleryList;
    }

    public Integer getHasChanged() {
        return hasChanged;
    }

    public void setHasChanged(Integer hasChanged) {
        this.hasChanged = hasChanged;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public ExchangeVO getExchange() {
        return exchange;
    }

    public void setExchange(ExchangeVO exchange) {
        this.exchange = exchange;
    }

    public Integer getMarketEnable() {
        return marketEnable;
    }

    public void setMarketEnable(Integer marketEnable) {
        this.marketEnable = marketEnable;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<GoodsMobileIntroVO> getIntroList() {
        return introList;
    }

    public void setIntroList(List<GoodsMobileIntroVO> introList) {
        this.introList = introList;
    }

    public String getGoodsVideo() {
        return goodsVideo;
    }

    public void setGoodsVideo(String goodsVideo) {
        this.goodsVideo = goodsVideo;
    }

    @Override
    public String toString() {
        return "GoodsDTO{" +
                "goodsId=" + goodsId +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", shopCatId=" + shopCatId +
                ", brandId=" + brandId +
                ", goodsName='" + goodsName + '\'' +
                ", sn='" + sn + '\'' +
                ", price=" + price +
                ", cost=" + cost +
                ", mktprice=" + mktprice +
                ", weight=" + weight +
                ", goodsTransfeeCharge=" + goodsTransfeeCharge +
                ", intro='" + intro + '\'' +
                ", haveSpec=" + haveSpec +
                ", quantity=" + quantity +
                ", marketEnable=" + marketEnable +
                ", goodsParamsList=" + goodsParamsList +
                ", goodsGalleryList=" + goodsGalleryList +
                ", exchange=" + exchange +
                ", hasChanged=" + hasChanged +
                ", pageTitle='" + pageTitle + '\'' +
                ", metaKeywords='" + metaKeywords + '\'' +
                ", metaDescription='" + metaDescription + '\'' +
                ", commentNum=" + commentNum +
                ", templateId=" + templateId +
                ", isAuth=" + isAuth +
                ", enableQuantity=" + enableQuantity +
                ", introList=" + introList +
                ", goodsVideo='" + goodsVideo + '\'' +
                '}';
    }
}
