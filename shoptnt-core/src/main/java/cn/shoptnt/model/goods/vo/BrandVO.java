/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 品牌vo
 * 
 * @author fk
 * @version v1.0
 * @since v7.0 2018年3月16日 下午4:44:55
 */
@Schema
@JsonNaming(value =  PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BrandVO {

	@Schema(description = "品牌名称")
	private String name;

	@Schema(description = "品牌图标")
	private String logo;

	@Schema(description = "品牌id")
	private Long brandId;

	@Schema(description = "品牌关联的分类id")
	private Long categoryId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "BrandVO{" +
				"name='" + name + '\'' +
				", logo='" + logo + '\'' +
				", brandId=" + brandId +
				", categoryId=" + categoryId +
				'}';
	}
}
