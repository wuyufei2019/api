package cn.shoptnt.model.goods.dto;


import cn.shoptnt.model.goods.enums.StockType;

/**
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年10月25日 15:51
 */
public class StockParam {

    /**
     * 变更库存的编号,可以是 goodsId或skuId
     */
    private Long stockSn;

    /**
     * 更新数量  正数为扣减 负数为增加
     */
    private Integer quantity;

    /**
     * 扣减库存类型
     */
    StockType stockType;


    /**
     * 缓存key
     */
    private String cacheKey;

    /**
     * 更新库存
     */
    private Integer updateQuantity;

    public Long getStockSn() {
        return stockSn;
    }

    public void setStockSn(Long stockSn) {
        this.stockSn = stockSn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public StockType getStockType() {
        return stockType;
    }

    public void setStockType(StockType stockType) {
        this.stockType = stockType;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public Integer getUpdateQuantity() {
        return updateQuantity;
    }

    public void setUpdateQuantity(Integer updateQuantity) {
        this.updateQuantity = updateQuantity;
    }
}
