/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * 规格项实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-20 09:31:27
 */
@TableName("es_specification")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SpecificationDO implements Serializable {

    private static final long serialVersionUID = 5111769180376075L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long specId;
    /**
     * 规格项名称
     */
    @NotEmpty(message = "规格项名称不能为空")
    @Schema(name = "spec_name", description =  "规格项名称", required = true)
    private String specName;
    /**
     * 是否被删除0 删除   1  没有删除
     */
    @Schema(hidden = true)
    private Integer disabled;
    /**
     * 规格描述
     */
    @Schema(name = "spec_memo", description =  "规格描述")
    @Length(max = 50, message = "规格备注为0到50个字符之间")
    private String specMemo;
    /**
     * 所属卖家 0属于平台
     */
    @Schema(hidden = true)
    private Long sellerId;

    public SpecificationDO() {
    }


    public SpecificationDO(String specName, Integer disabled, String specMemo, Long sellerId) {
        super();
        this.specName = specName;
        this.disabled = disabled;
        this.specMemo = specMemo;
        this.sellerId = sellerId;
    }

    @PrimaryKeyField
    public Long getSpecId() {
        return specId;
    }

    public void setSpecId(Long specId) {
        this.specId = specId;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public String getSpecMemo() {
        return specMemo;
    }

    public void setSpecMemo(String specMemo) {
        this.specMemo = specMemo;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "SpecificationDO [specId=" + specId + ", specName=" + specName + ", disabled=" + disabled + ", specMemo="
                + specMemo + ", sellerId=" + sellerId + "]";
    }

}
