/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 分类规格关联表实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-20 10:04:26
 */
@TableName("es_category_spec")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CategorySpecDO implements Serializable {

    private static final long serialVersionUID = 3127259064985991L;

    /**
     * 分类id
     */
    @Schema(description =  "分类id")
    private Long categoryId;
    /**
     * 规格id
     */
    @Schema(description =  "规格id")
    private Long specId;

    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    public CategorySpecDO() {

    }

    public CategorySpecDO(Long categoryId, Long specId) {
        super();
        this.categoryId = categoryId;
        this.specId = specId;
    }


    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getSpecId() {
        return specId;
    }

    public void setSpecId(Long specId) {
        this.specId = specId;
    }

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CategorySpecDO [categoryId=" + categoryId + ", specId=" + specId + "]";
    }


}
