package cn.shoptnt.model.promotion.crowdfunding.vo;

import cn.shoptnt.framework.database.mybatisplus.util.BeanUtils;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.promotion.crowdfunding.allowable.CrowdfundingAllowable;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingPriceStep;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.List;

/**
 * 众筹活动详情
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Data
@NoArgsConstructor
@Schema(description = "众筹活动详情")
public class CrowdfundingVO extends Crowdfunding {

    @Schema(name = "status", description = "活动状态")
    private CrowdfundingStatus status;

    @Schema(name = "allowable", description = "活动允许的操作")
    private CrowdfundingAllowable allowable;

    @Schema(name = "goods_sku", description = "sku信息")
    private GoodsSkuDO goodsSku;

    @Schema(name = "price_percent", description = "金额进度")
    private Double pricePercent;

    @Schema(name = "status_text", description = "活动状态中文描述")
    private String statusText;

    @Schema(name = "curr_price", description = "当前售卖价格")
    private Double currPrice;

    @Schema(name = "remain_second", description = "活动剩余秒数")
    private Long remainSecond;

    @Schema(name = "front_status", description = "前端状态判断 0活动进行中，需要显示 1活动已结束，需要显示 2不需要显示")
    private Integer frontStatus;

    public CrowdfundingVO(Crowdfunding crowdfunding, GoodsSkuDO goodsSkuDO) {
        this(crowdfunding);
        this.setGoodsSku(goodsSkuDO);
    }

    public CrowdfundingVO(Crowdfunding crowdfunding) {
        BeanUtils.copyProperties(crowdfunding, this);
    }

    public CrowdfundingStatus getStatus() {
        if (status == null) {
            status = CrowdfundingStatus.getStatus(this);
        }
        return status;
    }

    public CrowdfundingAllowable getAllowable() {
        if (allowable == null) {
            allowable = new CrowdfundingAllowable(this);
        }
        return allowable;
    }

    public Double getPricePercent() {
        if (pricePercent == null) {
            pricePercent = CurrencyUtil.div(super.getAlreadyPrice() * 100, super.getTotalPrice());
        }
        return pricePercent;
    }

    public Integer getPricePercentInt() {
        return (int) CurrencyUtil.round(this.getPricePercent(), 0);
    }

    public String getStatusText() {
        if (statusText == null) {
            statusText = getStatus().getText();
        }
        return statusText;
    }

    public Double getCurrPrice() {
        if (currPrice == null) {
            List<CrowdfundingPriceStep> stepList = super.getPriceStepList();
            stepList.sort(Comparator.comparing(CrowdfundingPriceStep::getThreshold).reversed());
            for (CrowdfundingPriceStep supplierPriceRatioStep : stepList) {
                if (super.getAlreadyGoodsCount() >= supplierPriceRatioStep.getThreshold()) {
                    currPrice = supplierPriceRatioStep.getPrice();
                    break;
                }
            }
        }
        return currPrice;
    }

    public Long getRemainSecond() {
        if (remainSecond == null) {
            remainSecond = super.getEndTime() - DateUtil.getDateline();
        }
        return remainSecond;
    }

    public Integer getFrontStatus() {
        if (frontStatus == null) {
            if (goodsSku == null) {
                frontStatus = 2;
            } else {
                CrowdfundingStatus currStatus = this.getStatus();
                if (currStatus == CrowdfundingStatus.Underway) {
                    frontStatus = 0;
                } else if ((currStatus == CrowdfundingStatus.WaitSettlement || currStatus == CrowdfundingStatus.Success || currStatus == CrowdfundingStatus.Fail) && this.getEndShowFlag()) {
                    frontStatus = 1;
                } else {
                    frontStatus = 2;
                }
            }
        }
        return frontStatus;

    }
}

