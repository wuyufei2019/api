package cn.shoptnt.model.promotion.crowdfunding.dto;

import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * 众筹活动 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Data
@Schema(description = "众筹活动 新增|编辑 请求参数")
public class CrowdfundingDTO {

    @Schema(name = "name", description = "活动名称")
    @NotBlank(message = "活动名称不能为空")
    private String name;

    @Schema(name = "total_price", description = "众筹总金额")
    @NotNull(message = "众筹总金额不能为空")
    @DecimalMin(value = "0.01", message = "众筹总金额最低0.01")
    private Double totalPrice;

    @Schema(name = "price_ratio", description = "金额百分比（达到后就算众筹成功）")
    @NotNull(message = "金额百分比不能为空")
    @DecimalMin(value = "0.01", message = "金额百分比最低0.01")
    private Double priceRatio;

    @Schema(name = "sku_id", description = "众筹sku_id")
    @NotNull(message = "众筹sku_id不能为空")
    private Long skuId;

    @Schema(name = "start_time", description = "开始时间")
    @NotNull(message = "开始时间不能为空")
    private Long startTime;

    @Schema(name = "end_time", description = "结束时间")
    @NotNull(message = "结束时间不能为空")
    private Long endTime;

    @Schema(name = "price_step_list", description = "价格阶梯")
    @NotEmpty(message = "价格阶梯不能为空")
    @Valid
    private CrowdfundingPriceStepList priceStepList;

    @Schema(name = "description", description = "活动说明")
    private String description;

    // ============= 系统内部传递 =============
    
    @JsonIgnore
    @Schema(hidden = true)
    private Long id;

    @JsonIgnore
    @Schema(hidden = true)
    private GoodsSkuDO goodsSkuDO;

    @JsonIgnore
    @Schema(hidden = true)
    private Double targetPrice;

}

