package cn.shoptnt.model.promotion.crowdfunding.enums;

import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 众筹活动状态枚举
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Getter
@RequiredArgsConstructor
public enum CrowdfundingStatus {
    /**
     * 状态
     */
    NotStart("未开始"),
    Underway("进行中"),
    Stop("手动终止"),
    WaitSettlement("已结束，等待结算"),
    Success("众筹成功"),
    Fail("众筹失败"),
    ;
    private final String text;

    public static CrowdfundingStatus getStatus(Crowdfunding crowdfunding) {
        // 手动终止
        if (crowdfunding.getStopFlag()) {
            return Stop;
        }

        // 如果是已结算，众筹结果就是成功或失败
        if (crowdfunding.getSettlementFlag()) {
            if (isSuccess(crowdfunding.getRemainPrice())) {
                return Success;
            } else {
                return Fail;
            }
        }

        // 其余状态根据时间进行判断
        long dateline = DateUtil.getDateline();
        if (crowdfunding.getStartTime() > dateline) {
            return NotStart;
        } else if (crowdfunding.getEndTime() < dateline) {
            return WaitSettlement;
        } else {
            return Underway;
        }
    }

    /**
     * 是否众筹成功
     * @return true成功 false失败
     */
    public static boolean isSuccess(double remainPrice){
        return remainPrice <= 0;
    }
}

