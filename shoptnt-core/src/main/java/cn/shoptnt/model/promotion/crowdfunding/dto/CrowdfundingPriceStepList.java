package cn.shoptnt.model.promotion.crowdfunding.dto;

import lombok.Data;

import java.util.ArrayList;

/**
 * 众筹活动价格阶梯集合
 * 解决mybatis TypeHandle泛型擦除问题
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Data
public class CrowdfundingPriceStepList extends ArrayList<CrowdfundingPriceStep> {

}

