/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.exchange.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 积分商品搜索参数
 * @author Snow create in 2018/5/29
 * @version v2.0
 * @since v7.0.0
 */
@Schema
public class ExchangeQueryParam {


    @Schema(name="name",description = "商品名称")
    private String name;

    @Schema(name="cat_id",description = "积分分类ID")
    private Long catId;

    @Schema(name="sn",description = "商品编号")
    private String sn;

    @Schema(name="seller_name",description = "店铺名称")
    private String sellerName;

    @Schema(name="page_no",description = "第几页")
    private Long pageNo;

    @Schema(name="page_size",description = "每页条数")
    private Long pageSize;

    @Schema(description = "开始时间")
    private Long startTime;

    @Schema(description = "结束时间")
    private Long endTime;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        ExchangeQueryParam that = (ExchangeQueryParam) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(catId, that.catId)
                .append(sn, that.sn)
                .append(sellerName, that.sellerName)
                .append(pageNo, that.pageNo)
                .append(pageSize, that.pageSize)
                .append(startTime, that.startTime)
                .append(endTime, that.endTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(catId)
                .append(sn)
                .append(sellerName)
                .append(pageNo)
                .append(pageSize)
                .append(startTime)
                .append(endTime)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "ExchangeQueryParam{" +
                "name='" + name + '\'' +
                ", catId=" + catId +
                ", sn='" + sn + '\'' +
                ", sellerName='" + sellerName + '\'' +
                ", pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
