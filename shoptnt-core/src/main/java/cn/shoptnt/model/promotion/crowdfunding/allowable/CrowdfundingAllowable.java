package cn.shoptnt.model.promotion.crowdfunding.allowable;

import cn.shoptnt.model.promotion.crowdfunding.dos.Crowdfunding;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 众筹活动允许的操作
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Data
@NoArgsConstructor
@Schema(description = "众筹活动允许的操作")
public class CrowdfundingAllowable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "edit", description = "是否允许编辑")
    private Boolean edit;

    @Schema(name = "delete", description = "是否允许删除")
    private Boolean delete;

    @Schema(name = "stop", description = "是否允许终止")
    private Boolean stop;

    public CrowdfundingAllowable(Crowdfunding crowdfunding) {
        CrowdfundingStatus status = CrowdfundingStatus.getStatus(crowdfunding);
        if (status == null) {
            return;
        }

        this.setEdit(status == CrowdfundingStatus.NotStart);
        this.setDelete(status == CrowdfundingStatus.NotStart);
        this.setStop(status == CrowdfundingStatus.Underway);
    }
}

