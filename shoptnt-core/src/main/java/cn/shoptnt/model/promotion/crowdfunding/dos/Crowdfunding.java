package cn.shoptnt.model.promotion.crowdfunding.dos;

import cn.shoptnt.framework.database.mybatisplus.base.BaseDO;
import cn.shoptnt.framework.database.mybatisplus.util.BeanUtils;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingDTO;
import cn.shoptnt.model.promotion.crowdfunding.dto.CrowdfundingPriceStepList;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.Fastjson2TypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 众筹活动 DO
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@TableName(value = "es_crowdfunding", autoResultMap = true)
@Data
@NoArgsConstructor
public class Crowdfunding extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @Schema(name = "name", description = "活动名称")
    private String name;

    @Schema(name = "total_price", description = "众筹总金额")
    private Double totalPrice;

    @Schema(name = "price_ratio", description = "金额百分比（达到后就算众筹成功）")
    private Double priceRatio;

    @Schema(name = "target_price", description = "目标金额（众筹总金额 * 金额百分比）")
    private Double targetPrice;

    @Schema(name = "goods_id", description = "众筹商品id")
    private Long goodsId;

    @Schema(name = "sku_id", description = "众筹sku_id")
    private Long skuId;

    @Schema(name = "start_time", description = "开始时间")
    private Long startTime;

    @Schema(name = "end_time", description = "结束时间")
    private Long endTime;

    @Schema(name = "price_step_list", description = "价格阶梯")
    @TableField(typeHandler = Fastjson2TypeHandler.class)
    private CrowdfundingPriceStepList priceStepList;

    @Schema(name = "already_price", description = "已众筹金额")
    private Double alreadyPrice;

    @Schema(name = "already_goods_count", description = "已众筹商品数量")
    private Integer alreadyGoodsCount;

    @Schema(name = "remain_price", description = "还差多少金额众筹成功（目标金额 - 已众筹金额）")
    private Double remainPrice;

    @Schema(name = "already_people_count", description = "已众筹人数")
    private Integer alreadyPeopleCount;

    @Schema(name = "stop_flag", description = "是否已终止")
    private Boolean stopFlag;

    @Schema(name = "settlement_flag", description = "所有众筹订单是否已结算")
    private Boolean settlementFlag;

    @Schema(name = "end_show_flag", description = "活动结束后，是否继续展示在会员端")
    private Boolean endShowFlag;

    @Schema(name = "description", description = "活动说明")
    @TableField("`description`")
    private String description;

    public Crowdfunding(CrowdfundingDTO crowdfundingDTO) {
        BeanUtils.copyProperties(crowdfundingDTO, this);
        this.setGoodsId(crowdfundingDTO.getGoodsSkuDO().getGoodsId());
        this.setAlreadyPrice(0.0);
        this.setAlreadyGoodsCount(0);
        this.setAlreadyPeopleCount(0);
        this.setRemainPrice(this.getTargetPrice());
        this.setStopFlag(false);
        this.setSettlementFlag(false);
        this.setEndShowFlag(true);
    }

}
