package cn.shoptnt.model.promotion.crowdfunding.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 众筹活动价格阶梯
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Data
@Schema(description = "众筹活动价格阶梯")
public class CrowdfundingPriceStep {

    @Schema(name = "threshold", description = "门槛人数")
    @NotNull(message = "门槛人数不能为空")
    @Min(value = 0, message = "门槛人数最低为0")
    private Double threshold;

    @Schema(name = "price", description = "众筹价格")
    @NotNull(message = "众筹价格不能为空")
    @Min(value = 0, message = "众筹价格最低为0")
    private Double price;

}

