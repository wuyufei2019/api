package cn.shoptnt.model.promotion.crowdfunding.query;

import cn.shoptnt.framework.database.mybatisplus.base.BaseQueryParam;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 众筹活动分页查询参数
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Data
@Schema(description = "众筹活动分页查询参数")
public class CrowdfundingQueryParams extends BaseQueryParam {

    @Schema(name = "name", description = "活动名称")
    private String name;

    @Schema(name = "status", description = "活动状态")
    private CrowdfundingStatus status;

    @Schema(name = "start_time", description = "开始时间")
    private Long startTime;

    @Schema(name = "end_time", description = "结束时间")
    private Long endTime;

    @Schema(name = "buyer_flag", description = "会员端列表")
    private Boolean buyerFlag;

}

