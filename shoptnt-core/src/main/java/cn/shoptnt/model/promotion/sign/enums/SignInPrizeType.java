/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.sign.enums;

/**
 * 签到奖品类型
 *
 * @author fk
 * @version v6.4
 * @since v6.4
 * 2017年12月15日 下午3:32:43
 */
public enum SignInPrizeType {

    /**
     * 已关闭
     */
    POINT("积分"),

    /**
     * 已开启
     */
    COUPON("优惠券");

    private String description;

    SignInPrizeType(String description) {
        this.description = description;

    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
