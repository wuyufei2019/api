/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.coupon.enums;

/**
 * 优惠券使用范围
 *
 * @author fk
 * @version v2.0
 * @since v7.1.5
 * 2019-09-09 23:19:39
 */
public enum CouponUseScope {

    /**
     * 全部商品
     */
    ALL,

    /**
     * 某分类
     */
    CATEGORY,

    /**
     * 部分商品
     */
    SOME_GOODS;

}
