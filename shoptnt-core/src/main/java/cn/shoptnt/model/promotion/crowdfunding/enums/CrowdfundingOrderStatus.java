package cn.shoptnt.model.promotion.crowdfunding.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 众筹订单状态枚举
 *
 * @author 张崧
 * 2024-06-19 21:46:24
 */
@Getter
@RequiredArgsConstructor
public enum CrowdfundingOrderStatus {
    /**
     * 状态
     */
    WaitSettlement("等待结算"),
    Stop("众筹活动手动终止"),
    Success("众筹成功"),
    Fail("众筹失败"),
    ;
    private final String text;
}

