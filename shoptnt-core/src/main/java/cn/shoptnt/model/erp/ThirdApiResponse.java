package cn.shoptnt.model.erp;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.io.Serializable;
import java.util.List;

/**
 * 请求第三方接口统一响应数据
 *
 * @author 张崧
 * @since v7.2.2
 */
public class ThirdApiResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 接口是否成功
     */
    private boolean success;

    /**
     * 响应码
     */
    private String code;

    /**
     * 响应信息
     */
    private String msg;

    /**
     * 响应数据
     */
    private String data;

    public ThirdApiResponse checkSuccess() {
        if (!success) {
            throw new ThirdApiException(code, msg);
        }
        return this;
    }

    public JSONObject get() {
        return JSONUtil.parseObj(data);
    }

    public List<JSONObject> getList() {
        return JSONUtil.toList(JSONUtil.parseArray(data), JSONObject.class);
    }

    public <T> T get(Class<T> clazz) {
        return JSONUtil.toBean(data, clazz);
    }

    public <T> List<T> getList(Class<T> clazz) {
        return JSONUtil.toList(JSONUtil.parseArray(data), clazz);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
