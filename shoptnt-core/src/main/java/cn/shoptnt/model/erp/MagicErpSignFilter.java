package cn.shoptnt.model.erp;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.digest.HMac;
import cn.hutool.crypto.digest.HmacAlgorithm;
import cn.shoptnt.framework.parameter.EmojiEncodeRequestWrapper;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.service.erp.MagicErpConfig;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * MagicErp 验签 过滤器
 *
 * @author 张崧
 * @since 2024-02-21
 */
@WebFilter(filterName = "magicErpSignFilter", urlPatterns = "/open/magic-erp/*")
public class MagicErpSignFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MagicErpConfig magicErpConfig;

    /**
     * 时间戳最大误差 1分钟
     */
    private static final int TIMESTAMP_ERROR = 60 * 1000;

    @Override
    @SuppressWarnings("NullableProblems")
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        EmojiEncodeRequestWrapper emojiRequest = new EmojiEncodeRequestWrapper(request);
        String url = emojiRequest.getRequestURL().toString();
        String sign = emojiRequest.getHeader("sign");
        String timestamp = emojiRequest.getHeader("timestamp");
        logger.debug("MagicErp请求接口地址：{}\n 签名：{}\n 时间戳：{}", url, sign, timestamp);

        if (StringUtil.isEmpty(sign)) {
            logger.warn("MagicErp签名为空");
            return;
        }
        if (StringUtil.isEmpty(timestamp)) {
            logger.warn("MagicErp时间戳为空");
            return;
        }

        // 校验时间戳，有效期1分钟
        long cha = System.currentTimeMillis() - Long.parseLong(timestamp);
        if (Math.abs(cha) > TIMESTAMP_ERROR) {
            logger.warn("MagicErp请求过期，时间差：{}", cha);
            return;
        }

        String body = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
        body = body == null ? "" : body;
        // 验签内容
        String signContent = body + timestamp;
        if (!verifySign(signContent, sign)) {
            logger.warn("MagicErp验签失败");
            return;
        }

        // 继续过滤链
        chain.doFilter(request, response);
    }

    private boolean verifySign(String signContent, String sign) {
        HMac mac = new HMac(HmacAlgorithm.HmacSHA256, magicErpConfig.getSignSecret().getBytes());
        String signResult = Base64.encode(mac.digest(signContent));
        return signResult.equals(sign);
    }

}
