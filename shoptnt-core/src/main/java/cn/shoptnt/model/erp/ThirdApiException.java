package cn.shoptnt.model.erp;


import cn.shoptnt.framework.exception.ServiceException;

/**
 * 第三方api返回结果为失败，抛出该异常
 * @author 张崧
 * @since v7.2.2
 */
public class ThirdApiException extends ServiceException {

    public ThirdApiException(String code, String message) {
        super(code, message);
    }
    public ThirdApiException(String message) {
        super("500", message);
    }

}
