package cn.shoptnt.model.erp;

/**
 * MagicErp接口API枚举
 *
 * @author 张崧
 * @since 2024-04-01
 */
public enum MagicErpApiEnum {
    /**
     * 接收消息
     */
    MessageReceive("/open/message-receive");

    private final String url;

    MagicErpApiEnum(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
