/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * 菜单管理店铺实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-02 15:38:40
 */
public class ShopMenuVO {

    /**
     * 菜单父id
     */
    @NotNull(message = "父菜单id不能为空")
    @Min(message = "父菜单必须为数字且不能为负数", value =  0)
    @Schema(name = "parent_id", description =  "菜单父id")
    private Long parentId;
    /**
     * 菜单标题
     */
    @NotEmpty(message = "菜单标题不能为空")
    @Schema(name = "title", description =  "菜单标题")
    private String title;
    /**
     * 菜单唯一标识
     */
    @NotEmpty(message = "菜单唯一标识不能为空")
    @Schema(name = "identifier", description =  "菜单唯一标识")
    private String identifier;
    /**
     * 权限表达式
     */
    @NotEmpty(message = "权限表达式不能为空")
    @Schema(name = "auth_regular", description =  "权限表达式")
    private String authRegular;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getAuthRegular() {
        return authRegular;
    }

    public void setAuthRegular(String authRegular) {
        this.authRegular = authRegular;
    }

    @Override
    public String toString() {
        return "ShopMenuVO{" +
                "parentId=" + parentId +
                ", title='" + title + '\'' +
                ", identifier='" + identifier + '\'' +
                ", authRegular='" + authRegular + '\'' +
                '}';
    }
}
