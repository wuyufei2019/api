/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 店铺实体
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-20 10:33:40
 */
@TableName(value = "es_shop")
@Schema
public class  ShopDO implements Serializable {

    private static final long serialVersionUID = 8432303547724758L;

    /**店铺Id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long shopId;
    /**会员Id*/
    @Schema(name="member_id",description ="会员Id")
    private Long memberId;
    /**会员名称*/
    @Schema(name="member_name",description ="会员名称")
    private String memberName;
    /**店铺名称*/
    @Schema(name="shop_name",description ="店铺名称")
    private String shopName;
    /**店铺状态*/
    @Schema(name="shop_disable",description ="店铺状态")
    private String shopDisable;
    /**店铺创建时间*/
    @Schema(name="shop_createtime",description ="店铺创建时间")
    private Long shopCreatetime;
    /**店铺关闭时间*/
    @Schema(name="shop_endtime",description ="店铺关闭时间")
    private Long shopEndtime;

    @PrimaryKeyField
    public Long getShopId() {
        return shopId;
    }
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getMemberId() {
        return memberId;
    }
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getShopName() {
        return shopName;
    }
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopDisable() {
        return shopDisable;
    }
    public void setShopDisable(String shopDisable) {
        this.shopDisable = shopDisable;
    }

    public Long getShopCreatetime() {
        return shopCreatetime;
    }
    public void setShopCreatetime(Long shopCreatetime) {
        this.shopCreatetime = shopCreatetime;
    }

    public Long getShopEndtime() {
        return shopEndtime;
    }
    public void setShopEndtime(Long shopEndtime) {
        this.shopEndtime = shopEndtime;
    }
	@Override
	public String toString() {
		return "ShopDO [shopId=" + shopId + ", memberId=" + memberId + ", memberName=" + memberName + ", shopName="
				+ shopName + ", shopDisable=" + shopDisable + ", shopCreatetime=" + shopCreatetime + ", shopEndtime="
				+ shopEndtime + "]";
	}



}
