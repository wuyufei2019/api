/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo.operator;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 *
 * 卖家操作店铺
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月27日 上午10:29:48
 */
public class SellerEditShop {

	@Schema(description =  "卖家id" )
	private Long sellerId;

	@Schema(description =  "操作")
	private String operator;


	public SellerEditShop() {

	}

	public SellerEditShop(Long sellerId, String operator) {
		super();
		this.sellerId = sellerId;
		this.operator = operator;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}


}
