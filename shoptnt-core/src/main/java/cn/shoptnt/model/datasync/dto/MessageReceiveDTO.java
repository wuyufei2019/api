package cn.shoptnt.model.datasync.dto;

import cn.shoptnt.model.datasync.enums.MessageReceiveTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 接收第三方系统消息DTO
 *
 * @author 张崧
 * @since 2024-04-01
 */
public class MessageReceiveDTO {

    @Schema(name = "msg_id", description = "外部系统的消息id，每次请求必须唯一")
    @NotBlank(message = "外部系统的消息id不能为空")
    private String msgId;

    @Schema(name = "type", description = "消息类型")
    @NotNull(message = "消息类型不能为空")
    private MessageReceiveTypeEnum type;

    @Schema(name = "content", description = "消息内容")
    @NotBlank(message = "消息内容不能为空")
    private String content;

    @Schema(name = "produce_time", description = "外部系统生成消息的时间")
    @NotNull(message = "外部系统生成消息的时间不能为空")
    private Long produceTime;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public MessageReceiveTypeEnum getType() {
        return type;
    }

    public void setType(MessageReceiveTypeEnum type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(Long produceTime) {
        this.produceTime = produceTime;
    }

}

