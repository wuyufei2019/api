package cn.shoptnt.model.datasync.dos;

import cn.shoptnt.model.datasync.enums.MessagePushStatusEnum;
import cn.shoptnt.model.datasync.enums.TargetSystemEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 消息推送实体类
 *
 * @author 张崧
 * @since 2023-12-18 16:24:10
 */
@TableName("es_message_push")
public class MessagePushDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    @Schema(name = "target_system", description = "推送的目标系统")
    private TargetSystemEnum targetSystem;

    @Schema(name = "business_type", description = "消息业务类型")
    private String businessType;

    @Schema(name = "summary", description = "消息摘要（前端展示和快速定位数据用）")
    private String summary;

    /**
     * todo 大文本字段单独存一张表
     */
    @Schema(name = "content", description = "消息内容")
    private String content;

    @Schema(name = "produce_time", description = "消息生产时间")
    private Long produceTime;

    @Schema(name = "status", description = "消息状态")
    private MessagePushStatusEnum status;

    @Schema(name = "fail_count", description = "推送失败次数")
    private Integer failCount;

    @Schema(name = "remark", description = "推送备注")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TargetSystemEnum getTargetSystem() {
        return targetSystem;
    }

    public void setTargetSystem(TargetSystemEnum targetSystem) {
        this.targetSystem = targetSystem;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(Long produceTime) {
        this.produceTime = produceTime;
    }

    public MessagePushStatusEnum getStatus() {
        return status;
    }

    public void setStatus(MessagePushStatusEnum status) {
        this.status = status;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "MessagePushDO{" +
                "id=" + id +
                ", targetSystem=" + targetSystem +
                ", businessType='" + businessType + '\'' +
                ", summary='" + summary + '\'' +
                ", content='" + content + '\'' +
                ", produceTime=" + produceTime +
                ", status=" + status +
                ", failCount=" + failCount +
                ", remark='" + remark + '\'' +
                '}';
    }
}
