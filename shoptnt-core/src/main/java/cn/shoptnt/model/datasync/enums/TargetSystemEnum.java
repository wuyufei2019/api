package cn.shoptnt.model.datasync.enums;


/**
 * 消息推送的目标系统
 *
 * @author 张崧
 * @since 2024-01-05
 */
public enum TargetSystemEnum {

    /**
     * MAGIC-ERP系统
     */
    MagicErp,

}
