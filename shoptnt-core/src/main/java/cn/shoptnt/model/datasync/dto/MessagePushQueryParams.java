package cn.shoptnt.model.datasync.dto;

import cn.shoptnt.framework.database.mybatisplus.base.BaseQueryParam;
import cn.shoptnt.model.datasync.enums.MessagePushStatusEnum;
import cn.shoptnt.model.datasync.enums.TargetSystemEnum;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 消息推送查询参数
 *
 * @author 张崧
 * @since 2023-12-18 16:24:10
 */
public class MessagePushQueryParams extends BaseQueryParam {

    @Schema(name = "target_system", description = "推送的目标系统")
    private TargetSystemEnum targetSystem;

    @Schema(name = "business_type", description = "消息业务类型")
    private String businessType;

    @Schema(name = "summary", description = "消息摘要（前端展示和快速定位数据用）")
    private String summary;

    @Schema(name = "content", description = "消息内容")
    private String content;

    @Schema(name = "produce_time", description = "消息生产时间")
    private Long[] produceTime;

    @Schema(name = "status", description = "消息状态")
    private MessagePushStatusEnum status;

    @Schema(name = "remark", description = "推送结果备注")
    private String remark;

    public TargetSystemEnum getTargetSystem() {
        return targetSystem;
    }

    public void setTargetSystem(TargetSystemEnum targetSystem) {
        this.targetSystem = targetSystem;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long[] getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(Long[] produceTime) {
        this.produceTime = produceTime;
    }

    public MessagePushStatusEnum getStatus() {
        return status;
    }

    public void setStatus(MessagePushStatusEnum status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

