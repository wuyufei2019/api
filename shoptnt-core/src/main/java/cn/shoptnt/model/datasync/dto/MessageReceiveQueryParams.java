package cn.shoptnt.model.datasync.dto;

import cn.shoptnt.framework.database.mybatisplus.base.BaseQueryParam;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 消息接收查询参数
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
public class MessageReceiveQueryParams extends BaseQueryParam {

    @Schema(name = "type", description = "消息类型")
    private String type;
    
    @Schema(name = "msg_id", description = "外部系统的消息id")
    private String msgId;
    
    @Schema(name = "content", description = "消息内容")
    private String content;
    
    @Schema(name = "produce_time", description = "外部系统生成消息的时间")
    private Long[] produceTime;
    
    @Schema(name = "receive_time", description = "接收消息的时间")
    private Long[] receiveTime;
    
    @Schema(name = "status", description = "状态")
    private String status;
    
    @Schema(name = "remark", description = "处理备注")
    private String remark;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long[] getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(Long[] produceTime) {
        this.produceTime = produceTime;
    }

    public Long[] getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Long[] receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

