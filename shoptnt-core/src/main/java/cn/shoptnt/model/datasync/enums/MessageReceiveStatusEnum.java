package cn.shoptnt.model.datasync.enums;


/**
 * 消息接收状态
 *
 * @author 张崧
 * @since 2023-12-18
 */
public enum MessageReceiveStatusEnum {

    /**
     * 待处理
     */
    Wait,
    /**
     * 处理成功
     */
    Success,
    /**
     * 处理失败
     */
    Fail,

}
