package cn.shoptnt.model.datasync.dto;


/**
 * 消息处理结果
 *
 * @author 张崧
 * @since 2023-12-20
 */
public class Result {

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 备注
     */
    private String remark;

    public Result() {
    }

    public Result(Boolean success, String remark) {
        this.success = success;
        this.remark = remark;
    }

    public static Result ok() {
        return new Result(true, "成功");
    }

    public static Result ok(String remark) {
        return new Result(true, remark);
    }

    public static Result fail(String... errorList) {
        return new Result(false, String.join(",", errorList));
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

