package cn.shoptnt.model.datasync.enums;


/**
 * 消息推送状态
 *
 * @author 张崧
 * @since 2023-12-18
 */
public enum MessagePushStatusEnum {

    /**
     * 待推送
     */
    Wait,
    /**
     * 推送成功
     */
    Success,
    /**
     * 推送失败
     */
    Fail,

}
