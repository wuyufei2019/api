package cn.shoptnt.model.datasync.dos;

import cn.shoptnt.model.datasync.enums.MessageReceiveStatusEnum;
import cn.shoptnt.model.datasync.enums.MessageReceiveTypeEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 消息接收实体类
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@TableName("es_message_receive")
public class MessageReceiveDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    @Schema(name = "type", description = "消息类型")
    private MessageReceiveTypeEnum type;

    @Schema(name = "msg_id", description = "外部系统的消息id")
    private String msgId;

    /**
     * todo 大文本字段单独存一张表
     */
    @Schema(name = "content", description = "消息内容")
    private String content;

    @Schema(name = "produce_time", description = "外部系统生成消息的时间")
    private Long produceTime;

    @Schema(name = "receive_time", description = "接收消息的时间")
    private Long receiveTime;

    @Schema(name = "status", description = "状态")
    private MessageReceiveStatusEnum status;

    @Schema(name = "remark", description = "处理结果备注")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MessageReceiveTypeEnum getType() {
        return type;
    }

    public void setType(MessageReceiveTypeEnum type) {
        this.type = type;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(Long produceTime) {
        this.produceTime = produceTime;
    }

    public Long getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Long receiveTime) {
        this.receiveTime = receiveTime;
    }

    public MessageReceiveStatusEnum getStatus() {
        return status;
    }

    public void setStatus(MessageReceiveStatusEnum status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "MessageReceiveDO{" +
                "id=" + id +
                ", type=" + type +
                ", msgId='" + msgId + '\'' +
                ", content='" + content + '\'' +
                ", produceTime=" + produceTime +
                ", receiveTime=" + receiveTime +
                ", status=" + status +
                ", remark='" + remark + '\'' +
                '}';
    }
}
