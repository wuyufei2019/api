package cn.shoptnt.model.datasync.enums;


/**
 * 操作类型
 *
 * @author 张崧
 * @since 2023-12-27
 */
public enum OperateTypeEnum {

    /**
     * 新增
     */
    Add,

    /**
     * 更新
     */
    Update,

    /**
     * 删除
     */
    Delete,

}
