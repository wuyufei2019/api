package cn.shoptnt.model.member.dos;

import cn.shoptnt.model.member.enums.MemberLevelConditionOperator;
import cn.shoptnt.model.member.enums.MemberLevelConditionType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditions
 * @description 会员等级升级条件
 * @program: api
 * 2024/5/20 20:49
 */
@TableName("es_member_level_upgrade_condition")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberLevelUpgradeCondition implements Serializable {
    private static final long serialVersionUID = 1684466537187003610L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    /**
     * 等级id
     */
    @Schema(name = "level_id", description = "等级id", hidden = true)
    private Long levelId;

    /**
     * 条件类型
     *
     * @see cn.shoptnt.model.member.enums.MemberLevelConditionType
     */
    @Schema(name = "condition_type", description = "条件类型")
    private MemberLevelConditionType conditionType;


    /**
     * 条件类型
     *
     * @see cn.shoptnt.model.member.enums.MemberLevelConditionOperator
     */
    @Schema(name = "condition_operator", description = "条件关系")
    private MemberLevelConditionOperator conditionOperator;


    /**
     * 条件最小值
     */
    @Schema(name = "condition_value_min", description = "条件最小值")
    private Long conditionValueMin;

    /**
     * 条件最大值
     */
    @Schema(name = "condition_value_max", description = "条件最大值")
    private Long conditionValueMax;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public MemberLevelConditionType getConditionType() {
        return conditionType;
    }

    public void setConditionType(MemberLevelConditionType conditionType) {
        this.conditionType = conditionType;
    }

    public MemberLevelConditionOperator getConditionOperator() {
        return conditionOperator;
    }

    public void setConditionOperator(MemberLevelConditionOperator conditionOperator) {
        this.conditionOperator = conditionOperator;
    }

    public Long getConditionValueMin() {
        return conditionValueMin;
    }

    public void setConditionValueMin(Long conditionValueMin) {
        this.conditionValueMin = conditionValueMin;
    }

    public Long getConditionValueMax() {
        return conditionValueMax;
    }

    public void setConditionValueMax(Long conditionValueMax) {
        this.conditionValueMax = conditionValueMax;
    }


    public boolean isWithinRange(long points) {
        return points >= conditionValueMin && points <= conditionValueMax;
    }
    @Override
    public String toString() {
        return "MemberLevelUpgradeConditions{" +
                "id=" + id +
                ", levelId='" + levelId + '\'' +
                ", conditionType='" + conditionType + '\'' +
                ", conditionOperator='" + conditionOperator + '\'' +
                ", conditionValueMin=" + conditionValueMin +
                ", conditionValueMax=" + conditionValueMax +
                '}';
    }
}
