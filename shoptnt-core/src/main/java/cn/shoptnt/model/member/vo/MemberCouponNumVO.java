/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * 会员优惠券—各个状态数量
 * @author Snow create in 2018/8/20
 * @version v2.0
 * @since v7.0.0
 */
public class MemberCouponNumVO implements Serializable {

    @Schema(description =  "未使用优惠券数量")
    private Long unUseNum;

    @Schema(description =  "已使用优惠券数量")
    private Long useNum;

    @Schema(description =  "已过期优惠券数量")
    private Long expiredNum;

    public Long getUnUseNum() {
        return unUseNum;
    }

    public void setUnUseNum(Long unUseNum) {
        this.unUseNum = unUseNum;
    }

    public Long getUseNum() {
        return useNum;
    }

    public void setUseNum(Long useNum) {
        this.useNum = useNum;
    }

    public Long getExpiredNum() {
        return expiredNum;
    }

    public void setExpiredNum(Long expiredNum) {
        this.expiredNum = expiredNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        MemberCouponNumVO that = (MemberCouponNumVO) o;

        return new EqualsBuilder()
                .append(unUseNum, that.unUseNum)
                .append(useNum, that.useNum)
                .append(expiredNum, that.expiredNum)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(unUseNum)
                .append(useNum)
                .append(expiredNum)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "MemberCouponNumVO{" +
                "unUseNum=" + unUseNum +
                ", useNum=" + useNum +
                ", expiredNum=" + expiredNum +
                '}';
    }
}
