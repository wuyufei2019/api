/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.enums;

/**
 * @author liuyulei
 * @version 1.0
 * @Description: 会员等级条件关系
 * @date 2019/6/25 9:42
 * @since v7.0
 */
public enum MemberLevelConditionOperator {

    /**
     * 或者
     */
    OR("或者"),


    /**
     * 并且
     */
    AND("并且");

    private String description;

    MemberLevelConditionOperator(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }

    public String value() {
        return this.name();
    }
}
