/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 请求微信https://api.weixin.qq.com/sns/userinfo返回的实体
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2020-09-24
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class WeChatUserDTO implements Serializable {

    private static final long serialVersionUID = -1232483319436590972L;

    @Schema(name = "openid", description = "openid", required = true)
    private String openid;

    @Schema(name = "unionid", description = "unionid", required = true)
    private String unionid;

    @Schema(name = "headimgurl", description = "头像",hidden = true)
    private String headimgurl;

    @Schema(name = "nickName", description = "用户昵称")
    private String nickName;

    @Schema(name = "sex", description = "性别：1:男;0:女")
    private Integer sex;

    @Schema(name = "accessToken", description = "app端登陆传入access_token", required = true)
    private String accessToken;

    @Schema(name = "refreshToken", description = "app授权登陆时需要传入refreshToken")
    private String refreshToken;

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
}
