/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

/**
 * 会员数据附属数据统计
 *
 * @author zh
 * @version v7.0
 * @date 18/6/13 下午3:07
 * @since v7.0
 */
public class MemberStatisticsDTO {
    /**
     * 订单数
     */
    private Long orderCount;
    /**
     * 商品收藏数
     */
    private Long goodsCollectCount;
    /**
     * 店铺收藏数
     */
    private Long shopCollectCount;
    /**
     * 待评论数
     */
    private Long pendingCommentCount;

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public Long getGoodsCollectCount() {
        return goodsCollectCount;
    }

    public void setGoodsCollectCount(Long goodsCollectCount) {
        this.goodsCollectCount = goodsCollectCount;
    }

    public Long getShopCollectCount() {
        return shopCollectCount;
    }

    public void setShopCollectCount(Long shopCollectCount) {
        this.shopCollectCount = shopCollectCount;
    }

    public Long getPendingCommentCount() {
        return pendingCommentCount;
    }

    public void setPendingCommentCount(Long pendingCommentCount) {
        this.pendingCommentCount = pendingCommentCount;
    }

    @Override
    public String toString() {
        return "MemberStatisticsDTO{" +
                "orderCount=" + orderCount +
                ", goodsCollectCount=" + goodsCollectCount +
                ", shopCollectCount=" + shopCollectCount +
                ", pendingCommentCount=" + pendingCommentCount +
                '}';
    }
}
