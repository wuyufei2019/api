package cn.shoptnt.model.member.dos;

import cn.shoptnt.model.member.enums.MemberLevelBenefits;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelUpgradeConditions
 * @description 等级福利详情
 * @program: api
 * 2024/5/20 20:49
 */
@TableName("es_member_level_benefit")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberLevelBenefit implements Serializable {

    private static final long serialVersionUID = -8369558751520262394L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    /**
     * 等级id
     */
    @Schema(name = "level_id", description = "等级id", hidden = true)
    private Long levelId;

    /**
     * 福利类型
     *
     * @see cn.shoptnt.model.member.enums.MemberLevelBenefits
     */
    @Schema(name = "benefit_type", description = "福利类型")
    private MemberLevelBenefits benefitType;


    /**
     * 条件类型
     */
    @Schema(name = "benefit_value", description = "福利值")
    private Long benefitValue;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public MemberLevelBenefits getBenefitType() {
        return benefitType;
    }

    public void setBenefitType(MemberLevelBenefits benefitType) {
        this.benefitType = benefitType;
    }

    public Long getBenefitValue() {
        return benefitValue;
    }

    public void setBenefitValue(Long benefitValue) {
        this.benefitValue = benefitValue;
    }

    @Override
    public String toString() {
        return "MemberLevelBenefits{" +
                "id=" + id +
                ", levelId='" + levelId + '\'' +
                ", benefitType='" + benefitType + '\'' +
                ", benefitValue='" + benefitValue + '\'' +
                '}';
    }
}
