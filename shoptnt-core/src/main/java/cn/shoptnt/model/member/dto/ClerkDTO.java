/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import cn.shoptnt.model.member.vo.MemberVO;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 店员dto
 *
 * @author zh
 * @version v7.0
 * @date 18/8/10 下午3:51
 * @since v7.0
 */

public class ClerkDTO extends MemberVO {

    @Schema(name = "role_id",description =  "角色id")
    private Integer roleId;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "ClerkDTO{" +
                "roleId=" + roleId +
                '}';
    }
}
