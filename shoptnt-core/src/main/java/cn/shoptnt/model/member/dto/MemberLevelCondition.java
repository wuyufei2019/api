package cn.shoptnt.model.member.dto;

import cn.shoptnt.model.member.dos.MemberLevel;
import cn.shoptnt.model.member.dos.MemberLevelBenefit;
import cn.shoptnt.model.member.dos.MemberLevelUpgradeCondition;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevelCondition
 * @description 会员等级条件dto
 * @program: api
 * 2024/5/20 18:12
 */
public class MemberLevelCondition extends MemberLevel implements Serializable {
    private static final long serialVersionUID = -8146121268955009250L;

    @Schema(name = "upgrade_conditions", description = "等级升级条件")
    private List<MemberLevelUpgradeCondition> upgradeConditions;

    @Schema(name = "benefits", description = "等级福利")
    private List<MemberLevelBenefit> benefits;


    public List<MemberLevelUpgradeCondition> getUpgradeConditions() {
        return upgradeConditions;
    }

    public void setUpgradeConditions(List<MemberLevelUpgradeCondition> upgradeConditions) {
        this.upgradeConditions = upgradeConditions;
    }

    public List<MemberLevelBenefit> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<MemberLevelBenefit> benefits) {
        this.benefits = benefits;
    }

    @Override
    public String toString() {
        return "MemberLevelCondition{" +
                "upgradeConditions=" + upgradeConditions +
                ", benefits=" + benefits +
                '}';
    }
}
