package cn.shoptnt.model.member.dos;

import cn.shoptnt.framework.database.annotation.Column;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevel
 * @description 会员等级
 * @program: api
 * 2024/5/20 16:22
 */
@TableName("es_member_level")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberLevel implements Serializable {
    private static final long serialVersionUID = -6698537431688429877L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    /**
     * 等级名称
     */
    @NotBlank(message = "等级名称不能为空")
    @Schema(name = "level_name", description = "等级名称")
    private String levelName;

    /**
     * 等级图标
     */
    @Schema(name = "level_icon", description = "等级图标")
    private String levelIcon;

    /**
     * 分摊比例
     */
    @Schema(name = "allocation_ratio", description = "分摊比例")
    private Double allocationRatio;

    /**
     * 创建时间
     */
    @Schema(name = "create_time", description = "创建时间", hidden = true)
    private Long createTime;
    /**
     * 等级说明
     */
    @Schema(name = "description", description = "等级说明")
    private String description;
    /**
     * 排序
     */
    @Schema(name = "sort_order", description = "排序", hidden = true)
    private int sortOrder;

    /**
     * 是否默认1是，0否
     */
    @Schema(name = "is_default", description = "是否默认1是，0否")
    private Integer isDefault;


    public MemberLevel() {

    }

    public MemberLevel(Long id, String name, int sortOrder) {
        this.sortOrder = sortOrder;
        this.id = id;
        this.levelName = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getLevelIcon() {
        return levelIcon;
    }

    public void setLevelIcon(String levelIcon) {
        this.levelIcon = levelIcon;
    }

    public Double getAllocationRatio() {
        return allocationRatio;
    }

    public void setAllocationRatio(Double allocationRatio) {
        this.allocationRatio = allocationRatio;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public String toString() {
        return "MemberLevel{" +
                "id=" + id +
                ", levelName='" + levelName + '\'' +
                ", levelIcon='" + levelIcon + '\'' +
                ", allocationRatio=" + allocationRatio +
                ", createTime=" + createTime +
                ", description='" + description + '\'' +
                ", sortOrder=" + sortOrder +
                ", isDefault=" + isDefault +
                '}';
    }
}
