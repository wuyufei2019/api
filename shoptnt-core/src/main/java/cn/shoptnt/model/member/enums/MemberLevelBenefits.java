/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.enums;

/**
 * @author liuyulei
 * @version 1.0
 * @Description: 会员等级福利类型
 * @date 2019/6/25 9:42
 * @since v7.0
 */
public enum MemberLevelBenefits {

    /**
     * 积分
     */
    POINTS("积分"),


    /**
     * 折扣
     */
    DISCOUNT("折扣");

    private String description;

    MemberLevelBenefits(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }

    public String value() {
        return this.name();
    }
}
