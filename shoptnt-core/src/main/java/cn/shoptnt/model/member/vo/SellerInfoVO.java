/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

/**
 * 商家登录后返回信息
 *
 * @author zh
 * @version v7.0
 * @date 18/8/15 下午1:54
 * @since v7.0
 */
public class SellerInfoVO extends MemberVO {

    /**
     * 角色id
     */
    private Long roleId;
    /**
     * 是否是店主
     */
    private Integer founder;

    /**
     * 商家id
     */
    private Long sellerId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Integer getFounder() {
        return founder;
    }

    public void setFounder(Integer founder) {
        this.founder = founder;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "SellerInfoVO{" +
                "roleId=" + roleId +
                ", founder=" + founder +
                '}';
    }
}
