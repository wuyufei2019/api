package cn.shoptnt.model.member.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title MemberLevel
 * @description 会员等级
 * @program: api
 * 2024/5/20 16:22
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberLevelDTO implements Serializable {
    private static final long serialVersionUID = -6698557431688429877L;



    /**
     * 等级名称
     */
    @NotBlank(message = "等级名称不能为空")
    @Schema(name = "level_name", description = "等级名称")
    private String levelName;

    /**
     * 等级图标
     */
    @NotBlank(message = "等级图标不能为空")
    @Schema(name = "level_icon", description = "等级图标")
    private String levelIcon;

    /**
     * 分摊比例
     */
    @Schema(name = "allocation_ratio", description = "分摊比例")
    private Double allocationRatio;


    /**
     * 等级说明
     */
    @Schema(name = "description", description = "等级说明")
    private String description;


    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getLevelIcon() {
        return levelIcon;
    }

    public void setLevelIcon(String levelIcon) {
        this.levelIcon = levelIcon;
    }

    public Double getAllocationRatio() {
        return allocationRatio;
    }

    public void setAllocationRatio(Double allocationRatio) {
        this.allocationRatio = allocationRatio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
