/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import cn.shoptnt.model.member.dos.CommentReply;
import cn.shoptnt.model.member.dos.MemberComment;
import cn.shoptnt.model.member.dos.MemberShopScore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * @author fk
 * @version v2.0
 * @Description: 评论vo
 * @date 2018/5/31 5:03
 * @since v7.0.0
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CommentVO extends MemberComment{

    @Schema(name = "images", description =  "评论图片")
    private List<String> images;

    @Schema(name = "reply", description =  "评论回复")
    private CommentReply reply;

    @Schema(name = "additional_comment", description =  "评论追评信息")
    private CommentVO additionalComment;

    @Schema(name = "goods_shop_score", description =  "店铺评分信息")
    private MemberShopScore memberShopScore;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public CommentReply getReply() {
        return reply;
    }

    public void setReply(CommentReply reply) {
        this.reply = reply;
    }

    public CommentVO getAdditionalComment() {
        return additionalComment;
    }

    public void setAdditionalComment(CommentVO additionalComment) {
        this.additionalComment = additionalComment;
    }

    public MemberShopScore getMemberShopScore() {
        return memberShopScore;
    }

    public void setMemberShopScore(MemberShopScore memberShopScore) {
        this.memberShopScore = memberShopScore;
    }

    @Override
    public String toString() {
        return "CommentVO{" +
                "images=" + images +
                ", reply=" + reply +
                ", additionalComment=" + additionalComment +
                ", memberShopScore=" + memberShopScore +
                '}';
    }
}
