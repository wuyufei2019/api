/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.enums;

/**
 * Created by 妙贤 on 2019-02-11.
 * 订单个性化数据key
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-02-11
 */
public enum  OrderDataKey {

    /**
     * 拼团
     */
    pintuan,

    /**
     * 测试用
     */
    test
}
