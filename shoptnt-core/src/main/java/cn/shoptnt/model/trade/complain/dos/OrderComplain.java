/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.complain.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 交易投诉表实体
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-27 16:48:27
 */
@TableName(value = "es_order_complain", autoResultMap = true)
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class OrderComplain implements Serializable {

    private static final long serialVersionUID = 2519567680196080L;

    /**
     * 主键
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long complainId;
    /**
     * 投诉主题
     */
    @Schema(name = "complain_topic", description = "投诉主题")
    private String complainTopic;
    /**
     * 投诉内容
     */
    @Schema(name = "content", description = "投诉内容")
    private String content;
    /**
     * 投诉时间
     */
    @Schema(name = "create_time", description = "投诉时间")
    private Long createTime;
    /**
     * 投诉凭证图片
     */
    @Schema(name = "images", description =  "投诉凭证图片")
    private String images;
    /**
     * 状态，见ComplainStatusEnum.java
     */
    @Schema(name = "status", description =  "状态，见ComplainStatusEnum.java")
    private String status;
    /**
     * 商家申诉内容
     */
    @Schema(name = "appeal_content", description =  "商家申诉内容")
    private String appealContent;
    /**
     * 商家申诉时间
     */
    @Schema(name = "appeal_time", description =  "商家申诉时间")
    private Long appealTime;
    /**
     * 商家申诉上传的图片
     */
    @Schema(name = "appeal_images", description =  "商家申诉上传的图片")
    private String appealImages;
    /**
     * 订单号
     */
    @Schema(name = "order_sn", description =  "订单号")
    private String orderSn;
    /**
     * 下单时间
     */
    @Schema(name = "order_time", description =  "下单时间")
    private Long orderTime;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description =  "商品名称")
    private String goodsName;
    /**
     * 商品id
     */
    @Schema(name = "goods_id", description =  "商品id")
    private Long goodsId;

    /**
     * skuid
     */
    @Schema(name = "sku_id", description =  "sku主键")
    private Long skuId;
    /**
     * 商品价格
     */
    @Schema(name = "goods_price", description =  "商品价格")
    private Double goodsPrice;

    /**
     * 商品图片
     */
    @Schema(name = "goods_image", description =  "商品图片")
    private String goodsImage;
    /**
     * 购买的商品数量
     */
    @Schema(name = "num", description =  "购买的商品数量")
    private Integer num;
    /**
     * 运费
     */
    @Schema(name = "shipping_price", description =  "运费")
    private Double shippingPrice;
    /**
     * 订单金额
     */
    @Schema(name = "order_price", description =  "订单金额")
    private Double orderPrice;
    /**
     * 物流单号
     */
    @Schema(name = "ship_no", description =  "物流单号")
    private String shipNo;
    /**
     * 商家id
     */
    @Schema(name = "seller_id", description =  "商家id")
    private Long sellerId;
    /**
     * 商家名称
     */
    @Schema(name = "seller_name", description =  "商家名称")
    private String sellerName;
    /**
     * 会员id
     */
    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;
    /**
     * 会员名称
     */
    @Schema(name = "member_name", description =  "会员名称")
    private String memberName;
    /**
     * 收货人
     */
    @Schema(name = "ship_name", description =  "收货人")
    private String shipName;
    /**
     * 收货地址
     */
    @Schema(name = "ship_addr", description =  "收货地址")
    private String shipAddr;
    /**
     * 收货人手机
     */
    @Schema(name = "ship_mobile", description =  "收货人手机")
    @SecretField(SecretType.MOBILE)
    private String shipMobile;
    /**
     * 仲裁结果
     */
    @Schema(name = "arbitration_result", description =  "仲裁结果")
    private String arbitrationResult;

    @PrimaryKeyField
    public Long getComplainId() {
        return complainId;
    }

    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    public String getComplainTopic() {
        return complainTopic;
    }

    public void setComplainTopic(String complainTopic) {
        this.complainTopic = complainTopic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppealContent() {
        return appealContent;
    }

    public void setAppealContent(String appealContent) {
        this.appealContent = appealContent;
    }

    public Long getAppealTime() {
        return appealTime;
    }

    public void setAppealTime(Long appealTime) {
        this.appealTime = appealTime;
    }

    public String getAppealImages() {
        return appealImages;
    }

    public void setAppealImages(String appealImages) {
        this.appealImages = appealImages;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(Double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getShipNo() {
        return shipNo;
    }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getArbitrationResult() {
        return arbitrationResult;
    }

    public void setArbitrationResult(String arbitrationResult) {
        this.arbitrationResult = arbitrationResult;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderComplain that = (OrderComplain) o;
        if (complainId != null ? !complainId.equals(that.complainId) : that.complainId != null) {
            return false;
        }
        if (complainTopic != null ? !complainTopic.equals(that.complainTopic) : that.complainTopic != null) {
            return false;
        }
        if (content != null ? !content.equals(that.content) : that.content != null) {
            return false;
        }
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) {
            return false;
        }
        if (images != null ? !images.equals(that.images) : that.images != null) {
            return false;
        }
        if (status != null ? !status.equals(that.status) : that.status != null) {
            return false;
        }
        if (appealContent != null ? !appealContent.equals(that.appealContent) : that.appealContent != null) {
            return false;
        }
        if (appealTime != null ? !appealTime.equals(that.appealTime) : that.appealTime != null) {
            return false;
        }
        if (appealImages != null ? !appealImages.equals(that.appealImages) : that.appealImages != null) {
            return false;
        }
        if (orderSn != null ? !orderSn.equals(that.orderSn) : that.orderSn != null) {
            return false;
        }
        if (orderTime != null ? !orderTime.equals(that.orderTime) : that.orderTime != null) {
            return false;
        }
        if (goodsName != null ? !goodsName.equals(that.goodsName) : that.goodsName != null) {
            return false;
        }
        if (goodsId != null ? !goodsId.equals(that.goodsId) : that.goodsId != null) {
            return false;
        }
        if (goodsPrice != null ? !goodsPrice.equals(that.goodsPrice) : that.goodsPrice != null) {
            return false;
        }
        if (num != null ? !num.equals(that.num) : that.num != null) {
            return false;
        }
        if (shippingPrice != null ? !shippingPrice.equals(that.shippingPrice) : that.shippingPrice != null) {
            return false;
        }
        if (orderPrice != null ? !orderPrice.equals(that.orderPrice) : that.orderPrice != null) {
            return false;
        }
        if (shipNo != null ? !shipNo.equals(that.shipNo) : that.shipNo != null) {
            return false;
        }
        if (sellerId != null ? !sellerId.equals(that.sellerId) : that.sellerId != null) {
            return false;
        }
        if (sellerName != null ? !sellerName.equals(that.sellerName) : that.sellerName != null) {
            return false;
        }
        if (memberId != null ? !memberId.equals(that.memberId) : that.memberId != null) {
            return false;
        }
        if (memberName != null ? !memberName.equals(that.memberName) : that.memberName != null) {
            return false;
        }
        if (shipName != null ? !shipName.equals(that.shipName) : that.shipName != null) {
            return false;
        }
        if (shipAddr != null ? !shipAddr.equals(that.shipAddr) : that.shipAddr != null) {
            return false;
        }
        if (shipMobile != null ? !shipMobile.equals(that.shipMobile) : that.shipMobile != null) {
            return false;
        }
        if (arbitrationResult != null ? !arbitrationResult.equals(that.arbitrationResult) : that.arbitrationResult != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (complainId != null ? complainId.hashCode() : 0);
        result = 31 * result + (complainTopic != null ? complainTopic.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (appealContent != null ? appealContent.hashCode() : 0);
        result = 31 * result + (appealTime != null ? appealTime.hashCode() : 0);
        result = 31 * result + (appealImages != null ? appealImages.hashCode() : 0);
        result = 31 * result + (orderSn != null ? orderSn.hashCode() : 0);
        result = 31 * result + (orderTime != null ? orderTime.hashCode() : 0);
        result = 31 * result + (goodsName != null ? goodsName.hashCode() : 0);
        result = 31 * result + (goodsId != null ? goodsId.hashCode() : 0);
        result = 31 * result + (goodsPrice != null ? goodsPrice.hashCode() : 0);
        result = 31 * result + (num != null ? num.hashCode() : 0);
        result = 31 * result + (shippingPrice != null ? shippingPrice.hashCode() : 0);
        result = 31 * result + (orderPrice != null ? orderPrice.hashCode() : 0);
        result = 31 * result + (shipNo != null ? shipNo.hashCode() : 0);
        result = 31 * result + (sellerId != null ? sellerId.hashCode() : 0);
        result = 31 * result + (sellerName != null ? sellerName.hashCode() : 0);
        result = 31 * result + (memberId != null ? memberId.hashCode() : 0);
        result = 31 * result + (memberName != null ? memberName.hashCode() : 0);
        result = 31 * result + (shipName != null ? shipName.hashCode() : 0);
        result = 31 * result + (shipAddr != null ? shipAddr.hashCode() : 0);
        result = 31 * result + (shipMobile != null ? shipMobile.hashCode() : 0);
        result = 31 * result + (arbitrationResult != null ? arbitrationResult.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrderComplain{" +
                "complainId=" + complainId +
                ", complainTopic='" + complainTopic + '\'' +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                ", images='" + images + '\'' +
                ", status='" + status + '\'' +
                ", appealContent='" + appealContent + '\'' +
                ", appealTime=" + appealTime +
                ", appealImages='" + appealImages + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", orderTime=" + orderTime +
                ", goodsName='" + goodsName + '\'' +
                ", goodsId=" + goodsId +
                ", goodsPrice=" + goodsPrice +
                ", num=" + num +
                ", shippingPrice=" + shippingPrice +
                ", orderPrice=" + orderPrice +
                ", shipNo='" + shipNo + '\'' +
                ", sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", shipName='" + shipName + '\'' +
                ", shipAddr='" + shipAddr + '\'' +
                ", shipMobile='" + shipMobile + '\'' +
                ", arbitrationResult='" + arbitrationResult + '\'' +
                '}';
    }


}
