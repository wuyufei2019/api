/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.cart.enums;

/**
 * Created by 妙贤 on 2018/12/20.
 * 购物车类型
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/20
 */
public enum  CartType {

    /**
     * 表明是在购物车
     */
    CART,

    /**
     * 表明是在结算页
     */
    CHECKOUT,

    /**
     * 表明是在拼团
     */
    PINTUAN,

    /**
     * 表明是在众筹
     */
    Crowdfunding


}
