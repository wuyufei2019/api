/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dto;

import cn.shoptnt.model.promotion.coupon.vo.GoodsCouponPrice;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountGiftDO;
import cn.shoptnt.model.trade.cart.vo.CouponVO;
import cn.shoptnt.model.trade.order.vo.OrderOperateAllowable;
import cn.shoptnt.model.trade.order.vo.OrderSkuVO;
import cn.shoptnt.framework.util.JsonUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单信息DTO
 *
 * @author Snow create in 2018/5/28
 * @version v2.0
 * @since v7.0.0
 */
@Schema(description = "订单信息DTO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderDetailDTO {

    /**
     * 主键ID
     */
    @Schema(hidden = true)
    private Integer orderId;

    /**
     * 交易编号
     */
    @Schema(name = "trade_sn", description = "交易编号")
    private String tradeSn;

    /**
     * 订单编号
     */
    @Schema(name = "sn", description = "订单编号")
    private String sn;

    /**
     * 店铺ID
     */
    @Schema(name = "seller_id", description = "店铺ID")
    private Long sellerId;

    /**
     * 店铺名称
     */
    @Schema(name = "seller_name", description = "店铺名称")
    private String sellerName;

    /**
     * 会员ID
     */
    @Schema(name = "member_id", description = "会员ID")
    private Long memberId;

    /**
     * 买家账号
     */
    @Schema(name = "member_name", description = "买家账号")
    private String memberName;

    /**
     * 订单状态
     */
    @Schema(name = "order_status", description = "订单状态")
    private String orderStatus;

    /**
     * 付款状态
     */
    @Schema(name = "pay_status", description = "付款状态")
    private String payStatus;

    /**
     * 货运状态
     */
    @Schema(name = "ship_status", description = "货运状态")
    private String shipStatus;

    /**
     * 配送方式ID
     */
    @Schema(name = "shipping_id", description = "配送方式ID")
    private Integer shippingId;

    @Schema(name = "comment_status", description = "评论是否完成")
    private String commentStatus;

    /**
     * 配送方式
     */
    @Schema(name = "shipping_type", description = "配送方式")
    private String shippingType;

    /**
     * 支付方式id
     */
    @Schema(name = "payment_method_id", description = "支付方式id")
    private String paymentMethodId;

    /**
     * 支付插件id
     */
    @Schema(name = "payment_plugin_id", description = "支付插件id")
    private String paymentPluginId;

    /**
     * 支付方式名称
     */
    @Schema(name = "payment_method_name", description = "支付方式名称")
    private String paymentMethodName;

    /**
     * 支付方式类型
     */
    @Schema(name = "payment_type", description = "支付方式类型")
    private String paymentType;

    /**
     * 支付时间
     */
    @Schema(name = "payment_time", description = "支付时间")
    private Long paymentTime;

    /**
     * 已支付金额
     */
    @Schema(name = "pay_money", description = "已支付金额")
    private Double payMoney;

    /**
     * 收货人姓名
     */
    @Schema(name = "ship_name", description = "收货人姓名")
    private String shipName;

    /**
     * 收货地址
     */
    @Schema(name = "ship_addr", description = "收货地址")
    private String shipAddr;

    /**
     * 收货人邮编
     */
    @Schema(name = "ship_zip", description = "收货人邮编")
    private String shipZip;

    /**
     * 收货人手机
     */
    @Schema(name = "ship_mobile", description = "收货人手机")
    private String shipMobile;

    /**
     * 收货人电话
     */
    @Schema(name = "ship_tel", description = "收货人电话")
    private String shipTel;

    /**
     * 收货时间
     */
    @Schema(name = "receive_time", description = "收货时间")
    private String receiveTime;

    /**
     * 配送地区-省份ID
     */
    @Schema(name = "ship_province_id", description = "配送地区-省份ID")
    private Long shipProvinceId;

    /**
     * 配送地区-城市ID
     */
    @Schema(name = "ship_city_id", description = "配送地区-城市ID")
    private Long shipCityId;

    /**
     * 配送地区-区(县)ID
     */
    @Schema(name = "ship_county_id", description = "配送地区-区(县)ID")
    private Long shipCountyId;

    /**
     * 配送街道id
     */
    @Schema(name = "ship_town_id", description = "配送街道id")
    private Long shipTownId;

    /**
     * 配送地区-省份
     */
    @Schema(name = "ship_province", description = "配送地区-省份")
    private String shipProvince;

    /**
     * 配送地区-城市
     */
    @Schema(name = "ship_city", description = "配送地区-城市")
    private String shipCity;

    /**
     * 配送地区-区(县)
     */
    @Schema(name = "ship_county", description = "配送地区-区(县)")
    private String shipCounty;

    /**
     * 配送街道
     */
    @Schema(name = "ship_town", description = "配送街道")
    private String shipTown;

    /**
     * 订单总额
     */
    @Schema(name = "order_price", description = "订单总额")
    private Double orderPrice;

    /**
     * 商品总额
     */
    @Schema(name = "goods_price", description = "商品总额")
    private Double goodsPrice;

    /**
     * 配送费用
     */
    @Schema(name = "shipping_price", description = "配送费用")
    private Double shippingPrice;

    /**
     * 优惠金额
     */
    @Schema(name = "discount_price", description = "优惠金额")
    private Double discountPrice;

    /**
     * 是否被删除
     */
    @Schema(name = "disabled", description = "是否被删除")
    private Integer disabled;

    @Schema(name = "weight", description = "订单商品总重量")
    private Double weight;

    @Schema(name = "goods_num", description = "商品数量")
    private Integer goodsNum;

    @Schema(name = "remark", description = "订单备注")
    private String remark;

    @Schema(name = "cancel_reason", description = "订单取消原因")
    private String cancelReason;

    @Schema(name = "the_sign", description = "签收人")
    private String theSign;

    /**
     * 转换 List<OrderSkuVO>
     */
    @Schema(name = "items_json", description = "货物列表json")
    private String itemsJson;

    @Schema(name = "warehouse_id", description = "发货仓库ID")
    private Integer warehouseId;

    @Schema(name = "need_pay_money", description = "应付金额")
    private Double needPayMoney;

    @Schema(name = "ship_no", description = "发货单号")
    private String shipNo;

    @Schema(name = "address_id", description = "收货地址ID")
    private Long addressId;

    @Schema(name = "admin_remark", description = "管理员备注")
    private Integer adminRemark;

    @Schema(name = "logi_id", description = "物流公司ID")
    private Integer logiId;

    @Schema(name = "logi_name", description = "物流公司名称")
    private String logiName;

    @Schema(name = "need_receipt", description = "是否需要发票")
    private String needReceipt;

    @Schema(name = "receipt_title", description = "发票抬头")
    private String receiptTitle;

    @Schema(name = "receipt_content", description = "发票内容")
    private String receiptContent;

    @Schema(name = "duty_invoice", description = "税号")
    private String dutyInvoice;

    @Schema(name = "receipt_type", description = "发票类型")
    private String receiptType;

    @Schema(name = "complete_time", description = "完成时间")
    private Long completeTime;

    @Schema(name = "create_time", description = "订单创建时间")
    private Long createTime;

    @Schema(name = "signing_time", description = "签收时间")
    private Long signingTime;

    @Schema(name = "ship_time", description = "送货时间")
    private Long shipTime;

    @Schema(name = "pay_order_no", description = "支付方式返回的交易号")
    private String payOrderNo;

    @Schema(name = "service_status", description = "售后状态")
    private String serviceStatus;

    @Schema(name = "bill_status", description = "结算状态")
    private Integer billStatus;

    @Schema(name = "bill_sn", description = "结算单号")
    private String billSn;

    @Schema(name = "client_type", description = "订单来源")
    private String clientType;

    @Schema(description = "订单状态文字")
    private String orderStatusText;

    @Schema(description = "付款状态文字")
    private String payStatusText;

    @Schema(description = "发货状态文字")
    private String shipStatusText;

    @Schema(description = "支付方式")
    private String paymentName;

    @Schema(description = "sku列表")
    private List<OrderSkuDTO> orderSkuList;

    @Schema(description = "订单赠品列表")
    private List<FullDiscountGiftDO> giftList;

    @Schema(description = "订单操作允许情况")
    private OrderOperateAllowable orderOperateAllowableVO;




    @Schema(description = "返现金额" )
    private Double cashBack;

    @Schema(description = "优惠券抵扣金额" )
    private Double couponPrice;

    @Schema(description = "赠送的积分")
    private Integer giftPoint;

    @Schema(description = "赠送的优惠券")
    private CouponVO giftCoupon;


    @Schema(description = "此订单使用的积分")
    private Integer usePoint;

    @Schema(description = "优惠券使用详情列表")
    private List<GoodsCouponPrice> goodsCouponPrices;

    @Schema(description = "订单类型")
    private String orderType;

    @Schema(description = "会员等级折扣优惠金额")
    private Double memberLevelDiscountPrice;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getTradeSn() {
        return tradeSn;
    }

    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getShipStatus() {
        return shipStatus;
    }

    public void setShipStatus(String shipStatus) {
        this.shipStatus = shipStatus;
    }

    public Integer getShippingId() {
        return shippingId;
    }

    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentPluginId() {
        return paymentPluginId;
    }

    public void setPaymentPluginId(String paymentPluginId) {
        this.paymentPluginId = paymentPluginId;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Double getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Double payMoney) {
        this.payMoney = payMoney;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getShipZip() {
        return shipZip;
    }

    public void setShipZip(String shipZip) {
        this.shipZip = shipZip;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getShipTel() {
        return shipTel;
    }

    public void setShipTel(String shipTel) {
        this.shipTel = shipTel;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getShipProvinceId() {
        return shipProvinceId;
    }

    public void setShipProvinceId(Long shipProvinceId) {
        this.shipProvinceId = shipProvinceId;
    }

    public Long getShipCityId() {
        return shipCityId;
    }

    public void setShipCityId(Long shipCityId) {
        this.shipCityId = shipCityId;
    }

    public Long getShipCountyId() {
        return shipCountyId;
    }

    public void setShipCountyId(Long shipCountyId) {
        this.shipCountyId = shipCountyId;
    }

    public Long getShipTownId() {
        return shipTownId;
    }

    public void setShipTownId(Long shipTownId) {
        this.shipTownId = shipTownId;
    }

    public String getShipProvince() {
        return shipProvince;
    }

    public void setShipProvince(String shipProvince) {
        this.shipProvince = shipProvince;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipCounty() {
        return shipCounty;
    }

    public void setShipCounty(String shipCounty) {
        this.shipCounty = shipCounty;
    }

    public String getShipTown() {
        return shipTown;
    }

    public void setShipTown(String shipTown) {
        this.shipTown = shipTown;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Double getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(Double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public Double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getTheSign() {
        return theSign;
    }

    public void setTheSign(String theSign) {
        this.theSign = theSign;
    }

    public String getItemsJson() {
        return itemsJson;
    }

    public void setItemsJson(String itemsJson) {
        this.itemsJson = itemsJson;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Double getNeedPayMoney() {
        return needPayMoney;
    }

    public void setNeedPayMoney(Double needPayMoney) {
        this.needPayMoney = needPayMoney;
    }

    public String getShipNo() {
        return shipNo;
    }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Integer getAdminRemark() {
        return adminRemark;
    }

    public void setAdminRemark(Integer adminRemark) {
        this.adminRemark = adminRemark;
    }

    public Integer getLogiId() {
        return logiId;
    }

    public void setLogiId(Integer logiId) {
        this.logiId = logiId;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public String getNeedReceipt() {
        return needReceipt;
    }

    public void setNeedReceipt(String needReceipt) {
        this.needReceipt = needReceipt;
    }

    public String getReceiptTitle() {
        return receiptTitle;
    }

    public void setReceiptTitle(String receiptTitle) {
        this.receiptTitle = receiptTitle;
    }

    public String getReceiptContent() {
        return receiptContent;
    }

    public void setReceiptContent(String receiptContent) {
        this.receiptContent = receiptContent;
    }

    public String getDutyInvoice() {
        return dutyInvoice;
    }

    public void setDutyInvoice(String dutyInvoice) {
        this.dutyInvoice = dutyInvoice;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getSigningTime() {
        return signingTime;
    }

    public void setSigningTime(Long signingTime) {
        this.signingTime = signingTime;
    }

    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Integer getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(Integer billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillSn() {
        return billSn;
    }

    public void setBillSn(String billSn) {
        this.billSn = billSn;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getOrderStatusText() {
        return orderStatusText;
    }

    public void setOrderStatusText(String orderStatusText) {
        this.orderStatusText = orderStatusText;
    }

    public String getPayStatusText() {
        return payStatusText;
    }

    public void setPayStatusText(String payStatusText) {
        this.payStatusText = payStatusText;
    }

    public String getShipStatusText() {
        return shipStatusText;
    }

    public void setShipStatusText(String shipStatusText) {
        this.shipStatusText = shipStatusText;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public Double getMemberLevelDiscountPrice() {
        return memberLevelDiscountPrice;
    }

    public void setMemberLevelDiscountPrice(Double memberLevelDiscountPrice) {
        this.memberLevelDiscountPrice = memberLevelDiscountPrice;
    }

    public List<OrderSkuDTO> getOrderSkuList() {

        if(orderSkuList==null && itemsJson!=null){
            List<OrderSkuVO> skuList = JsonUtil.jsonToList(itemsJson, OrderSkuVO.class);
            List<OrderSkuDTO> res = new ArrayList<>();
            for(OrderSkuVO vo : skuList){
                OrderSkuDTO dto = new OrderSkuDTO();
                BeanUtils.copyProperties(vo,dto);
                res.add(dto);
            }

            return res;
        }
        return orderSkuList;
    }

    public void setOrderSkuList(List<OrderSkuDTO> orderSkuList) {
        this.orderSkuList = orderSkuList;
    }

    public OrderOperateAllowable getOrderOperateAllowableVO() {
        return orderOperateAllowableVO;
    }

    public void setOrderOperateAllowableVO(OrderOperateAllowable orderOperateAllowableVO) {
        this.orderOperateAllowableVO = orderOperateAllowableVO;
    }

    public List<FullDiscountGiftDO> getGiftList() {
        return giftList;
    }

    public Double getCashBack() {
        return cashBack;
    }

    public void setCashBack(Double cashBack) {
        this.cashBack = cashBack;
    }

    public Double getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(Double couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Integer getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(Integer giftPoint) {
        this.giftPoint = giftPoint;
    }

    public CouponVO getGiftCoupon() {
        return giftCoupon;
    }

    public void setGiftCoupon(CouponVO giftCoupon) {
        this.giftCoupon = giftCoupon;
    }

    public Integer getUsePoint() {
        return usePoint;
    }

    public void setUsePoint(Integer usePoint) {
        this.usePoint = usePoint;
    }

    public void setGiftList(List<FullDiscountGiftDO> giftList) {
        this.giftList = giftList;
    }

    public List<GoodsCouponPrice> getGoodsCouponPrices() {
        return goodsCouponPrices;
    }

    public void setGoodsCouponPrices(List<GoodsCouponPrice> goodsCouponPrices) {
        this.goodsCouponPrices = goodsCouponPrices;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @Override
    public String toString() {
        return "OrderDetailDTO{" +
                "orderId=" + orderId +
                ", tradeSn='" + tradeSn + '\'' +
                ", sn='" + sn + '\'' +
                ", sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", payStatus='" + payStatus + '\'' +
                ", shipStatus='" + shipStatus + '\'' +
                ", shippingId=" + shippingId +
                ", commentStatus='" + commentStatus + '\'' +
                ", shippingType='" + shippingType + '\'' +
                ", paymentMethodId='" + paymentMethodId + '\'' +
                ", paymentPluginId='" + paymentPluginId + '\'' +
                ", paymentMethodName='" + paymentMethodName + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", paymentTime=" + paymentTime +
                ", payMoney=" + payMoney +
                ", shipName='" + shipName + '\'' +
                ", shipAddr='" + shipAddr + '\'' +
                ", shipZip='" + shipZip + '\'' +
                ", shipMobile='" + shipMobile + '\'' +
                ", shipTel='" + shipTel + '\'' +
                ", receiveTime='" + receiveTime + '\'' +
                ", shipProvinceId=" + shipProvinceId +
                ", shipCityId=" + shipCityId +
                ", shipCountyId=" + shipCountyId +
                ", shipTownId=" + shipTownId +
                ", shipProvince='" + shipProvince + '\'' +
                ", shipCity='" + shipCity + '\'' +
                ", shipCounty='" + shipCounty + '\'' +
                ", shipTown='" + shipTown + '\'' +
                ", orderPrice=" + orderPrice +
                ", goodsPrice=" + goodsPrice +
                ", shippingPrice=" + shippingPrice +
                ", discountPrice=" + discountPrice +
                ", disabled=" + disabled +
                ", weight=" + weight +
                ", goodsNum=" + goodsNum +
                ", remark='" + remark + '\'' +
                ", cancelReason='" + cancelReason + '\'' +
                ", theSign='" + theSign + '\'' +
                ", itemsJson='" + itemsJson + '\'' +
                ", warehouseId=" + warehouseId +
                ", needPayMoney=" + needPayMoney +
                ", shipNo='" + shipNo + '\'' +
                ", addressId=" + addressId +
                ", adminRemark=" + adminRemark +
                ", logiId=" + logiId +
                ", logiName='" + logiName + '\'' +
                ", needReceipt='" + needReceipt + '\'' +
                ", receiptTitle='" + receiptTitle + '\'' +
                ", receiptContent='" + receiptContent + '\'' +
                ", dutyInvoice='" + dutyInvoice + '\'' +
                ", receiptType='" + receiptType + '\'' +
                ", completeTime=" + completeTime +
                ", createTime=" + createTime +
                ", signingTime=" + signingTime +
                ", shipTime=" + shipTime +
                ", payOrderNo='" + payOrderNo + '\'' +
                ", serviceStatus='" + serviceStatus + '\'' +
                ", billStatus=" + billStatus +
                ", billSn='" + billSn + '\'' +
                ", clientType='" + clientType + '\'' +
                ", orderStatusText='" + orderStatusText + '\'' +
                ", payStatusText='" + payStatusText + '\'' +
                ", shipStatusText='" + shipStatusText + '\'' +
                ", paymentName='" + paymentName + '\'' +
                ", orderSkuList=" + orderSkuList +
                ", giftList=" + giftList +
                ", orderOperateAllowableVO=" + orderOperateAllowableVO +
                ", cashBack=" + cashBack +
                ", couponPrice=" + couponPrice +
                ", giftPoint=" + giftPoint +
                ", giftCoupon=" + giftCoupon +
                ", usePoint=" + usePoint +
                ", goodsCouponPrices=" + goodsCouponPrices +
                ", orderType='" + orderType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderDetailDTO that = (OrderDetailDTO) o;

        return new EqualsBuilder()
                .append(orderId, that.orderId)
                .append(tradeSn, that.tradeSn)
                .append(sn, that.sn)
                .append(sellerId, that.sellerId)
                .append(sellerName, that.sellerName)
                .append(memberId, that.memberId)
                .append(memberName, that.memberName)
                .append(orderStatus, that.orderStatus)
                .append(payStatus, that.payStatus)
                .append(shipStatus, that.shipStatus)
                .append(shippingId, that.shippingId)
                .append(commentStatus, that.commentStatus)
                .append(shippingType, that.shippingType)
                .append(paymentMethodId, that.paymentMethodId)
                .append(paymentPluginId, that.paymentPluginId)
                .append(paymentMethodName, that.paymentMethodName)
                .append(paymentType, that.paymentType)
                .append(paymentTime, that.paymentTime)
                .append(payMoney, that.payMoney)
                .append(shipName, that.shipName)
                .append(shipAddr, that.shipAddr)
                .append(shipZip, that.shipZip)
                .append(shipMobile, that.shipMobile)
                .append(shipTel, that.shipTel)
                .append(receiveTime, that.receiveTime)
                .append(shipProvinceId, that.shipProvinceId)
                .append(shipCityId, that.shipCityId)
                .append(shipCountyId, that.shipCountyId)
                .append(shipTownId, that.shipTownId)
                .append(shipProvince, that.shipProvince)
                .append(shipCity, that.shipCity)
                .append(shipCounty, that.shipCounty)
                .append(shipTown, that.shipTown)
                .append(orderPrice, that.orderPrice)
                .append(goodsPrice, that.goodsPrice)
                .append(shippingPrice, that.shippingPrice)
                .append(discountPrice, that.discountPrice)
                .append(disabled, that.disabled)
                .append(weight, that.weight)
                .append(goodsNum, that.goodsNum)
                .append(remark, that.remark)
                .append(cancelReason, that.cancelReason)
                .append(theSign, that.theSign)
                .append(itemsJson, that.itemsJson)
                .append(warehouseId, that.warehouseId)
                .append(needPayMoney, that.needPayMoney)
                .append(shipNo, that.shipNo)
                .append(addressId, that.addressId)
                .append(adminRemark, that.adminRemark)
                .append(logiId, that.logiId)
                .append(logiName, that.logiName)
                .append(needReceipt, that.needReceipt)
                .append(receiptTitle, that.receiptTitle)
                .append(receiptContent, that.receiptContent)
                .append(dutyInvoice, that.dutyInvoice)
                .append(receiptType, that.receiptType)
                .append(completeTime, that.completeTime)
                .append(createTime, that.createTime)
                .append(signingTime, that.signingTime)
                .append(shipTime, that.shipTime)
                .append(payOrderNo, that.payOrderNo)
                .append(serviceStatus, that.serviceStatus)
                .append(billStatus, that.billStatus)
                .append(billSn, that.billSn)
                .append(clientType, that.clientType)
                .append(orderStatusText, that.orderStatusText)
                .append(payStatusText, that.payStatusText)
                .append(shipStatusText, that.shipStatusText)
                .append(paymentName, that.paymentName)
                .append(orderSkuList, that.orderSkuList)
                .append(giftList, that.giftList)
                .append(orderOperateAllowableVO, that.orderOperateAllowableVO)
                .append(cashBack, that.cashBack)
                .append(couponPrice, that.couponPrice)
                .append(giftPoint, that.giftPoint)
                .append(giftCoupon, that.giftCoupon)
                .append(usePoint, that.usePoint)
                .append(goodsCouponPrices, that.goodsCouponPrices)
                .append(orderType, that.orderType)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(orderId)
                .append(tradeSn)
                .append(sn)
                .append(sellerId)
                .append(sellerName)
                .append(memberId)
                .append(memberName)
                .append(orderStatus)
                .append(payStatus)
                .append(shipStatus)
                .append(shippingId)
                .append(commentStatus)
                .append(shippingType)
                .append(paymentMethodId)
                .append(paymentPluginId)
                .append(paymentMethodName)
                .append(paymentType)
                .append(paymentTime)
                .append(payMoney)
                .append(shipName)
                .append(shipAddr)
                .append(shipZip)
                .append(shipMobile)
                .append(shipTel)
                .append(receiveTime)
                .append(shipProvinceId)
                .append(shipCityId)
                .append(shipCountyId)
                .append(shipTownId)
                .append(shipProvince)
                .append(shipCity)
                .append(shipCounty)
                .append(shipTown)
                .append(orderPrice)
                .append(goodsPrice)
                .append(shippingPrice)
                .append(discountPrice)
                .append(disabled)
                .append(weight)
                .append(goodsNum)
                .append(remark)
                .append(cancelReason)
                .append(theSign)
                .append(itemsJson)
                .append(warehouseId)
                .append(needPayMoney)
                .append(shipNo)
                .append(addressId)
                .append(adminRemark)
                .append(logiId)
                .append(logiName)
                .append(needReceipt)
                .append(receiptTitle)
                .append(receiptContent)
                .append(dutyInvoice)
                .append(receiptType)
                .append(completeTime)
                .append(createTime)
                .append(signingTime)
                .append(shipTime)
                .append(payOrderNo)
                .append(serviceStatus)
                .append(billStatus)
                .append(billSn)
                .append(clientType)
                .append(orderStatusText)
                .append(payStatusText)
                .append(shipStatusText)
                .append(paymentName)
                .append(orderSkuList)
                .append(giftList)
                .append(orderOperateAllowableVO)
                .append(cashBack)
                .append(couponPrice)
                .append(giftPoint)
                .append(giftCoupon)
                .append(usePoint)
                .append(goodsCouponPrices)
                .append(orderType)
                .toHashCode();
    }


}
