/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * 收款单实体
 *
 * @author xlp
 * @version v2.0
 * @since v7.0.0
 * 2018-07-18 10:39:51
 */
@TableName("es_pay_log")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PayLog implements Serializable {

    private static final long serialVersionUID = 7244927488352294L;

    /**
     * 收款单ID
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long payLogId;

    /**
     * 收款单流水号
     */
    @Schema(name = "order_sn", description = "收款单流水号")
    private String payLogSn;

    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description = "订单编号")
    private String orderSn;

    /**
     * 支付类型 PaymentTypeEnum
     */
    @Schema(name = "pay_type", description = "支付类型")
    private String payType;

    /**
     * 支付方式 alipay,wechat
     */
    @Schema(name = "pay_way", description = "支付方式")
    private String payWay;

    /**
     * 付款时间
     */
    @Schema(name = "pay_time", description = "付款时间")
    private Long payTime;

    /**
     * 付款金额
     */
    @Schema(name = "pay_money", description = "付款金额")
    private Double payMoney;

    /**
     * 付款会员名
     */
    @Schema(name = "pay_member_name", description = "付款会员名")
    private String payMemberName;

    /**
     * 付款状态
     */
    @Schema(name = "pay_status", description = "付款状态")
    private String payStatus;


    /**
     * 支付方式返回的流水号
     */
    @Schema(name = "pay_order_no", description = "支付方式返回的交易号")
    private String payOrderNo;

    @PrimaryKeyField
    public Long getPayLogId() {
        return payLogId;
    }

    public void setPayLogId(Long payLogId) {
        this.payLogId = payLogId;
    }

    public String getPayLogSn() {
        return payLogSn;
    }

    public void setPayLogSn(String payLogSn) {
        this.payLogSn = payLogSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public Double getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Double payMoney) {
        this.payMoney = payMoney;
    }

    public String getPayMemberName() {
        return payMemberName;
    }

    public void setPayMemberName(String payMemberName) {
        this.payMemberName = payMemberName;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PayLog payLog = (PayLog) o;

        return new EqualsBuilder()
                .append(payLogId, payLog.payLogId)
                .append(payLogSn, payLog.payLogSn)
                .append(orderSn, payLog.orderSn)
                .append(payType, payLog.payType)
                .append(payWay, payLog.payWay)
                .append(payTime, payLog.payTime)
                .append(payMoney, payLog.payMoney)
                .append(payMemberName, payLog.payMemberName)
                .append(payStatus, payLog.payStatus)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(payLogId)
                .append(payLogSn)
                .append(orderSn)
                .append(payType)
                .append(payWay)
                .append(payTime)
                .append(payMoney)
                .append(payMemberName)
                .append(payStatus)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "PayLog{" +
                "payLogId=" + payLogId +
                ", payLogSn='" + payLogSn + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", payType='" + payType + '\'' +
                ", payWay='" + payWay + '\'' +
                ", payTime=" + payTime +
                ", payMoney=" + payMoney +
                ", payMemberName='" + payMemberName + '\'' +
                ", payStatus='" + payStatus + '\'' +
                '}';
    }
}
