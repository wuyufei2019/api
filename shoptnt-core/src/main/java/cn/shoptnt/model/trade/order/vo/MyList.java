/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.vo;


import cn.shoptnt.model.trade.complain.enums.ComplainStatusEnum;
import cn.shoptnt.model.trade.order.enums.OrderServiceStatusEnum;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-03-11
 */

public class MyList {
    private List<OrderFlowNode> list;

    public MyList( ) {
        this.list = new ArrayList<>();
    }

    public MyList add(OrderStatusEnum status) {
        OrderFlowNode orderCreateFlow = new OrderFlowNode(status);
        list.add(orderCreateFlow);
        return this;
    }


    public MyList addComplain(ComplainStatusEnum status) {
        OrderFlowNode orderCreateFlow = new OrderFlowNode(status);
        list.add(orderCreateFlow);
        return this;
    }

    public MyList add(OrderServiceStatusEnum status) {
        OrderFlowNode orderCreateFlow = new OrderFlowNode(status);
        list.add(orderCreateFlow);
        return this;
    }

    public List<OrderFlowNode>  getList() {

        return list;
    }


}
