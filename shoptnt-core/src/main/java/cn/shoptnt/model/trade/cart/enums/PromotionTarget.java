/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.cart.enums;

/**
 * Created by 妙贤 on 2018/12/12.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/12
 */
public enum PromotionTarget {

    /**
     * CART
     */
    CART,

    /**
     * SKU
     */
    SKU

}
