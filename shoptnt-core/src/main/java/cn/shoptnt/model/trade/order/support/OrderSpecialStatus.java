/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.support;

import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingOrderStatus;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.enums.OrderTypeEnum;
import cn.shoptnt.model.trade.order.enums.PaymentTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 妙贤 on 2019-02-12.
 * 订单特殊状态text处理
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-02-12
 */
public class OrderSpecialStatus {

    private static final String WAIT_ROG = "待发货";

    /**
     * 定义特殊的流程状态显示
     */
    private static Map<String, String> map = new HashMap(16);

    static {

        //拼团已经成团的为待发货
        map.put(OrderTypeEnum.PINTUAN + "_" + PaymentTypeEnum.ONLINE + "_" + OrderStatusEnum.FORMED, WAIT_ROG);

        //普通订单在线付款的，已经付款显示为待发货
        map.put(OrderTypeEnum.NORMAL + "_" + PaymentTypeEnum.ONLINE + "_" + OrderStatusEnum.PAID_OFF, WAIT_ROG);

        //普通订单在线付款的，已确认显示为待付款
        map.put(OrderTypeEnum.NORMAL + "_" + PaymentTypeEnum.ONLINE + "_" + OrderStatusEnum.CONFIRM, "待付款");

        //普通订单货到付款的，已确认的显示为待发货
        map.put(OrderTypeEnum.NORMAL + "_" + PaymentTypeEnum.COD + "_" + OrderStatusEnum.CONFIRM, WAIT_ROG);

        //换货售后服务重新创建的订单在线付款的，已经付款显示为待发货
        map.put(OrderTypeEnum.CHANGE + "_" + PaymentTypeEnum.ONLINE + "_" + OrderStatusEnum.PAID_OFF, WAIT_ROG);

        //补发商品售后服务重新创建的订单在线付款的，已经付款显示为待发货
        map.put(OrderTypeEnum.SUPPLY_AGAIN + "_" + PaymentTypeEnum.ONLINE + "_" + OrderStatusEnum.PAID_OFF, WAIT_ROG);
    }


    /**
     * 获取特殊状态text
     *
     * @param orderType          订单类型
     * @param paymentType        支付类型
     * @param orderStatus        订单状态
     * @param crowdfundingFlag
     * @param crowdfundingStatus
     * @return
     */
    public static String getStatusText(String orderType, String paymentType, String orderStatus, Boolean crowdfundingFlag, CrowdfundingOrderStatus crowdfundingStatus) {
        String statusText = map.get(orderType + "_" + paymentType + "_" + orderStatus);
        if (WAIT_ROG.equals(statusText) && crowdfundingFlag && crowdfundingStatus != CrowdfundingOrderStatus.Success) {
            statusText = "等待众筹结果";
        }
        return statusText;
    }

}
