/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.cart.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 众筹购物车原始数据
 *
 * @author 张崧
 * @since 2024-06-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrowdfundingCartVO implements Serializable {

    private static final long serialVersionUID = -7457589664804806186L;

    /**
     * 众筹活动id
     */
    private Long crowdfundingId;

    /**
     * 众筹数量
     */
    private Integer num;

}
