/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dos;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingOrderStatus;
import cn.shoptnt.model.promotion.crowdfunding.enums.CrowdfundingStatus;
import cn.shoptnt.model.trade.cart.vo.PriceDetailVO;
import cn.shoptnt.model.trade.complain.enums.ComplainSkuStatusEnum;
import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.enums.*;
import cn.shoptnt.model.trade.order.vo.ConsigneeVO;
import cn.shoptnt.model.trade.order.vo.OrderParam;
import cn.shoptnt.model.trade.order.vo.OrderSkuVO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;


/**
 * 订单表实体
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-05-10 16:13:37
 */
@TableName(value = "es_order", autoResultMap = true)
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class OrderDO implements Serializable {

    private static final long serialVersionUID = 3154292647603519L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long orderId;

    /**
     * 交易编号
     */
    @Column(name = "trade_sn")
    @Schema(name = "trade_sn", description = "交易编号")
    private String tradeSn;
    /**
     * 订单编号
     */
    @Column(name = "sn")
    @Schema(name = "sn", description = "订单编号")
    private String sn;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    @Schema(name = "seller_id", description = "店铺ID")
    private Long sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    @Schema(name = "seller_name", description = "店铺名称")
    private String sellerName;
    /**
     * 会员ID
     */
    @Column(name = "member_id")
    @Schema(name = "member_id", description = "会员ID")
    private Long memberId;
    /**
     * 买家账号
     */
    @Column(name = "member_name")
    @Schema(name = "member_name", description = "买家账号")
    private String memberName;
    /**
     * 订单状态
     */
    @Column(name = "order_status")
    @Schema(name = "order_status", description = "订单状态")
    private String orderStatus;
    /**
     * 付款状态
     */
    @Column(name = "pay_status")
    @Schema(name = "pay_status", description = "付款状态")
    private String payStatus;
    /**
     * 货运状态
     */
    @Column(name = "ship_status")
    @Schema(name = "ship_status", description = "货运状态")
    private String shipStatus;
    /**
     * 配送方式ID
     */
    @Column(name = "shipping_id")
    @Schema(name = "shipping_id", description = "配送方式ID")
    private Integer shippingId;
    /**
     * 评论是否完成
     */
    @Column(name = "comment_status")
    @Schema(name = "comment_status", description = "评论是否完成")
    private String commentStatus;
    /**
     * 配送方式
     */
    @Column(name = "shipping_type")
    @Schema(name = "shipping_type", description = "配送方式")
    private String shippingType;
    /**
     * 支付方式id
     */
    @Column(name = "payment_method_id")
    @Schema(name = "payment_method_id", description = "支付方式id")
    private Long paymentMethodId;
    /**
     * 支付插件id
     */
    @Column(name = "payment_plugin_id")
    @Schema(name = "payment_plugin_id", description = "支付插件id")
    private String paymentPluginId;
    /**
     * 支付方式名称
     */
    @Column(name = "payment_method_name")
    @Schema(name = "payment_method_name", description = "支付方式名称")
    private String paymentMethodName;
    /**
     * 支付方式类型
     */
    @Column(name = "payment_type")
    @Schema(name = "payment_type", description = "支付方式类型")
    private String paymentType;
    /**
     * 支付时间
     */
    @Column(name = "payment_time")
    @Schema(name = "payment_time", description = "支付时间")
    private Long paymentTime;
    /**
     * 已支付金额
     */
    @Column(name = "pay_money")
    @Schema(name = "pay_money", description = "已支付金额")
    private Double payMoney;
    /**
     * 收货人姓名
     */
    @Column(name = "ship_name")
    @Schema(name = "ship_name", description = "收货人姓名")
    private String shipName;
    /**
     * 收货地址
     */
    @Column(name = "ship_addr")
    @Schema(name = "ship_addr", description = "收货地址")
    private String shipAddr;
    /**
     * 收货人邮编
     */
    @Column(name = "ship_zip")
    @Schema(name = "ship_zip", description = "收货人邮编")
    private String shipZip;
    /**
     * 收货人手机
     */
    @Column(name = "ship_mobile")
    @Schema(name = "ship_mobile", description = "收货人手机")
    @SecretField(SecretType.MOBILE)
    private String shipMobile;
    /**
     * 收货人电话
     */
    @Column(name = "ship_tel")
    @Schema(name = "ship_tel", description = "收货人电话")
    private String shipTel;
    /**
     * 收货时间
     */
    @Column(name = "receive_time")
    @Schema(name = "receive_time", description = "收货时间")
    private String receiveTime;
    /**
     * 配送地区-省份ID
     */
    @Column(name = "ship_province_id")
    @Schema(name = "ship_province_id", description = "配送地区-省份ID")
    private Long shipProvinceId;

    /**
     * 配送地区-城市ID
     */
    @Column(name = "ship_city_id")
    @Schema(name = "ship_city_id", description = "配送地区-城市ID")
    private Long shipCityId;

    /**
     * 配送地区-区(县)ID
     */
    @Column(name = "ship_county_id")
    @Schema(name = "ship_county_id", description = "配送地区-区(县)ID")
    private Long shipCountyId;

    /**
     * 配送街道id
     */
    @Column(name = "ship_town_id")
    @Schema(name = "ship_town_id", description = "配送街道id")
    private Long shipTownId;
    /**
     * 配送地区-省份
     */
    @Column(name = "ship_province")
    @Schema(name = "ship_province", description = "配送地区-省份")
    private String shipProvince;
    /**
     * 配送地区-城市
     */
    @Column(name = "ship_city")
    @Schema(name = "ship_city", description = "配送地区-城市")
    private String shipCity;
    /**
     * 配送地区-区(县)
     */
    @Column(name = "ship_county")
    @Schema(name = "ship_county", description = "配送地区-区(县)")
    private String shipCounty;
    /**
     * 配送街道
     */
    @Column(name = "ship_town")
    @Schema(name = "ship_town", description = "配送街道")
    private String shipTown;
    /**
     * 订单总额
     */
    @Column(name = "order_price")
    @Schema(name = "order_price", description = "订单总额")
    private Double orderPrice;
    /**
     * 商品总额
     */
    @Column(name = "goods_price")
    @Schema(name = "goods_price", description = "商品总额")
    private Double goodsPrice;
    /**
     * 配送费用
     */
    @Column(name = "shipping_price")
    @Schema(name = "shipping_price", description = "配送费用")
    private Double shippingPrice;
    /**
     * 优惠金额
     */
    @Column(name = "discount_price")
    @Schema(name = "discount_price", description = "优惠金额")
    private Double discountPrice;
    /**
     * 是否被删除
     */
    @Column(name = "disabled")
    @Schema(name = "disabled", description = "是否被删除")
    private Integer disabled;
    /**
     * 订单商品总重量
     */
    @Column(name = "weight")
    @Schema(name = "weight", description = "订单商品总重量")
    private Double weight;
    /**
     * 商品数量
     */
    @Column(name = "goods_num")
    @Schema(name = "goods_num", description = "商品数量")
    private Integer goodsNum;
    /**
     * 订单备注
     */
    @Column(name = "remark")
    @Schema(name = "remark", description = "订单备注")
    private String remark;
    /**
     * 订单取消原因
     */
    @Column(name = "cancel_reason")
    @Schema(name = "cancel_reason", description = "订单取消原因")
    private String cancelReason;
    /**
     * 签收人
     */
    @Column(name = "the_sign")
    @Schema(name = "the_sign", description = "签收人")
    private String theSign;

    /**
     * 转换 List<OrderSkuVO>
     */
    @Column(name = "items_json")
    @Schema(name = "items_json", description = "货物列表json")
    private String itemsJson;

    @Column(name = "warehouse_id")
    @Schema(name = "warehouse_id", description = "发货仓库ID")
    private Integer warehouseId;

    @Column(name = "need_pay_money")
    @Schema(name = "need_pay_money", description = "应付金额")
    private Double needPayMoney;

    @Column(name = "ship_no")
    @Schema(name = "ship_no", description = "发货单号")
    private String shipNo;

    /**
     * 收货地址ID
     */
    @Column(name = "address_id")
    @Schema(name = "address_id", description = "收货地址ID")
    private Long addressId;
    /**
     * 管理员备注
     */
    @Column(name = "admin_remark")
    @Schema(name = "admin_remark", description = "管理员备注")
    private Integer adminRemark;
    /**
     * 物流公司ID
     */
    @Column(name = "logi_id")
    @Schema(name = "logi_id", description = "物流公司ID")
    private Long logiId;
    /**
     * 物流公司名称
     */
    @Column(name = "logi_name")
    @Schema(name = "logi_name", description = "物流公司名称")
    private String logiName;

    /**
     * 完成时间
     */
    @Column(name = "complete_time")
    @Schema(name = "complete_time", description = "完成时间")
    private Long completeTime;
    /**
     * 订单创建时间
     */
    @Column(name = "create_time")
    @Schema(name = "create_time", description = "订单创建时间")
    private Long createTime;

    /**
     * 签收时间
     */
    @Column(name = "signing_time")
    @Schema(name = "signing_time", description = "签收时间")
    private Long signingTime;

    /**
     * 送货时间
     */
    @Column(name = "ship_time")
    @Schema(name = "ship_time", description = "送货时间")
    private Long shipTime;

    /**
     * 支付方式返回的交易号
     */
    @Column(name = "pay_order_no")
    @Schema(name = "pay_order_no", description = "支付方式返回的交易号")
    private String payOrderNo;

    /**
     * 售后状态
     */
    @Column(name = "service_status")
    @Schema(name = "service_status", description = "售后状态")
    private String serviceStatus;

    /**
     * 结算状态
     */
    @Column(name = "bill_status")
    @Schema(name = "bill_status", description = "结算状态")
    private Integer billStatus;

    /**
     * 结算单号
     */
    @Column(name = "bill_sn")
    @Schema(name = "bill_sn", description = "结算单号")
    private String billSn;

    /**
     * 订单来源
     */
    @Column(name = "client_type")
    @Schema(name = "client_type", description = "订单来源")
    private String clientType;

    @Column(name = "need_receipt")
    @Schema(name = "need_receipt", description = "是否需要发票,0：否，1：是")
    private Integer needReceipt;

    /**
     * @see OrderTypeEnum
     * 因增加拼团业务新增订单类型字段 妙贤 2019/1/28 on v7.1.0
     */
    @Schema(description = "订单类型")
    @Column(name = "order_type")
    private String orderType;


    /**
     * 订单的扩展数据
     * 为了增加订单的扩展性，个性化的业务可以将个性化数据（如拼团所差人数）存在此字段 妙贤 2019/1/28 on v7.1.0
     */
    @Schema(description = "扩展数据", hidden = true)
    @Column(name = "order_data")
    private String orderData;

    /**
     * 预存款抵扣金额
     */
    @Schema(description = "预存款抵扣金额", hidden = true)
    @Column(name = "balance")
    private Double balance;

    @Schema(description = "会员等级折扣优惠金额")
    private Double memberLevelDiscountPrice;

    @Schema(description = "是否为众筹订单")
    private Boolean crowdfundingFlag;

    @Schema(description = "众筹活动id")
    private Long crowdfundingId;

    @Schema(description = "众筹状态")
    private CrowdfundingOrderStatus crowdfundingStatus;

    @JsonIgnore
    protected String scanRounds;


    public String getScanRounds() {
        return scanRounds;
    }

    public void setScanRounds(String scanRounds) {
        this.scanRounds = scanRounds;
    }

    @PrimaryKeyField
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getTradeSn() {
        return tradeSn;
    }

    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getShipStatus() {
        return shipStatus;
    }

    public void setShipStatus(String shipStatus) {
        this.shipStatus = shipStatus;
    }


    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }


    public String getPaymentPluginId() {
        return paymentPluginId;
    }

    public void setPaymentPluginId(String paymentPluginId) {
        this.paymentPluginId = paymentPluginId;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Double getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Double payMoney) {
        this.payMoney = payMoney;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getShipZip() {
        return shipZip;
    }

    public void setShipZip(String shipZip) {
        this.shipZip = shipZip;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getShipTel() {
        return shipTel;
    }

    public void setShipTel(String shipTel) {
        this.shipTel = shipTel;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getShipProvinceId() {
        return shipProvinceId;
    }

    public void setShipProvinceId(Long shipProvinceId) {
        this.shipProvinceId = shipProvinceId;
    }

    public Long getShipCityId() {
        return shipCityId;
    }

    public void setShipCityId(Long shipCityId) {
        this.shipCityId = shipCityId;
    }

    public Long getShipCountyId() {
        return shipCountyId;
    }

    public void setShipCountyId(Long shipCountyId) {
        this.shipCountyId = shipCountyId;
    }

    public Long getShipTownId() {
        return shipTownId;
    }

    public void setShipTownId(Long shipTownId) {
        this.shipTownId = shipTownId;
    }

    public String getShipProvince() {
        return shipProvince;
    }

    public void setShipProvince(String shipProvince) {
        this.shipProvince = shipProvince;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipCounty() {
        return shipCounty;
    }

    public void setShipCounty(String shipCounty) {
        this.shipCounty = shipCounty;
    }

    public String getShipTown() {
        return shipTown;
    }

    public void setShipTown(String shipTown) {
        this.shipTown = shipTown;
    }

    public Double getOrderPrice() {
        if (orderPrice == null) {
            orderPrice = 0.0d;
        }
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Double getGoodsPrice() {
        if (goodsPrice == null) {
            goodsPrice = 0.0d;
        }
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Double getShippingPrice() {
        if (shippingPrice == null) {
            shippingPrice = 0.0d;
        }
        return shippingPrice;
    }

    public void setShippingPrice(Double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public Double getDiscountPrice() {
        if (discountPrice == null) {
            discountPrice = 0.0d;
        }
        return discountPrice;
    }

    public Double getBalance() {
        if (balance == null) {
            balance = 0.0d;
        }
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getOrderData() {
        return orderData;
    }

    public void setOrderData(String orderData) {
        this.orderData = orderData;
    }

    public void setDiscountPrice(Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getTheSign() {
        return theSign;
    }

    public void setTheSign(String theSign) {
        this.theSign = theSign;
    }

    public String getItemsJson() {
        return itemsJson;
    }

    public void setItemsJson(String itemsJson) {
        this.itemsJson = itemsJson;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Double getNeedPayMoney() {
        return needPayMoney;
    }

    public void setNeedPayMoney(Double needPayMoney) {
        this.needPayMoney = needPayMoney;
    }

    public String getShipNo() {
        return shipNo;
    }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Integer getAdminRemark() {
        return adminRemark;
    }

    public void setAdminRemark(Integer adminRemark) {
        this.adminRemark = adminRemark;
    }

    public Long getLogiId() {
        return logiId;
    }

    public void setLogiId(Long logiId) {
        this.logiId = logiId;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getSigningTime() {
        return signingTime;
    }

    public void setSigningTime(Long signingTime) {
        this.signingTime = signingTime;
    }

    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Integer getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(Integer billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillSn() {
        return billSn;
    }

    public void setBillSn(String billSn) {
        this.billSn = billSn;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getNeedReceipt() {
        if (needReceipt == null) {
            needReceipt = 0;
        }
        return needReceipt;
    }

    public void setNeedReceipt(Integer needReceipt) {
        this.needReceipt = needReceipt;
    }

    public Integer getShippingId() {
        return shippingId;
    }

    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Double getMemberLevelDiscountPrice() {
        return memberLevelDiscountPrice;
    }

    public void setMemberLevelDiscountPrice(Double memberLevelDiscountPrice) {
        this.memberLevelDiscountPrice = memberLevelDiscountPrice;
    }

    public Boolean getCrowdfundingFlag() {
        return crowdfundingFlag;
    }

    public void setCrowdfundingFlag(Boolean crowdfundingFlag) {
        this.crowdfundingFlag = crowdfundingFlag;
    }

    public Long getCrowdfundingId() {
        return crowdfundingId;
    }

    public void setCrowdfundingId(Long crowdfundingId) {
        this.crowdfundingId = crowdfundingId;
    }

    public CrowdfundingOrderStatus getCrowdfundingStatus() {
        return crowdfundingStatus;
    }

    public void setCrowdfundingStatus(CrowdfundingOrderStatus crowdfundingStatus) {
        this.crowdfundingStatus = crowdfundingStatus;
    }

    @Override
    public String toString() {
        return "OrderDO{" +
                "orderId=" + orderId +
                ", tradeSn='" + tradeSn + '\'' +
                ", sn='" + sn + '\'' +
                ", sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", payStatus='" + payStatus + '\'' +
                ", shipStatus='" + shipStatus + '\'' +
                ", shippingId=" + shippingId +
                ", commentStatus='" + commentStatus + '\'' +
                ", shippingType='" + shippingType + '\'' +
                ", paymentMethodId=" + paymentMethodId +
                ", paymentPluginId='" + paymentPluginId + '\'' +
                ", paymentMethodName='" + paymentMethodName + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", paymentTime=" + paymentTime +
                ", payMoney=" + payMoney +
                ", shipName='" + shipName + '\'' +
                ", shipAddr='" + shipAddr + '\'' +
                ", shipZip='" + shipZip + '\'' +
                ", shipMobile='" + shipMobile + '\'' +
                ", shipTel='" + shipTel + '\'' +
                ", receiveTime='" + receiveTime + '\'' +
                ", shipProvinceId=" + shipProvinceId +
                ", shipCityId=" + shipCityId +
                ", shipCountyId=" + shipCountyId +
                ", shipTownId=" + shipTownId +
                ", shipProvince='" + shipProvince + '\'' +
                ", shipCity='" + shipCity + '\'' +
                ", shipCounty='" + shipCounty + '\'' +
                ", shipTown='" + shipTown + '\'' +
                ", orderPrice=" + orderPrice +
                ", goodsPrice=" + goodsPrice +
                ", shippingPrice=" + shippingPrice +
                ", discountPrice=" + discountPrice +
                ", disabled=" + disabled +
                ", weight=" + weight +
                ", goodsNum=" + goodsNum +
                ", remark='" + remark + '\'' +
                ", cancelReason='" + cancelReason + '\'' +
                ", theSign='" + theSign + '\'' +
                ", itemsJson='" + itemsJson + '\'' +
                ", warehouseId=" + warehouseId +
                ", needPayMoney=" + needPayMoney +
                ", shipNo='" + shipNo + '\'' +
                ", addressId=" + addressId +
                ", adminRemark=" + adminRemark +
                ", logiId=" + logiId +
                ", logiName='" + logiName + '\'' +
                ", completeTime=" + completeTime +
                ", createTime=" + createTime +
                ", signingTime=" + signingTime +
                ", shipTime=" + shipTime +
                ", payOrderNo='" + payOrderNo + '\'' +
                ", serviceStatus='" + serviceStatus + '\'' +
                ", billStatus=" + billStatus +
                ", billSn='" + billSn + '\'' +
                ", clientType='" + clientType + '\'' +
                ", needReceipt=" + needReceipt +
                ", orderType='" + orderType + '\'' +
                ", orderData='" + orderData + '\'' +
                ", balance=" + balance +
                ", scanRounds='" + scanRounds + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderDO that = (OrderDO) o;

        return new EqualsBuilder()
                .append(orderId, that.orderId)
                .append(tradeSn, that.tradeSn)
                .append(sn, that.sn)
                .append(sellerId, that.sellerId)
                .append(sellerName, that.sellerName)
                .append(memberId, that.memberId)
                .append(memberName, that.memberName)
                .append(orderStatus, that.orderStatus)
                .append(payStatus, that.payStatus)
                .append(shipStatus, that.shipStatus)
                .append(shippingId, that.shippingId)
                .append(commentStatus, that.commentStatus)
                .append(shippingType, that.shippingType)
                .append(paymentMethodId, that.paymentMethodId)
                .append(paymentPluginId, that.paymentPluginId)
                .append(paymentMethodName, that.paymentMethodName)
                .append(paymentType, that.paymentType)
                .append(paymentTime, that.paymentTime)
                .append(payMoney, that.payMoney)
                .append(shipName, that.shipName)
                .append(shipAddr, that.shipAddr)
                .append(shipZip, that.shipZip)
                .append(shipMobile, that.shipMobile)
                .append(shipTel, that.shipTel)
                .append(receiveTime, that.receiveTime)
                .append(shipProvinceId, that.shipProvinceId)
                .append(shipCityId, that.shipCityId)
                .append(shipCountyId, that.shipCountyId)
                .append(shipTownId, that.shipTownId)
                .append(shipProvince, that.shipProvince)
                .append(shipCity, that.shipCity)
                .append(shipCounty, that.shipCounty)
                .append(shipTown, that.shipTown)
                .append(orderPrice, that.orderPrice)
                .append(goodsPrice, that.goodsPrice)
                .append(shippingPrice, that.shippingPrice)
                .append(discountPrice, that.discountPrice)
                .append(disabled, that.disabled)
                .append(weight, that.weight)
                .append(goodsNum, that.goodsNum)
                .append(remark, that.remark)
                .append(cancelReason, that.cancelReason)
                .append(theSign, that.theSign)
                .append(itemsJson, that.itemsJson)
                .append(warehouseId, that.warehouseId)
                .append(needPayMoney, that.needPayMoney)
                .append(shipNo, that.shipNo)
                .append(addressId, that.addressId)
                .append(adminRemark, that.adminRemark)
                .append(logiId, that.logiId)
                .append(logiName, that.logiName)
                .append(completeTime, that.completeTime)
                .append(createTime, that.createTime)
                .append(signingTime, that.signingTime)
                .append(shipTime, that.shipTime)
                .append(payOrderNo, that.payOrderNo)
                .append(serviceStatus, that.serviceStatus)
                .append(billStatus, that.billStatus)
                .append(billSn, that.billSn)
                .append(clientType, that.clientType)
                .append(needReceipt, that.needReceipt)
                .append(orderType, that.orderType)
                .append(orderData, that.orderData)
                .append(balance, that.balance)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(orderId)
                .append(tradeSn)
                .append(sn)
                .append(sellerId)
                .append(sellerName)
                .append(memberId)
                .append(memberName)
                .append(orderStatus)
                .append(payStatus)
                .append(shipStatus)
                .append(shippingId)
                .append(commentStatus)
                .append(shippingType)
                .append(paymentMethodId)
                .append(paymentPluginId)
                .append(paymentMethodName)
                .append(paymentType)
                .append(paymentTime)
                .append(payMoney)
                .append(shipName)
                .append(shipAddr)
                .append(shipZip)
                .append(shipMobile)
                .append(shipTel)
                .append(receiveTime)
                .append(shipProvinceId)
                .append(shipCityId)
                .append(shipCountyId)
                .append(shipTownId)
                .append(shipProvince)
                .append(shipCity)
                .append(shipCounty)
                .append(shipTown)
                .append(orderPrice)
                .append(goodsPrice)
                .append(shippingPrice)
                .append(discountPrice)
                .append(disabled)
                .append(weight)
                .append(goodsNum)
                .append(remark)
                .append(cancelReason)
                .append(theSign)
                .append(itemsJson)
                .append(warehouseId)
                .append(needPayMoney)
                .append(shipNo)
                .append(addressId)
                .append(adminRemark)
                .append(logiId)
                .append(logiName)
                .append(completeTime)
                .append(createTime)
                .append(signingTime)
                .append(shipTime)
                .append(payOrderNo)
                .append(serviceStatus)
                .append(billStatus)
                .append(billSn)
                .append(clientType)
                .append(needReceipt)
                .append(orderType)
                .append(orderData)
                .append(balance)
                .toHashCode();
    }

    public OrderDO() {

    }

    /**
     * 带初始化参数的构造器
     *
     * @param orderDTO
     */
    public OrderDO(OrderDTO orderDTO) {
        this.tradeSn = orderDTO.getTradeSn();
        this.receiveTime = orderDTO.getReceiveTime();
        this.sn = orderDTO.getSn();
        this.shipTime = orderDTO.getShipTime();
        this.paymentType = orderDTO.getPaymentType();

        // 收货人
        ConsigneeVO consignee = orderDTO.getConsignee();
        if (consignee != null) {
            shipName = consignee.getName();
            shipAddr = consignee.getAddress();

            this.addressId = consignee.getConsigneeId();
            this.shipMobile = consignee.getMobile();
            this.shipTel = consignee.getTelephone();

            this.shipProvince = consignee.getProvince();
            this.shipCity = consignee.getCity();
            this.shipCounty = consignee.getCounty();
            this.shipTown = consignee.getTown();

            this.shipProvinceId = consignee.getProvinceId();
            this.shipCityId = consignee.getCityId();
            this.shipCountyId = consignee.getCountyId();
            this.shipTownId = consignee.getTownId();
        }


        // 价格
        PriceDetailVO priceDetail = orderDTO.getPrice();
        this.orderPrice = orderDTO.getOrderPrice();
        this.goodsPrice = priceDetail.getGoodsPrice();
        this.shippingPrice = priceDetail.getFreightPrice();
        this.discountPrice = priceDetail.getDiscountPrice();
        this.needPayMoney = orderDTO.getNeedPayMoney();
        this.weight = orderDTO.getWeight();
        this.shippingId = orderDTO.getShippingId();
        this.balance = 0D;
        this.memberLevelDiscountPrice = priceDetail.getMemberLevelDiscountPrice();

        // 卖家
        this.sellerId = orderDTO.getSellerId();
        this.sellerName = orderDTO.getSellerName();

        // 买家
        this.memberId = orderDTO.getMemberId();
        this.memberName = orderDTO.getMemberName();

        // 创建时间
        this.createTime = orderDTO.getCreateTime();

        // 初始化状态
        this.shipStatus = ShipStatusEnum.SHIP_NO.value();
        this.payStatus = PayStatusEnum.PAY_NO.value();
        this.commentStatus = CommentStatusEnum.UNFINISHED.value();
        this.serviceStatus = OrderServiceStatusEnum.NOT_APPLY.value();
        this.disabled = 0;

        List<OrderSkuVO> orderSkuVOList = orderDTO.getOrderSkuList();
        Integer goodsNum = 0;
        for (OrderSkuVO orderSkuVO : orderSkuVOList) {
            orderSkuVO.setActualPayTotal(orderSkuVO.getActualPayTotal());
            orderSkuVO.setComplainStatus(ComplainSkuStatusEnum.NO_APPLY.name());
            goodsNum = goodsNum + orderSkuVO.getNum();
        }
        //该订单的商品数量
        this.goodsNum = goodsNum;
        // 产品列表
        this.itemsJson = JsonUtil.objectToJson(orderSkuVOList);

        // 备注
        this.remark = orderDTO.getRemark();

        //发票
        this.needReceipt = orderDTO.getNeedReceipt();

        //订单来源
        this.clientType = orderDTO.getClientType();
        this.orderType = orderDTO.getOrderType();

        this.crowdfundingId = orderDTO.getCrowdfundingId();
        this.crowdfundingFlag = orderDTO.getCrowdfundingId() != null;
        this.crowdfundingStatus = CrowdfundingOrderStatus.WaitSettlement;
    }

    /**
     * 带初始化参数的构造器
     *
     * @param orderParam
     */
    public OrderDO(OrderParam orderParam) {

    }

}
