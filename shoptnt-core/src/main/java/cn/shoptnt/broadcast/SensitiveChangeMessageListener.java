package cn.shoptnt.broadcast;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 敏感词变化广播消息监听
 * @author kingapex
 * @version 1.0
 * @data 2022/10/24 17:08
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class SensitiveChangeMessageListener {

    @Autowired
    private SensitiveReceiver sensitiveReceiver;

    /**
     * 默认在事务提交后执行
     * @param message
     */
    @TransactionalEventListener(fallbackExecution = true)
    public void listener(SensitiveWordsMsg message){
        sensitiveReceiver.receiveMsg(message);
    }
}
