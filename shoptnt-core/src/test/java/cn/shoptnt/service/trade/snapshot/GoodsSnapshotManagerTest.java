/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.snapshot;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.trade.snapshot.GoodsSnapshot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 订单出库状态业务层测试
 * @author 张崧
 * @version 1.0
 * @since 7.2.2
 * 2020/08/14
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class GoodsSnapshotManagerTest {

    @Autowired
    private GoodsSnapshotManager goodsSnapshotManager;

    @Test
    public void updateServiceStatus() {
        GoodsSnapshot snapshot = new GoodsSnapshot();
        snapshot.setGoodsId(1L);
        snapshot.setName("xx");
        snapshot.setSn("xx");
        snapshot.setCategoryName("xx");
        snapshot.setBrandName("xx");
        snapshot.setGoodsType("xx");
        snapshot.setHaveSpec(0);
        snapshot.setWeight(0.0);
        snapshot.setIntro("xx");
        snapshot.setPrice(0.0);
        snapshot.setCost(0.0);
        snapshot.setMktprice(0.0);



        snapshot.setParamsJson("xx");
        snapshot.setImgJson("xx");
        snapshot.setPoint(99999);
        snapshot.setSellerId(1L);



        snapshot.setCreateTime(DateUtil.getDateline());
        snapshot.setPromotionJson("xx");
        snapshot.setCouponJson("xx");
        snapshot.setMobileIntro("xx");

        snapshot.setMemberId(1L);
        goodsSnapshotManager.add(snapshot);
    }

}
