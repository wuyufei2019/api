/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment;

import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.mapper.payment.PaymentBillMapper;
import cn.shoptnt.model.payment.dos.PaymentBillDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 支付帐单业务层测试
 * @author 张崧
 * @version 1.0
 * @since 7.2.2
 * 2020/07/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class PaymentBillManagerTest {

    @Autowired
    private PaymentBillManager paymentBillManager;

    @Autowired
    private PaymentBillMapper paymentBillMapper;


    @Test
    public void list() {

        WebPage list = paymentBillManager.list(1, 10);
        
    }

    @Test
    public void getByBillSn() {

        PaymentBillDO bill_sn = new QueryChainWrapper<>(paymentBillMapper).eq("bill_sn", "29416667834896394").one();

        
    }

    @Test
    public void getBillByReturnTradeNo() {

        PaymentBillDO bill_sn = paymentBillManager.getBillByReturnTradeNo("1");

        
    }

    @Test
    public void edit() {

        PaymentBillDO paymentBillDO = new PaymentBillDO();
        paymentBillDO.setReturnTradeNo("axxa");

        paymentBillManager.edit(paymentBillDO, "29416667834896394");
    }

    @Test
    public void getBySubSnAndServiceType() {

        PaymentBillDO bySubSnAndServiceType = paymentBillManager.getBySubSnAndServiceType("29416648620527617", "RECHARGE");

        

        PaymentBillDO bySubSnAndServiceType1 = paymentBillManager.getBySubSnAndServiceType("29416648620527617", "TRADE");

        
    }

    @Test
    public void paySuccess() {

        new UpdateChainWrapper<>(paymentBillMapper)
                .set("is_pay", 1)
                .set("return_trade_no", "哈哈")
                .eq("bill_id", 29416667843022859l)
                .update();
    }

}
