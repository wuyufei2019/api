/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.mapper.goods.GoodsMapper;
import cn.shoptnt.mapper.payment.PayLogMapper;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.trade.order.dos.PayLog;
import cn.shoptnt.service.goods.GoodsQueryManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收款单业务层测试
 * @author 张崧
 * @version 1.0
 * @since 7.2.2
 * 2020/07/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class PayLogManagerTest {

    @Autowired
    private PayLogManager payLogManager;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private GoodsQueryManager goodsQueryManager;
    @Autowired
    private ShopTntConfig shoptntConfig;

    @Test
    public void add() {

        PayLog payLog = new PayLog();
        payLog.setOrderSn("xxxxxxaaaa");

        payLogManager.add(payLog);
    }

    @Test
    public void edit() {

        PayLog payLog = new PayLog();
        payLog.setOrderSn("xxxxxxaaaa哈哈");

        payLogManager.edit(payLog, 1288403695152320513l);
    }


    @Test
    public void delete() {

        payLogManager.delete(1288403695152320513l);
    }

    @Test
    public void getModel() {

        PayLog model = payLogManager.getModel(24418821716742168l);
        
    }

    @Test
    public void getModel1() {

        PayLog model = payLogManager.getModel("29046245100371970");
        
    }

    @Test
    public void test1() {

        GoodsQueryParam goodsQueryParam = new GoodsQueryParam();

        goodsQueryParam.setPageNo(1L);
        goodsQueryParam.setPageSize(10L);

        

//        Map param = new HashMap();
//        param.put("enable_quantity", 44);
//        param.put("quantity", 55);
//        param.put("goods_id", 1295180068793155585L);
//        this.goodsMapper.updateQuantity(param);

        new UpdateChainWrapper<>(goodsMapper)
                .set("enable_quantity", 101)
                .set("quantity", 109)
                .eq("goods_id", 1295180068793155585L)
                .update();



        
//        Map param = new HashMap();
//        param.put("enable_quantity", 27);
//        param.put("quantity", 28);
//        param.put("goods_id", 1295245501688819714L);
//        this.goodsMapper.updateQuantity(param);
//
//        QueryWrapper<GoodsDO> queryWrapper = new QueryWrapper<>();
////        queryWrapper.eq("goods_id", 1295245501688819714L);
//
//        IPage<Map<String, Object>> page =
//                goodsMapper.selectMapsPage(new Page(1, 10), queryWrapper);
//
//        
    }

}
