/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.promotion.minus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.mapper.promotion.seckill.SeckillApplyMapper;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.dto.SeckillQueryParam;
import cn.shoptnt.service.promotion.seckill.SeckillGoodsManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * 单品立减业务层测试
 * @author 张崧
 * @version 1.0
 * @since 7.2.2
 * 2020/08/11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class MinusManagerTest {

    @Autowired
    private MinusManager minusManager;

    @Test
    public void list() {

        
        
    }

}
