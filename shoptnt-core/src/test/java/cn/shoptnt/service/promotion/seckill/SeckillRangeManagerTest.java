/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.promotion.seckill;

import cn.shoptnt.framework.test.TestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *  限时抢购时刻业务层测试
 * @author 张崧
 * @version 1.0
 * @since 7.2.2
 * 2020/08/11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class SeckillRangeManagerTest {

    @Autowired
    private SeckillRangeManager seckillRangeManager;

    @Test
    public void list() {

        
    }

    @Test
    public void readTimeList() {

        
    }


}
