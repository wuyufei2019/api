package cn.shoptnt;

import cn.shoptnt.framework.util.ApplicationLogUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by kingapex on 2018/3/18.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/18
 */
@SpringBootApplication
@ComponentScan(basePackages = "cn.shoptnt",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASPECTJ, pattern = "cn.shoptnt.mapper.*"))
@EnableScheduling
@EnableAsync
@MapperScan(basePackages = "cn.shoptnt.mapper")
@ServletComponentScan
public class RestApiApplication {

    public static void main(String[] args) {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
        ConfigurableApplicationContext application = SpringApplication.run(RestApiApplication.class, args);
        //打印启动日志
        ApplicationLogUtils.info(application);

    }
}
