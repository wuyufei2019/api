package cn.shoptnt.framework.message.broadcast;

/**
 * 广播消息
 * @author miaoxian
 * @version 1.0
 * @data 2022/10/24 16:28
 **/
public interface BroadcastMessage {

    /**
     * 定义广播频道
     * @return
     */
    String getChannel();

}
