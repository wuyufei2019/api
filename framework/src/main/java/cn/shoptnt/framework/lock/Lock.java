package cn.shoptnt.framework.lock;

/**
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/10/21 18:53
 **/
public interface Lock {

    void lock();

    void unlock();
}
