package cn.shoptnt.framework.database.mybatisplus.wrapper;

import cn.shoptnt.framework.database.mybatisplus.base.BaseQueryParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.PageConvert;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;

import static cn.shoptnt.framework.database.mybatisplus.base.BaseQueryParam.PAGE_NONE;


/**
 * 拓展 MyBatis Plus QueryChainWrapper 类，主要增加如下功能：
 *
 * 1. 拼接条件的方法，增加 xxxIfPresent 方法，用于判断值不存在的时候，不要拼接到条件中。
 *
 * @param <T> 数据类型
 */
public class QueryChainWrapperX<T> extends QueryChainWrapper<T> {

    public QueryChainWrapperX(BaseMapper<T> baseMapper) {
        super(baseMapper);
    }

    public QueryChainWrapperX<T> likeIfPresent(String column, String val) {
        if (StringUtils.hasText(val)) {
            return (QueryChainWrapperX<T>) super.like(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> inIfPresent(String column, Collection<?> values) {
        if (!CollectionUtils.isEmpty(values)) {
            return (QueryChainWrapperX<T>) super.in(column, values);
        }
        return this;
    }

    public QueryChainWrapperX<T> inIfPresent(String column, Object... values) {
        if (!ArrayUtils.isEmpty(values)) {
            return (QueryChainWrapperX<T>) super.in(column, values);
        }
        return this;
    }

    public QueryChainWrapperX<T> eqIfPresent(String column, Object val) {
        if (val != null) {
            return (QueryChainWrapperX<T>) super.eq(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> neIfPresent(String column, Object val) {
        if (val != null) {
            return (QueryChainWrapperX<T>) super.ne(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> gtIfPresent(String column, Object val) {
        if (val != null) {
            return (QueryChainWrapperX<T>) super.gt(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> geIfPresent(String column, Object val) {
        if (val != null) {
            return (QueryChainWrapperX<T>) super.ge(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> ltIfPresent(String column, Object val) {
        if (val != null) {
            return (QueryChainWrapperX<T>) super.lt(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> leIfPresent(String column, Object val) {
        if (val != null) {
            return (QueryChainWrapperX<T>) super.le(column, val);
        }
        return this;
    }

    public QueryChainWrapperX<T> betweenIfPresent(String column, Object val1, Object val2) {
        if (val1 != null && val2 != null) {
            return (QueryChainWrapperX<T>) super.between(column, val1, val2);
        }
        if (val1 != null) {
            return (QueryChainWrapperX<T>) ge(column, val1);
        }
        if (val2 != null) {
            return (QueryChainWrapperX<T>) le(column, val2);
        }
        return this;
    }

    public QueryChainWrapperX<T> betweenIfPresent(String column, Object[] values) {
        if (values!= null && values.length != 0 && values[0] != null && values[1] != null) {
            return (QueryChainWrapperX<T>) super.between(column, values[0], values[1]);
        }
        if (values!= null && values.length != 0 && values[0] != null) {
            return (QueryChainWrapperX<T>) ge(column, values[0]);
        }
        if (values!= null && values.length != 0 && values[1] != null) {
            return (QueryChainWrapperX<T>) le(column, values[1]);
        }
        return this;
    }

    // ========== 重写父类方法，方便链式调用 ==========

    @Override
    public QueryChainWrapperX<T> eq(boolean condition, String column, Object val) {
        super.eq(condition, column, val);
        return this;
    }

    @Override
    public QueryChainWrapperX<T> eq(String column, Object val) {
        super.eq(column, val);
        return this;
    }

    @Override
    public QueryChainWrapperX<T> orderByDesc(String column) {
        super.orderByDesc(true, column);
        return this;
    }

    @Override
    public QueryChainWrapperX<T> last(String lastSql) {
        super.last(lastSql);
        return this;
    }

    @Override
    public QueryChainWrapperX<T> in(String column, Collection<?> coll) {
        super.in(column, coll);
        return this;
    }

    /**
     * 分页查询
     * @param pageParam 分页查询参数
     * @return 分页数据
     */
    public WebPage<T> page(BaseQueryParam pageParam) {
        // 如果不分页，直接查列表
        if(pageParam.getPageSize() == PAGE_NONE){
            List<T> list = list();
            return new WebPage<>(pageParam.getPageNo(), (long) list.size(), pageParam.getPageSize(), list);
        }

        IPage<T> mpPage = new Page<>(pageParam.getPageNo(), pageParam.getPageSize());
        super.page(mpPage);
        return PageConvert.convert(mpPage);
    }

}
