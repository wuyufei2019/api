package cn.shoptnt.framework.database.mybatisplus.base;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.yulichang.base.MPJBaseMapper;
import com.github.yulichang.base.MPJBaseServiceImpl;

/**
 * Mybatis Plus ServiceImpl的拓展
 * 1. {@link ServiceImpl} 为 MyBatis Plus 的基础接口，提供基础的 CRUD 能力
 * 2. {@link MPJBaseServiceImpl} 为 MyBatis Plus Join 的基础接口，提供连表 Join 能力
 *
 * @author 张崧
 */
public class BaseServiceImpl<M extends MPJBaseMapper<T>, T> extends MPJBaseServiceImpl<M, T> implements BaseService<T> {

}
