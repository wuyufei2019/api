/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.database;

import cn.shoptnt.framework.database.impl.DaoSupportImpl;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by 妙贤 on 2018/3/6.
 * 数据源配置
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/6
 */
@Configuration
@EnableTransactionManagement
public class DataSourceConfig {

    /*----------------------------------------------------------------------------*/
    /*                           DaoSupport配置                                    */
    /*----------------------------------------------------------------------------*/

    /**
     * 商品daoSupport
     *
     * @param jdbcTemplate 商品jdbcTemplate
     * @return
     */
    @Bean(name = "goodsDaoSupport")
    @Primary
    public DaoSupport goodsDaoSupport(JdbcTemplate jdbcTemplate) {
        DaoSupport daosupport = new DaoSupportImpl(jdbcTemplate);
        return daosupport;
    }

    /**
     * 交易daoSupport
     *
     * @param jdbcTemplate 交易jdbcTemplate
     * @return
     */
    @Bean(name = "tradeDaoSupport")
    public DaoSupport tradeDaoSupport(JdbcTemplate jdbcTemplate) {
        DaoSupport daosupport = new DaoSupportImpl(jdbcTemplate);
        return daosupport;
    }


    /**
     * 会员daoSupport
     *
     * @param jdbcTemplate 会员jdbcTemplate
     * @return
     */
    @Bean(name = "memberDaoSupport")
    public DaoSupport memberDaoSupport(JdbcTemplate jdbcTemplate) {
        DaoSupport daosupport = new DaoSupportImpl(jdbcTemplate);
        return daosupport;
    }

    /**
     * 系统daoSupport
     *
     * @param jdbcTemplate 系统 jdbcTemplate
     * @return
     */
    @Bean(name = "systemDaoSupport")
    public DaoSupport systemDaoSupport(JdbcTemplate jdbcTemplate) {
        DaoSupport daosupport = new DaoSupportImpl(jdbcTemplate);
        return daosupport;
    }


    /**
     * 统计 daoSupport
     *
     * @param jdbcTemplate 统计jdbcTemplate
     * @return
     */
    @Bean(name = "sssDaoSupport")
    public DaoSupport sssDaoSupport(JdbcTemplate jdbcTemplate) {
        DaoSupport daosupport = new DaoSupportImpl(jdbcTemplate);
        return daosupport;
    }


    /**
     * 分销 daoSupport
     *
     * @param jdbcTemplate 分销jdbcTemplate
     * @return
     */
    @Bean(name = "distributionDaoSupport")
    public DaoSupport distributionDaoSupport(JdbcTemplate jdbcTemplate) {
        DaoSupport daosupport = new DaoSupportImpl(jdbcTemplate);
        return daosupport;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    /*----------------------------------------------------------------------------*/
    /*                           事务配置                                         */
    /*----------------------------------------------------------------------------*/

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    /**
     * 会员事务
     *
     * @return
     */
    @Bean
    public PlatformTransactionManager memberTransactionManager(PlatformTransactionManager transactionManager) {
        return transactionManager;
    }

    /**
     * 商品事务
     *
     * @return
     */
    @Bean
    @Primary
    public PlatformTransactionManager goodsTransactionManager(PlatformTransactionManager transactionManager) {
        return transactionManager;
    }

    /**
     * 交易事务
     *
     * @return
     */
    @Bean
    public PlatformTransactionManager tradeTransactionManager(PlatformTransactionManager transactionManager) {
        return transactionManager;
    }

    /**
     * 系统事务
     *
     * @return
     */
    @Bean
    public PlatformTransactionManager systemTransactionManager(PlatformTransactionManager transactionManager) {
        return transactionManager;
    }

    /**
     * 统计事务
     *
     * @return
     */
    @Bean
    public PlatformTransactionManager sssTransactionManager(PlatformTransactionManager transactionManager) {
        return transactionManager;
    }


    /**
     * 分销事务
     *
     * @return
     */
    @Bean
    public PlatformTransactionManager distributionTransactionManager(PlatformTransactionManager transactionManager) {
        return transactionManager;
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        // 分页插件
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        //乐观锁插件
        mybatisPlusInterceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return mybatisPlusInterceptor;
    }
}
