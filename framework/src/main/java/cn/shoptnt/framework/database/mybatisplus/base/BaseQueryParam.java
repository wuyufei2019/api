package cn.shoptnt.framework.database.mybatisplus.base;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 基础查询参数
 *
 * @author 张崧
 * @since 2024-06-19
 */
@Data
public class BaseQueryParam {

    /**
     * 不分页
     */
    public static final long PAGE_NONE = -1L;

    /**
     * 页码
     */
    @Schema(name = "page_no", description = "页码")
    @NotNull(message = "页码不能为空")
    private Long pageNo = 1L;

    /**
     * 页大小
     */
    @Schema(name = "page_size", description = "页大小")
    @NotNull(message = "页大小不能为空")
    private Long pageSize = 10L;
}
