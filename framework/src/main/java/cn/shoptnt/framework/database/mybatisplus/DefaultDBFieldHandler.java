package cn.shoptnt.framework.database.mybatisplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.database.mybatisplus.base.BaseDO;
import cn.shoptnt.framework.util.DateUtil;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Objects;

/**
 * 通用参数填充实现类
 * 如果没有显式的对通用参数进行赋值，这里会对通用参数进行填充、赋值
 */
public class DefaultDBFieldHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        if (Objects.nonNull(metaObject) && metaObject.getOriginalObject() instanceof BaseDO) {
            BaseDO baseDO = (BaseDO) metaObject.getOriginalObject();

            // 创建时间为空，则以当前时间为插入时间
            if (Objects.isNull(baseDO.getCreateTime())) {
                baseDO.setCreateTime(DateUtil.getDateline());
            }
            // 更新时间为空，则以当前时间为更新时间
            if (Objects.isNull(baseDO.getUpdateTime())) {
                baseDO.setUpdateTime(DateUtil.getDateline());
            }
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 更新时间为空，则以当前时间为更新时间
        Object modifyTime = getFieldValByName("updateTime", metaObject);
        if (Objects.isNull(modifyTime)) {
            setFieldValByName("updateTime", DateUtil.getDateline(), metaObject);
        }
    }
}
