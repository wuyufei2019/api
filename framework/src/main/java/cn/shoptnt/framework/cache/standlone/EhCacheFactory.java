package cn.shoptnt.framework.cache.standlone;


import cn.shoptnt.framework.util.PathUtil;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;

/**
 * ehcache 工厂类
 * @author gy
 * @version 1.0
 * @data 2022/10/21 11:55
 * 使用枚举的方式实现工厂，保质线程安全
 * 当一个Java类第一次被真正使用到的时候静态资源被初始化、 Java类的加载和初始化过程都是线程安全的。
 * 因为虚拟机在加载枚举的类的时候，会使用ClassLoader的loadClass方法，而这个方法使用同步代码块保证了线程安全。
 * 所以，创建一个enum类型是线程安全的。
 * 同时也不会出现反序列化会破坏单例的问题
 * 创建config manager及cache
 **/

public enum EhCacheFactory {

    CACHE_FACTORY;

    public static final String CACHE_NAME = "shopCache";

    private Cache<String, byte[]> cache;


    EhCacheFactory() {

        String cacheDir = PathUtil.getCacheRoot();
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .with(CacheManagerBuilder.persistence(cacheDir))
                .withCache(CACHE_NAME, CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, byte[].class,
                        ResourcePoolsBuilder.newResourcePoolsBuilder()
                                .heap(50, EntryUnit.ENTRIES) //一层缓存
                                .offheap(10, MemoryUnit.MB) //二层缓存
                                .disk(500, MemoryUnit.MB, true)) //三层缓存
                ).build();
        //初始化工厂
        cacheManager.init();

        cache = cacheManager.getCache(CACHE_NAME, String.class, byte[].class);

    }


    /**
     * 构建 Cache bean
     *
     * @return
     */

    public Cache<String, byte[]> getCache() {
        return cache;
    }

}
