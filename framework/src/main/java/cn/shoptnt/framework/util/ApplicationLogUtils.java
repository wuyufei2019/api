package cn.shoptnt.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 打印启动日志工具类
 * 因为项目启动成功后，可能会访问swagger等资源进行调试
 * 所以这里统一加上项目访问的相关地址
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年12月30日 16:42
 */
public class ApplicationLogUtils {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationLogUtils.class);

    /**
     * 打印启动日志
     * @param application spring上下文
     */
    public static void info(ApplicationContext application) {
        Environment env = application.getEnvironment();
        try {
            logger.info("\n----------------------------------------------------------\n\t" +
                            "Application '{}' is running! Access URLs:\n\t" +
                            "Local: \t\thttp://localhost:{}\n\t" +
                            "External: \thttp://{}:{}\n\t" +
                            "Doc: \t\thttp://{}:{}/doc.html\n\t" +
                            "Swagger3: \thttp://{}:{}/swagger-ui/index.html\n" +
                            "----------------------------------------------------------",
                    env.getProperty("spring.application.name"),
                    env.getProperty("server.port"),
                    InetAddress.getLocalHost().getHostAddress(),
                    env.getProperty("server.port"),
                    InetAddress.getLocalHost().getHostAddress(),
                    env.getProperty("server.port"),
                    InetAddress.getLocalHost().getHostAddress(),
                    env.getProperty("server.port"));
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}
