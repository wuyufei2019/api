package cn.shoptnt.framework.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

/**
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2023年01月05日 16:02
 */
public class LogUtils {


    public static void consumerInfo(String msg) {
        String path = System.getProperty("user.dir") + File.separator + "consumerLog.txt";

        info(msg, path);

    }

    public static void sendInfo(String msg) {
        String path = System.getProperty("user.dir") + File.separator + "sendLog.txt";

        info(msg, path);

    }

    private static void info(String msg, String path) {
        try{


            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.newLine();
            String data = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss.SSS");
            bw.write(data + "  |"+ msg);
            bw.flush();
            bw.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
