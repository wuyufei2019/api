/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.util;

/**
 * UserTokenPrefix
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-12-19 下午3:08
 */
public enum UserTokenPrefix {

    /**
     * 访问令牌
     */
    ACCESS_TOKEN,
    /**
     * 刷新令牌
     */
    REFRESH_TOKEN,

    /**
     * 会员
     */
    MEMBER_DISABLED,

    /**
     * 管理员
     */
    ADMIN_DISABLED;

    public String getPrefix() {
        return "{" +this.name() + "}_";
    }
}
