/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.context.request;

import org.springframework.lang.Nullable;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 数据脱敏的拦截器，主要是这是哪些api需要脱敏，需要脱敏的，则ThreadLoacal设置成true
 *
 * @author fk
 * @version v1.0
 * @since v5.2.3
 * 2021年11月17日10:18:28
 */
public class DesensitizationRequestInterceptor extends HandlerInterceptorAdapter {


    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        //进入此拦截器，设置需要脱敏
        DesensitizationThreadContextHolder.setDesensitizationFlag(true);

        return super.preHandle(request, response, handler);

    }


    /**
     * 处理完成 从上下文中移除
     *
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        DesensitizationThreadContextHolder.remove();

        super.afterCompletion(request, response, handler, ex);
    }


}
