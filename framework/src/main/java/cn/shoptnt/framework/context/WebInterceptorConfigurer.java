/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.context;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.context.params.SnakeToCamelArgumentResolver;
import cn.shoptnt.framework.context.params.SnakeToCamelModelAttributeMethodProcessor;
import cn.shoptnt.framework.context.request.DesensitizationRequestInterceptor;
import cn.shoptnt.framework.context.request.ShopTntRequestInterceptor;
import cn.shoptnt.framework.parameter.ParameterFilter;
import cn.shoptnt.framework.security.XssStringJsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.DispatcherType;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 加载自定义的 拦截器
 * @author Chopper
 * @version v1.0
 * @since v6.2 2017年3月7日 下午5:29:56
 *
 */
@Configuration
public class WebInterceptorConfigurer implements WebMvcConfigurer {

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {

		//参数蛇形转驼峰拦截
		argumentResolvers.add(new SnakeToCamelModelAttributeMethodProcessor(true));
		argumentResolvers.add(new SnakeToCamelArgumentResolver());

	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(shoptntRequestInterceptor()).addPathPatterns("/**");
		//设置需要脱敏的url
		List<String> urls = new ArrayList<>();
		//会员端
		urls.add("/members/addresses");
		urls.add("/members/address/{spring:[0-9]+}");
		urls.add("/members");
		urls.add("/members/zpzz/detail");
		urls.add("/after-sales/detail/{spring:[0-9]+}");
		urls.add("/after-sales/apply/{spring:[0-9]+}/{spring:[0-9]+}");
		urls.add("/trade/orders/cashier");
		urls.add("/members/receipt/ELECTRO");
		urls.add("/members/receipt/history/{spring:[0-9]+}");
		//管理端
		urls.add("/admin/members");
		urls.add("/admin/index/page");
		//商家端
		urls.add("/seller/shops/clerks");
		urls.add("/seller/trade/invoice");
		urls.add("/seller/waybill");
		urls.add("/seller/waybill/creates");
		registry.addInterceptor(desensitizationRequestInterceptor())
				.addPathPatterns(urls);
	}

	/**
	 * 声明shoptntRequestInterceptor bean
	 * @return
	 */
	@Bean
	public ShopTntRequestInterceptor shoptntRequestInterceptor() {
		return new ShopTntRequestInterceptor();
	}

	/**
	 * 声明DesensitizationRequestInterceptor bean
	 * @return
	 */
	@Bean
	public DesensitizationRequestInterceptor desensitizationRequestInterceptor() {
		return new DesensitizationRequestInterceptor();
	}

	/**
	 * 自定义 ObjectMapper ，用于xss攻击过滤
 	 * @param builder
	 * @return
	 */
	@Bean
	@Primary
	public ObjectMapper xssObjectMapper(Jackson2ObjectMapperBuilder builder) {
		//解析器
		ObjectMapper objectMapper = builder.createXmlMapper(false).build();
		//注册xss解析器
		SimpleModule xssModule = new SimpleModule("XssStringJsonSerializer");
		xssModule.addSerializer(new XssStringJsonSerializer());
		objectMapper.registerModule(xssModule);
		//返回
		return objectMapper;
	}

	@Autowired
	private ShopTntConfig shoptntConfig;


	/**
	 * 处理参数拦截器声明，目前只处理emoji表情拦截
	 * @return
	 */
	@Bean
	public FilterRegistrationBean<ParameterFilter> parameterFilter() {
		FilterRegistrationBean<ParameterFilter> filter=new FilterRegistrationBean<>();
		filter.setDispatcherTypes(DispatcherType.REQUEST);
		filter.setFilter(new ParameterFilter(shoptntConfig));
		//拦截所有请求，如果没有设置则默认“/*”
		filter.addUrlPatterns("/*");
		//设置注册的名称，如果没有指定会使用Bean的名称。此name也是过滤器的名称
		filter.setName("parameterFilter");
		//该filter在filterChain中的执行顺序
		filter.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
		return filter;
	}

	/**
	 * 雪花算法生成唯一数字的bean
	 * @return
	 */
	@Bean
	public IdentifierGenerator keyGenerator() {
		return new DefaultIdentifierGenerator();
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		//指定资源的 URL 路径
		registry.addMapping("/**")
				.allowedOriginPatterns("*")
				.allowedMethods("*")
				.allowedHeaders("*")
				//是否允许发送cookie
				.allowCredentials(true);

	}
}
