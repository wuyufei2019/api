package cn.shoptnt.framework.trigger.standlone;


import cn.shoptnt.framework.trigger.Interface.TimeTrigger;
import cn.shoptnt.framework.trigger.Interface.TimerTaskData;
import cn.shoptnt.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 延迟消息单机版实现
 * @author kingapex
 * @version 1.0
 * @data 2022/10/26 14:32
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class StandaloneTrigger implements TimeTrigger {

    private static  Map<String,TimerTask> taskMap = new HashMap<>();
    private static  Timer timer = new Timer("Timer");

    @Autowired
    private TimerTaskDataSource timerTaskDataSource;

    public static void main(String[] args) throws InterruptedException, UnsupportedEncodingException {
//
////        JdkSerializationRedisSerializer serializer = new JdkSerializationRedisSerializer();
//        TimerTask task1 = new LocalTimerTask("1", "myName", "param");
//        TimerTask task2 = new LocalTimerTask("2", "myName2", "param");
////        byte[] bytes =serializer.serialize(task);
////        String str = new String(bytes);
////        System.out.println(str);
////         task =(TimerTask) serializer.deserialize(str.getBytes("utf-8"));
////        System.out.println(task);
//        Timer timer1 = new Timer("Timer1");
//        Timer timer2 = new Timer("Timer1");
//        try {
//            timer1.schedule(task1, 1000L);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        timer2.schedule(task2, 1000L);
//
//        Thread.sleep(5000L);

    }


    @Override
    public void add(String executerName, Object param, Long triggerTime, String uniqueKey) {
        synchronized (uniqueKey) {

            long delay = convertDelay(triggerTime);
            TimerTask task = new LocalTimerTask(uniqueKey, executerName, param);

            //构建持久化数据
            TimerTaskData timerTaskData = new TimerTaskData(triggerTime, uniqueKey, task);

            //持久化到数据库
            boolean result = timerTaskDataSource.add(timerTaskData);

            //持久化成功后建立延迟任务
            if(result) {
                timer.schedule(task, delay);
                taskMap.put(uniqueKey, task);
            }

        }
    }

    /**
     * 触发一个延时任务
     * @param task 要触发的任务对象
     * @param triggerTime 触发时间
     * @param uniqueKey 唯一key
     */
    public void trigger( TimerTask task, Long triggerTime , String uniqueKey) {
        long delay = convertDelay(triggerTime);
        timer.schedule(task, delay);
        taskMap.put(uniqueKey, task);
    }

    @Override
    public void edit(String executerName, Object param, Long oldTriggerTime, Long triggerTime, String uniqueKey) {
        synchronized (uniqueKey) {

            TimerTask task =taskMap.get(uniqueKey);

            long delay = convertDelay(triggerTime);
            TimerTask newTask = new LocalTimerTask(uniqueKey, executerName, param);

            //构建持久化数据
            TimerTaskData timerTaskData = new TimerTaskData(triggerTime, uniqueKey, task);

            //更新持久化数据
            boolean result = timerTaskDataSource.update(uniqueKey, timerTaskData);

            //持久化成功后建立延迟任务
            if(result) {
                task.cancel();
                timer.schedule(newTask, delay);
                taskMap.put(uniqueKey, newTask);
            }

        }


    }




    private long convertDelay(Long triggerTime ) {
        long now = DateUtil.getDateline();
        long delay = triggerTime -now;
        delay=delay*1000;
        return delay;
    }



    @Override
    public void delete(String executerName, Long triggerTime, String uniqueKey) {
        synchronized (uniqueKey) {
            this.timerTaskDataSource.delete(uniqueKey);
            TimerTask task =taskMap.get(uniqueKey);
            if (task != null) {
                task.cancel();
                taskMap.remove(uniqueKey);
            }
        }
    }

    public void delete(String uniqueKey) {
        synchronized (uniqueKey) {
            this.timerTaskDataSource.delete(uniqueKey);
                taskMap.remove(uniqueKey);
        }
    }


}
