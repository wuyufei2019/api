package cn.shoptnt.framework.trigger.standlone;


import cn.shoptnt.framework.trigger.Interface.TimerTaskData;

import java.util.List;

/**
 * 延时任务数据持久化接口
 * @author kingapex
 * @version 1.0
 * @data 2022/11/4 16:19
 **/
public interface TimerTaskDataSource {

    /**
     * 新增数据
     *
     * @param timerTask
     * @return 是否成功
     */
    Boolean add(TimerTaskData timerTask);

    /**
     * 根据唯一key删除数据
     *
     * @param uniqueKey
     * @return 是否成功
     */
    Boolean delete(String uniqueKey);

    /**
     * 删除失效的
     * @return
     */
    Boolean deleteInvalid();

    /**
     * 根据唯一key更新持久化数据
     * @param uniqueKey
     * @param timerTask
     * @return
     */
    Boolean update(String uniqueKey, TimerTaskData timerTask);

    /**
     * 获取详情
     *
     * @param id
     * @return TimerTask
     */
    TimerTaskData getById(String id);

    /**
     * 获取所有有效的任务
     * @return
     */
    List<TimerTaskData> getEnableTasks();



}
