/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by 妙贤 on 2018/7/11.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/7/11
 */
@Configuration
@ComponentScan("cn.shoptnt")
public class TestConfig {
}
