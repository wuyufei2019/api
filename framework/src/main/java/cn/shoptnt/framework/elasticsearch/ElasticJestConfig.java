/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.elasticsearch;

import io.searchbox.client.JestClient;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * Elastic 配置类  使用时 注入 JestClient
 *
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2021/1/19  15:33
 */
@Configuration
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "cluster")
public class ElasticJestConfig implements FactoryBean<JestClient>, InitializingBean, DisposableBean {

    @Value("${elastic-jest.cluster-ips}")
    private String[] clusterIps;
    @Value("${elastic-jest.cluster-rest-port}")
    private int clusterRestPort;
    @Value("${elastic-jest.username}")
    private String username;

    @Value("${elastic-jest.password}")
    private String password;

    @Value("${elastic-jest.ssl}")
    private Boolean ssl;

    private String scheme = "http://";

    @Value("${elastic-jest.index-name}")
    private String indexName;

    private JestClient jestClient;



    public ElasticJestConfig() {
    }



    public String[] getClusterIps() {
        return clusterIps;
    }

    public void setClusterIps(String[] clusterIps) {
        this.clusterIps = clusterIps;
    }

    public int getClusterRestPort() {
        return clusterRestPort;
    }

    public void setClusterRestPort(int clusterRestPort) {
        this.clusterRestPort = clusterRestPort;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScheme() {
        if (this.getSsl()) {
            return "https://";
        }
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public Boolean getSsl() {
        return ssl;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }

    @Override
    public JestClient getObject() throws Exception {
        jestClient = ElasticBuilderUtil.buildJestClient(clusterIps,clusterRestPort,scheme,username,password);
        return jestClient;
    }

    @Override
    public Class<?> getObjectType() {
        return JestClient.class;
    }

    @Override
    public void afterPropertiesSet() {
        jestClient = ElasticBuilderUtil.buildJestClient(clusterIps,clusterRestPort,scheme,username,password);
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    @Override
    public void destroy() throws Exception {
        if (jestClient != null) {
            jestClient.close();
        }
    }
}
