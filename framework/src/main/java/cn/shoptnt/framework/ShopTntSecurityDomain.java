package cn.shoptnt.framework;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 配置文件中url白名单
 *
 * @author 张崧
 */

@Configuration
@ConfigurationProperties(prefix = "shoptnt.security")
public class ShopTntSecurityDomain {

    private List<String> domain;

    public List<String> getDomain() {
        return domain;
    }

    public void setDomain(List<String> domain) {
        this.domain = domain;
    }
}
