/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 *  用于
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:29:42
 */
@Component
public class TestReceiver {

	private final Logger logger = LoggerFactory.getLogger(getClass());


	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = RabbitMqTest.KEY+ "_QUEUE"),
			exchange = @Exchange(value =  RabbitMqTest.KEY, type = ExchangeTypes.FANOUT)
	))
	public void categoryChange(String msg){

		

	}
}
